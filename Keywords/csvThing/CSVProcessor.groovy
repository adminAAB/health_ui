package csvThing

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.google.common.collect.Table
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.sql.ResultSet
import java.sql.Statement
import java.awt.List

import internal.GlobalVariable

public class CSVProcessor {
	static Map<String, Integer> map = new HashMap<String, Integer>();

	@Keyword
	def csvSumAgeTable() {


		String fileToParse = ""
		//Input file which needs to be parsed
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'|| GlobalVariable.SystemUser =='LUR'
		|| GlobalVariable.SystemUser =='KAO'|| GlobalVariable.SystemUser =='ION'){
			fileToParse = "C:\\UploadA2ISHealth\\Age_Table.csv"
		}else{
			fileToParse = "E:\\UploadA2ISHealth\\Age_Table.csv"
		}
		BufferedReader fileReader = null;
		String str="" ;

		//Delimiter used in CSV file
		final String DELIMITER = ",";
		try
		{
			String line = "";

			//Create the file reader
			fileReader = new BufferedReader(new FileReader(fileToParse));

			int row= 0;
			//Read the file line by line
			while ((line = fileReader.readLine()) != null)
			{
				if(row==0){
					row++
					str=str+line+'\n'
					continue
				}
				//Get all tokens available in line
				String[] tokens = line.split(DELIMITER);
				for(String token : tokens)
				{
					if(token==tokens[0]){
						token = (Integer.parseInt(token)+1)+','
					}else
					if (token==tokens[1]){
						token='Premi HS-AUTOMATE-1300-'+GlobalVariable.Counter+','
					}else if(token==' '){
						token=token+','
					}
					else token=token+','
					str=str+token
				}
				str=str.substring(0,str.length()-1)+'\n'
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			try {
				fileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		//Write File
		File file = new File(fileToParse);
		FileWriter fr = null;
		BufferedWriter br = null;
		try{
			fr = new FileWriter(file);
			br = new BufferedWriter(fr);
			br.write(str);

		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			try {
				br.close();
				fr.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Keyword
	def compareAgeTable(){
		String file1 = ""
		String file2 = ""
		//Input file which needs to be parsed
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'|| GlobalVariable.SystemUser =='LUR'
		|| GlobalVariable.SystemUser =='KAO'|| GlobalVariable.SystemUser =='ION'){
			file1 = "C:\\HasilDownloadA2IS\\Age Table.csv";
			file2 = "C:\\UploadA2ISHealth\\Age_Table.csv";
		}else{
			file1 = "E:\\HasilDownloadA2IS\\Age Table.csv";
			file2 = "E:\\UploadA2ISHealth\\Age_Table.csv";
		}
		ArrayList al1 = new ArrayList()
		ArrayList al2 = new ArrayList()

		BufferedReader csvFile1 = new BufferedReader(new FileReader(file1))
		int row1= 0;
		String dataRow1 = csvFile1.readLine()
		while(dataRow1!=null){
			if(row1==0){
				row1++
				continue
			}
			String[] dataArray1 = dataRow1.split(",")
			for (String item1:dataArray1)
			{
				if(item1 == dataArray1[0]){
					item1 = 'filled'
				}else if(item1 == dataArray1[7]){
					continue
				}
				al1.add(item1)
			}
			dataRow1 = csvFile1.readLine();
		}
		csvFile1.close()

		BufferedReader csvFile2 = new BufferedReader(new FileReader(file2))
		int row2= 0;
		String dataRow2 = csvFile2.readLine()
		while(dataRow2!=null){
			if(row2==0){
				row2++
				continue
			}
			String[] dataArray2 = dataRow2.split(",")
			for (String item2:dataArray2)
			{
				if(item2 == dataArray2[0]){
					item2 = 'filled'
				}
				al2.add(item2)
			}
			dataRow2 = csvFile2.readLine()
		}
		csvFile2.close()

		for(int i = 0;i<al1.size();i++){
			println al1[i]
			println al2[i]
			WebUI.verifyEqual(al1[i], al2[i])
		}
		(new healthKeyword.utilityKeyword().Delete_File(file1))
	}
	@Keyword
	def csvSumBenefitList() {

		String fileToParse = ""
		//Input file which needs to be parsed
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'|| GlobalVariable.SystemUser =='LUR'
		|| GlobalVariable.SystemUser =='KAO'|| GlobalVariable.SystemUser =='ION'){
			fileToParse = "C:\\UploadA2ISHealth\\Benefit_List.csv"
		}else{
			fileToParse = "E:\\UploadA2ISHealth\\Benefit_List.csv"
		}
		BufferedReader fileReader = null;
		String str="" ;

		//Delimiter used in CSV file
		final String DELIMITER = ",";
		try
		{
			String line = "";

			//Create the file reader
			fileReader = new BufferedReader(new FileReader(fileToParse));
			int row = 0
			//Read the file line by line
			while ((line = fileReader.readLine()) != null)
			{
				if(row==0){
					row++
					str=str+line+'\n'
					continue
				}
				//Get all tokens available in line
				String[] tokens = line.split(DELIMITER);
				for(String token : tokens)
				{
					if (token==tokens[0]){
						token='HS-AUTOMATE-1300-'+GlobalVariable.Counter+','
					}
					else token=token+','
					str=str+token
				}
				str=str.substring(0,str.length()-1)+'\n'
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			try {
				fileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		//Write File
		File file = new File(fileToParse);
		FileWriter fr = null;
		BufferedWriter br = null;
		try{
			fr = new FileWriter(file);
			br = new BufferedWriter(fr);
			br.write(str);

		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			try {
				br.close();
				fr.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Keyword
	def compareBenefitList(){

		String file1 = ""
		String file2 = ""
		//Input file which needs to be parsed
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'|| GlobalVariable.SystemUser =='LUR'
		|| GlobalVariable.SystemUser =='KAO'|| GlobalVariable.SystemUser =='ION'){
			file1 = "C:\\HasilDownloadA2IS\\Benefit List.csv"
			file2 = "C:\\UploadA2ISHealth\\Benefit_List.csv"
		}else{
			file1 = "E:\\HasilDownloadA2IS\\Benefit List.csv"
			file2 = "E:\\UploadA2ISHealth\\Benefit_List.csv"
		}
		ArrayList al1 = new ArrayList()
		ArrayList al2 = new ArrayList()

		BufferedReader csvFile1 = new BufferedReader(new FileReader(file1))
		String dataRow1 = csvFile1.readLine()
		while(dataRow1!=null){
			String[] dataArray1 = dataRow1.split(",")
			for (String item1:dataArray1)
			{
				al1.add(item1)
			}
			dataRow1 = csvFile1.readLine();
		}
		csvFile1.close()

		BufferedReader csvFile2 = new BufferedReader(new FileReader(file2))
		String dataRow2 = csvFile2.readLine()
		while(dataRow2!=null){
			String[] dataArray2 = dataRow2.split(",")
			for (String item2:dataArray2)
			{
				al2.add(item2)
			}
			dataRow2 = csvFile2.readLine()
		}
		csvFile2.close()

		for(int i = 0;i<al1.size();i++){
			println al1[i]
			println al2[i]
			WebUI.verifyEqual(al1[i], al2[i])
		}
		(new healthKeyword.utilityKeyword().Delete_File(file1))
	}
	@Keyword

	def csvSumProductInfo() {

		String fileToParse = ""
		//Input file which needs to be parsed
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'|| GlobalVariable.SystemUser =='LUR'
		|| GlobalVariable.SystemUser =='KAO'|| GlobalVariable.SystemUser =='ION'){
			fileToParse = "C:\\UploadA2ISHealth\\Product_Info.csv"
		}else{
			fileToParse = "E:\\UploadA2ISHealth\\Product_Info.csv"
		}
		BufferedReader fileReader = null;
		String str="" ;

		//Delimiter used in CSV file
		final String DELIMITER = ",";
		try
		{
			String line = "";

			//Create the file reader
			fileReader = new BufferedReader(new FileReader(fileToParse));
			int row = 0
			//Read the file line by line
			while ((line = fileReader.readLine()) != null)
			{
				if(row==0){
					row++
					str=str+line+'\n'
					continue
				}
				//Get all tokens available in line
				String[] tokens = line.split(DELIMITER);
				for(String token : tokens)
				{
					if (token==tokens[0]){
						token='HS-AUTOMATE-1300-'+GlobalVariable.Counter+','
						GlobalVariable.ProductPlan = token.substring(0, token.length()-1)
					}
					else if (token==tokens[1]){
						token='AUTOMATE IP Plan 1300 - '+GlobalVariable.Counter+','
					}
					else if (token==tokens[2]){
						token='AUTOMATE IP Plan 1300 - '+GlobalVariable.Counter+','
					}
					else if (token==tokens[3]){
						token='HS-AUTOMATE-'+GlobalVariable.Counter+','
					}
					else if (token==tokens[4]){
						token='Rawat Inap Automate - '+GlobalVariable.Counter+','
					}
					else if (token==tokens[14]){
						token=(Integer.parseInt(token)+1)+','
					}
					else token=token+','
					str=str+token
				}
				str=str.substring(0,str.length()-1)
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			try {
				fileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		//Write File
		File file = new File(fileToParse);
		FileWriter fr = null;
		BufferedWriter br = null;
		try{
			fr = new FileWriter(file);
			br = new BufferedWriter(fr);
			br.write(str);

		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			try {
				br.close();
				fr.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Keyword
	def compareProductInfo(){

		String file1 = ""
		String file2 = ""
		//Input file which needs to be parsed
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'|| GlobalVariable.SystemUser =='LUR'
		|| GlobalVariable.SystemUser =='KAO'|| GlobalVariable.SystemUser =='ION'){
			file1 = "C:\\HasilDownloadA2IS\\Product Info.csv"
			file2 = "C:\\UploadA2ISHealth\\Product_Info.csv"
		}else{
			file1 = "E:\\HasilDownloadA2IS\\Product Info.csv"
			file2 = "E:\\UploadA2ISHealth\\Product_Info.csv"
		}
		ArrayList al1 = new ArrayList()
		ArrayList al2 = new ArrayList()

		BufferedReader csvFile1 = new BufferedReader(new FileReader(file1))
		String dataRow1 = csvFile1.readLine()
		while(dataRow1!=null){
			String[] dataArray1 = dataRow1.split(",")
			for (String item1:dataArray1)
			{
				al1.add(item1)
			}
			dataRow1 = csvFile1.readLine();
		}
		csvFile1.close()

		BufferedReader csvFile2 = new BufferedReader(new FileReader(file2))
		String dataRow2 = csvFile2.readLine()
		while(dataRow2!=null){
			String[] dataArray2 = dataRow2.split(",")
			for (String item2:dataArray2)
			{
				al2.add(item2)
			}
			dataRow2 = csvFile2.readLine()
		}
		csvFile2.close()

		for(int i = 0;i<al1.size();i++){
			println al1[i]
			println al2[i]
			WebUI.verifyEqual(al1[i], al2[i])
		}
		(new healthKeyword.utilityKeyword().Delete_File(file1))
	}
	@Keyword
	def exportClaimMonitoring(){
		String file1 = ""
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'|| GlobalVariable.SystemUser =='LUR'
		|| GlobalVariable.SystemUser =='KAO'|| GlobalVariable.SystemUser =='ION'){
			file1 = "C:\\HasilDownloadA2IS\\VIP Person.csv"
		}else{
			file1 = "E:\\HasilDownloadA2IS\\VIP Person.csv"
		}
		GlobalVariable.FilePath = file1
		ArrayList al1 = new ArrayList()
		ArrayList al2 = new ArrayList()
		String item =""
		BufferedReader csvFile1 = new BufferedReader(new FileReader(file1))
		String dataRow1 = csvFile1.readLine()
		String line =""
		int row = 0
		while((line = csvFile1.readLine())!=null){

			/*if(row==0){
			 row++
			 line+='\n'
			 continue
			 }*/

			String[] dataArray1 = line.split(",")
			for (String item1:dataArray1)
			{
				al1.add(item1)
			}
			//			dataRow1 = csvFile1.readLine();
		}
		csvFile1.close()
		//		Table<R, C, V> hasil = (new healthKeyword.utilityDB().getExportClaimMonitoring(GlobalVariable.tempSequence))
		ResultSet hasil = new healthKeyword.utilityDB().getExportClaimMonitoring(GlobalVariable.tempSequence)
		while(hasil.next()){
			for(int i = 1;i<31;i++){
				//				item = hasil.getString(i)
				if(hasil.getString(i)!=null){
					item = hasil.getString(i)
				}else{
					item = ""
				}
				al2.add(item)

			}
		}
		for(int i = 0;i<31;i++){
			println al1[i]
			println al2[i]
			WebUI.verifyEqual(al1[i], al2[i])
		}
		(new healthKeyword.utilityKeyword().Delete_File(GlobalVariable.FilePath))
	}
	@Keyword
	def getCompareExportToCSV(def todayDate, def timeNow){
		//		String file = "C:\\HasilDownloadA2IS\\Registered Claim Documents20190612-1428.csv"
		String file = ""
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			file = "C:\\HasilDownloadA2IS\\Registered Claim Documents"+todayDate+"-"+timeNow+".csv"
		}else{
			file = "E:\\HasilDownloadA2IS\\Registered Claim Documents"+todayDate+"-"+timeNow+".csv"
		}
		GlobalVariable.FilePath = file
		BufferedReader csvFile = new BufferedReader(new FileReader(file))
		String dataRow = csvFile.readLine()
		String line2 = csvFile.readLine()
		line2 = line2.replaceAll("\"","")
		csvFile.close()

		def hasil = new healthKeyword.utilityDB().getExportToCSV()
		hasil = hasil.replaceAll("\"","")
		println line2
		println hasil

		WebUI.verifyEqual(line2,hasil)
		(new healthKeyword.utilityKeyword().Delete_File(GlobalVariable.FilePath))
	}

	@Keyword
	def getCompareMemberAccount(def todayDate, def timeNow){
		String file1 = ""
		String file2 = ""
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			file1 = "C:\\UploadA2ISHealth\\TS48\\Import_Account_1.csv"
			file2 = "C:\\HasilDownloadA2IS\\Export.MemberAccount."+todayDate+"."+timeNow+".csv"
		}else{
			file1 = "E:\\UploadA2ISHealth\\TS48\\Import_Account_1.csv"
			file2 = "E:\\HasilDownloadA2IS\\Export.MemberAccount."+todayDate+"."+timeNow+".csv"
		}
		GlobalVariable.FilePath = file2
		BufferedReader csvFile1 = new BufferedReader(new FileReader(file1))
		BufferedReader csvFile2 = new BufferedReader(new FileReader(file2))
		String dataRow1 =""
		String dataRow2 =""
		for(int i = 0;i<2;i++){
			dataRow1 = csvFile1.readLine()
			dataRow2 = csvFile2.readLine()
		}
		csvFile1.close()
		csvFile2.close()
		WebUI.verifyEqual(dataRow1,dataRow2)
		(new healthKeyword.utilityKeyword().Delete_File(GlobalVariable.FilePath))
	}

	@Keyword
	def getCompareMemberCard(def todayDate, def timeNow){
		String file1 = ""
		String file2 = ""
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			file1 = "C:\\UploadA2ISHealth\\TS48\\Import_Card_1.csv"
			file2 = "C:\\HasilDownloadA2IS\\Export.MemberCard."+todayDate+"."+timeNow+".csv"
		}else{
			file1 = "E:\\UploadA2ISHealth\\TS48\\Import_Card_1.csv"
			file2 = "E:\\HasilDownloadA2IS\\Export.MemberCard."+todayDate+"."+timeNow+".csv"
		}
		GlobalVariable.FilePath = file2
		BufferedReader csvFile1 = new BufferedReader(new FileReader(file1))
		BufferedReader csvFile2 = new BufferedReader(new FileReader(file2))
		String dataRow1 =""
		String dataRow2 =""
		for(int i = 0;i<2;i++){
			dataRow1 = csvFile1.readLine()
			dataRow2 = csvFile2.readLine()
		}
		csvFile1.close()
		csvFile2.close()
		WebUI.verifyEqual(dataRow1,dataRow2)
		(new healthKeyword.utilityKeyword().Delete_File(GlobalVariable.FilePath))
	}
	@Keyword
	def compareImportPolicyClassification(){
		String file1 = ""
		String file2 = ""
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			file1 = "C:\\UploadA2ISHealth\\Import_Policy_Classification_"+GlobalVariable.Tipe+".csv"
			file2 = "C:\\HasilDownloadA2IS\\Policy Classification.csv"
		}else{
			file1 = "E:\\UploadA2ISHealth\\Import_Policy_Classification_"+GlobalVariable.Tipe+".csv"
			file2 = "E:\\HasilDownloadA2IS\\Policy Classification.csv"
		}
		BufferedReader csvFile1 = new BufferedReader(new FileReader(file1))
		BufferedReader csvFile2 = new BufferedReader(new FileReader(file2))
		String dataRow1 =""
		String dataRow2 =""
		for(int i = 0;i<3;i++){
			dataRow1 = csvFile1.readLine()
			dataRow2 = csvFile2.readLine()
			WebUI.verifyEqual(dataRow1,dataRow2)
		}
		csvFile1.close()
		csvFile2.close()
		(new healthKeyword.utilityKeyword().Delete_File(file2))
	}

	@Keyword
	def compareExportPolicyClassification(){
		String file1 = ""
		String file2 = ""
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			file1 = "C:\\HasilDownloadA2IS\\Policy Classification.csv"
			file2 = "C:\\HasilDownloadA2IS\\Policy Classification (1).csv"
		}else{
			file1 = "E:\\HasilDownloadA2IS\\Policy Classification.csv"
			file2 = "E:\\HasilDownloadA2IS\\Policy Classification (1).csv"
		}
		BufferedReader csvFile1 = new BufferedReader(new FileReader(file1))
		BufferedReader csvFile2 = new BufferedReader(new FileReader(file2))
		String dataRow1 =""
		String dataRow2 =""
		for(int i = 0;i<3;i++){
			dataRow1 = csvFile1.readLine()
			dataRow2 = csvFile2.readLine()
			WebUI.verifyEqual(dataRow1,dataRow2)
		}
		csvFile1.close()
		csvFile2.close()
		(new healthKeyword.utilityKeyword().Delete_File(file1))
		(new healthKeyword.utilityKeyword().Delete_File(file2))
	}

	@Keyword
	def importMemberPolicy(){
		String fileToParse = ""
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			fileToParse = "C:\\UploadA2ISHealth\\Import_Member_"+GlobalVariable.Tipe+".csv"
		}else{
			fileToParse = "E:\\UploadA2ISHealth\\Import_Member_"+GlobalVariable.Tipe+".csv"
		}
		BufferedReader fileReader = null;
		String str="" ;

		//Delimiter used in CSV file
		final String DELIMITER = ",";
		try
		{
			String line = "";

			//Create the file reader
			fileReader = new BufferedReader(new FileReader(fileToParse));
			int row = 0
			//Read the file line by line
			while ((line = fileReader.readLine()) != null)
			{
				if(row==0){
					row++
					str=str+line+'\n'
					continue
				}
				//Get all tokens available in line
				String[] tokens = line.split(DELIMITER);
				for(String token : tokens)
				{
					if (token.startsWith("AUTOCHUHATSU-")){
						String[] output = token.split("-")
						def nominal = (Integer.parseInt(output[1]) + 1)
						token='AUTOCHUHATSU-'+nominal+','
						new healthKeyword.utilityDB().updateParam4(("AUTOCHUHATSU-"+nominal+"-B"))
					}else if (token.startsWith("AUTO-")){
						String[] output = token.split("-")
						def nominal = (Integer.parseInt(output[1]) + 1)
						token='AUTO-'+nominal+','
						new healthKeyword.utilityDB().updateParam4(("AUTO-"+nominal+"-B"))
					}
					else token=token+','
					str=str+token
				}
				str=str.substring(0,str.length()-1)+'\n'
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			try {
				fileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		//Write File
		File file = new File(fileToParse);
		FileWriter fr = null;
		BufferedWriter br = null;
		try{
			fr = new FileWriter(file);
			br = new BufferedWriter(fr);
			br.write(str);

		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			try {
				br.close();
				fr.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Keyword
	def compareImportMemberPolicy(def policyName){
		def file1 = ""
		def file2 = ""
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			//			file1 = "C:\\HasilDownloadA2IS\\ImportMemberTemplate."+policyName+".csv"
			file1 = "C:\\HasilDownloadA2IS\\"+(new healthKeyword.general().searchFileNameLike(policyName))
			file2 = "C:\\UploadA2ISHealth\\Import_Member_"+GlobalVariable.Tipe+".csv"
		}else{
			//			file1 = "E:\\HasilDownloadA2IS\\ImportMemberTemplate."+policyName+".csv"
			file1 = "E:\\HasilDownloadA2IS\\"+(new healthKeyword.general().searchFileNameLike(policyName))
			file2 = "E:\\UploadA2ISHealth\\Import_Member_"+GlobalVariable.Tipe+".csv"
		}
		ArrayList al1 = new ArrayList()
		ArrayList al2 = new ArrayList()

		BufferedReader csvFile1 = new BufferedReader(new FileReader(file1))
		String dataRow1 = csvFile1.readLine()
		while(dataRow1!=null){
			String[] dataArray1 = dataRow1.split(",")
			for (String item1:dataArray1)
			{
				al1.add(item1)
			}
			dataRow1 = csvFile1.readLine();
		}
		csvFile1.close()

		BufferedReader csvFile2 = new BufferedReader(new FileReader(file2))
		String dataRow2 = csvFile2.readLine()
		while(dataRow2!=null){
			String[] dataArray2 = dataRow2.split(",")
			for (String item2:dataArray2)
			{
				al2.add(item2)
			}
			dataRow2 = csvFile2.readLine()
		}
		csvFile2.close()

		for(int i = 0;i<al1.size();i++){
			println al1[i]
			println al2[i]
			if(al1[i]==""){
				al1[i]=al2[i]
			}
			WebUI.verifyEqual(al1[i], al2[i])
		}
		(new healthKeyword.utilityKeyword().Delete_File(file1))
	}

	@Keyword
	def compareExportPremium(def policyName){
		def file1 = ""
		def file2 = ""
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			//			file1 = "C:\\HasilDownloadA2IS\\MemberEnrollment."+policyName+".csv"
			file1 = "C:\\HasilDownloadA2IS\\"+(new healthKeyword.general().searchFileNameLike(policyName))
			file2 = "C:\\UploadA2ISHealth\\Export_Premi_Registration_"+GlobalVariable.Tipe+".csv"
		}else{
			//			file1 = "E:\\HasilDownloadA2IS\\MemberEnrollment."+policyName+".csv"
			file1 = "E:\\HasilDownloadA2IS\\"+(new healthKeyword.general().searchFileNameLike(policyName))
			file2 = "E:\\UploadA2ISHealth\\Export_Premi_Registration_"+GlobalVariable.Tipe+".csv"
		}
		ArrayList al1 = new ArrayList()
		ArrayList al2 = new ArrayList()

		BufferedReader csvFile1 = new BufferedReader(new FileReader(file1))
		String dataRow1 = csvFile1.readLine()
		while(dataRow1!=null){
			String[] dataArray1 = dataRow1.split(",")
			for (String item1:dataArray1)
			{
				al1.add(item1)
			}
			dataRow1 = csvFile1.readLine();
		}
		csvFile1.close()

		BufferedReader csvFile2 = new BufferedReader(new FileReader(file2))
		String dataRow2 = csvFile2.readLine()
		while(dataRow2!=null){
			String[] dataArray2 = dataRow2.split(",")
			for (String item2:dataArray2)
			{
				al2.add(item2)
			}
			dataRow2 = csvFile2.readLine()
		}
		csvFile2.close()

		for(int i = 0;i<al1.size();i++){
			println al1[i]
			println al2[i]
			if(al2[i]==""){
				al2[i]=al1[i]
			}
			WebUI.verifyEqual(al1[i], al2[i])
		}
		(new healthKeyword.utilityKeyword().Delete_File(file1))
	}
}
