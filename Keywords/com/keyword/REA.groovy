package com.keyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date

import com.kms.katalon.core.util.KeywordUtil
import internal.GlobalVariable

public class REA extends UI {

	@Keyword
	public static void ComboBoxReact (TestObject Combo, String Value) {
		TestObject tObj = Combo
		String findXpath = "${tObj.findPropertyValue('xpath')}"
		String children = '(' +findXpath + '//parent::*//*[text()=\''+ Value +'\'])[1]'

		if (Combo != null || Combo != "") {
			Click(Combo)
		} else {
			KeywordUtil.markFailedAndStop('Button Open Combo Box is not found, Please check your Xpath')
		}

		if (newTestObject(children) != null || newTestObject(children) != "") {
			Click(newTestObject(children))
		} else {
			KeywordUtil.markFailedAndStop('Button ' + Value + ' is not found')
		}
	}

	@Keyword
	public static ArrayList getArrayIcon (TestObject Object) {
		WebDriver driver = DriverFactory.getWebDriver()
		TestObject tObj = Object
		String Xpath = "${tObj.findPropertyValue('xpath')}"

		WebElement drivers = driver.findElement(By.xpath(Xpath))
		List<WebElement> elementList=  drivers.findElements(By.className("style-card-view"))
		ArrayList getValue = new ArrayList()
		ArrayList finalValue = new ArrayList()

		int i
		for (i = 1 ; i <= elementList.size() ; i++) {
			getValue.add(WebUI.getAttribute(newTestObject("("+Xpath + "/div[" + i + "]/div)[1]"), "id"))
		}

		int a
		for (a = 0 ; a < getValue.size() ; a++) {
			String result = getValue[a]
			String[] separate = result.split('Type')d

			finalValue.add(separate[1])
		}
		return finalValue
	}

	@Keyword
	public static ArrayList getArrayButton (TestObject Object, String type ,String Class) {
		WebDriver driver = DriverFactory.getWebDriver()
		TestObject tObj = Object
		String Xpath = "${tObj.findPropertyValue('xpath')}"

		ArrayList getValue = new ArrayList()
		WebElement drivers = driver.findElement(By.xpath(Xpath))
		ArrayList ListElement = new ArrayList()

		if (type.toLowerCase() == "class") {
			List<WebElement> elementList=  drivers.findElements(By.className(Class))
			ListElement = elementList

			int i
			for (i = 1 ; i <= ListElement.size() ; i++) {
				getValue.add(WebUI.getText(newTestObject("("+Xpath + "/*[" + i + "]//*[text()])")))
			}
		} else if (type.toLowerCase() == "id") {
			List<WebElement> elementList=  drivers.findElements(By.id(Class))
			ListElement = elementList

			int i
			for (i = 1 ; i <= ListElement.size() ; i++) {
				getValue.add(WebUI.getText(newTestObject("("+Xpath + "/*[" + i + "]//*[text()])")))
			}
		} else if (type.toLowerCase() == "xpath"){
			List<WebElement> elementList=  drivers.findElements(By.xpath(Class))
			ListElement = elementList

			int i
			for (i = 2 ; i <= ListElement.size() + 1 ; i++) {
				getValue.add(WebUI.getText(newTestObject("("+Xpath + "/*[" + i + "]//*[text()])")))
			}
		} else if (type.toLowerCase() == "tagname") {
			List<WebElement> elementList = drivers.findElements(By.tagName(Class))
			ListElement = elementList

			int i
			for (i = 1 ; i <= ListElement.size() ; i++) {
				getValue.add(WebUI.getAttribute(newTestObject("("+Xpath + "/*)[" + i + "]"), "aria-label"))
			}
		}
		return getValue
	}
	
	@Keyword
	public static ArrayList getColumnDataTableUI (TestObject Table, int column) {
		WebDriver Driver = DriverFactory.getWebDriver()
		TestObject tObj = Table
		String XpathTable = "${tObj.findPropertyValue('xpath')}"
		
		WebElement table = Driver.findElement(By.xpath(XpathTable))
		List<WebElement> body = table.findElements(By.tagName("tr"))
		ArrayList result = new ArrayList()
		
		int index = column -1
		
		int i
		for (i = 0 ; i < body.size() ; i++) {
			List<WebElement> Colls = body[i].findElements(By.tagName("td"))
			
			if (Colls.size() == 0){
				continue
			} else {
				result.add(Colls[index].getText())
			}
		}
		return result
	}
	
	@Keyword
	public static void datePicker (String date, TestObject FieldDate) {
		WebElement element = WebUiCommonHelper.findWebElement(FieldDate,10)
		WebUI.executeJavaScript("arguments[0].value=\'" + date + "\'", Arrays.asList(element))
	}
	
	@Keyword
	public static String convertDate (String Date, boolean check) {
		String[] separate = Date.split(" ")
		String tanggal
		
		if (separate[1].contains("Jan")) {
			String date1 = Date.replace(separate[1],"01")
			tanggal = date1
			
		} else if (separate[1].contains("Feb")) {
			String date1 = Date.replace(separate[1],"02")
			tanggal = date1
			
		} else if (separate[1].contains("Mar")) {
			String date1 = Date.replace(separate[1],"03")
			tanggal = date1
			
		} else if (separate[1].contains("Apr")) {
			String date1 = Date.replace(separate[1],"04")
			tanggal = date1
			
		} else if (separate[1].contains("Mei") || separate[1].contains("May")) {
			String date1 = Date.replace(separate[1],"05")
			tanggal = date1
			
		} else if (separate[1].contains("Jun")) {
			String date1 = Date.replace(separate[1],"06")
			tanggal = date1
			
		} else if (separate[1].contains("Jul")) {
			String date1 = Date.replace(separate[1],"07")
			tanggal = date1
			
		} else if (separate[1].contains("Agu") || separate[1].contains("Aug")) {
			String date1 = Date.replace(separate[1],"08")
			tanggal = date1
			
		} else if (separate[1].contains("Sep")) {
			String date1 = Date.replace(separate[1],"09")
			tanggal = date1
			
		} else if (separate[1].contains("Okt") || separate[1].contains("Oct")) {
			String date1 = Date.replace(separate[1],"10")
			tanggal = date1
			
		} else if (separate[1].contains("Nov")) {
			String date1 = Date.replace(separate[1],"11")
			tanggal = date1
			
		} else if (separate[1].contains("Des") || separate[1].contains("Dec")) {
			String date1 = Date.replace(separate[1],"12")
			tanggal = date1
			
		}
		
		String getDate = tanggal.replace(" ","/")
		Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(getDate);
		String fixDate
		
		if (check == true) {
			String fixDate1 = date1.format('yyyy-MM-dd HH:mm:ss')
			fixDate = fixDate1
			
		} else {
			String fixDate1 = date1.format('yyyy-MM-dd')
			fixDate = fixDate1
		}
		return fixDate
	}
}
