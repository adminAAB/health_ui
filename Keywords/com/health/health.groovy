package com.health

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class health {

	@Keyword
	public static ArrayList RandomDate () {
		int RandomMonth = Math.floor(Math.random()*12)
		ArrayList randomDate = new ArrayList()
		DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy")
		Calendar cal = Calendar.getInstance()
		String date2 = dateFormat.format(cal.getTime())
		cal.add(Calendar.MONTH, - RandomMonth)
		String date = dateFormat.format(cal.getTime())

		while (date == date2) {
			cal.set(Calendar.MONTH, - RandomMonth)
			date = dateFormat.format(cal.getTime())
		}

		String[] separate = date.split("/")
		int getRandom = Math.floor(Math.random()*29)
		String randomStart
		String randomEnd

		while (getRandom == 0) {
			getRandom = Math.floor(Math.random()*29)
		}

		if (getRandom < 10) {
			if (getRandom < 9) {
				randomStart = '0' + getRandom
				randomEnd = '0' + (getRandom + 1)
			} else if (getRandom == 9) {
				randomStart = '0' + getRandom
				randomEnd = getRandom + 1
			}

			String startDate = randomStart + "/" +separate[1] + "/" + separate[2]
			String endDate = randomEnd + "/" +separate[1] + "/" + separate[2]

			randomDate.add(startDate)
			randomDate.add(endDate)
		} else {
			String startDate = getRandom + "/" +separate[1] + "/" + separate[2]
			String endDate = (getRandom + 1) + "/" +separate[1] + "/" + separate[2]

			randomDate.add(startDate)
			randomDate.add(endDate)
		}

		return randomDate
	}
}
