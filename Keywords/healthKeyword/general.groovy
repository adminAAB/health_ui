package healthKeyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.By
import org.openqa.selenium.remote.RemoteWebElement
import com.kms.katalon.core.exception.StepErrorException
import internal.GlobalVariable
import java.text.SimpleDateFormat
import org.apache.tools.ant.DirectoryScanner

public class general {
	@Keyword
	def Boolean waitProcessingCommand(TestObject objectToFind){
		Integer stale = 0
		int countTO
		int countTO2
		//wait until element appear
		while(!WebUI.verifyElementClickable(objectToFind, FailureHandling.OPTIONAL)){
			countTO2++
			if(countTO2 == 3 ){
				throw new Exception("Error Element Not Found")
			}
		}
		//element appear but processing command too
		WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)
		WebDriver myDriver = DriverFactory.getWebDriver()
		try
		{
			while(true){
				//what if there isnt blocker? > done with try catch
				//Watchout for stale element error
				//need timeout parameter?
				//findTestObject has default 30s timeout > changed to driverfactory
				//for multiple processing command > "display: none" only appear once in last
				String result = myDriver.findElement(By.xpath("//*[contains(@id,'Blocker_Box')]")).getAttribute("style")
				//String result = WebUI.getAttribute(findTestObject('Page_a2is Retail/blocker'), 'style').toString()
				if(result.contains('display: none;'))
					break
				WebUI.delay(1)
				WebUI.println('Found')
				countTO = countTO+1
				if(countTO == 30 ){
					throw new Exception("Freeze Processing Command")
				}
			}
		}
		catch(Exception ex)
		{
			WebUI.println('Not Found')
			WebUI.println(ex)
		}
		finally
		{
			WebUI.switchToDefaultContent()
			return true
		}

	}

	@Keyword
	def Boolean waitProcessingCommand2(){
		Integer stale = 0
		int countTO
		//wait until element appear
		//while(!WebUI.verifyElementClickable(objectToFind, FailureHandling.OPTIONAL)){}
		//element appear but processing command too
		WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)
		WebDriver myDriver = DriverFactory.getWebDriver()
		try
		{
			while(true){
				//what if there isnt blocker? > done with try catch
				//Watchout for stale element error
				//need timeout parameter?
				//findTestObject has default 30s timeout > changed to driverfactory
				//for multiple processing command > "display: none" only appear once in last
				String result = myDriver.findElement(By.xpath("//*[contains(@id,'Blocker_Box')]")).getAttribute("style")
				//String result = WebUI.getAttribute(findTestObject('Page_a2is Retail/blocker'), 'style').toString()
				if(result.contains('display: none;'))
					break
				WebUI.delay(1)
				WebUI.println('Found')
				countTO = countTO+1
				if(countTO == 30 ){
					throw new Exception("Freeze Processing Command")
				}
			}
		}
		catch(Exception ex)
		{
			WebUI.println('Not Found')
			WebUI.println(ex)
		}
		finally
		{
			WebUI.switchToDefaultContent()
			return true
		}
	}

	@Keyword
	def Boolean waitA2ISBlocker(){
		Integer stale = 0
		int countTO
		WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)
		WebDriver myDriver = DriverFactory.getWebDriver()
		try
		{
			while(true){
				//what if there isnt blocker? > done with try catch
				//Watchout for stale element error
				//need timeout parameter?
				//findTestObject has default 30s timeout > changed to driverfactory
				//for multiple processing command > "display: none" only appear once in last
				boolean result = myDriver.findElement(By.xpath('//*[@id="D_a2is_Modal"]/div[2]/div/div[1]')).isDisplayed()
				//String result = WebUI.getAttribute(findTestObject('Page_a2is Retail/blocker'), 'style').toString()
				WebUI.delay(1)
				WebUI.println('Found')
				WebUI.click(myDriver.findElement(By.xpath('//*[@id="D_a2is_Modal"]/div[2]/div/div[1]/button')))
			}
		}
		catch(Exception ex)
		{
			WebUI.println('Not Found')
			WebUI.println(ex)
		}
		finally
		{
			WebUI.switchToDefaultContent()
			return true
		}
	}

	@Keyword
	def UploadFile(){
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			Runtime.getRuntime().exec('C:\\UploadA2ISHealth\\Upload File.exe');
		}else{
			Runtime.getRuntime().exec('E:\\UploadA2ISHealth\\Upload File.exe');
		}
	}

	@Keyword
	def UploadFileTS46(){
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			Runtime.getRuntime().exec('C:\\UploadA2ISHealth\\TS46\\Upload File.exe');
		}else{
			Runtime.getRuntime().exec('E:\\UploadA2ISHealth\\TS46\\Upload File.exe');
		}
	}

	@Keyword
	def UploadFileTS48(){
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			Runtime.getRuntime().exec('C:\\UploadA2ISHealth\\TS48\\Upload File.exe');
		}else{
			Runtime.getRuntime().exec('E:\\UploadA2ISHealth\\TS48\\Upload File.exe');
		}
	}

	@Keyword
	def RewriteFileNameTS48(def fileName){
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			String fileToParse = "C:\\UploadA2ISHealth\\TS48\\FileName.txt"
			File file = new File(fileToParse);
			FileWriter fr = null;
			BufferedWriter br = null;
			try{
				fr = new FileWriter(file);
				br = new BufferedWriter(fr);
				br.write(fileName);

			}
			catch (IOException e) {
				e.printStackTrace();
			}
			finally{
				try {
					br.close();
					fr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}else{
			String fileToParse = "E:\\UploadA2ISHealth\\TS48\\FileName.txt"
			File file = new File(fileToParse);
			FileWriter fr = null;
			BufferedWriter br = null;
			try{
				fr = new FileWriter(file);
				br = new BufferedWriter(fr);
				br.write(fileName);

			}
			catch (IOException e) {
				e.printStackTrace();
			}
			finally{
				try {
					br.close();
					fr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Keyword
	def RewriteFileName(def fileName){
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			String fileToParse = "C:\\UploadA2ISHealth\\FileName.txt"
			File file = new File(fileToParse);
			FileWriter fr = null;
			BufferedWriter br = null;
			try{
				fr = new FileWriter(file);
				br = new BufferedWriter(fr);
				br.write(fileName);

			}
			catch (IOException e) {
				e.printStackTrace();
			}
			finally{
				try {
					br.close();
					fr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}else{
			String fileToParse = "E:\\UploadA2ISHealth\\FileName.txt"
			File file = new File(fileToParse);
			FileWriter fr = null;
			BufferedWriter br = null;
			try{
				fr = new FileWriter(file);
				br = new BufferedWriter(fr);
				br.write(fileName);

			}
			catch (IOException e) {
				e.printStackTrace();
			}
			finally{
				try {
					br.close();
					fr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	@Keyword
	def AllowMultipleFileDownload(){
		if(GlobalVariable.SystemUser=='RJW' || GlobalVariable.SystemUser =='OHS'){
			Runtime.getRuntime().exec('C:\\UploadA2ISHealth\\Automate Download Permission.exe');
		}else{
			Runtime.getRuntime().exec('E:\\UploadA2ISHealth\\Automate Download Permission.exe');
		}
	}

	@Keyword
	def searchFileNameLike(def fileName){
		def completeFileName
		def baseDir
		if (GlobalVariable.SystemUser == 'RJW' || GlobalVariable.SystemUser == 'OHS'){
			baseDir = 'C:\\HasilDownloadA2IS\\'
		}else{
			baseDir = 'E:\\HasilDownloadA2IS\\'
		}
		DirectoryScanner scanner = new DirectoryScanner();
		//		fileName = fileName+"*"
		//		println "fileName Baru >>>>>"+fileName
		//		scanner.setIncludes(fileName)
		//		scanner.setIncludes("ImportMemberTemplate.PT Sushi Tei Indonesia.Auto-ShushiTei-36*")
		scanner.setIncludes("*"+fileName+"*")
		scanner.setBasedir(baseDir)
		scanner.scan()

		String[] files = scanner.getIncludedFiles();
		for (int i = 0; i < files.length; i++) {
			completeFileName = files[i]
		}
		println "FILENYA KETEMU NIIIIH >>>>>> "+completeFileName
		return completeFileName
	}

	@Keyword
	def convertNominalToInteger(def nominal){
		nominal = nominal.replace("Rp. ", "")
		nominal = nominal.replace(",", "")
		nominal = nominal.replace(".00", "")
		Integer hasil = Integer.parseInt(nominal)

		return hasil
	}
	@Keyword
	def countDays(def firstDate, def secondDate){
		Calendar cal1 = new GregorianCalendar();
		Calendar cal2 = new GregorianCalendar();

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");

		Date date = sdf.parse(firstDate);
		cal1.setTime(date)
		date = sdf.parse(secondDate);
		cal2.setTime(date);

		//cal1.set(2008, 8, 1);
		//cal2.set(2008, 9, 31);
		System.out.println("Days= "+daysBetween(cal1.getTime(),cal2.getTime()));
	}
	public int daysBetween(Date d1, Date d2){
		return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}
}
