package healthKeyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.awt.List
import java.lang.invoke.MemberName
import java.sql.ResultSet
import java.*
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import sun.management.counter.StringCounter
import com.kms.katalon.core.util.KeywordUtil

public class utilityDB {
	@Keyword
	def getInvoiceNo(){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def Number = "SELECT Number FROM dbo.MasterID WHERE id='MYE001'"
		(new healthKeyword.configDB().executeQuery(Number))
	}

	@Keyword
	def queryCurrentDate(){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "SELECT replace(convert(VARCHAR, getdate(), 102), '.', '-') AS CurrentDate"
		(new healthKeyword.configDB().executeQuery(query))
	}
	@Keyword
	def compareDiagnosis(def diagnosisId){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'sea', 'ITAPP', 'It5Fu#H0m317'))

		def data = "SELECT d.name,d.Description_IN AS Description,mt.Description AS MedicalTreatment,dc.Description AS DiagnosisCatergory, FORMAT(d.CreatedDate , 'dd/MMM/yyyy') AS CreatedDate,d.CreatedBy, CASE  WHEN d.ModifiedDate IS NULL THEN '' ELSE FORMAT(d.ModifiedDate, 'dd/MMM/yyyy') END  AS ModifiedDate,  CASE  WHEN d.ModifiedBy IS NULL THEN '' ELSE d.ModifiedBy END  AS ModifiedBy, d.iITAPPctive FROM dbo.Diagnosis AS d LEFT JOIN DiagnosisTreatment AS dt ON dt.Diagnosis = d.Diagnosis LEFT JOIN dbo.MedicalTreatment AS mt ON dt.MedicalTreatmentID=mt.MedicalTreatmentID LEFT JOIN dbo.DiagnosisCategoryDiagnosis AS dcd ON dcd.Diagnosis = d.Diagnosis LEFT JOIN dbo.DiagnosisCategory AS dc ON dcd.DiagnosisCategoryID = dc.DiagnosisCategoryID WHERE  d.Diagnosis='"+diagnosisId+"' AND dt.IITAPPctive=1 AND dcd.IITAPPctive=1"
		(new healthKeyword.configDB().executeQuery(data))
	}

	@Keyword
	def compareClientArea(def clientArea){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'sea', 'ITAPP', 'It5Fu#H0m317'))

		def data = "SELECT p.Name,bfm.B2B_Area AS ClientArea, bfm.Enrollment_Area AS GardaMedikaArea, bfm.IITAPPctive, FORMAT(bfm.EntryDate , 'dd/MMM/yyyy') AS EntryDate, bfm.EntryUser, CASE  WHEN bfm.ModifyDate IS NULL THEN '' ELSE FORMAT(bfm.ModifyDate , 'dd/MMM/yyy') END  AS ModifyDate, CASE  WHEN bfm.ModifyUser IS NULL THEN '' ELSE bfm.ModifyUser END  AS ModifyUser FROM B2B_FIF_MappingArea AS bfm  LEFT JOIN dbo.Profile AS p ON p.ID=bfm.clientid WHERE bfm.B2B_Area='"+clientArea+"'"
		(new healthKeyword.configDB().executeQuery(data))
	}

	@Keyword
	def updateInvoiceNo(def InvoiceNo){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))
		int newInvoiceNo = InvoiceNo.toInteger()
		int no = 1
		def queryUpdate = "UPDATE dbo.MasterID SET Number= "+newInvoiceNo+"+"+no+ " WHERE id = 'MYE001'"
		(new healthKeyword.configDB().execute(queryUpdate))
	}

	@Keyword
	def dbGeneralDocReg(def tipeTest){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def data = "SELECT TOP 1 * FROM HealthDocumentReg WHERE isRUn='0' AND tipe='"+tipeTest+"'"
		(new healthKeyword.configDB().executeQuery(data))
	}

	@Keyword
	def updateParam1(def param){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "UPDATE HealthParam SET param1 = '"+param+"' WHERE User_Id = 'RJW'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateParam2(def param){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "UPDATE HealthParam SET param2 = '"+param+"' WHERE User_Id = 'RJW'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateParam3(def param){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "UPDATE HealthParam SET param3 = '"+param+"' WHERE User_Id = 'RJW'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateParam4(def param){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "UPDATE HealthParam SET param4 = '"+param+"' WHERE User_Id = 'RJW'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateParam5(def param){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "UPDATE HealthParam SET param5 = '"+param+"' WHERE User_Id = 'RJW'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateData(def param1, def param2, def param3, def param4, def param5){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def queryUpdate = "UPDATE "+param1+" SET "+param2+" = "+param3+" WHERE "+param4+" = "+param5
		(new healthKeyword.configDB().execute(queryUpdate))
	}



	@Keyword
	def dbAddInfoChk(def idDocReg){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))
		def dataAddInfo = "SELECT HealthAddInfo.itemAddInfo, HealthAddInfo.AdditionalInfoID, HealthAddInfo.rowStatus FROM HealthAddInfo INNER JOIN HealthDocumentReg ON HealthAddInfo.idDocReg = HealthDocumentReg.Id WHERE dbo.HealthAddInfo.idDocReg='"+idDocReg+"' AND dbo.HealthAddInfo.rowStatus = 1"
		ResultSet info = (new healthKeyword.configDB().executeQuery(dataAddInfo))
		GlobalVariable.AdditionalInfoID.clear()
		while(info.next()){
			GlobalVariable.AdditionalInfoID.add(info.getString(2))
		}
	}

	@Keyword
	def dbAddInfoSEA(def DocNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def dataDoc = "SELECT CAI.AdditionalInfoID,CAI.Status,CAST(CAI.Fdate AS DATE) AS Fdate,CAI.FUser FROM dbo.ClaimAdditionalInfo AS CAI WHERE CAI.DocumentNo='"+DocNo+"' AND Status='1'"
		ResultSet info = (new healthKeyword.configDB().executeQuery(dataDoc))
		GlobalVariable.dataAddInfoSEA.clear()
		while(info.next()){
			GlobalVariable.dataAddInfoSEA.add(info.getString(1))
		}
	}

	@Keyword
	def updateDocNo(def DocNo, def idDocReg){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def docDoc =  "UPDATE HealthDocumentReg SET DocNo ='"+DocNo+"' WHERE id ='"+idDocReg+"'"

		(new healthKeyword.configDB().execute(docDoc))
	}

	@Keyword
	def compareDocNo(def docNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def query =  "SELECT TDRD.Dispatcher AS TDRD_Dispatcher,TDRD.DocumentStatus AS TDRD_DocumentStatus,TDRD.Status AS TDRD_Status,P4.Name AS ClientName,PM.Name AS MemberName,CAST(TDRD.TreatmentStart AS DATE) AS TDRD_TreatmentStart,CAST(TDRD.TreatmentEnd AS DATE ) AS TDRD_TreatmentEnd,REPLACE(CONVERT(varchar,CONVERT(MONEY, TDRD.TotalBill),1),'.00','') AS TDRD_TotalBill,TDRD.Sender AS TDRD_Sender,TDRD.Receiver AS TDRD_Receiver,TDRD.CUser AS TDRD_CUser, CAST(TDRD.CDate AS DATE) AS TDRD_CDate,TDRD.TreatmentPlace AS TDRD_TreatmentPlace,TDRD.PIC AS TDRD_PIC,TDRD.CompleteF AS TDRD_CompleteF,TDRD.BoxNo AS TDRD_BoxNo,TDRD.DiagnosisID AS TDRD_DiagnosisID,TDRD.ProductType AS TDRD_ProductType,TDRD.RoomOption AS TDRD_RoomOption,TDRD.DeliveryNo AS TDRD_DeliveryNo,TDRD.DeliveryReceiveDate AS TDRD_DeliveryReceiveDate,TDRD.ReceiveDate AS TDRD_ReceiveDate,TDRD.isCobBPJS AS TDRD_isCobBPJS,TDRD.isCashPlanBPJS AS TDRD_isCashPlanBPJS, TDRD.isDoubleInsured AS TDRD_isDoubleInsured,TDRD.isNotEligible AS TDRD_isNotEligible,TDRD.isSpecialTreatment AS TDRD_isSpecialTreatment,TDRD.isDeceased AS TDRD_isDeceased,TDRD.ClaimType AS TDRD_ClaimType,TDRD.BankNameDisp AS TDRD_BankNameDisp,TDRD.BankAccountDisp AS TDRD_BankAccountDisp,TDRD.AccountNoDisp AS TDRD_AccountNoDisp,TDRD.PayToDisp AS TDRD_PayToDisp,TDRD.isTrafficAccident AS TDRD_isTrafficAccident,TDR.ClaimType AS TDR_ClaimType,TDR.DocumentSource AS TDR_DocumentSource, CAST(TDR.DocumentReceiveDate AS DATE) AS TDR_DocumentReceiveDate,TDR.CUser AS TDR_CUser,CAST(TDR.CDate AS DATE) AS TDR_CDate,TDR.RefNo AS TDR_RefNo,REPLACE(CONVERT(varchar,CONVERT(MONEY, TDR.TotalBill),1),'.00','') AS TDR_TotalBill ,TDR.DeliveryNo AS TDR_DeliveryNo,TDR.DeliveryReceiveDate AS TDR_DeliveryReceiveDate,CAST(PCB.ReceiveDate AS DATE ) AS PCB_ReceiveDate ,PCB.TotalqtyClaim AS PCB_TotalqtyClaim,REPLACE(CONVERT(varchar,CONVERT(MONEY, PCB.TotalClaimAmount),1),'.00','') AS PCB_TotalClaimAmount ,PCB.ReimbursementF AS pcb_ReimbursementF,CAST(PCB.FDATE AS DATE) AS PCB_FDATE, PCB.FUser AS PCB_FUser,PCB.RefNo AS PCB_RefNo,PCB.DiscountF AS PCB_DiscountF,CAST(PCB.Discount AS INT) AS PCB_Discount,PCB.Status AS PCB_Status,PCB.AUser AS PCB_AUser,CAST(PCB.ADate AS DATE) AS PCB_ADate,PCB.Stamp AS PCB_Stamp,PCB.AccountNo AS PCB_AccountNo,PCB.BankAccount AS PCB_BankAccount,PCB.BankName AS PCB_BankName,PCB.AutoApprovalStatus AS PCB_AutoApprovalStatus,CAST(PCB.AutoApprovalDate AS DATE) AS PCB_AutoApprovalDate FROM dbo.tbl_disp_registration_document AS TDRD LEFT JOIN dbo.tbl_disp_registration AS TDR ON TDR.RegistrationNo = TDRD.RegistrationNo LEFT JOIN dbo.Post_Claim_Batch AS PCB ON TDR.RefBatchNo=CAST(pcb.RegID AS VARCHAR(10)) AND PCB.RegID > 0 LEFT JOIN dbo.Profile AS P2 ON P2.Name = TDRD.TreatmentPlace LEFT JOIN dbo.Policy_Member AS PM ON TDRD.MNO = PM.MNo LEFT JOIN dbo.Policy AS P3 ON PM.PNO = P3.PNO LEFT JOIN dbo.Profile AS P4 ON P3.ClientID = P4.ID WHERE TDRD.DocumentNo='"+docNo+"'"

		(new healthKeyword.configDB().executeQuery(query))
	}

	@Keyword
	def queryApproveClaim(def DocNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def query = "SELECT TDRD.DocumentStatus AS TDRD_DocumentStatus, TDRD.Status AS TDRD_Status, CH.TreatmentPlace, PM.Name AS MemberName, FORMAT(CH.START,'dd/MMM/yyyy') AS TreatmentStart, FORMAT(CH.Finish,'dd/MMM/yyyy') AS TreatmentFinish, CH.Doctor, CH.DoctorSpecialty, PT.Description AS ProductType, CASE CH.ProviderF WHEN 0 THEN 'Reimbursement' ELSE 'Cashless' END AS ClaimType, CASE CH.RoomOption WHEN 'ONPLAN' THEN 'On Plan' ELSE CH.RoomOption END AS RoomOption, CH.TR_Class AS TreatmentRBClass, CH.TR_Amount AS TreatmentRBRate, CH.RB_Class AS AppropriateRBClass, CH.AR_Rate AS AppropriateRBRate, CONCAT('Rp.',' ',CONVERT(VARCHAR(50),CONVERT(VARCHAR,CAST(CH.Billed AS MONEY),1 ))) AS Billed,CONCAT('Rp.',' ',CONVERT(VARCHAR(50),CONVERT(VARCHAR,CAST(CH.Unpaid AS MONEY),1 ))) AS Unpaid,CONCAT('Rp.',' ',CONVERT(VARCHAR(50),CONVERT(VARCHAR,CAST(CH.Accepted AS MONEY),1 ))) AS Accepted,CONCAT('Rp.',' ',CONVERT(VARCHAR(50),CONVERT(VARCHAR,CAST(CH.Excess AS MONEY),1))) AS Excess,CONCAT('Rp.',' ',CONVERT(VARCHAR(50),CONVERT(VARCHAR,CAST(CH.ExcessCoy AS MONEY),1 ))) AS ExcessCoy, CONCAT('Rp.',' ',CONVERT(VARCHAR(50),CONVERT(VARCHAR,CAST(CH.ExcessEmp AS MONEY),1 ))) AS ExcessEmp, CASE CH.PayTo WHEN 'P' THEN 'Provider' WHEN 'C' THEN 'Client' WHEN 'M' THEN 'Member' END AS PayTo, CH.BankName AS AccountBank, CH.BankAccount AS AccountName, CH.AccountNo, CH.Status AS CH_Status, CH.RUser, CH.AutoApprovalStatus, CH.AutoApprovalUserID, CH.isCobBPJS, CH.isTrafficAccident, CH.isSpecialTreatment, CH.isCashPlanBPJS, CH.isDeceased, CH.isDoubleInsured FROM dbo.tbl_disp_registration_document AS TDRD LEFT JOIN dbo.ClaimH AS CH ON TDRD.ClaimNo = CH.ClaimNo LEFT JOIN dbo.Profile AS P2 ON P2.Name = CH.TreatmentPlace LEFT JOIN dbo.Policy_Member AS PM ON CH.MNO = PM.MNo LEFT JOIN dbo.Policy AS P3 ON PM.PNO = P3.PNO LEFT JOIN dbo.Profile AS P4 ON P3.ClientID = P4.ID LEFT JOIN dbo.ProductType AS PT ON CH.ProductType = PT.ProductType WHERE TDRD.DocumentNo ='"+DocNo+"'"
		(new healthKeyword.configDB().executeQuery(query))
	}

	@Keyword
	def querySettlement(def DocNo, def type){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def query = "SELECT CASE WHEN ISNULL(CL.IsCorrection,0) = 1 THEN CASE WHEN CL.Code = 'A' THEN 'CorrectionClaimPaid' WHEN (CL.Code = 'E' AND ExcessType = 'Coy') THEN 'CorrectionExcessCompany' WHEN (CL.Code = 'E' AND ExcessType = 'Emp') THEN 'CorrectionExcessEmployee' WHEN (CL.Code = 'E' AND ExcessType = 'Total') THEN 'CorrectionExcessTotal' END ELSE CASE WHEN CL.Code = 'A' THEN 'ClaimPaid' WHEN (CL.Code = 'E' AND ExcessType = 'Coy') THEN 'ExcessCompany' WHEN (CL.Code = 'E' AND ExcessType = 'Emp') THEN 'ExcessEmployee' WHEN (CL.Code = 'E' AND ExcessType = 'Total') THEN 'ExcessTotal' END END AS NoteType, CAST(ch.SendToSettlementDate AS DATE) AS SendToSettlementDate, ch.SendToSettlementStatus, ch.SendToSettlementUser, NV.Voucher AS NoteNo, CONCAT('Rp.',' ',CONVERT(VARCHAR(50),CONVERT(VARCHAR,CAST(NV.Nominal_CC AS MONEY),1 ))) AS Nominal_CC, NV.DebtorF, NV.SettledStatus AS NoteSettledStatus, FV.Voucher AS VoucherNo, FV.Remarks, GND.GroupNoteNo FROM dbo.tbl_disp_registration_document AS TDRD JOIN dbo.ClaimH AS CH ON TDRD.ClaimNo = CH.ClaimNo JOIN dbo.ProductType AS PT ON CH.ProductType = PT.ProductType LEFT JOIN (SELECT DISTINCT CNO, ADMNO, Code, Type, IsCorrection, ExcessType FROM ClaimLedger) CL ON CH.CNO = CL.CNO LEFT JOIN AdmLink AL ON CL.AdmNo = AL.AdmNo LEFT JOIN nVoucher NV ON AL.Voucher = NV.Voucher LEFT JOIN FSL ON NV.Voucher = FSL.Doc_No LEFT JOIN fVoucher FV ON FV.Voucher = FSL.Voucher LEFT JOIN GroupNoteD GND ON NV.Voucher = GND.Voucher  WHERE TDRD.DocumentNo = '"+DocNo+"'  AND (CASE WHEN ISNULL(CL.IsCorrection,0) = 1 THEN CASE WHEN CL.Code = 'A' THEN 'CorrectionClaimPaid' WHEN (CL.Code = 'E' AND ExcessType = 'Coy') THEN 'CorrectionExcessCompany' WHEN (CL.Code = 'E' AND ExcessType = 'Emp') THEN 'CorrectionExcessEmployee' WHEN (CL.Code = 'E' AND ExcessType = 'Total') THEN 'CorrectionExcessTotal' END ELSE  CASE WHEN CL.Code = 'A' THEN 'ClaimPaid' WHEN (CL.Code = 'E' AND ExcessType = 'Coy') THEN 'ExcessCompany' WHEN (CL.Code = 'E' AND ExcessType = 'Emp') THEN 'ExcessEmployee' WHEN (CL.Code = 'E' AND ExcessType = 'Total') THEN 'ExcessTotal' END END) = '"+type+"'"
		(new healthKeyword.configDB().executeQuery(query))
	}

	@Keyword
	def queryInquiryHomeMember(def memberNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def query = "declare @@ClientName varchar(50); SELECT @@ClientName = ''; declare @@MemberName varchar(50); SELECT @@MemberName = ''; SELECT * FROM (select top 100 a.ClientID,c.Name as ClientName,b.MemberNo,b.Name as MemberName,b.EmpID, case b.Membership when '1. EMP' then 'Employee' when '2. SPO' then 'Spouse' when '3. CHI' then 'Child' end as MembershipType, case b.Sex when 'M' then 'Male' when 'F' then 'Female' end as Sex, RIGHT('0' + CONVERT(NVARCHAR(2), DATEPART(DAY, b.BirthDate)), 2)+'/'+LEFT(DATENAME(MONTH,b.BirthDate),3) + '/' + RIGHT(DATENAME(YEAR,b.BirthDate),4) AS BirthDate from Policy a join Policy_Member b on a.PNO=b.PNO join Profile c on a.ClientID=c.ID WHERE a.PStatus <> 'O' and b.MemberNo <> '' and a.EffectiveDate>='2013-01-01' and c.Name like '%'+@@ClientName+'%' and b.Name like '%'+@@MemberName+'%' and b.MemberNo ='"+memberNo+"' group by c.Name, b.MemberNo,b.Name,b.EmpID,b.Membership,b.Sex,b.BirthDate,a.ClientID) as x"

		(new healthKeyword.configDB().executeQuery(query))
	}

	@Keyword
	def queryInquiryMemberInfo(def memberNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		//		def query = "SELECT c.name[ClientName], b.MemberNo, b.name + case isnull(b.ImportantF,0) when 1 then ' [VIP]' else '' end [MemberName], RIGHT('0' + CONVERT(NVARCHAR(2), DATEPART(DAY, b.BirthDate)), 2)+'/'+LEFT(DATENAME(MONTH,b.BirthDate),3) + '/' + RIGHT(DATENAME(YEAR,b.BirthDate),4)[DateOfBirth],b.EmpID, convert(int,DATEDIFF(d, b.birthdate, getdate())/365.25)[Age], d.Description[Classification], case b.maritalstatus when 'M' then 'Married' when 'S' then 'Single' end [MaritalStatus], b.Area, b.correspondenceid_claim[Phone], case b.Membership when '1. EMP' then 'Employee' when '2. SPO' then 'Spouse' when '3. CHI' then 'Child' end [MembershipType], b.Email, case when exists (select 1 from policy_member where memberno=b.memberno and getdate() between pdate and ppdate) then 'Active' else 'Inactive' end[MembershipStatus], g.BankName, case b.Sex when 'M' then 'Male' when 'F' then 'Female' end as Sex, g.AccountNo, (select top 1 CONVERT(varchar(30),x.CardNo) from policy_member_card x left join policy_member_card y on x.id=y.previd where y.id is null and x.ClientID=a.ClientID and x.MemberNo=b.MemberNo)[CardNo], g.AccountName,RIGHT('0' + CONVERT(NVARCHAR(2), DATEPART(DAY, i.RequestDate)), 2)+'/'+LEFT(DATENAME(MONTH,i.RequestDate),3) + '/' + RIGHT(DATENAME(YEAR,i.RequestDate),4)[CardRequestDate], RIGHT('0' + CONVERT(NVARCHAR(2), DATEPART(DAY, i.PrintDate)), 2)+'/'+LEFT(DATENAME(MONTH,i.PrintDate),3) + '/' + RIGHT(DATENAME(YEAR,i.PrintDate),4)[CardPrintDate], RIGHT('0' + CONVERT(NVARCHAR(2), DATEPART(DAY, i.SendDate)), 2)+'/'+LEFT(DATENAME(MONTH,i.SendDate),3) + '/' + RIGHT(DATENAME(YEAR,i.SendDate),4)[CardSendDate],RIGHT('0' + CONVERT(NVARCHAR(2), DATEPART(DAY, i.ReceiveDate)), 2)+'/'+LEFT(DATENAME(MONTH,i.ReceiveDate),3) + '/' + RIGHT(DATENAME(YEAR,i.ReceiveDate),4)[CardReceiveDate] FROM policy a join policy_member b on a.pno=b.pno join profile c on a.clientid=c.id join policy_classification d on a.pno=d.pno and b.classno=d.classno join (select max(y.MNO)[MNO] from policy x join policy_member y on x.pno=y.pno WHERE y.MemberNo='"+memberNo+"'and x.PStatus<>'O') e on b.mno=e.mno left join (select opno,memberno,max(ID_Account)[ID_Account] from policy_member_account where ResPNO<=0 group by opno,memberno) f on f.opno=a.opno and f.memberno=b.memberno left join policy_member_account g on f.id_account=g.id_account left join (select ClientID,MemberNo,max(ID)[ID] from policy_member_card group by ClientID,MemberNo) h on h.ClientID=a.ClientID and h.MemberNo=b.MemberNo left join policy_member_card i on h.id=i.id"
		def query = "SELECT c.name[ClientName], b.MemberNo, b.name + case isnull(b.ImportantF,0) when 1 then ' [VIP]' else '' end [MemberName], RIGHT('0' + CONVERT(NVARCHAR(2), DATEPART(DAY, b.BirthDate)), 2)+'/'+LEFT(DATENAME(MONTH,b.BirthDate),3) + '/' + RIGHT(DATENAME(YEAR,b.BirthDate),4)[DateOfBirth],b.EmpID, convert(int,DATEDIFF(d, b.birthdate, getdate())/365.25)[Age], d.Description[Classification], case b.maritalstatus when 'M' then 'Married' when 'S' then 'Single' end [MaritalStatus], b.Area, b.correspondenceid_claim[Phone], case b.Membership when '1. EMP' then 'Employee' when '2. SPO' then 'Spouse' when '3. CHI' then 'Child' end [MembershipType], b.Email, case when exists (select 1 from policy_member where memberno=b.memberno and getdate() between pdate and ppdate) then 'Active' else 'Inactive' end[MembershipStatus], g.BankName, case b.Sex when 'M' then 'Male' when 'F' then 'Female' end as Sex, g.AccountNo, (select top 1 CONVERT(varchar(30),x.CardNo) from policy_member_card x left join policy_member_card y on x.id=y.previd where y.id is null and x.ClientID=a.ClientID and x.MemberNo=b.MemberNo)[CardNo], g.AccountName,RIGHT('0' + CONVERT(NVARCHAR(2), DATEPART(DAY, i.RequestDate)), 2)+'/'+LEFT(DATENAME(MONTH,i.RequestDate),3) + '/' + RIGHT(DATENAME(YEAR,i.RequestDate),4)[CardRequestDate], RIGHT('0' + CONVERT(NVARCHAR(2), DATEPART(DAY, i.PrintDate)), 2)+'/'+LEFT(DATENAME(MONTH,i.PrintDate),3) + '/' + RIGHT(DATENAME(YEAR,i.PrintDate),4)[CardPrintDate], RIGHT('0' + CONVERT(NVARCHAR(2), DATEPART(DAY, i.SendDate)), 2)+'/'+LEFT(DATENAME(MONTH,i.SendDate),3) + '/' + RIGHT(DATENAME(YEAR,i.SendDate),4)[CardSendDate],RIGHT('0' + CONVERT(NVARCHAR(2), DATEPART(DAY, i.ReceiveDate)), 2)+'/'+LEFT(DATENAME(MONTH,i.ReceiveDate),3) + '/' + RIGHT(DATENAME(YEAR,i.ReceiveDate),4)[CardReceiveDate] from policy a join policy_member b on a.pno=b.pno join profile c on a.clientid=c.id join policy_classification d on a.pno=d.pno and b.classno=d.classno join (select max(y.MNO)[MNO] from policy x join policy_member y on x.pno=y.pno where y.MemberNo='"+memberNo+"' and x.PStatus<>'O') e on b.mno=e.mno left join (select MemberNo,max(ID)[ID] from policy_member_card group by MemberNo) h on h.MemberNo=b.MemberNo left join policy_member_card i on h.id=i.id outer apply fn_GetAccountInfo(b.MemberNo) g"
		(new healthKeyword.configDB().executeQuery(query))
	}

	@Keyword
	def queryInquiryBenefitInfo (def memberNo, def clientId){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def query = "SET NOCOUNT ON DECLARE @temp AS TABLE ( ProductPlan VARCHAR(25), ProductType VARCHAR(255), PNO INT, PolicyNo VARCHAR(20), EffectiveStart SMALLDATETIME, EffectiveEnd SMALLDATETIME, AnnualLimit DECIMAL, LimitUITAPPge DECIMAL, RemainingLimit DECIMAL, LimitType VARCHAR(50), ApplyHighPlanF BIT ) INSERT INTO @temp EXEC sp_getmemberbenefitinfo '"+memberNo+"', 1, 'y.PPlan', 'DESC', '"+clientId+"' SELECT PNO, ProductPlan, ProductType, CONCAT(FORMAT(EffectiveStart,'dd/MMM/yyyy'),' - ',FORMAT(EffectiveEnd,'dd/MMM/yyyy')) AS EffectivePeriod ,CONCAT('Rp.',' ',CONVERT(VARCHAR(50),CONVERT(VARCHAR,CAST(AnnualLimit AS MONEY),1 ))) AS AnnualLimit ,CONCAT('Rp.',' ',CONVERT(VARCHAR(50),CONVERT(VARCHAR,CAST(LimitUITAPPge AS MONEY),1 ))) AS LimitUITAPPge,CONCAT('Rp.',' ',CONVERT(VARCHAR(50),CONVERT(VARCHAR,CAST(RemainingLimit AS MONEY),1 ))) AS RemainingLimit,LimitType FROM @temp"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		while(info.next()){
			GlobalVariable.listComPNo.add(info.getObject('PNO').toString())
			GlobalVariable.listComProductPlan.add(info.getObject('ProductPlan').toString())
			GlobalVariable.listComProductType.add(info.getObject('ProductType'))
			GlobalVariable.listComEffectivePeriod.add(info.getObject('EffectivePeriod').toString())
			GlobalVariable.listComAnnualLimit.add(info.getObject('AnnualLimit').toString())
			GlobalVariable.listComLimitUITAPPge.add(info.getObject('LimitUITAPPge').toString())
			GlobalVariable.listComRemainingLimit.add(info.getObject('RemainingLimit').toString())
			GlobalVariable.listComLimitType.add(info.getObject('LimitType').toString())
		}
	}

	@Keyword
	def queryInquiryDetailBenefitInfo (def memberNo, def pno, def pplan){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def query = "SELECT d.BenefitID, e.Name[BenefitName], CASE WHEN d.LimitApplyF = 0 THEN '-' WHEN d.LimitApplyF = 1 AND (d.AmountLimit = 0 OR d.AmountLimit=999999999)THEN 'As Charged' ELSE CONCAT('Rp. ', CONVERT(VARCHAR, CAST(d.AmountLimit AS MONEY), 1)) END [LimitOccurrence], CASE WHEN d.ConLimitApplyF = 0 THEN '-' WHEN d.ConLimitApplyF = 1 AND (d.ConAmountLimit = 0 OR d.ConAmountLimit = 999999999) THEN 'As Charged' ELSE CONCAT('Rp. ', CONVERT(VARCHAR, CAST(d.ConAmountLimit AS MONEY), 1)) END [LimitConfinement], CASE WHEN d.AnnualLimitApplyF = 0 THEN '-' WHEN d.AnnualLimitApplyF = 1 AND (d.AnnualAmountLimit = 0 OR d.AnnualAmountLimit = 999999999) THEN 'As Charged' ELSE CONCAT('Rp. ', CONVERT(VARCHAR, CAST(d.AnnualAmountLimit AS MONEY), 1)) END [LimitYear], CASE WHEN d.CompanyLimitApplyF = 0 THEN '-' WHEN d.CompanyLimitApplyF = 1 AND (d.CompanyAmountLimit = 0 OR d.CompanyAmountLimit = 999999999) THEN 'As Charged' ELSE CONCAT('Rp. ', CONVERT(VARCHAR, CAST(d.CompanyAmountLimit AS MONEY), 1)) END [LimitGroup], CASE WHEN d.HP_LimitApplyF= 0 THEN '-' WHEN d.HP_LimitApplyF= 1 AND (d.HP_AmountLimit = 0 OR d.HP_AmountLimit = 999999999 )THEN 'As Charged' ELSE CONCAT('Rp. ', CONVERT(VARCHAR, CAST(d.HP_AmountLimit AS MONEY), 1)) END [HP_LimitOccurrence], CASE WHEN d.HP_ConLimitApplyF = 0 THEN '-' WHEN d.HP_ConLimitApplyF = 1 AND (d.HP_ConAmountLimit = 0 OR d.HP_ConAmountLimit = 999999999) THEN 'As Charged' ELSE CONCAT('Rp. ', CONVERT(VARCHAR, CAST(d.HP_ConAmountLimit AS MONEY), 1)) END [HP_LimitConfinement], CASE WHEN d.HP_AnnualLimitApplyF = 0 THEN '-' WHEN d.HP_AnnualLimitApplyF = 1 AND (d.HP_AnnualAmountLimit = 0 OR d.HP_AnnualAmountLimit = 999999999 )THEN 'As Charged' ELSE CONCAT('Rp. ', CONVERT(VARCHAR, CAST(d.HP_AnnualAmountLimit AS MONEY), 1)) end [HP_LimitYear], CASE WHEN d.HP_CompanyLimitApplyF = 0 THEN '-' WHEN d.HP_CompanyLimitApplyF = 1 AND (d.HP_CompanyAmountLimit = 0 OR d.HP_CompanyAmountLimit = 999999999) THEN 'As Charged' ELSE CONCAT('Rp. ', CONVERT(VARCHAR, CAST(d.HP_CompanyAmountLimit AS MONEY), 1)) END [HP_LimitGroup] FROM policy a join policy_member b on a.pno=b.pno join policy_member_plan c on b.pno=c.pno and b.mno=c.mno and c.excludef=0 join policy_benefit_schedule d on c.pno=d.pno and c.pplan=d.pplan and c.bamount=d.bamount join benefit e on d.benefitid=e.benefitid join pplan f on c.pplan=f.pplan where b.memberno='"+memberNo+"' and c.pplan='"+pplan+"' and a.pno='"+pno+"' order by d.Benefit_SeqNo Asc"

		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		while(info.next()){
			GlobalVariable.listComBenefitId.add(info.getString(1).trim())
			GlobalVariable.listComBenefitName.add(info.getString(2).trim())
			GlobalVariable.listComLimitOccurrence.add(info.getString(3).trim())
			GlobalVariable.listComLimitConfinement.add(info.getString(4).trim())
			GlobalVariable.listComLimitYear.add(info.getString(5).trim())
			GlobalVariable.listComLimitGroup.add(info.getString(6).trim())
		}
	}

	@Keyword
	def queryInquiryClaimInfo (def memberNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def query = "SELECT ISNULL(C1.MemberName, C2.MemberName) MemberName , ISNULL(C1.TreatmentPlace, C2.TreatmentPlace) ProviderName , FORMAT(ISNULL(C1.Start, C2.Start),'dd/MMM/yyyy') START, FORMAT(ISNULL(C1.Finish, C2.Finish),'dd/MMM/yyyy') Finish , ISNULL(CAST(C1.Status as VARCHAR(50)), CAST(C2.Status as VARCHAR(50))) Status , ISNULL(C1.Billed, C2.Billed) Billed , ISNULL(C1.Paid, C2.Paid) Paid , FORMAT(ISNULL(C1.PaymentDate, C2.PaymentDate),'dd/MMM/yyyy') PaymentDate , ISNULL(C1.ClaimNo, C2.ClaimNo) ClaimNo , ISNULL(C1.ProductType, C2.ProductType) ProductType , ISNULL(C1.ClaimType, C2.ClaimType) ClaimType , ISNULL(C1.Unpaid, C2.Unpaid) Unpaid , ISNULL(C1.Accepted, C2.Accepted) Accepted , ISNULL(C1.Excess, C2.Excess) Excess , ISNULL(C1.ExcessCompany, C2.ExcessCompany) ExcessCompany , ISNULL(C1.ExcessEmployee, C2.ExcessEmployee) ExcessEmployee , CASE WHEN ISNULL(C1.AccountName,'') = '' THEN C2.AccountName ELSE C1.AccountName END [AccountName], CASE WHEN ISNULL(C1.AccountNo,'') = '' THEN C2.AccountNo ELSE C1.AccountNo END [AccountNo], CASE WHEN ISNULL(C1.AccountBank,'') = '' THEN C2.AccountBank ELSE C1.AccountBank END [AccountBank],ISNULL(C1.Start, C2.START) Start2 FROM ( SELECT a.CNO , a.ClaimNo , b.Name [MemberName] , a.TreatmentPlace , a.Start , a.Finish , d.Description [ProductType] , CASE a.ProviderF WHEN 1 THEN 'Cashless' ELSE 'Reimbursement' END [ClaimType] , CASE WHEN a.Status = 'R' THEN 'Approved' WHEN a.Status = 'X' THEN 'Rejected' WHEN a.Status = 'C' THEN 'Corrected' WHEN cfuh.Status = 'O' THEN 'Follow Up' WHEN a.Status = 'V' THEN 'Verified' WHEN a.Status = 'H' THEN 'Hold' WHEN a.Status = 'P' THEN 'Postponed' WHEN a.Status = 'O' THEN 'Open' WHEN tdrd.DocumentStatus = 'SCOR' THEN 'Follow Up' WHEN tdrd.Status = 'REGISTERED' THEN 'Document Registered' WHEN tdrd.Status = 'SEND' THEN 'Document Sent' WHEN tdrd.Status = 'CHECKED' THEN 'Document Checked' ELSE '' END [Status], a.Billed , a.Unpaid , a.Billed - a.Unpaid [Paid] , a.Accepted , a.Excess , ISNULL(a.ExcessCoy, 0) [ExcessCompany] , ISNULL(a.ExcessEmp, 0) [ExcessEmployee] , CASE WHEN ISNULL(g.SettledStatus, 0) = 1 THEN g.PDate END [PaymentDate] , a.BankAccount [AccountName] , a.AccountNo [AccountNo], a.BankName [AccountBank] , ISNULL(TDRD.DocumentNo,0) AS DocumentNo FROM claimh a JOIN policy_member b ON a.mno = b.mno JOIN policy c ON b.pno = c.pno JOIN producttype d ON a.producttype = d.producttype LEFT JOIN ( SELECT DISTINCT cno , admno , description FROM claimledger WHERE code = 'A' ) e ON a.cno = e.cno AND e.Description NOT LIKE 'Correction%' LEFT JOIN admlink f ON e.admno = f.admno LEFT JOIN nvoucher g ON f.voucher = g.voucher LEFT JOIN dbo.tbl_disp_registration_document AS TDRD ON TDRD.ClaimNo = a.ClaimNo LEFT JOIN CorrespondentFollowUpHeader cfuh ON tdrd.DocumentNo = cfuh.DocumentNo WHERE b.MemberNo= '"+memberNo+"' AND a.ProductType='IP' ) C1 FULL OUTER JOIN ( SELECT 0 AS CNO , ISNULL(a.ClaimNo, '') ClaimNo, ISNULL(b.Name, '') [MemberName] , ISNULL(a.TreatmentPlace, '') AS TreatmentPlace, a.TreatmentStart Start , a.TreatmentEnd Finish , ISNULL(d.Description, '') [ProductType] , CASE WHEN a.ClaimType = 'C' THEN 'Cashless' ELSE 'Reimbursement' END [ClaimType] , CASE WHEN cfuh.Status = 'O' THEN 'Follow Up' WHEN a.DocumentStatus = 'SCOR' THEN 'Follow Up' WHEN a.Status = 'REGISTERED' THEN 'Document Registered' WHEN a.Status = 'SEND' THEN 'Document Sent' WHEN a.Status = 'CHECKED' THEN 'Document Checked' ELSE '' END [Status], ISNULL(a.TotalBill,0) Billed , 0 Unpaid , 0 [Paid] , 0 Accepted , 0 Excess , 0 [ExcessCompany] , 0 [ExcessEmployee] , NULL [PaymentDate] , a.BankAccountDisp [AccountName] , a.AccountNoDisp [AccountNo] , a.BankNameDisp [AccountBank] , ISNULL(a.DocumentNo,'') AS DocumentNo FROM dbo.tbl_disp_registration_document a JOIN policy_member b ON a.mno = b.mno JOIN policy c ON b.pno = c.pno JOIN producttype d ON a.producttype = d.producttype LEFT JOIN CorrespondentFollowUpHeader cfuh ON a.DocumentNo = cfuh.DocumentNo WHERE b.MemberNo= '"+memberNo+"' AND a.ProductType='IP' ) C2 ON C1.DocumentNo = C2.DocumentNo order by Start2 DESC"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))

		GlobalVariable.listComMemberName.clear()
		GlobalVariable.listComProviderName.clear()
		GlobalVariable.listComStartDt.clear()
		GlobalVariable.listComFinishDt.clear()
		GlobalVariable.listComStatus.clear()
		GlobalVariable.listComBilled.clear()
		GlobalVariable.listComPaid.clear()
		GlobalVariable.listComPaymentDt.clear()
		GlobalVariable.listComClaimNo.clear()
		GlobalVariable.listComProductType.clear()
		GlobalVariable.listComClaimType.clear()
		GlobalVariable.listComUnpaid.clear()
		GlobalVariable.listComAccepted.clear()
		GlobalVariable.listComExcessTotal.clear()
		GlobalVariable.listComExcessCompany.clear()
		GlobalVariable.listComExcessEmployee.clear()
		GlobalVariable.listComAccountName.clear()
		GlobalVariable.listComAccountNo.clear()
		GlobalVariable.listComAccountBank.clear()

		while(info.next()){
			GlobalVariable.listComMemberName.add(info.getString(1).trim())
			GlobalVariable.listComProviderName.add(info.getString(2).trim())
			GlobalVariable.listComStartDt.add(info.getString(3).trim())
			GlobalVariable.listComFinishDt.add(info.getString(4).trim())
			GlobalVariable.listComStatus.add(info.getString(5).trim())
			GlobalVariable.listComBilled.add(info.getString(6).replace(".0", "").trim())
			GlobalVariable.listComPaid.add(info.getString(7).replace(".0", "").trim())
			GlobalVariable.listComPaymentDt.add(info.getString(8))
			GlobalVariable.listComClaimNo.add(info.getString(9).trim())
			GlobalVariable.listComProductType.add(info.getString(10).trim())
			GlobalVariable.listComClaimType.add(info.getString(11).trim())
			GlobalVariable.listComUnpaid.add(info.getString(12).replace(".0", "").trim())
			GlobalVariable.listComAccepted.add(info.getString(13).replace(".0", "").trim())
			GlobalVariable.listComExcessTotal.add(info.getString(14).replace(".0", "").trim())
			GlobalVariable.listComExcessCompany.add(info.getString(15).replace(".0", "").trim())
			GlobalVariable.listComExcessEmployee.add(info.getString(16).replace(".0", "").trim())
			GlobalVariable.listComAccountName.add(info.getString(17).trim())
			GlobalVariable.listComAccountNo.add(info.getString(18).trim())
			GlobalVariable.listComAccountBank.add(info.getString(19).trim())
		}
	}


	@Keyword
	def queryParam(String IP, String dbName, String query, ArrayList<String> param){
		(new healthKeyword.configDB().connectDB(IP, dbName, 'ITAPP', 'It5Fu#H0m317'))

		ResultSet info = (new healthKeyword.configDB().executeQuery2(query,param))
		return info
	}

	@Keyword
	def queryClaimDiagnosis(def DocNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def query = "SELECT cd.DiagnosisID,d.Description FROM dbo.ClaimH AS ch LEFT JOIN dbo.Claim_Diagnosis AS cd ON ch.CNO=cd.CNO LEFT JOIN dbo.Diagnosis AS d ON cd.DiagnosisID=d.Diagnosis LEFT JOIN dbo.tbl_disp_registration_document AS tdrd ON tdrd.ClaimNo=ch.ClaimNo WHERE tdrd.DocumentNo='"+DocNo+"' ORDER BY ch.CNO DESC"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		while(info.next()){
			GlobalVariable.listComIdDiagnosis.add(info.getString(1))
		}
	}

	@Keyword
	def queryClaimTreatment(def DocNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def query = "SELECT mt.Description FROM dbo.ClaimH AS ch LEFT JOIN dbo.ClaimTreatment AS ct2 ON ct2.CNO = ch.CNO LEFT JOIN dbo.MedicalTreatment AS mt ON mt.MedicalTreatmentID = ct2.MedicalTreatmentID LEFT JOIN dbo.tbl_disp_registration_document AS tdrd ON tdrd.ClaimNo=ch.ClaimNo WHERE tdrd.DocumentNo='"+DocNo+"' ORDER BY ch.CNO DESC"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		while(info.next()){
			GlobalVariable.listComMedicalTreatment.add(info.getString(1))
		}
	}
	@Keyword
	def updateCounter(def tipe){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))
		def query
		if(tipe.startsWith("TS")){
			tipe = tipe.substring(2)
			//ini buat ngecek ts nya polis atau claim
			int ts = Integer.parseInt(tipe)
			println "isi substring TS nya "+ts
			//balikin jadi TS lg
			tipe = "TS"+ts
			if (ts < 53 || ts > 59){
				query = "UPDATE dbo.HealthDocumentReg SET Counter = (SELECT Counter FROM HealthDocumentReg WHERE Tipe = '"+tipe+"')+1 WHERE dbo.HealthDocumentReg.Tipe = '"+tipe+"'"
			}else{
				query = "UPDATE dbo.HealthPolicyInfo SET Counter = (SELECT Counter FROM HealthPolicyInfo WHERE Tipe = '"+tipe+"')+1 WHERE dbo.HealthPolicyInfo.Tipe = '"+tipe+"'"
			}
		}else{
			query = "UPDATE dbo.HealthPolicyInfo SET Counter = (SELECT Counter FROM HealthPolicyInfo WHERE Tipe = '"+tipe+"')+1 WHERE dbo.HealthPolicyInfo.Tipe = '"+tipe+"'"
		}
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateBilled(def tipe){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query =  "UPDATE dbo.HealthDocumentReg SET totBilled = (SELECT totBilled FROM HealthDocumentReg WHERE Tipe = '"+tipe+"')+100 WHERE dbo.HealthDocumentReg.Tipe = '"+tipe+"'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateDateMin(def tipe){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query =  "UPDATE dbo.HealthDocumentReg SET TreatmentFrom = FORMAT(CONVERT(DATETIME, TreatmentFrom, 105)-1,'dd/MMM/yyyy') , TreatmentTo = FORMAT(CONVERT(DATETIME, TreatmentTo, 105)-1,'dd/MMM/yyyy') WHERE Tipe= '"+tipe+"'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateDatePlus(def tipe){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query =  "UPDATE dbo.HealthDocumentReg SET TreatmentFrom = FORMAT(CONVERT(DATETIME, TreatmentFrom, 105)+3,'dd/MMM/yyyy') , TreatmentTo = FORMAT(CONVERT(DATETIME, TreatmentTo, 105)+3,'dd/MMM/yyyy') WHERE Tipe= '"+tipe+"'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateBPJS(def tipe){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "UPDATE dbo.HealthDocumentReg SET BPJS=1 WHERE dbo.HealthDocumentReg.Tipe = '"+tipe+"'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateTrafficAcc(def tipe){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "UPDATE dbo.HealthDocumentReg SET TrafficAcc=1 WHERE dbo.HealthDocumentReg.Tipe = '"+tipe+"'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updatePerawatanKhusus(def tipe){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "UPDATE dbo.HealthDocumentReg SET PerawatanKhusus=1 WHERE dbo.HealthDocumentReg.Tipe = '"+tipe+"'"
		(new healthKeyword.configDB().execute(query))
	}


	@Keyword
	def updatePasienMeninggal(def tipe){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "UPDATE dbo.HealthDocumentReg SET Pasien_Meninggal=1 WHERE dbo.HealthDocumentReg.Tipe = '"+tipe+"'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateDoubleInsured(def tipe){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "UPDATE dbo.HealthDocumentReg SET Double_Insured=1 WHERE dbo.HealthDocumentReg.Tipe = '"+tipe+"'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateDiscount(def tipe, def discount){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "UPDATE dbo.HealthDocumentReg SET discount='"+discount+"' WHERE dbo.HealthDocumentReg.Tipe = '"+tipe+"'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateStamp(def tipe, def stamp){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "UPDATE dbo.HealthDocumentReg SET stamp ='"+stamp+"' WHERE dbo.HealthDocumentReg.Tipe = '"+tipe+"'"
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def updateAddInfo(def idDocReg){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317'))

		def query = "UPDATE HealthAddInfo SET rowStatus = 1 WHERE idDOcReg ="+idDocReg
		(new healthKeyword.configDB().execute(query))
	}

	@Keyword
	def getMemberInfo(def memberNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def query = "select c.name[ClientName], b.MemberNo, b.name + case isnull(b.ImportantF,0) when 1 then ' [VIP]' else '' end [MemberName], b.birthdate[DateOfBirth], b.EmpID, convert(int,DATEDIFF(d, b.birthdate, getdate())/365.25)[Age], d.Description[Classification], case b.maritalstatus when 'M' then 'Married' when 'S' then 'Single' end [MaritalStatus], b.Area, b.correspondenceid_claim[Phone], case b.Membership when '1. EMP' then 'Employee' when '2. SPO' then 'Spouse' when '3. CHI' then 'Child' end [MembershipType], b.Email, case when exists (select 1 from policy_member where memberno=b.memberno and getdate() between pdate and ppdate) then 'Active' else 'Inactive' end[MembershipStatus], g.BankName, case b.Sex when 'M' then 'Male' when 'F' then 'Female' end as Sex, g.AccountNo, (select top 1 CONVERT(varchar(30),x.CardNo) from policy_member_card x left join policy_member_card y on x.id=y.previd where y.id is null and x.ClientID=a.ClientID and x.MemberNo=b.MemberNo)[CardNo], g.AccountName, i.RequestDate[CardRequestDate], i.PrintDate[CardPrintDate], i.SendDate[CardSendDate],i.ReceiveDate[CardReceiveDate] from policy a join policy_member b on a.pno=b.pno join profile c on a.clientid=c.id join policy_classification d on a.pno=d.pno and b.classno=d.classno join (select max(y.MNO)[MNO] from policy x join policy_member y on x.pno=y.pno where y.MemberNo='"+memberNo+"' and x.PStatus<>'O') e on b.mno=e.mno left join (select opno,memberno,max(ID_Account)[ID_Account] from policy_member_account where ResPNO<=0 group by opno,memberno) f on f.opno=a.opno and f.memberno=b.memberno left join policy_member_account g on f.id_account=g.id_account left join (select ClientID,MemberNo,max(ID)[ID] from policy_member_card group by ClientID,MemberNo) h on h.ClientID=a.ClientID and h.MemberNo=b.MemberNo left join policy_member_card i on h.id=i.id"
		(new healthKeyword.configDB().execute(query))
	}
	@Keyword
	def getMembershipTaskList(def docNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def query = "SELECT p4.Name AS Client,tdru.EmployeeName,FORMAT(tdr.DeliveryReceiveDate, 'dd/MMM/yyyy') AS DeliveryReceiveDate, FORMAT(tdr.DocumentReceiveDate, 'dd/MMM/yyyy') AS DocumentReceiveDate,p5.Name AS Provider, FORMAT(tdrd.TreatmentStart, 'dd/MMM/yyyy') AS TreatmentStart, FORMAT(tdrd.TreatmentEnd, 'dd/MMM/yyyy') AS TreatmentEnd, pt.DESCRIPTION AS ProductType ,d.Description AS Diagnosis, CONCAT('Rp.',' ',CONVERT(VARCHAR(50),CONVERT(VARCHAR,CAST(tdrd.TotalBill AS MONEY),1 ))) AS TotalBill, tdrd.Receiver,tdrd.PayToDisp,tdrd.AccountNoDisp,tdrd.BankNameDisp, tdrd.BankAccountDisp,tdrd.BankBranchDisp FROM dbo.tbl_disp_registration_document AS tdrd LEFT JOIN dbo.tbl_disp_registration AS TDR ON TDR.RegistrationNo = TDRD.RegistrationNo LEFT JOIN dbo.Profile AS P4 ON tdrd.Client = P4.ID LEFT JOIN dbo.Profile AS P5 ON tdrd.Provider = P5.ID LEFT JOIN dbo.tbl_disp_registration_unregmember AS tdru ON tdru.DocumentNo = tdrd.DocumentNo LEFT JOIN dbo.Diagnosis AS d ON tdrd.DiagnosisID=d.Diagnosis LEFT JOIN Producttype AS pt ON pt.producttype = tdrd.producttype WHERE tdrd.DocumentNo='"+docNo+"'"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
	}

	@Keyword
	def getMembershipTaskListDocReg2(def docNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def query = "SELECT TDRD.DocumentStatus AS TDRD_DocumentStatus, TDRD.Status AS TDRD_Status, P4.Name AS ClientName, PM.Name AS MemberName, FORMAT(TDRD.TreatmentStart, 'dd/MMM/yyyy') AS TDRD_TreatmentStart, FORMAT(TDRD.TreatmentEnd, 'dd/MMM/yyyy') AS TDRD_TreatmentEnd, CONCAT('Rp.',' ',CONVERT(VARCHAR(50),CONVERT(VARCHAR,CAST(tdr.TotalBill AS MONEY),1 ))) AS TDR_TotalBill, su.name AS TDRD_Receiver, TDRD.TreatmentPlace AS TDRD_TreatmentPlace, TDRD.CompleteF AS TDRD_CompleteF, TDRD.BoxNo AS TDRD_BoxNo, d.Description AS TDRD_DiagnosisID,  pt.Description AS TDRD_ProductType, TDRD.RoomOption AS TDRD_RoomOption, FORMAT(TDRD.DeliveryReceiveDate, 'dd/MMM/yyyy') AS TDRD_DeliveryReceiveDate, FORMAT(TDRD.ReceiveDate, 'dd/MMM/yyyy') AS ReceiveDate, TDRD.ClaimType AS TDRD_ClaimType, TDRD.BankNameDisp AS TDRD_BankNameDisp, TDRD.BankAccountDisp AS TDRD_BankAccountDisp, TDRD.AccountNoDisp AS TDRD_AccountNoDisp, TDRD.PayToDisp AS TDRD_PayToDisp, TDR.ClaimType AS TDR_ClaimType, TDR.DocumentSource AS TDR_DocumentSource, FORMAT(TDR.DocumentReceiveDate, 'dd/MMM/yyyy') AS TDR_DocumentReceiveDate, CONCAT('Rp.',' ',CONVERT(VARCHAR(50),CONVERT(VARCHAR,CAST(tdr.TotalBill AS MONEY),1 ))) AS  TDR_TotalBill, FORMAT(TDR.DeliveryReceiveDate, 'dd/MMM/yyyy') AS TDR_DeliveryReceiveDate FROM dbo.tbl_disp_registration_document AS TDRD LEFT JOIN dbo.tbl_disp_registration AS TDR ON TDR.RegistrationNo = TDRD.RegistrationNo LEFT JOIN dbo.Post_Claim_Batch AS PCB ON TDR.RefBatchNo=CAST(pcb.RegID AS VARCHAR(10)) AND PCB.RegID > 0 LEFT JOIN dbo.Profile AS P2 ON P2.Name = TDRD.TreatmentPlace LEFT JOIN dbo.Policy_Member AS PM ON TDRD.MNO = PM.MNo  LEFT JOIN dbo.Policy AS P3 ON PM.PNO = P3.PNO LEFT JOIN dbo.Profile AS P4 ON P3.ClientID = P4.ID  LEFT JOIN dbo.ProductType AS pt ON pt.ProductType=tdrd.ProductType LEFT JOIN dbo.Diagnosis AS d ON d.Diagnosis = tdrd.DiagnosisID LEFT JOIN dbo.SysUser AS su ON TDRD.Receiver=su.ID  WHERE TDRD.DocumentNo='"+docNo+"'"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
	}
	@Keyword
	def getExportClaimDocumentList(def boxNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		def query = "SELECT BoxNo,BoxType,CUser,FORMAT(CreatedDate, 'dd/MMM/yyyy') AS CreatedDate, FORMAT(ClosedDate, 'dd/MMM/yyyy') AS ClosedDate,Status, PrintCount,FirstPrintedBy, FORMAT(FirstPrintedDate, 'dd/MMM/yyyy') AS FirstPrintedDate,LastPrintedBy, FORMAT(LastPrintDate, 'dd/MMM/yyyy') AS LastPrintDate FROM dbo.ClaimBox AS cb WHERE BoxNo='"+boxNo+"'"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
	}
	@Keyword
	def getDataCompareSettlement(def docNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))

		//ini query yang lama
		//def query = "SELECT ch.CNO, CASE WHEN ISNULL(CL.IsCorrection,0) = 1 THEN CASE WHEN CL.Code = 'A' THEN 'CorrectionClaimPaid' WHEN (CL.Code = 'E' AND ExcessType = 'Coy') THEN 'CorrectionExcessCompany' WHEN (CL.Code = 'E' AND ExcessType = 'Emp') THEN 'CorrectionExcessEmployee' WHEN (CL.Code = 'E' AND ExcessType = 'Total') THEN 'CorrectionExcessTotal' END ELSE CASE WHEN CL.Code = 'A' THEN 'ClaimPaid' WHEN (CL.Code = 'E' AND ExcessType = 'Coy') THEN 'ExcessCompany' WHEN (CL.Code = 'E' AND ExcessType = 'Emp') THEN 'ExcessEmployee' WHEN (CL.Code = 'E' AND ExcessType = 'Total') THEN 'ExcessTotal' END END AS NoteType, CAST(ch.SendToSettlementDate AS DATE) AS SendToSettlementDate, ch.SendToSettlementStatus, ch.SendToSettlementUser, NV.Voucher AS NoteNo, NV.Nominal_CC, NV.DebtorF, NV.SettledStatus AS NoteSettledStatus, FV.Voucher AS VoucherNo, FV.Remarks, GND.GroupNoteNo FROM dbo.tbl_disp_registration_document AS TDRD JOIN dbo.ClaimH AS CH ON TDRD.ClaimNo = CH.ClaimNo JOIN dbo.ProductType AS PT ON CH.ProductType = PT.ProductType LEFT JOIN (SELECT DISTINCT CNO, ADMNO, Code, Type, IsCorrection, ExcessType FROM ClaimLedger) CL ON CH.CNO = CL.CNO LEFT JOIN AdmLink AL ON CL.AdmNo = AL.AdmNo LEFT JOIN nVoucher NV ON AL.Voucher = NV.Voucher LEFT JOIN FSL ON NV.Voucher = FSL.Doc_No LEFT JOIN fVoucher FV ON FV.Voucher = FSL.Voucher LEFT JOIN GroupNoteD GND ON NV.Voucher = GND.Voucher WHERE TDRD.DocumentNo ='"+docNo+"' ORDER BY CH.CNO ASC, CL.IsCorrection, NoteType"

		//ini query baru
		def query = "SELECT ch.CNO, CASE WHEN ISNULL(CL.IsCorrection,0) = 1 THEN CASE WHEN CL.Code = 'A' THEN 'CorrectionClaimPaid' WHEN (CL.Code = 'E' AND ExcessType = 'Coy') THEN 'CorrectionExcessCompany' WHEN (CL.Code = 'E' AND ExcessType = 'Emp') THEN 'CorrectionExcessEmployee' WHEN (CL.Code = 'E' AND ExcessType = 'Total') THEN 'CorrectionExcessTotal' END ELSE CASE WHEN CL.Code = 'A' THEN 'ClaimPaid' WHEN (CL.Code = 'E' AND ExcessType = 'Coy') THEN 'ExcessCompany' WHEN (CL.Code = 'E' AND ExcessType = 'Emp') THEN 'ExcessEmployee' WHEN (CL.Code = 'E' AND ExcessType = 'Total') THEN 'ExcessTotal' END END AS NoteType, ISNULL (CAST(CAST(ch.SendToSettlementDate AS DATE) AS VARCHAR(10)),'') AS SendToSettlementDate, ISNULL (CAST(ch.SendToSettlementStatus AS VARCHAR(1)),'') AS SendToSettlementStatus, ISNULL (ch.SendToSettlementUser,'') AS SendToSettlementUser, CASE WHEN NV.Voucher IS NOT NULL THEN 'Filled' ELSE '' END AS NoteNo , NV.Nominal_CC, NV.DebtorF, NV.SettledStatus AS NoteSettledStatus, CASE WHEN FV.Voucher IS NOT NULL THEN 'Filled' ELSE '' END AS VoucherNo , ISNULL (FV.Remarks,'') AS Remarks, CASE WHEN GND.GroupNoteNo IS NOT NULL THEN 'Filled' ELSE '' END AS GroupNoteNo FROM dbo.tbl_disp_registration_document AS TDRD JOIN dbo.ClaimH AS CH ON TDRD.ClaimNo = CH.ClaimNo JOIN dbo.ProductType AS PT ON CH.ProductType = PT.ProductType LEFT JOIN (SELECT DISTINCT CNO, ADMNO, Code, Type, IsCorrection, ExcessType FROM ClaimLedger) CL ON CH.CNO = CL.CNO LEFT JOIN AdmLink AL ON CL.AdmNo = AL.AdmNo LEFT JOIN nVoucher NV ON AL.Voucher = NV.Voucher LEFT JOIN FSL ON NV.Voucher = FSL.Doc_No LEFT JOIN fVoucher FV ON FV.Voucher = FSL.Voucher LEFT JOIN GroupNoteD GND ON NV.Voucher = GND.Voucher WHERE TDRD.DocumentNo = '"+docNo+"' ORDER BY CH.CNO ASC, CL.IsCorrection, NoteType"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		while(info.next()){
			GlobalVariable.CNO_stt.add(info.getObject('CNO').toString())
			GlobalVariable.NoteType_stt.add(info.getObject('NoteType').toString())
			GlobalVariable.SendToSettlementDate_stt.add(info.getObject('SendToSettlementDate').toString())
			GlobalVariable.SendToSettlementStatus_stt.add(info.getObject('SendToSettlementStatus').toString())
			GlobalVariable.SendToSettlementUser_stt.add(info.getObject('SendToSettlementUser').toString())
			GlobalVariable.NoteNo_stt.add(info.getObject('NoteNo').toString())
			GlobalVariable.Nominal_CC_stt.add(info.getObject('Nominal_CC').toString().replace(".0", ""))
			GlobalVariable.DebtorF_stt.add(info.getObject('DebtorF').toString())
			GlobalVariable.NoteSettledStatus_stt.add(info.getObject('NoteSettledStatus').toString())
			GlobalVariable.VoucherNo_stt.add(info.getObject('VoucherNo').toString())
			GlobalVariable.Remarks_stt.add(info.getObject('Remarks').toString())
			GlobalVariable.GroupNoteNo_stt.add(info.getObject('GroupNoteNo').toString())
		}
	}

	@Keyword
	def getExportClaimMonitoring(def sequence){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))
		def query = "SELECT Query FROM dbo.Claim_Monitoring AS cm WHERE Sequence = '"+sequence+"'"
		def query2 =''
		def queryFinal =''
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		info.next()
		query2 = info.getObject('Query').toString()
		def step1 = query2.replaceAll("Billed,","CASE ISNULL(Billed,'') WHEN 0 THEN '' END AS Billed,")
		def step2 = step1.replaceAll("Unpaid,", "CASE ISNULL(Unpaid,'') WHEN 0 THEN '' END AS Unpaid,")
		def step3 = step2.replaceAll("Accepted,","CASE ISNULL(Accepted,'') WHEN 0 THEN '' END AS Accepted,")
		def step4 = step3.replaceAll("ExcessTotal,","CASE ISNULL(ExcessTotal,'') WHEN 0 THEN '' END AS ExcessTotal,")
		def step5 = step4.replaceAll("CreateBatchDate,", "CASE ISNULL(CreateBatchDate,'') WHEN 0 THEN '' END AS CreateBatchDate,")
		def step6 = step5.replaceAll("VDate,","CASE ISNULL(Vdate,'') WHEN 0 THEN '' END AS VDate,")
		def step7 = step6.replaceAll("RDate,","CASE ISNULL(Rdate,'') WHEN 0 THEN '' END AS RDate,")
		def step8 = step7.replaceAll("TreatmentStart,", "CONVERT(VARCHAR, TreatmentStart, 101) + ' ' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(hh, TreatmentStart)),2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, TreatmentStart)), 2) AS TreatmentStart,")
		def step9 = step8.replaceAll("TreatmentFinish,","CONVERT(VARCHAR, TreatmentFinish, 101) + ' ' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(hh, TreatmentFinish)),2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, TreatmentFinish)), 2) AS TreatmentFinish,")
		def step10 = step9.replaceAll("DocumentEntryDate,","CONVERT(VARCHAR, DocumentEntryDate, 101) + ' ' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(hh, DocumentEntryDate)),2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, DocumentEntryDate)), 2) AS DocumentEntryDate,")
		def step11 = step10.replaceAll("ClaimEntryDate,","CONVERT(VARCHAR, ClaimEntryDate, 101) + ' ' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(hh, ClaimEntryDate)),2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, ClaimEntryDate)), 2) AS ClaimEntryDate,")
		def step12 = step11.replaceAll("LDate,", "CONVERT(VARCHAR, LDate, 101) + ' ' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(hh, LDate)),2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LDate)), 2) AS LDate,")
		def step13 = step12.replaceAll("ProcessDocumentStart,","CONVERT(VARCHAR, ProcessDocumentStart, 101) + ' ' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(hh, ProcessDocumentStart)),2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, ProcessDocumentStart)), 2) AS ProcessDocumentStart,")
		def step14 = step13.replaceAll("ProcessDocumentFinish","CONVERT(VARCHAR, ProcessDocumentFinish, 101) + ' ' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(hh, ProcessDocumentFinish)),2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, ProcessDocumentFinish)), 2) AS ProcessDocumentFinish")

		queryFinal = step14
		//		println(queryFinal)
		ResultSet hasil = (new healthKeyword.configDB().executeQuery(queryFinal))
		println (hasil)
		return hasil
	}

	@Keyword
	def getExportToCSV(){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))
		def query = "EXEC sp_ExportCSV_RegisteredClaimDocuments '2019-04-01','2019-04-30'"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		for(int row = 0;row<2;row++){
			info.next()
		}
		def result = info.getObject('Result').toString()
		def result2 = result.replaceAll("\"", "")
		println result2

		return result2
	}

	@Keyword
	def getCurrentPNO(def policyNo){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))
		def query = "SELECT TOP 1 PNO FROM dbo.Policy AS p WHERE PolicyNo='"+policyNo+"' ORDER by CDate desc"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		def hasil
		while(info.next()){
			hasil = info.getObject('PNO')
		}
		println hasil
		return hasil
	}
	@Keyword
	def getPartialHeader(def clientId, def effDate){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))
		def query = "SELECT CoverageType,Premium FROM PartialCoverageHeader WHERE ClientID='"+clientId+"' AND effectiveDate = convert(varchar, '"+effDate+"', 23)"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		while(info.next()){
			GlobalVariable.coverageType = info.getObject('CoverageType').toString()
			GlobalVariable.premium = info.getObject('Premium').toString()
		}
	}
	@Keyword
	def getPartialDetail(def clientId, def effDate){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))
		def query = "SELECT pch.CoverageType,pcd.sequence,pcd.coveragePercentage FROM PartialCoverageHeader AS pch LEFT JOIN partialCoverageDetail AS pcd ON pch.coverageHeaderID=pcd.coverageHeaderID WHERE pch.ClientID='"+clientId+"' AND effectiveDate = convert(varchar, '"+effDate+"', 23)"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		while(info.next()){
			GlobalVariable.tabSequence.add(info.getObject('sequence').toString())
			GlobalVariable.tabCoverage.add(info.getObject('coveragePercentage').toString().replace(".0", "")+" %")
		}
	}
	@Keyword
	def getCurrentTranITAPPctionPolicyMember(def policyNo, def currentPNO){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))
		def query = "SELECT DISTINCT PM.EmpID, PM.MemberNo, PM.Name, C.ShortDesc AS Classification, CASE WHEN PM.Membership = '1. EMP' THEN 'Employee' WHEN PM.Membership = '2. SPO' THEN 'Spouse' WHEN PM.Membership = '3. CHI' THEN 'Child' END AS MembershipType, REPLACE(CONVERT(VARCHAR, PM.PDate, 106),' ','/') AS PDate, REPLACE(CONVERT(VARCHAR, PM.PPDate, 106),' ','/') AS PPDate, CASE WHEN PM.TType = 'C' THEN 'Change' WHEN PM.TType = 'N' THEN 'Registration' WHEN PM.TType = 'R' THEN 'Terminate' END AS 'TranITAPPction' FROM Policy P INNER JOIN Policy_Member PM ON P.PNO = PM.PNO LEFT JOIN Policy_Classification C ON C.CLASSNO = PM.CLASSNO WHERE P.PolicyNo = '"+policyNo+"' AND C.PNO = '"+currentPNO+"' AND P.PNO = C.PNO ORDER BY Name DESC"
		def row
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		while(info.next()){
			GlobalVariable.EmpId_comp.add(info.getObject('EmpID').toString())
			GlobalVariable.MemberNo_comp.add(info.getObject('MemberNo').toString())
			GlobalVariable.MemberName_comp.add(info.getObject('Name').toString())
			GlobalVariable.Classification_comp.add(info.getObject('Classification').toString())
			GlobalVariable.MembershipType_comp.add(info.getObject('MembershipType').toString())
			GlobalVariable.EffStart_comp.add(info.getObject('PDate').toString())
			GlobalVariable.EffEnd_comp.add(info.getObject('PPDate').toString())
			GlobalVariable.TranITAPPction_comp.add(info.getObject('TranITAPPction').toString())
		}
		def query2 = "SELECT COUNT(*) AS 'Row' FROM Policy P INNER JOIN Policy_Member PM ON P.PNO = PM.PNO LEFT JOIN Policy_Classification C ON C.CLASSNO = PM.CLASSNO WHERE P.PolicyNo = '"+policyNo+"' AND C.PNO = '"+currentPNO+"' AND P.PNO = C.PNO"
		info = (new healthKeyword.configDB().executeQuery(query2))
		while(info.next()){
			row = info.getObject('Row').toString()
		}
		return row
	}
	@Keyword
	def getStandardPremium(def currentPNO){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))
		def query = "DECLARE @PNOparam INT = "+currentPNO+" DECLARE @PNO INT = @PNOparam SELECT PM.MNO, P.PNO, PC.Description AS Classification, PM.Membership AS MembershipType, PM.EmpID AS EmployeeID, CAST('' AS VARCHAR(50)) AS EmployeeName, PM.Area, PM.Name AS MemberName, CASE PM.Sex WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' ELSE PM.Sex END AS Sex, PM.BirthDate AS DateOfBirth, PM.MemberNo, PM.PDate AS EffectiveStart, PM.PPDate AS EffectiveEnd, CAST(DATEDIFF(DAY,PM.PDate,PM.PPDate) + 1 AS VARCHAR(5)) + ' / ' + CAST(DATEDIFF(DAY,P.EffectiveDate,P.RenewalDate) + 1 AS VARCHAR(5)) AS EffectiveDays, CAST('' AS VARCHAR(50)) AS CardNo, CASE LEFT(PM.MaritalStatus,1) WHEN 'S' THEN 'Single' WHEN 'M' THEN 'Married' ELSE PM.MaritalStatus END AS MaritalStatus, PMP.PPlan AS ProductPlan, CASE WHEN P.InsuranceType = 'IAS' THEN 0 ELSE ISNULL(PMR.Standard_Premi,0) END AS StandardPremium, CASE WHEN P.InsuranceType = 'IAS' THEN 0 ELSE CASE WHEN PMR.Net_Premi > 0 THEN ISNULL(PMR.Net_Premi,0) ELSE 0 END END AS AdditionalPremium, CASE WHEN P.InsuranceType = 'IAS' THEN 0 ELSE CASE WHEN PMR.Net_Premi < 0 THEN ISNULL(PMR.Net_Premi,0) ELSE 0 END END AS PremiumRefund, CASE WHEN P.InsuranceType = 'IAS' THEN 0 ELSE ISNULL(PMR.Net_Premi,0) END AS TotalPremium, CASE WHEN PM.TType = 'N' THEN 'Registration' WHEN PM.TType = 'R' THEN 'Termination' WHEN PM.TType IN ('C','M') THEN 'Change' ELSE PM.TType END AS TranITAPPctionType, CASE PMP.ExcludeF WHEN 1 THEN 'No' ELSE 'Yes' END AS CoverStatus, PM.Remarks INTO #temp FROM Policy P INNER JOIN Policy_Member PM ON P.PNO = PM.PNO INNER JOIN Policy_Classification PC ON P.PNO = PC.PNO AND PM.CLASSNO = PC.CLASSNO LEFT JOIN ( Policy_Member_Plan PMP LEFT JOIN Policy_Member_Premium PMR ON PMP.PNO = PMR.PNO AND PMP.MNO = PMR.MNO AND PMP.PPlan = PMR.PPlan AND PMP.BAmount = PMR.BAmount LEFT JOIN PPlan PP ON PMP.PPlan = PP.PPlan LEFT JOIN Product PR ON PP.ProductID = PR.ProductID LEFT JOIN ProductType PT ON PR.ProductType = PT.ProductType) ON PM.PNO = PMP.PNO AND PM.MNO = PMP.MNO WHERE P.PNO = @PNO INSERT INTO #temp SELECT PM.MNO, P.PNO, PC.Description AS Classification, PMX.Membership AS MembershipType, PMX.EmpID AS EmployeeID, '' AS EmployeeName, PMX.Area, PMX.Name AS MemberName, CASE PMX.Sex WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' ELSE PMX.Sex END AS Sex, PMX.BirthDate AS DateOfBirth, PMX.MemberNo, PMX.PDate AS EffectiveStart, PMX.PPDate AS EffectiveEnd, CAST(DATEDIFF(DAY,PMX.PDate,PMX.PPDate) + 1 AS VARCHAR(5)) + ' / ' + CAST(DATEDIFF(DAY,P.EffectiveDate,P.RenewalDate) + 1 AS VARCHAR(5)) AS EffectiveDays, '' AS CardNo, CASE LEFT(PMX.MaritalStatus,1) WHEN 'S' THEN 'Single' WHEN 'M' THEN 'Married' ELSE PMX.MaritalStatus END AS MaritalStatus, PMP.PPlan AS ProductPlan, CASE WHEN P.InsuranceType = 'IAS' THEN 0 ELSE ISNULL(PMR.Standard_Premi,0) END AS StandardPremium, CASE WHEN P.InsuranceType = 'IAS' THEN 0 ELSE CASE WHEN PMR.Net_Premi > 0 THEN ISNULL(PMR.Net_Premi,0) ELSE 0 END END AS AdditionalPremium, CASE WHEN P.InsuranceType = 'IAS' THEN 0 ELSE CASE WHEN PMR.Net_Premi < 0 THEN ISNULL(PMR.Net_Premi,0) ELSE 0 END END AS PremiumRefund, CASE WHEN P.InsuranceType = 'IAS' THEN 0 ELSE ISNULL(PMR.Net_Premi,0) END AS TotalPremium, CASE WHEN PM.TType = 'N' THEN 'Registration' WHEN PM.TType = 'R' THEN 'Termination' WHEN PM.TType IN ('C','M') THEN 'Change' ELSE PM.TType END AS TranITAPPctionType, CASE PMP.ExcludeF WHEN 1 THEN 'No' ELSE 'Yes' END AS CoverStatus, PM.Remarks FROM Policy P INNER JOIN Policy_Member PM ON P.PNO = PM.PNO INNER JOIN Policy_Member PMX ON PM.PMNO = PMX.MNO INNER JOIN Policy_Classification PC ON PMX.PNO = PC.PNO AND PMX.CLASSNO = PC.CLASSNO INNER JOIN ( Policy_Member_Plan PMP INNER JOIN Policy_Member_Premium PMR ON PMP.MNO = PMR.MNO AND PMP.PPlan = PMR.PPlan AND PMP.BAmount = PMR.BAmount INNER JOIN PPlan PP ON PMP.PPlan = PP.PPlan INNER JOIN Product PR ON PP.ProductID = PR.ProductID INNER JOIN ProductType PT ON PR.ProductType = PT.ProductType) ON PMX.PNO = PMP.PNO AND PMX.MNO = PMP.MNO AND P.PNO = PMR.PNO WHERE P.PNO = @PNO AND COALESCE(PMR.Net_Premi, 0) < 0 INSERT INTO #temp SELECT PM.MNO, P.PNO, PC.Description AS Classification, PMX.Membership AS MembershipType, PMX.EmpID AS EmployeeID, '' AS EmployeeName, PMX.Area, PMX.Name AS MemberName, CASE PMX.Sex WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' ELSE PMX.Sex END AS Sex, PMX.BirthDate AS DateOfBirth, PMX.MemberNo, PMX.PDate AS EffectiveStart, PMX.PPDate AS EffectiveEnd, CAST(DATEDIFF(DAY,PMX.PDate,PMX.PPDate) + 1 AS VARCHAR(5)) + ' / ' + CAST(DATEDIFF(DAY,P.EffectiveDate,P.RenewalDate) + 1 AS VARCHAR(5)) AS EffectiveDays, '' AS CardNo, CASE LEFT(PMX.MaritalStatus,1) WHEN 'S' THEN 'Single' WHEN 'M' THEN 'Married' ELSE PMX.MaritalStatus END AS MaritalStatus, PMP.PPlan AS ProductPlan, CASE WHEN P.InsuranceType = 'IAS' THEN 0 ELSE ISNULL(PMR.Standard_Premi,0) END AS StandardPremium, CASE WHEN P.InsuranceType = 'IAS' THEN 0 ELSE CASE WHEN PMR.Net_Premi > 0 THEN ISNULL(PMR.Net_Premi,0) ELSE 0 END END AS AdditionalPremium, CASE WHEN P.InsuranceType = 'IAS' THEN 0 ELSE CASE WHEN PMR.Net_Premi < 0 THEN ISNULL(PMR.Net_Premi,0) ELSE 0 END END AS PremiumRefund, CASE WHEN P.InsuranceType = 'IAS' THEN 0 ELSE ISNULL(PMR.Net_Premi,0) END AS TotalPremium, CASE WHEN PM.TType = 'N' THEN 'Registration' WHEN PM.TType = 'R' THEN 'Termination' WHEN PM.TType IN ('C','M') THEN 'Change' ELSE PM.TType END AS TranITAPPctionType, CASE PMP.ExcludeF WHEN 1 THEN 'No' ELSE 'Yes' END AS CoverStatus, PM.Remarks FROM Policy P INNER JOIN Policy_Member PM ON P.PNO = PM.PNO INNER JOIN Policy_Member PMX ON PM.PMNO = PMX.MNO INNER JOIN Policy_Classification PC ON PMX.PNO = PC.PNO AND PMX.CLASSNO = PC.CLASSNO INNER JOIN Policy_Member_Plan PMP ON PMX.PNo = PMP.PNo AND PMX.MNo = PMP.MNo AND PMP.ExcludeF = 0 LEFT JOIN Policy_Member_Premium PMR ON P.PNo = PMR.PNO AND PM.PMNo = PMR.MNo AND PMP.PPlan = PMR.PPlan AND PMP.BAmount = PMR.BAmount WHERE P.PNO = @PNO AND PM.TType = 'R' AND PMR.PNo IS NULL SELECT PM.MNO, ISNULL(PM2.Name,'') AS EmployeeName, ISNULL(PMC.CardNo,'') AS CardNo INTO #temp2 FROM Policy P INNER JOIN Policy_Member PM ON P.PNO = PM.PNO LEFT JOIN ( SELECT P1.OPNO, PM1.EmpID, MAX(PM1.MNO) AS EmpMNO FROM Policy P1 JOIN Policy_Member PM1 ON P1.PNO = PM1.PNO AND PM1.Membership = '1. EMP' GROUP BY P1.OPNO, PM1.EmpID ) X1 ON X1.OPNO = P.OPNO AND X1.EmpID = PM.EmpID LEFT JOIN Policy_Member pm2 ON x1.EmpMNO = pm2.MNO LEFT JOIN (SELECT MemberNo, MAX(ID) AS MaxID FROM Policy_Member_Card GROUP BY MemberNo ) x2 ON x2.MemberNo = PM.MemberNo AND ISNULL(PM.MemberNo,'') != '' LEFT JOIN Policy_Member_Card PMC ON PMC.ID = x2.MaxID WHERE P.PNO = @PNO UPDATE T1 SET EmployeeName = T2.EmployeeName, CardNo = T2.CardNo FROM #temp T1 INNER JOIN #temp2 T2 ON T1.MNO = T2.MNO INSERT INTO #temp (MNO,PNO,Classification,MembershipType,EmployeeID,EmployeeName,Area,MemberName,Sex,DateOfBirth,MemberNo,EffectiveStart,EffectiveEnd,EffectiveDays,CardNo,MaritalStatus,ProductPlan,StandardPremium,AdditionalPremium,PremiumRefund,TotalPremium,TranITAPPctionType,CoverStatus,Remarks) SELECT 0,@PNO,'Deposit','','','','','','','','','','','','','','','', CASE WHEN P.DepositAmount > 0 THEN P.DepositAmount ELSE '' END, CASE WHEN P.DepositAmount < 0 THEN P.DepositAmount ELSE '' END, P.DepositAmount, '', '', '' FROM Policy P WHERE P.InsuranceType = 'IAS' AND P.DepositAmount != 0 AND P.PNO = @PNO INSERT INTO #temp (MNO,PNO,Classification,MembershipType,EmployeeID,EmployeeName,Area,MemberName,Sex,DateOfBirth,MemberNo,EffectiveStart,EffectiveEnd,EffectiveDays,CardNo,MaritalStatus,ProductPlan,StandardPremium,AdditionalPremium,PremiumRefund,TotalPremium,TranITAPPctionType,CoverStatus,Remarks) SELECT 0,@PNO,'Member Fee','','','','','','','','','','','','','','','', CASE WHEN P.FeeAmount > 0 THEN P.FeeAmount ELSE '' END, CASE WHEN P.FeeAmount < 0 THEN P.FeeAmount ELSE '' END, P.FeeAmount, '', '', '' FROM Policy P WHERE P.InsuranceType = 'IAS' AND P.FeeAmount != 0 AND P.PNO = @PNO SELECT PNO, MembershipType, EmployeeID, REPLACE(EmployeeName,',',''), Area, REPLACE(MemberName,',',''), Sex, FORMAT(DateOfBirth, 'dd/MMM/yyyy'), MemberNo, FORMAT(EffectiveStart, 'dd/MMM/yyyy'), FORMAT(EffectiveEnd, 'dd/MMM/yyyy'), EffectiveDays, REPLACE(CardNo,',',''), MaritalStatus, ProductPlan, FORMAT(StandardPremium, '#.##########') AS StandardPremium, FORMAT(AdditionalPremium, '#.##########'), FORMAT(PremiumRefund, '#.##########'), FORMAT(TotalPremium, '#.##########'), TranITAPPctionType, CoverStatus, Remarks FROM #temp WHERE StandardPremium !='' DROP TABLE #temp DROP TABLE #temp2"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		while(info.next()){
			GlobalVariable.StandardPremium.add(info.getObject('StandardPremium'))
		}
	}
	@Keyword
	def getStatusProccess(){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))
		def query = "SELECT TOP 1 * FROM dbo.PolicyBackgroundProcesses AS pbp WHERE RequestBy='"+GlobalVariable.SystemUser+"' ORDER BY RequestDate DESC"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		info.next()
		return info.getObject('ProcessStatus').toString()
	}
	@Keyword
	def getStatusPolicy(){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))
		def query = "SELECT TOP 1 * FROM dbo.Policy WHERE PolicyNo = '"+GlobalVariable.policyNo+"'"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		info.next()
		GlobalVariable.PolicyStatus = info.getObject('PStatus').toString()
		if(GlobalVariable.PolicyStatus=='A'){
			GlobalVariable.PolicyStatus ='Approved'
		}else if(GlobalVariable.PolicyStatus=='C'){
			GlobalVariable.PolicyStatus ='Changed'
		}else if(GlobalVariable.PolicyStatus=='O'){
			GlobalVariable.PolicyStatus ='Open'
		}
	}
	@Keyword
	def getCountRow(String queryTable){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'LiTT', 'ITAPP', 'It5Fu#H0m317'))
		ResultSet info = (new healthKeyword.configDB().executeQuery(queryTable))
		ArrayList countRow = new ArrayList()

		while (info.next()) {
			Object getData = info.getObject(1)
			countRow.add(getData)
		}
		KeywordUtil.markPassed('Total row is ' + countRow.size())
		return countRow.size()
	}
	@Keyword
	def getHealthPremiRegistration(){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'LiTT', 'ITAPP', 'It5Fu#H0m317'))
		String query = "SELECT TranITAPPctionType, Classification, MembershipType, ProductType, NumOfMember, Premium FROM HealthPremiRegistration where Tipe =(SELECT param1 FROM dbo.HealthParam WHERE USER_ID='"+GlobalVariable.SystemUser+"')"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		while(info.next()){
			GlobalVariable.compTransType.add(info.getObject("TranITAPPctionType").toString())
			GlobalVariable.compClassification.add(info.getObject("Classification").toString())
			GlobalVariable.compMembershipType.add(info.getObject("MembershipType").toString())
			GlobalVariable.compProductType.add(info.getObject("ProductType").toString())
			GlobalVariable.compNumOfMember.add(info.getObject("NumOfMember").toString())
			GlobalVariable.compPremium.add(info.getObject("Premium").toString())
		}
	}
	@Keyword
	def getHealthPremiInstallment(){
		(new healthKeyword.configDB().connectDB('172.16.94.48', 'LiTT', 'ITAPP', 'It5Fu#H0m317'))
		String query = "SELECT Description,DueDate,GrossPremiumFee FROM HealthPremiInstallment where Tipe =(SELECT param1 FROM dbo.HealthParam WHERE USER_ID='"+GlobalVariable.SystemUser+"')"
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		while(info.next()){
			GlobalVariable.compDescription.add(info.getObject("Description").toString())
			GlobalVariable.compDueDate.add(info.getObject("DueDate").toString())
			GlobalVariable.compGrossPremiumFee.add(info.getObject("GrossPremiumFee").toString())
		}
	}

	@Keyword
	def getDiagnosisExclusion(int pNo, String effDate){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))
		String query = "SELECT FORMAT (b.EffectiveDate, 'dd/MMM/yyyy') EffectiveDate, b.OPNo, a.Description, b.Coverage, b.Payer, CASE WHEN b.IsNeedConfirmation=0 THEN 'No' WHEN b.IsNeedConfirmation=1 THEN 'Yes' END AS IsNeedConfirmation FROM dbo.DiagnosisCategory a, dbo.PolicyDiagnosisExclusion b WHERE b.CreatedBy LIKE '"+GlobalVariable.SystemUser+"' AND b.OPNo = "+pNo+" AND FORMAT (b.EffectiveDate, 'dd/MMM/yyyy') = '"+effDate+"' AND  a.DiagnosisCategoryID = b.DiagnosisCategoryID"
		println "isi query nya -> "+query
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		while(info.next()){
			GlobalVariable.DxDescription.add(info.getObject("Description").toString())
			GlobalVariable.DxCoverage.add(info.getObject("Coverage").toString())
			GlobalVariable.DxPayer.add(info.getObject("Payer").toString())
			GlobalVariable.DxConfirmation.add(info.getObject("IsNeedConfirmation").toString())
		}
	}
	@Keyword
	def getCountDiagnosisExclusion(int pNo, String effDate){
		(new healthKeyword.configDB().connectDB('172.16.94.70', 'SEA', 'ITAPP', 'It5Fu#H0m317'))
		String query = "SELECT COUNT(*) AS Jumlah FROM dbo.PolicyDiagnosisExclusion WHERE CreatedBy LIKE '"+GlobalVariable.SystemUser+"' AND OPNo = "+pNo+" AND FORMAT (EffectiveDate, 'dd/MMM/yyyy') = '"+effDate+"'"
		println "isi query count -> "+ query
		ResultSet info = (new healthKeyword.configDB().executeQuery(query))
		info.next()
		def jumlah = info.getObject("Jumlah")
		return jumlah
	}

}
