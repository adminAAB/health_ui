	package healthKeyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.lang.Integer as Integer
import java.util.ArrayList
import java.awt.Desktop
import java.io.File
import java.io.IOException
import java.nio.file.*
import org.openqa.selenium.JavascriptExecutor
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
//import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

import internal.GlobalVariable

public class utilityKeyword {

	@Keyword
	def executeJs(String javascript) {
		((JavascriptExecutor) DriverFactory.getWebDriver()).executeScript(javascript)
	}

	@Keyword
	def getCompare(def param){
		def dataCompare = (new healthKeyword.utilityDB()).compareDocNo(param)
		dataCompare.next()

		GlobalVariable.comClientName = dataCompare.getObject("ClientName").toString()
		GlobalVariable.comMemberName = dataCompare.getObject("MemberName").toString()
		GlobalVariable.comBilled = dataCompare.getObject("TDR_TotalBill").toString()
		GlobalVariable.comSendTo = dataCompare.getObject("TDRD_Receiver").toLowerCase().toString()
		GlobalVariable.comProvider = dataCompare.getObject("TDRD_TreatmentPlace").toString()
		GlobalVariable.comBoxNo = dataCompare.getObject("TDRD_BoxNo").toString()
		GlobalVariable.comIdDiagnosis = dataCompare.getObject("TDRD_DiagnosisID").toString()
		GlobalVariable.comDeliveryNo = dataCompare.getObject("TDRD_DeliveryNo").toString()
		GlobalVariable.comBankName = dataCompare.getObject("TDRD_BankNameDisp").toString()
		GlobalVariable.comBankAccount = dataCompare.getObject("TDRD_BankAccountDisp").toString()
		GlobalVariable.comAccountNo = dataCompare.getObject("TDRD_AccountNoDisp").toString()
		GlobalVariable.comInvoiceNo = dataCompare.getObject("TDR_RefNo").toString()
		GlobalVariable.comBilled2 = dataCompare.getObject("TDR_TotalBill").toString()
		GlobalVariable.comDeliveryNo2 = dataCompare.getObject("TDR_DeliveryNo").toString()
		GlobalVariable.comNumClaim = dataCompare.getObject("PCB_TotalqtyClaim").toString()
		GlobalVariable.comBilled3 = dataCompare.getObject("PCB_TotalClaimAmount").toString()
		GlobalVariable.comInvoiceNo2 = dataCompare.getObject("PCB_RefNo").toString()
		GlobalVariable.comDiscount = dataCompare.getObject("PCB_Discount").toString()
		GlobalVariable.comAccountNo2 = dataCompare.getObject("PCB_AccountNo").toString()
		GlobalVariable.comBankAccount2 = dataCompare.getObject("PCB_BankAccount").toString()
		GlobalVariable.comBankName2 = dataCompare.getObject("PCB_BankName").toString()
		GlobalVariable.comTDRD_Dispatcher = dataCompare.getObject("TDRD_Dispatcher").toString()
		GlobalVariable.comTDRD_DocumentStatus = dataCompare.getObject("TDRD_DocumentStatus").toString()
		GlobalVariable.comTDRD_Status = dataCompare.getObject("TDRD_Status").toString()
		GlobalVariable.comTDRD_Sender = dataCompare.getObject("TDRD_Sender").toString()
		GlobalVariable.comTDRD_CUser = dataCompare.getObject("TDRD_CUser").toString()
		GlobalVariable.comTDRD_CDate = dataCompare.getObject("TDRD_CDate").toString()
		GlobalVariable.comTDRD_PIC = dataCompare.getObject("TDRD_PIC").toLowerCase().toString()
		GlobalVariable.comTDRD_CompleteF = dataCompare.getObject("TDRD_CompleteF").toString()
		GlobalVariable.comTDRD_ProductType = dataCompare.getObject("TDRD_ProductType").toString()
		GlobalVariable.comTDRD_RoomOption = dataCompare.getObject("TDRD_RoomOption").toString()
		GlobalVariable.comTDRD_isCobBPJS = dataCompare.getObject("TDRD_isCobBPJS").toString()
		GlobalVariable.comTDRD_isCashPlanBPJS = dataCompare.getObject("TDRD_isCashPlanBPJS").toString()
		GlobalVariable.comTDRD_isDoubleInsured = dataCompare.getObject("TDRD_isDoubleInsured").toString()
		GlobalVariable.comTDRD_isNotEligible = dataCompare.getObject("TDRD_isNotEligible").toString()
		GlobalVariable.comTDRD_isSpecialTreatment = dataCompare.getObject("TDRD_isSpecialTreatment").toString()
		GlobalVariable.comTDRD_isDeceased = dataCompare.getObject("TDRD_isDeceased").toString()
		GlobalVariable.comTDRD_ClaimType = dataCompare.getObject("TDRD_ClaimType").toString()
		GlobalVariable.comTDRD_PayToDisp = dataCompare.getObject("TDRD_PayToDisp").toString()
		GlobalVariable.comTDRD_isTrafficAccident = dataCompare.getObject("TDRD_isTrafficAccident").toString()
		GlobalVariable.comTDR_ClaimType = dataCompare.getObject("TDR_ClaimType").toString()
		GlobalVariable.comTDR_DocumentSource = dataCompare.getObject("TDR_DocumentSource").toString()
		GlobalVariable.comTDR_CUser = dataCompare.getObject("TDR_CUser").toString()
		GlobalVariable.comTDR_CDate = dataCompare.getObject("TDR_CDate").toString()
		GlobalVariable.comPCB_ReceiveDate = dataCompare.getObject("PCB_ReceiveDate").toString()
		GlobalVariable.comPCB_TotalqtyClaim = dataCompare.getObject("PCB_TotalqtyClaim").toString()
		GlobalVariable.comPCB_TotalClaimAmount = dataCompare.getObject("PCB_TotalClaimAmount").toString()
		GlobalVariable.compcb_ReimbursementF = dataCompare.getObject("pcb_ReimbursementF").toString()
		GlobalVariable.comPCB_FDATE = dataCompare.getObject("PCB_FDATE").toString()
		GlobalVariable.comPCB_FUser = dataCompare.getObject("PCB_FUser").toString()
		GlobalVariable.comPCB_DiscountF = dataCompare.getObject("PCB_DiscountF").toString()
		GlobalVariable.comPCB_Status = dataCompare.getObject("PCB_Status").toString()
		GlobalVariable.comPCB_AUser = dataCompare.getObject("PCB_AUser").toString()
		GlobalVariable.comPCB_ADate = dataCompare.getObject("PCB_ADate").toString()
		GlobalVariable.comPCB_Stamp = dataCompare.getObject("PCB_Stamp").toString().replace(".0000", "")
		GlobalVariable.comPCB_AutoApprovalStatus = dataCompare.getObject("PCB_AutoApprovalStatus").toString()
		GlobalVariable.comPCB_AutoApprovalDate = dataCompare.getObject("PCB_AutoApprovalDate").toString()
	}

	@Keyword
	def getApproveClaimDB(def param){
		def dataCompare = (new healthKeyword.utilityDB()).queryApproveClaim(param)
		dataCompare.next()
		GlobalVariable.comProvider = dataCompare.getObject("TreatmentPlace").toString()
		GlobalVariable.comMemberName = dataCompare.getObject("MemberName").toString()
		GlobalVariable.comDoctor = dataCompare.getObject("Doctor").toString()
		GlobalVariable.comProductType = dataCompare.getObject("ProductType").toString()
		GlobalVariable.comClaimType = dataCompare.getObject("ClaimType").toString()
		GlobalVariable.comRoomOption = dataCompare.getObject("RoomOption").toString()
		GlobalVariable.comTreatmentRbClass = dataCompare.getObject("TreatmentRbClass").toString()
		GlobalVariable.comTreatmentRbRate = dataCompare.getObject("TreatmentRbRate").toString()
		GlobalVariable.comBilled = dataCompare.getObject("Billed").toString()
		GlobalVariable.comUnpaid = dataCompare.getObject("Unpaid").toString()
		GlobalVariable.comAccepted = dataCompare.getObject("Accepted").toString()
		GlobalVariable.comExcessTotal = dataCompare.getObject("Excess").toString()
		GlobalVariable.comExcessCompany = dataCompare.getObject("ExcessCoy").toString()
		GlobalVariable.comExcessEmployee = dataCompare.getObject("ExcessEmp").toString()
		GlobalVariable.comPayTo = dataCompare.getObject("PayTo").toString()
		GlobalVariable.comBankName = dataCompare.getObject("AccountBank").toString()
		GlobalVariable.comBankAccount = dataCompare.getObject("AccountName").toString()
		GlobalVariable.comAccountNo = dataCompare.getObject("AccountNo").toString()
		GlobalVariable.comBPJS = dataCompare.getObject("isCobBPJS").toString()
		GlobalVariable.comTrafficAccident = dataCompare.getObject("isTrafficAccident").toString()
		GlobalVariable.comSpecialTreatment = dataCompare.getObject("isSpecialTreatment").toString()
		GlobalVariable.comTDRD_DocumentStatus = dataCompare.getObject("TDRD_DocumentStatus").toString()
		GlobalVariable.comTDRD_Status = dataCompare.getObject("TDRD_Status").toString()
		GlobalVariable.comCH_Status = dataCompare.getObject("CH_Status").toString()
		GlobalVariable.comRUser = dataCompare.getObject("RUser").toString()
		GlobalVariable.comAutoApprovalStatus = dataCompare.getObject("AutoApprovalStatus").toString()
		GlobalVariable.comAutoApprovalUserID = dataCompare.getObject("AutoApprovalUserID").toString()
		GlobalVariable.comAppropriateRbClass = dataCompare.getObject("AppropriateRBClass").toString()
		GlobalVariable.comAppropriateRbRate = dataCompare.getObject("AppropriateRBRate").toString()
		GlobalVariable.comTreatmentStart = dataCompare.getObject("TreatmentStart").toString()
		GlobalVariable.comTreatmentFinish = dataCompare.getObject("TreatmentFinish").toString()
		GlobalVariable.comDoctorSpeciality = dataCompare.getObject("DoctorSpecialty").toString()
	}

	@Keyword
	def getInquiryHomeMember(def param){
		def data = (new healthKeyword.utilityDB()).queryInquiryHomeMember(param)
		data.next()

		GlobalVariable.comClientName = data.getObject('ClientName')
		GlobalVariable.comMemberNo = data.getObject('MemberNo')
		GlobalVariable.comMemberName = data.getObject('MemberName').trim()
		GlobalVariable.comEmpId = data.getObject('EmpID')
		GlobalVariable.comMembershipType = data.getString('MembershipType')
		GlobalVariable.comSex = data.getObject('Sex')
		GlobalVariable.comBirthDate = data.getObject('BirthDate').toString()
	}

	@Keyword
	def getInquiryMemberInfo(def param){
		def data = (new healthKeyword.utilityDB()).queryInquiryMemberInfo(param)

		data.next()
		GlobalVariable.comClientName = data.getObject('ClientName')
		GlobalVariable.comMemberNo = data.getObject('MemberNo')
		GlobalVariable.comMemberName = data.getObject('MemberName')
		GlobalVariable.comEmpId = data.getObject('EmpID')
		GlobalVariable.comMembershipType = data.getString('MembershipType')
		GlobalVariable.comSex = data.getObject('Sex')
		GlobalVariable.comBirthDate = data.getObject('DateOfBirth').toString()
		GlobalVariable.comAge = data.getObject('Age')
		GlobalVariable.comClassification = data.getObject('Classification')
		GlobalVariable.comMaritalStatus = data.getObject('MaritalStatus')
		GlobalVariable.comArea = data.getObject('Area')
		GlobalVariable.comPhone = data.getObject('Phone')
		GlobalVariable.comEmail = data.getObject('Email')
		GlobalVariable.comMembershipStatus = data.getObject('MembershipStatus')
		GlobalVariable.comBankName = data.getObject('BankName')
		GlobalVariable.comAccountNo = data.getObject('AccountNo')
		GlobalVariable.comCardNo = data.getObject('CardNo')
		GlobalVariable.comBankAccount = data.getObject('AccountName')
		GlobalVariable.comCardRequestDate = data.getObject('CardRequestDate')
		GlobalVariable.comCardPrintDate = data.getObject('CardPrintDate')
		GlobalVariable.comCardSendDate = data.getObject('CardSendDate')
		GlobalVariable.comCardReceiveDate = data.getObject('CardReceiveDate')
	}

	@Keyword
	def getCurrentDate(){
		def data = (new healthKeyword.utilityDB()).queryCurrentDate()
		data.next()
		GlobalVariable.CurrentDate = data.getString(1)
	}

	@Keyword
	def compareDocReg(def param1, def param2, def param3, def param4, def param5, def param6, def param7, def param8, def param9, def param10, def param11, def param12, def param13, def param14, def param15, def param16, def param17, def param18, def param19, def param20, def param21, def param22, def param23, def param24, def param25, def param26, def param27, def param28, def param29, def param30, def param31, def param32, def param33, def param34, def param35, def param36, def param37, def param38, def param39, def param40, def param41, def param42, def param43, def param44, def param45, def param46, def param47, def param48, def param49, def param50, def param51){
		WebUI.verifyEqual(GlobalVariable.comClientName,param1)
		//		WebUI.verifyEqual(GlobalVariable.comMemberName,param2)
		WebUI.verifyEqual(GlobalVariable.comMemberName.toString().toLowerCase(),param2.toString().toLowerCase())
		WebUI.verifyEqual(GlobalVariable.comBilled,param3)
		WebUI.verifyEqual(GlobalVariable.comSendTo.toString().toLowerCase(),param4.toString().toLowerCase())
		WebUI.verifyEqual(GlobalVariable.comProvider,param5)
		WebUI.verifyEqual(GlobalVariable.comBoxNo,param6)
		WebUI.verifyEqual(GlobalVariable.comIdDiagnosis,param7)
		WebUI.verifyEqual(GlobalVariable.comDeliveryNo,param8)
		WebUI.verifyEqual(GlobalVariable.comBankName,param9)
		WebUI.verifyEqual(GlobalVariable.comBankAccount,param10)
		WebUI.verifyEqual(GlobalVariable.comAccountNo,param11)
		WebUI.verifyEqual(GlobalVariable.comInvoiceNo,param12)
		WebUI.verifyEqual(GlobalVariable.comBilled2,param13)
		WebUI.verifyEqual(GlobalVariable.comDeliveryNo2,param14)
		WebUI.verifyEqual(GlobalVariable.comNumClaim,param15)
		WebUI.verifyEqual(GlobalVariable.comBilled3,param16)
		WebUI.verifyEqual(GlobalVariable.comInvoiceNo2,param17)
		WebUI.verifyEqual(GlobalVariable.comDiscount,param18)
		WebUI.verifyEqual(GlobalVariable.comAccountNo2,param19)
		WebUI.verifyEqual(GlobalVariable.comBankAccount2,param20)
		WebUI.verifyEqual(GlobalVariable.comBankName2,param21)
		WebUI.verifyEqual(GlobalVariable.comTDRD_Dispatcher,param22)
		WebUI.verifyEqual(GlobalVariable.comTDRD_DocumentStatus,param23)
		WebUI.verifyEqual(GlobalVariable.comTDRD_Status,param24)
		WebUI.verifyEqual(GlobalVariable.comTDRD_Sender,param25)
		WebUI.verifyEqual(GlobalVariable.comTDRD_CUser,param26)
		WebUI.verifyEqual(GlobalVariable.comTDRD_CDate,param27)
		WebUI.verifyEqual(GlobalVariable.comTDRD_PIC.toString().toLowerCase(),param28.toString().toLowerCase())
		WebUI.verifyEqual(GlobalVariable.comTDRD_CompleteF,param29)
		WebUI.verifyEqual(GlobalVariable.comTDRD_ProductType,param30)
		WebUI.verifyEqual(GlobalVariable.comTDRD_RoomOption,param31)
		WebUI.verifyEqual(GlobalVariable.comTDRD_isCobBPJS,param32)
		WebUI.verifyEqual(GlobalVariable.comTDRD_isCashPlanBPJS,param33)
		WebUI.verifyEqual(GlobalVariable.comTDRD_isDoubleInsured,param34)
		WebUI.verifyEqual(GlobalVariable.comTDRD_isNotEligible,param35)
		WebUI.verifyEqual(GlobalVariable.comTDRD_isSpecialTreatment,param36)
		WebUI.verifyEqual(GlobalVariable.comTDRD_isDeceased,param37)
		WebUI.verifyEqual(GlobalVariable.comTDRD_ClaimType,param38)
		WebUI.verifyEqual(GlobalVariable.comTDRD_PayToDisp,param39)
		WebUI.verifyEqual(GlobalVariable.comTDRD_isTrafficAccident,param40)
		WebUI.verifyEqual(GlobalVariable.comTDR_ClaimType,param41)
		WebUI.verifyEqual(GlobalVariable.comTDR_DocumentSource,param42)
		WebUI.verifyEqual(GlobalVariable.comTDR_CUser,param43)
		WebUI.verifyEqual(GlobalVariable.comTDR_CDate,param44)
		WebUI.verifyEqual(GlobalVariable.compcb_ReimbursementF,param45)
		WebUI.verifyEqual(GlobalVariable.comPCB_Status,param46)
		WebUI.verifyEqual(GlobalVariable.comPCB_AUser,param47)
		WebUI.verifyEqual(GlobalVariable.comPCB_ADate,param48)
		WebUI.verifyEqual(GlobalVariable.comPCB_Stamp,param49)
		WebUI.verifyEqual(GlobalVariable.comPCB_AutoApprovalStatus,param50)
		WebUI.verifyEqual(GlobalVariable.comPCB_AutoApprovalDate,param51)
		println("Comparing Data Document Registration Success")
	}

	@Keyword
	def compareAdditionalInfo(def param1, def param2){
		boolean result = false
		for (int i=0; i<param1.size(); i++) {
			result = false
			String compare1 = param1[i]
			for (int j=0; j<param2.size(); j++) {
				String compare2 = param2[j]
				if (compare1==compare2) {
					result = true
					break
				}
			}
			if (result == false) {
				//println("Comparing Data Additional Info Failed")
				//throw new Exception("Comparing Data Additional Info Create Failed")
				break
			}
		}
		if (param1.size() == 0 && param2.size() == 0){
			result = true
		}
		if (result == true) {
			println("Comparing Data Additional Info Success")
			//log.logInfo("Comparing Data Additional Info Success")
		}else{
			throw new Exception("Comparing Data Additional Info Failed")
		}
		WebUI.delay(1)
	}

	@Keyword
	def compareApprovalClaim(def param1,  def param2,  def param3,  def param4,  def param5,  def param6,  def param7,  def param8,  def param9,  def param10,  def param11,  def param12,  def param13,  def param14,  def param15,  def param16,  def param17,  def param18,  def param19,  def param20,  def param21,  def param22,  def param23,  def param24,  def param25,  def param26,  def param27,  def param28,  def param29,  def param30,  def param31){
		WebUI.verifyEqual(GlobalVariable.comProvider,param1)
		WebUI.verifyEqual(GlobalVariable.comMemberName,param2)
		WebUI.verifyEqual(GlobalVariable.comDoctor,param3)
		WebUI.verifyEqual(GlobalVariable.comProductType,param4)
		WebUI.verifyEqual(GlobalVariable.comClaimType,param5)
		WebUI.verifyEqual(GlobalVariable.comRoomOption,param6)
		WebUI.verifyEqual(GlobalVariable.comTreatmentRbClass,param7)
		WebUI.verifyEqual(GlobalVariable.comTreatmentRbRate,param8)
		WebUI.verifyEqual(GlobalVariable.comBilled,param9)
		WebUI.verifyEqual(GlobalVariable.comUnpaid,param10)
		WebUI.verifyEqual(GlobalVariable.comAccepted,param11)
		WebUI.verifyEqual(GlobalVariable.comExcessTotal,param12)
		WebUI.verifyEqual(GlobalVariable.comExcessCompany,param13)
		WebUI.verifyEqual(GlobalVariable.comExcessEmployee,param14)
		WebUI.verifyEqual(GlobalVariable.comPayTo,param15)
		WebUI.verifyEqual(GlobalVariable.comBankName,param16)
		WebUI.verifyEqual(GlobalVariable.comBankAccount,param17)
		WebUI.verifyEqual(GlobalVariable.comAccountNo,param18)
		WebUI.verifyEqual(GlobalVariable.comBPJS,param19)
		WebUI.verifyEqual(GlobalVariable.comTrafficAccident,param20)
		WebUI.verifyEqual(GlobalVariable.comSpecialTreatment,param21)
		WebUI.verifyEqual(GlobalVariable.comCH_Status,param22)
		WebUI.verifyEqual(GlobalVariable.comAutoApprovalStatus,param23)
		WebUI.verifyEqual(GlobalVariable.comAutoApprovalUserID,param24)
		WebUI.verifyEqual(GlobalVariable.comRUser,param25)
		WebUI.verifyEqual(GlobalVariable.comTDRD_DocumentStatus,param26)
		WebUI.verifyEqual(GlobalVariable.comTDRD_Status,param27)
		WebUI.verifyEqual(GlobalVariable.comAppropriateRbClass,param28)
		WebUI.verifyEqual(GlobalVariable.comAppropriateRbRate,param29)
		WebUI.verifyEqual(GlobalVariable.comTreatmentStart,param30)
		WebUI.verifyEqual(GlobalVariable.comTreatmentFinish,param31)
		println ('Compare Data Approval Claim Success')
	}

	// Custom keyword for opening file - provide file path as a parameter
	@Keyword
	def Open_File(String FilePath) {
		File file = new File(FilePath)

		//first check if Desktop is supported by Platform or not
		if(!Desktop.isDesktopSupported()){
			System.out.println("Desktop is not supported")
			return
		}
		Desktop desktop = Desktop.getDesktop()
		if(file.exists()) desktop.open(file)
		println "File Opened Sucessfully \r"
	}

	// Custom keyword for deleting file - provide file path as a parameter
	@Keyword
	def Delete_File(String FilePath) {
		File file = new File(FilePath)
		try
		{
			Files.deleteIfExists(Paths.get(FilePath))
		}
		catch(NoSuchFileException e)
		{
			System.out.println("No such File/Directory Exists")
		}
		catch(DirectoryNotEmptyException e)
		{
			System.out.println("Directory is not Empty.")
		}
		catch(IOException e)
		{
			System.out.println("Invalid Permissions.")
		}
		System.out.println("File Deletion is Successful.")
	}

	@Keyword
	// 10/Jan/2020 , 4
	def setDatePicker(String DateNow, String DatePickerDiv){
		String[] SplitDate = DateNow.split('/')
		if (SplitDate[0].startsWith('0')){
			SplitDate[0] = SplitDate[0].substring(1)
		}
		def Tanggal = SplitDate[0]
		def Bulan = SplitDate[1]
		def Tahun = Integer.parseInt(SplitDate[2])
		def Div = DatePickerDiv

		WebDriver driver = DriverFactory.getWebDriver()
		//object repository parent frame disesuaikan parent frame project anda
		WebUI.switchToFrame(findTestObject('Frame'), 5)

		//klik header Bulan
		driver.findElement(By.xpath("/html/body/div["+Div+"]/div/div[1]/table/thead/tr[1]/th[2]")).click()
		WebUI.delay(1)
		def headerTahun = Integer.parseInt(driver.findElement(By.xpath("/html/body/div["+Div+"]/div/div[2]/table/thead/tr/th[2]")).getText())
		if(Tahun<headerTahun){
			while(Tahun!=headerTahun){
				//cari tahun ke belakang
				driver.findElement(By.xpath("/html/body/div["+Div+"]/div/div[2]/table/thead/tr/th[1]")).click()
				headerTahun = Integer.parseInt(driver.findElement(By.xpath("/html/body/div["+Div+"]/div/div[2]/table/thead/tr/th[2]")).getText())
			}
		}else if(Tahun>headerTahun){
			while(Tahun!=headerTahun){
				//cari tahun ke depan
				driver.findElement(By.xpath("/html/body/div["+Div+"]/div/div[2]/table/thead/tr/th[3]")).click()
				headerTahun = Integer.parseInt(driver.findElement(By.xpath("/html/body/div["+Div+"]/div/div[2]/table/thead/tr/th[2]")).getText())
			}
		}
		WebUI.delay(1)
		//klik bulan yang tersedia
		driver.findElement(By.xpath("/html/body/div["+Div+"]/div/div[2]/table/tbody/tr//span[./text()='"+Bulan+"']")).click()
		WebUI.delay(1)
		//klik tanggal yang ada
		driver.findElement(By.xpath("/html/body/div["+Div+"]/div/div[1]/table/tbody//td[./text()='"+Tanggal+"' and not(@class='day old') and not(@class='day new')]")).click()
		WebUI.delay(1)
		WebUI.switchToDefaultContent()
	}
}

