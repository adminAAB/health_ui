<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_DocSource_getValue1</name>
   <tag></tag>
   <elementGuidId>93a75d91-c13c-400b-ae99-a57dac4b12af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;panelProvider&quot;]/a2is-datatable/div[2]/div/table/tbody/tr/td[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;panel${key}&quot;]/a2is-datatable/div[2]/div/table/tbody/tr/td[2][count(. | //*[@ref_element = 'Object Repository/frame']) = count(//*[@ref_element = 'Object Repository/frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;panel${key}&quot;]/a2is-datatable/div[2]/div/table/tbody/tr/td[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;panelProvider&quot;]/a2is-datatable/div[2]/div/table/tbody/tr/td[2]</value>
   </webElementXpaths>
</WebElementEntity>
