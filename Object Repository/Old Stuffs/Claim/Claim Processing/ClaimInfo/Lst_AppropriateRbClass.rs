<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_AppropriateRbClass</name>
   <tag></tag>
   <elementGuidId>78100075-7b80-4aaf-b1a2-b391b7df9713</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;ProviderRoomsTableA&quot;]/a2is-datatable/div[2]/div/table/tbody/tr//span[./text()=(translate('${treatmentKey2}','Kelas','KELAS')) or ./text()=(translate('${treatmentKey2}','KELAS','Kelas'))][count(. | //*[@ref_element = 'Object Repository/frame']) = count(//*[@ref_element = 'Object Repository/frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;ProviderRoomsTableA&quot;]/a2is-datatable/div[2]/div/table/tbody/tr//span[./text()=(translate('${treatmentKey2}','Kelas','KELAS')) or ./text()=(translate('${treatmentKey2}','KELAS','Kelas'))]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
</WebElementEntity>
