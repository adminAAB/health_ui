<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_TreatmentRbClass</name>
   <tag></tag>
   <elementGuidId>c7b2af59-ba75-49ef-be7d-cc286b02cf35</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;ProviderRoomsTableT&quot;]/a2is-datatable/div[2]/div/table/tbody/tr//span[./text()=(translate('${treatmentKey}','Kelas','KELAS')) or ./text()=(translate('${treatmentKey}','KELAS','Kelas'))][count(. | //*[@ref_element = 'Object Repository/frame']) = count(//*[@ref_element = 'Object Repository/frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;ProviderRoomsTableT&quot;]/a2is-datatable/div[2]/div/table/tbody/tr//span[./text()=(translate('${treatmentKey}','Kelas','KELAS')) or ./text()=(translate('${treatmentKey}','KELAS','Kelas'))]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
</WebElementEntity>
