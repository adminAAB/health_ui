<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_TabClaimDetail</name>
   <tag></tag>
   <elementGuidId>c6d7e0f3-22f1-42ff-9dc5-4d0002fe5236</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a2is-tabs/ul/li//a[./text()='Claim Details']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//a2is-tabs/ul/li//a[./text()='Claim Details']</value>
   </webElementXpaths>
</WebElementEntity>
