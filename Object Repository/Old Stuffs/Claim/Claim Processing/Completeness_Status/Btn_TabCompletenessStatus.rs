<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_TabCompletenessStatus</name>
   <tag></tag>
   <elementGuidId>c74a9128-396e-456a-a076-97516d89eebd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a2is-tabs/ul/li//a[./text()='Completeness Status']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//a2is-tabs/ul/li//a[./text()='Completeness Status']</value>
   </webElementXpaths>
</WebElementEntity>
