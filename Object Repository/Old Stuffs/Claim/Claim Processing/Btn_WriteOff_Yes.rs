<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_WriteOff_Yes</name>
   <tag></tag>
   <elementGuidId>223831c8-c2cb-4866-8f8c-68fa7c87bc07</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PopUpConfirmationProcess-0&quot;]/div[1]/div[2]/div/div/a2is-buttons/div/div/button[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/frame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;PopUpConfirmationProcess-0&quot;]/div[1]/div[2]/div/div/a2is-buttons/div/div/button[2]</value>
   </webElementXpaths>
</WebElementEntity>
