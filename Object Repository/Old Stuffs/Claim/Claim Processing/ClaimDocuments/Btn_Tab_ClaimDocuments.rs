<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Tab_ClaimDocuments</name>
   <tag></tag>
   <elementGuidId>7d53489d-8cdd-4327-bfa8-616bf56f2814</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a2is-tabs/ul/li//a[./text()='Claim Documents']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//a2is-tabs/ul/li//a[./text()='Claim Documents']</value>
   </webElementXpaths>
</WebElementEntity>
