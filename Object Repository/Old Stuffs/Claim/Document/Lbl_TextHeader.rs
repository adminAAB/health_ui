<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lbl_TextHeader</name>
   <tag></tag>
   <elementGuidId>535389eb-1af8-414a-89d3-ebc89d5e0918</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;D_a2is_ParentContainer&quot;]/a2is-popup/div[contains(@style, 'display: block')]/div[2]/div/div/h3</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;D_a2is_ParentContainer&quot;]/a2is-popup/div[contains(@style, 'display: block')]/div[2]/div/div/h3</value>
   </webElementXpaths>
</WebElementEntity>
