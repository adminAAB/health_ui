<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Chk_Doc_Key</name>
   <tag></tag>
   <elementGuidId>8d755dea-e795-4cad-955b-fadee89d96f3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;sectionContainer-3&quot;]/div[1]/div/a2is-datatable/div[2]/div/table/tbody//*[contains(text(),&quot;${docKey}&quot;)]/ancestor::tr/td[4]/span/input[count(. | //*[@ref_element = 'Object Repository/frame']) = count(//*[@ref_element = 'Object Repository/frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;sectionContainer-3&quot;]/div[1]/div/a2is-datatable/div[2]/div/table/tbody//*[contains(text(),&quot;${docKey}&quot;)]/ancestor::tr/td[4]/span/input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
</WebElementEntity>
