<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_Summary_ClientName</name>
   <tag></tag>
   <elementGuidId>e3e0c508-7b89-47d1-bb48-0c542bec550e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PopUpConfirmation-0&quot;]/div[1]/div[1]/a2is-textbox[5]/div/div/div/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/frame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;PopUpConfirmation-0&quot;]/div[1]/div[1]/a2is-textbox[5]/div/div/div/input</value>
   </webElementXpaths>
</WebElementEntity>
