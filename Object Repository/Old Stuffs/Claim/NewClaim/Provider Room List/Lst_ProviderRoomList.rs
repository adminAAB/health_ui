<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_ProviderRoomList</name>
   <tag></tag>
   <elementGuidId>e8bad9a7-0096-4375-8d60-dae5a6b2e95a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;ProviderRoomsTableT&quot;]/a2is-datatable/div[2]/div/table/tbody/tr//span[./text()=(translate('${key}','Kelas','KELAS')) or ./text()=(translate('${key}','KELAS','Kelas'))][count(. | //*[@ref_element = 'Object Repository/frame']) = count(//*[@ref_element = 'Object Repository/frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;ProviderRoomsTableT&quot;]/a2is-datatable/div[2]/div/table/tbody/tr//span[./text()=(translate('${key}','Kelas','KELAS')) or ./text()=(translate('${key}','KELAS','Kelas'))]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
</WebElementEntity>
