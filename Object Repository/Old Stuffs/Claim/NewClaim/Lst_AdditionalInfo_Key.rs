<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_AdditionalInfo_Key</name>
   <tag></tag>
   <elementGuidId>6686fbbe-a3e3-4d64-92b2-f801d82ae559</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;sectionContainer-2&quot;]/div[1]/div[6]/div/a2is-datatable/div[2]/div/table//*[contains(text(),&quot;${infoKey}&quot;)][count(. | //*[@ref_element = 'Object Repository/frame']) = count(//*[@ref_element = 'Object Repository/frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;sectionContainer-2&quot;]/div[1]/div[6]/div/a2is-datatable/div[2]/div/table//*[contains(text(),&quot;${infoKey}&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
</WebElementEntity>
