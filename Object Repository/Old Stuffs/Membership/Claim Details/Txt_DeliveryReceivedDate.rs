<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_DeliveryReceivedDate</name>
   <tag></tag>
   <elementGuidId>162af509-465b-4230-aecb-dd75d1af0048</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;P2-0&quot;]/div[1]/div/div[5]/div[1]/div[2]/div/a2is-datepicker-wide-nc/div[2]/div[1]/div/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;P2-0&quot;]/div[1]/div/div[5]/div[1]/div[2]/div/a2is-datepicker-wide-nc/div[2]/div[1]/div/input</value>
   </webElementXpaths>
</WebElementEntity>
