<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_AdmedikaAverageCode</name>
   <tag></tag>
   <elementGuidId>74f988f4-23e0-444a-9af5-102ba0319753</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;puaddAdmedikaCoverageCode&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;puaddAdmedikaCoverageCode&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/input</value>
   </webElementXpaths>
</WebElementEntity>
