<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_IndividualAL</name>
   <tag></tag>
   <elementGuidId>4abd3af5-adba-4a3d-84b6-203f7a7f9a24</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;TextboxIndividualAnnualLimit&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;TextboxIndividualAnnualLimit&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/input</value>
   </webElementXpaths>
</WebElementEntity>
