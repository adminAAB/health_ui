<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_OccurenceFrequency</name>
   <tag></tag>
   <elementGuidId>32b4eff0-29f1-4188-adcc-0e57954dc74d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;OccurenceFrequency&quot;]/a2is-textbox-wide/div[2]/div[1]/div/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;OccurenceFrequency&quot;]/a2is-textbox-wide/div[2]/div[1]/div/input</value>
   </webElementXpaths>
</WebElementEntity>
