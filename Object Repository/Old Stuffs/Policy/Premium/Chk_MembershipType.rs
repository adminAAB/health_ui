<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Chk_MembershipType</name>
   <tag></tag>
   <elementGuidId>1599ca43-9d4e-4680-aea7-a69d26bf57a0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;TabPremiumCurrentEndorsement&quot;]/a2is-multi-check/div[2]/div[1]/div/div[3]/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;TabPremiumCurrentEndorsement&quot;]/a2is-multi-check/div[2]/div[1]/div/div[3]/input</value>
   </webElementXpaths>
</WebElementEntity>
