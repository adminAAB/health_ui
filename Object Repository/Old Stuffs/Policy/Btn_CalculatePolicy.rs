<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_CalculatePolicy</name>
   <tag></tag>
   <elementGuidId>ee5d1cdd-1be2-4307-9590-233fa823e04f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;MainAddEditBtnPage&quot;]/a2is-buttons/div/div/button[./text()='Calculate Policy']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;MainAddEditBtnPage&quot;]/a2is-buttons/div/div/button[./text()='Calculate Policy']</value>
   </webElementXpaths>
</WebElementEntity>
