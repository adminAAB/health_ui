<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_EndorsePolicy</name>
   <tag></tag>
   <elementGuidId>5af46651-84ff-4882-a11d-b9a41a30dd63</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;MainAddEditBtnPage&quot;]/a2is-buttons/div/div/button[./text()='Endorse Policy']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;MainAddEditBtnPage&quot;]/a2is-buttons/div/div/button[./text()='Endorse Policy']</value>
   </webElementXpaths>
</WebElementEntity>
