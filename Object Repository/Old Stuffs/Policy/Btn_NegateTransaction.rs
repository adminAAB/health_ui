<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_NegateTransaction</name>
   <tag></tag>
   <elementGuidId>75a31b91-ef06-4e85-98ac-be6faac49ca4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;MainAddEditBtnPage&quot;]/a2is-buttons/div/div/button[./text()='Negate Transaction']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;MainAddEditBtnPage&quot;]/a2is-buttons/div/div/button[./text()='Negate Transaction']</value>
   </webElementXpaths>
</WebElementEntity>
