<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_MaritalStatus</name>
   <tag></tag>
   <elementGuidId>8f8acfd4-6432-4b3c-a26d-2b3daf5417be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;TabMemberPUMemberMaritalStatus&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[./text()='${key}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;TabMemberPUMemberMaritalStatus&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[./text()='${key}']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
</WebElementEntity>
