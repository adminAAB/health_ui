<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_ComboSearchTop1</name>
   <tag></tag>
   <elementGuidId>8e188188-7b1d-42cf-bc47-69e26fbeb02c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[starts-with(@class,'select2-')]//*[contains(text(),'${key}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[starts-with(@class,'select2-')]//*[contains(text(),'${key}')]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
</WebElementEntity>
