<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_SearchRoomCategory</name>
   <tag></tag>
   <elementGuidId>ddd3abe8-14ff-441e-89f6-450b5657a906</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;puAddEditRoomsRoomCategory&quot;]/a2is-textbox-wide/div[2]/div[1]/div/span/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;puAddEditRoomsRoomCategory&quot;]/a2is-textbox-wide/div[2]/div[1]/div/span/button</value>
   </webElementXpaths>
</WebElementEntity>
