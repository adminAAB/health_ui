<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Tab_ChildInpatient</name>
   <tag></tag>
   <elementGuidId>2d791bcf-3a25-404c-bc36-77e39a8878fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[./text()='Child Inpatient']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//a[./text()='Child Inpatient']</value>
   </webElementXpaths>
</WebElementEntity>
