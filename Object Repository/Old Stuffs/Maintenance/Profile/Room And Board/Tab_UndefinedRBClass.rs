<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Tab_UndefinedRBClass</name>
   <tag></tag>
   <elementGuidId>5d13d98c-3c99-45c4-a5be-501209965361</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[.//text()='Undefined R&amp;B Class']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//a[.//text()='Undefined R&amp;B Class']</value>
   </webElementXpaths>
</WebElementEntity>
