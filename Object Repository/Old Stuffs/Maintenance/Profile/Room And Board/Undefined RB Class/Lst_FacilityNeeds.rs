<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_FacilityNeeds</name>
   <tag></tag>
   <elementGuidId>2c1eec6a-2f4e-4466-b656-e9681996d744</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;ComboUndefinedFacility&quot;]/a2is-combo-wide/div[2]/div[1]/div/div/button[./text()='${key}'][count(. | //*[@ref_element = 'Object Repository/frame']) = count(//*[@ref_element = 'Object Repository/frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;ComboUndefinedFacility&quot;]/a2is-combo-wide/div[2]/div[1]/div/div/button[./text()='${key}']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
</WebElementEntity>
