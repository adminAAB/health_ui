<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_CityDistrict</name>
   <tag></tag>
   <elementGuidId>c89ddcda-a437-4a7a-9444-c6cde2f32850</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>(//*[@id=&quot;UndefinedRefProviderCityTable&quot;]/a2is-datatable/div[2]/div/table/tbody//span[contains(text(),'${key}')])[1][count(. | //*[@ref_element = 'Object Repository/frame']) = count(//*[@ref_element = 'Object Repository/frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//*[@id=&quot;UndefinedRefProviderCityTable&quot;]/a2is-datatable/div[2]/div/table/tbody//span[contains(text(),'${key}')])[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
</WebElementEntity>
