<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Chk_Default</name>
   <tag></tag>
   <elementGuidId>a85d8d30-743c-4251-9107-9e67f63d6c72</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[starts-with(@id,   'radioID')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[starts-with(@id,   'radioID')]</value>
   </webElementXpaths>
</WebElementEntity>
