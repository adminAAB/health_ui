<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Tab_GeneralInfo</name>
   <tag></tag>
   <elementGuidId>c3780c70-21e2-485d-a005-ebe97652f412</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[./text()=&quot;General Info&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//a[./text()=&quot;General Info&quot;]</value>
   </webElementXpaths>
</WebElementEntity>
