<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Default</name>
   <tag></tag>
   <elementGuidId>8d2df136-ffd6-4a3e-8581-2ee226079b85</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[starts-with(@id,   'radioAccountID')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[starts-with(@id,   'radioAccountID')]</value>
   </webElementXpaths>
</WebElementEntity>
