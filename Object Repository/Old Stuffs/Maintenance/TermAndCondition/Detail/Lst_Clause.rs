<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_Clause</name>
   <tag></tag>
   <elementGuidId>464ce1c4-b753-43d7-8f9e-e84377fbc8d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[starts-with(@class,'select2-')]//*[contains(text(),'${key}')][count(. | //*[@ref_element = 'Object Repository/frame']) = count(//*[@ref_element = 'Object Repository/frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[starts-with(@class,'select2-')]//*[contains(text(),'${key}')]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
</WebElementEntity>
