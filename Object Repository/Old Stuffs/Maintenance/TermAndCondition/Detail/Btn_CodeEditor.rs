<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_CodeEditor</name>
   <tag></tag>
   <elementGuidId>f85cab4f-1ea8-41e4-835f-6af92440235a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;TCClauseDiscription&quot;]/a2is-richeditor-nc//button[contains(@class,'note-btn btn btn-default btn-sm btn-codeview')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;TCClauseDiscription&quot;]/a2is-richeditor-nc//button[contains(@class,'note-btn btn btn-default btn-sm btn-codeview')]</value>
   </webElementXpaths>
</WebElementEntity>
