<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_AddDiagnosis</name>
   <tag></tag>
   <elementGuidId>a37d2c98-4735-44a6-bbb9-f40bc9abc5f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;P1-0&quot;]/div[1]/div/div/div[4]/a2is-datatable/div[2]/div/div/button[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/frame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;P1-0&quot;]/div[1]/div/div/div[4]/a2is-datatable/div[2]/div/div/button[1]</value>
   </webElementXpaths>
</WebElementEntity>
