<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_Specialty</name>
   <tag></tag>
   <elementGuidId>9b1f88b0-4739-4a22-acfc-7fa1e8f55551</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;lookUpSpecialtySpecialty&quot;]/a2is-textbox-wide-ts/div[2]/div[1]/div/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;lookUpSpecialtySpecialty&quot;]/a2is-textbox-wide-ts/div[2]/div[1]/div/input</value>
   </webElementXpaths>
</WebElementEntity>
