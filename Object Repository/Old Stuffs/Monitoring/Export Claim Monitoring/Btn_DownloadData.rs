<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_DownloadData</name>
   <tag></tag>
   <elementGuidId>9b153580-1d65-425d-bc47-cd72ee6c5f11</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*/button[./text()='Download Data']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*/button[./text()='Download Data']</value>
   </webElementXpaths>
</WebElementEntity>
