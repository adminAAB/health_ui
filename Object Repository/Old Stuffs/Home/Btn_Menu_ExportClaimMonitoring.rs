<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Menu_ExportClaimMonitoring</name>
   <tag></tag>
   <elementGuidId>6e9e153d-02ff-4673-837f-3e62a1c9db40</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[text()='Export Claim Monitoring']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[text()='Export Claim Monitoring']</value>
   </webElementXpaths>
</WebElementEntity>
