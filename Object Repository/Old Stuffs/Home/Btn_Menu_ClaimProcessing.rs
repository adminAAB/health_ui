<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Menu_ClaimProcessing</name>
   <tag></tag>
   <elementGuidId>82dcb857-3f27-4baf-a889-7e01c2f88477</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[.//text()=&quot;Claim Processing&quot;]/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[.//text()=&quot;Claim Processing&quot;]/button</value>
   </webElementXpaths>
</WebElementEntity>
