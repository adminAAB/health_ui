<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Menu_DocRegistration</name>
   <tag></tag>
   <elementGuidId>fd2605a4-ed02-44eb-a1d3-7e0b67d09181</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[.//text()=&quot;Document Registration&quot;]/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[.//text()=&quot;Document Registration&quot;]/button</value>
   </webElementXpaths>
</WebElementEntity>
