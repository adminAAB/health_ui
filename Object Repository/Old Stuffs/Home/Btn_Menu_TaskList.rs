<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Menu_TaskList</name>
   <tag></tag>
   <elementGuidId>8363bd44-4bd0-437f-8796-eae3af4b73e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[.//text()=&quot;Task List&quot;]/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[.//text()=&quot;Task List&quot;]/button</value>
   </webElementXpaths>
</WebElementEntity>
