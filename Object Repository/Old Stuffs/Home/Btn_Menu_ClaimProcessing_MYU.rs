<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Menu_ClaimProcessing_MYU</name>
   <tag></tag>
   <elementGuidId>e3b0562b-c4b1-4470-8350-86498c247b25</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[.//text()=&quot;Claim Processing&quot;]/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[.//text()=&quot;Claim Processing&quot;]/button</value>
   </webElementXpaths>
</WebElementEntity>
