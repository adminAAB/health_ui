<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Menu_MemberAccountCard</name>
   <tag></tag>
   <elementGuidId>06b6e282-3b72-4fe1-9162-eb366678f315</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[./text()=&quot;Member's Account &amp; Card&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Old Stuffs/frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[./text()=&quot;Member's Account &amp; Card&quot;]</value>
   </webElementXpaths>
</WebElementEntity>
