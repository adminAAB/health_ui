<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_MedicalTreatment</name>
   <tag></tag>
   <elementGuidId>1886ecdc-dc63-44a3-a8da-974591e9d9bf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[starts-with(@class,'select2-')]//*[contains(text(),'${MedicalTreatment}')][count(. | //*[@ref_element = 'Object Repository/Frame']) = count(//*[@ref_element = 'Object Repository/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[starts-with(@class,'select2-')]//*[contains(text(),'${MedicalTreatment}')]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
