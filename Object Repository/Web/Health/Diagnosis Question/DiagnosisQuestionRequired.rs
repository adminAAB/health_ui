<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DiagnosisQuestionRequired</name>
   <tag></tag>
   <elementGuidId>6003cb50-ec84-4bf3-939c-a48a8482126e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[text()=&quot;Add Diagnosis Question First&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[text()=&quot;Add Diagnosis Question First&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
