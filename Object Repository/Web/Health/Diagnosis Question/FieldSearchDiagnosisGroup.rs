<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FieldSearchDiagnosisGroup</name>
   <tag></tag>
   <elementGuidId>e9fe03c7-d81b-4056-96a0-892a386ce619</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//input[@class=&quot;form-control agreyborder&quot;])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//input[@class=&quot;form-control agreyborder&quot;])[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
