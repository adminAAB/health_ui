<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FieldQuestionUnknown</name>
   <tag></tag>
   <elementGuidId>f0cf9667-87e8-4aed-a297-37730d2fb668</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//select[@id=&quot;QuestionUnknown0&quot;][count(. | //*[@ref_element = 'Object Repository/Frame']) = count(//*[@ref_element = 'Object Repository/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@id=&quot;QuestionUnknown0&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
