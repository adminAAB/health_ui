<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_Specialty</name>
   <tag></tag>
   <elementGuidId>3e0d154f-1053-43a7-8f70-162714541e23</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;lookUpSpecialtySpecialty&quot;]/a2is-textbox-wide-ts/div[2]/div[1]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;lookUpSpecialtySpecialty&quot;]/a2is-textbox-wide-ts/div[2]/div[1]/div/input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
