<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_RoomCategory</name>
   <tag></tag>
   <elementGuidId>5473e117-f176-45bb-95bf-404d2cb1c01a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;puCategory&quot;]/a2is-textbox-wide-ts/div[2]/div[1]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;puCategory&quot;]/a2is-textbox-wide-ts/div[2]/div[1]/div/input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
