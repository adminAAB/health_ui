<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PopUp_WaitingRestore</name>
   <tag></tag>
   <elementGuidId>5cadad7a-fee5-4671-af99-5ad2b6ceb815</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[text()='Take it easy, please give some time and we’ll make sure it is worth waiting for.'][count(. | //*[@ref_element = 'Object Repository/Frame']) = count(//*[@ref_element = 'Object Repository/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[text()='Take it easy, please give some time and we’ll make sure it is worth waiting for.']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
