<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ButtonYesCloseApplication</name>
   <tag></tag>
   <elementGuidId>e127750d-424e-4a02-9d20-33f0dfe53d0a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[text()=&quot;Yes, Close Application&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[text()=&quot;Yes, Close Application&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
