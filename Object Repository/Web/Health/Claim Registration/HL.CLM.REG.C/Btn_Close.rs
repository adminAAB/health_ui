<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Close</name>
   <tag></tag>
   <elementGuidId>0ae3ef5a-c3ea-4f5a-af63-bfc253111f7f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//*[@id=&quot;ButtonHome&quot;]/a2is-buttons/div/div/button[1])[2][count(. | //*[@ref_element = 'Object Repository/Frame']) = count(//*[@ref_element = 'Object Repository/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//*[@id=&quot;ButtonHome&quot;]/a2is-buttons/div/div/button[1])[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
