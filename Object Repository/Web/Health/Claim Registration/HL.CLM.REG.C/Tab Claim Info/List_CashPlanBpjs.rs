<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>List_CashPlanBpjs</name>
   <tag></tag>
   <elementGuidId>36e81098-221b-497d-bc8c-2b2aefd3ba12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;CCashPlanBPJS&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[text()='${isCashPlanBpjs}'][count(. | //*[@ref_element = 'Object Repository/Frame']) = count(//*[@ref_element = 'Object Repository/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;CCashPlanBPJS&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[text()='${isCashPlanBpjs}']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
