<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_MemberNo</name>
   <tag></tag>
   <elementGuidId>3338f4a0-3195-4b55-9199-4868d322826e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//*[@id=&quot;PUMMemberNo&quot;]/a2is-textbox-ts/div/div/div/input)[2][count(. | //*[@ref_element = 'Object Repository/Frame']) = count(//*[@ref_element = 'Object Repository/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//*[@id=&quot;PUMMemberNo&quot;]/a2is-textbox-ts/div/div/div/input)[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
