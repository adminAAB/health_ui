<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_MemberNo</name>
   <tag></tag>
   <elementGuidId>ce5c4ea7-dd58-4951-ae5c-d64fdf95d2c1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//*[@id=&quot;PUMMemberNo&quot;]/a2is-textbox-ts/div/div/div/input)[2][count(. | //*[@ref_element = 'Object Repository/Frame']) = count(//*[@ref_element = 'Object Repository/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//*[@id=&quot;PUMMemberNo&quot;]/a2is-textbox-ts/div/div/div/input)[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
