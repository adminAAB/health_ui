<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PopupTextarea</name>
   <tag></tag>
   <elementGuidId>b2fe1ddd-6447-42f5-ace7-862f5fcd6ce5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//textarea[@class=&quot;form-control agreyborder&quot;])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//textarea[@class=&quot;form-control agreyborder&quot;])[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
