<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Table_SelectClaim</name>
   <tag></tag>
   <elementGuidId>6fae9e1e-5114-42f3-98cd-9bf571bef5b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//h3[contains(text(),&quot;Search Claim&quot;)]/ancestor::div[2]//div[@class=&quot;btn-group&quot;]/button)[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//h3[contains(text(),&quot;Search Claim&quot;)]/ancestor::div[2]//div[@class=&quot;btn-group&quot;]/button)[4]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
