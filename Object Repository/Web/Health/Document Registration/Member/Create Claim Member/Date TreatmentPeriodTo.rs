<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Date TreatmentPeriodTo</name>
   <tag></tag>
   <elementGuidId>45aafde1-bf72-4401-8cda-3bbef07b6dbb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;sectionContainer-2&quot;]/div[1]/div[3]/div/div[2]/div/a2is-datepicker-nc[2]/div/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;sectionContainer-2&quot;]/div[1]/div[3]/div/div[2]/div/a2is-datepicker-nc[2]/div/div/div/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
