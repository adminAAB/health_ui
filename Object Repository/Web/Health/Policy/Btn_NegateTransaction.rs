<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_NegateTransaction</name>
   <tag></tag>
   <elementGuidId>f5d2cf89-47c6-4611-9c3d-09b6675c61e1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;MainAddEditBtnPage&quot;]/a2is-buttons/div/div/button[./text()='Negate Transaction']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;MainAddEditBtnPage&quot;]/a2is-buttons/div/div/button[./text()='Negate Transaction']</value>
   </webElementXpaths>
</WebElementEntity>
