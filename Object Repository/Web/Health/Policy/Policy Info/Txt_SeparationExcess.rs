<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_SeparationExcess</name>
   <tag></tag>
   <elementGuidId>54067f00-41a8-4b45-baa1-92e4d60654bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;AddEditSeparationOfExcess&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;AddEditSeparationOfExcess&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/button</value>
   </webElementXpaths>
</WebElementEntity>
