<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_SearchClient</name>
   <tag></tag>
   <elementGuidId>5ea1f18b-53e6-4fc6-899c-3e43bb5a0feb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;PUPolicyInfoClientBtn&quot;]/a2is-buttons/div/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;PUPolicyInfoClientBtn&quot;]/a2is-buttons/div/div/button</value>
   </webElementXpaths>
</WebElementEntity>
