<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_OrderType</name>
   <tag></tag>
   <elementGuidId>efb19a58-1372-4985-a0c2-f51829471129</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/frame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;AddEditOrderType&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;AddEditOrderType&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/button</value>
   </webElementXpaths>
</WebElementEntity>
