<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_MaxBackDatedType</name>
   <tag></tag>
   <elementGuidId>b69ed25d-efa2-4bb6-a174-adc1af42bfbc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;AddEditMaxBackdatedType&quot;]/a2is-combo-wide-nc/div[2]/div[1]/div/div//button[./text()='${key}'][count(. | //*[@ref_element = 'Object Repository/Old Stuffs/frame']) = count(//*[@ref_element = 'Object Repository/Old Stuffs/frame'])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;AddEditMaxBackdatedType&quot;]/a2is-combo-wide-nc/div[2]/div[1]/div/div//button[./text()='${key}']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
