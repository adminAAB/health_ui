<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_CustomOverallLimit</name>
   <tag></tag>
   <elementGuidId>29dc96a4-6794-4573-bb89-98d1eb1c6688</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;ComboCustomOverallLimit&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[./text()='${key}'][count(. | //*[@ref_element = 'Object Repository/Old Stuffs/frame']) = count(//*[@ref_element = 'Object Repository/Old Stuffs/frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;ComboCustomOverallLimit&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[./text()='${key}']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
