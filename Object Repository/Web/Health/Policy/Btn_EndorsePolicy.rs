<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_EndorsePolicy</name>
   <tag></tag>
   <elementGuidId>a62be6a9-422b-4c8a-bb26-4e24e83a8cf6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;MainAddEditBtnPage&quot;]/a2is-buttons/div/div/button[./text()='Endorse Policy']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;MainAddEditBtnPage&quot;]/a2is-buttons/div/div/button[./text()='Endorse Policy']</value>
   </webElementXpaths>
</WebElementEntity>
