<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_TopUpWithdraw</name>
   <tag></tag>
   <elementGuidId>5069a65c-67ed-4bee-a058-dde3e082ac0f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;TabCalculatePolicyTextbox&quot;]/div[4]/a2is-textbox-wide-nc/div[2]/div[1]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;TabCalculatePolicyTextbox&quot;]/div[4]/a2is-textbox-wide-nc/div[2]/div[1]/div/input</value>
   </webElementXpaths>
</WebElementEntity>
