<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_DoB</name>
   <tag></tag>
   <elementGuidId>5b69b3db-c89e-4d44-bd52-aa841ad604cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;TabMemberPUMemberDateOfBirth&quot;]/a2is-datepicker-wide-dc/div[2]/div[1]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;TabMemberPUMemberDateOfBirth&quot;]/a2is-datepicker-wide-dc/div[2]/div[1]/div/input</value>
   </webElementXpaths>
</WebElementEntity>
