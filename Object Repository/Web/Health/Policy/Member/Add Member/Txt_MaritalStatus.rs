<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_MaritalStatus</name>
   <tag></tag>
   <elementGuidId>e6daa5d5-0677-4e88-822e-d783954d33e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;TabMemberPUMemberMaritalStatus&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;TabMemberPUMemberMaritalStatus&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/button</value>
   </webElementXpaths>
</WebElementEntity>
