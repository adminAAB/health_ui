<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_Classification</name>
   <tag></tag>
   <elementGuidId>71ac10d2-83c7-46cb-8d11-af582df26e2b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;TabMemberPUMemberClassification&quot;]/a2is-combo-searchable-wide-dc/div[2]/div[1]/div/span/span[1]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;TabMemberPUMemberClassification&quot;]/a2is-combo-searchable-wide-dc/div[2]/div[1]/div/span/span[1]/span</value>
   </webElementXpaths>
</WebElementEntity>
