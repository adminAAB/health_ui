<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_ImportantPerson</name>
   <tag></tag>
   <elementGuidId>7df7d80b-fbd0-4597-a39d-b8d3e188dac3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;TabMemberPUIsImportantPerson&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[./text()=&quot;${key}&quot;][count(. | //*[@ref_element = 'Object Repository/Old Stuffs/frame']) = count(//*[@ref_element = 'Object Repository/Old Stuffs/frame'])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;TabMemberPUIsImportantPerson&quot;]/a2is-combo-wide-dc/div[2]/div[1]/div/div/button[./text()=&quot;${key}&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
