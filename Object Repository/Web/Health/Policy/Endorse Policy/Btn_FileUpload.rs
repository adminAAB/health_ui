<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_FileUpload</name>
   <tag></tag>
   <elementGuidId>e03c5b21-21ce-4a05-a5e8-e6986c3edbca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;EndorseReferenceDocument&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/span/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;EndorseReferenceDocument&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/span/button</value>
   </webElementXpaths>
</WebElementEntity>
