<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ButtonTreatmentSettings</name>
   <tag></tag>
   <elementGuidId>b3052ca7-86e2-4fe2-96c7-8cbb01fa3f63</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[text()=&quot;Treatment Settings&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[text()=&quot;Treatment Settings&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
