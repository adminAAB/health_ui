<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>IconEditProfile</name>
   <tag></tag>
   <elementGuidId>1d6c687c-a118-46ac-850f-5ea665351a6e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//span[@class=&quot;glyphicon glyphicon-pencil&quot;])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//span[@class=&quot;glyphicon glyphicon-pencil&quot;])[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
