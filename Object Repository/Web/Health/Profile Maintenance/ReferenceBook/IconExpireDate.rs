<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>IconExpireDate</name>
   <tag></tag>
   <elementGuidId>c138eb79-22b8-44e6-8fc5-6f07ed86083d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//i[@class=&quot;glyphicon glyphicon-calendar&quot;])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//i[@class=&quot;glyphicon glyphicon-calendar&quot;])[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
