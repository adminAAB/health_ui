<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>IconAssignmentFinish</name>
   <tag></tag>
   <elementGuidId>ec7214fd-42a2-442b-916e-783a43ff2942</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//i[@class=&quot;glyphicon glyphicon-calendar&quot;])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//i[@class=&quot;glyphicon glyphicon-calendar&quot;])[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
