<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>IconSave</name>
   <tag></tag>
   <elementGuidId>7cbb5fc8-e673-4d2d-8edb-e1e11a9466d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class=&quot;glyphicon glyphicon-glyphicon glyphicon-floppy-disk&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[@class=&quot;glyphicon glyphicon-glyphicon glyphicon-floppy-disk&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
