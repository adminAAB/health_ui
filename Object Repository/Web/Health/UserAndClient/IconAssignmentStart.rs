<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>IconAssignmentStart</name>
   <tag></tag>
   <elementGuidId>064ab1bd-b9ee-4917-bbcf-7c16a0501799</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//i[@class=&quot;glyphicon glyphicon-calendar&quot;])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//i[@class=&quot;glyphicon glyphicon-calendar&quot;])[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
