<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_BenefitId</name>
   <tag></tag>
   <elementGuidId>feb71010-a0f6-470c-ab53-ee9e766ad80c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;lookUpBenefit&quot;]/a2is-datatable/div[2]/div/table/tbody//*[.//text()='${key}']/span[count(. | //*[@ref_element = 'Object Repository/Old Stuffs/frame']) = count(//*[@ref_element = 'Object Repository/Old Stuffs/frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;lookUpBenefit&quot;]/a2is-datatable/div[2]/div/table/tbody//*[.//text()='${key}']/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
