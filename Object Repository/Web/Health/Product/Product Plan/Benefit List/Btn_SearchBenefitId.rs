<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_SearchBenefitId</name>
   <tag></tag>
   <elementGuidId>e78df7e9-bd56-4efb-bcf3-82c05f91d0d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//*[@id=&quot;buttonSearchProductDefinition&quot;]/a2is-buttons/div/div/button)[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>(//*[@id=&quot;buttonSearchProductDefinition&quot;]/a2is-buttons/div/div/button)[2]</value>
   </webElementXpaths>
</WebElementEntity>
