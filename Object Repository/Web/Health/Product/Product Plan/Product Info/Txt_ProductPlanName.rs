<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_ProductPlanName</name>
   <tag></tag>
   <elementGuidId>0fbe63f7-08e5-4b16-bcfa-7013c7420dbc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;TextboxProductPlanName&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[@id=&quot;TextboxProductPlanName&quot;]/a2is-textbox-wide-dc/div[2]/div[1]/div/input</value>
   </webElementXpaths>
</WebElementEntity>
