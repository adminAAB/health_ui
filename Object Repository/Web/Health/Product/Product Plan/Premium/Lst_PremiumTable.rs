<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lst_PremiumTable</name>
   <tag></tag>
   <elementGuidId>4b4ad9c0-8b0b-428e-b925-821860a6b1d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;premiumTable&quot;]/a2is-datatable/div[2]/div/table/tbody/tr//*[.//text()='${key}']/span[count(. | //*[@ref_element = 'Object Repository/Old Stuffs/frame']) = count(//*[@ref_element = 'Object Repository/Old Stuffs/frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;premiumTable&quot;]/a2is-datatable/div[2]/div/table/tbody/tr//*[.//text()='${key}']/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
