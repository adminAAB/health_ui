<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FieldSearchParentBenefit</name>
   <tag></tag>
   <elementGuidId>69c3dbac-8763-48e4-9601-7a79c9e4980a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//*[@id=&quot;puSearchProductType&quot;]/a2is-textbox-wide-ts/div[2]/div[1]/div/input)[2][count(. | //*[@ref_element = 'Object Repository/Frame']) = count(//*[@ref_element = 'Object Repository/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//*[@id=&quot;puSearchProductType&quot;]/a2is-textbox-wide-ts/div[2]/div[1]/div/input)[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
