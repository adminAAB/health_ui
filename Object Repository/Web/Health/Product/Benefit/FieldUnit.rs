<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FieldUnit</name>
   <tag></tag>
   <elementGuidId>8b58e4ef-3a9a-41c9-bb20-b96e56e149f0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//span[@class=&quot;select2-selection select2-selection--single&quot;])[2][count(. | //*[@ref_element = 'Object Repository/Frame']) = count(//*[@ref_element = 'Object Repository/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//span[@class=&quot;select2-selection select2-selection--single&quot;])[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
