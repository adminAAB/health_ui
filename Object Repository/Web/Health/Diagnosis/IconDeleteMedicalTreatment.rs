<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>IconDeleteMedicalTreatment</name>
   <tag></tag>
   <elementGuidId>edfa2462-6b0f-4c71-93e9-9e3e9a546fa5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//span[@class=&quot;glyphicon glyphicon-minus&quot;])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//span[@class=&quot;glyphicon glyphicon-minus&quot;])[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Frame</value>
   </webElementProperties>
</WebElementEntity>
