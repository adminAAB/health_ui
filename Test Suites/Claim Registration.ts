<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Claim Registration</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7bd26eec-5e3d-4187-b3a5-bfda11dd0cb3</testSuiteGuid>
   <testCaseLink>
      <guid>53aca8e4-8579-40e8-8424-7e55a1f4160e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION01 - Claim Registration - DS Provider - IP - On Plan - Disc Stamp</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5bfce210-ee85-4282-8a12-83fe4eaaf48c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION02 - Claim Registration - DS Provider - IP - NAPS - Disc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e3f4638-a7dd-4248-8918-f9f158b8ff6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION03 - Claim Registration - DS Provider - IP - APS - Stamp</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d5fc8a1-26ea-4282-923e-45f00aac7a7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION04 - Claim Registration - DS Provider - MA - On Plan - No Disc No Stamp</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fec1ca3c-a698-41c1-8089-212a8948f544</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION05 - Claim Registration - DS Provider - OP - No Disc No Stamp</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e352cb4-b903-46ca-989b-b391058e53cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION06 - Claim Registration - DS Client - IP - On Plan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14ab4c1c-688f-47eb-ad93-8574ba4af061</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION07 - Claim Registration - DS Client - IP - NAPS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2af97f5a-e44f-434f-9653-0df7c5f1cf53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION08 - Claim Registration - DS Client - IP - APS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4b8680f6-c150-4c0f-b12b-0f3d0c47cd79</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION09 - Claim Registration - DS Client - MA - On Plan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ba9a1ae-b9bb-4424-8f88-5ac2974dfb7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION10 - Claim Registration - DS Client - OP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6c98eae-5d10-4fa5-adf1-ef459e712ee0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION11 - Claim Registration - DS Member - IP - On Plan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1df120e9-ccf4-430c-9705-f8e6fed40703</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION12 - Claim Registration - DS Member - IP - NAPS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f90fbbca-e582-403e-960c-189277a7b8b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION13 - Claim Registration - DS Member - IP - APS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a7a2e4a-463b-4b6c-b3a5-c0e1ff3f16d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION14 - Claim Registration - DS Member - MA - OnPlan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbfc90bb-67b4-455e-8d10-2e7d6f0026b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION15 - Claim Registration - DS Member - Outpatient</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5caa1909-7157-4c7b-89ac-ad0b5ece192a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION16 - Claim Registration - DS Provider - Unreg Member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>236f7c37-ed46-41f4-a16b-c9a2c7e5586f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION17 - Claim Registration - DS Member - UnregAccount</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a1e716a-d790-4ae0-b1c5-b129798a72a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Registration/ION18 - Claim Registration - DS Provider - Close</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
