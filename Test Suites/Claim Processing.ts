<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Claim Processing</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>998fd082-aa4a-4a3b-a15f-b8d424b9a4a6</testSuiteGuid>
   <testCaseLink>
      <guid>afad7b05-e909-4e81-909f-35617c1ee8a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/ION01 - ClaimProcessing - Doc Source Provider - IP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65604fa7-095d-47ca-a409-cc4dacf74683</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/ION02 - ClaimProcessing - Doc Source Client - IP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99d10130-d094-4ef2-8efc-5f012c166d64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/ION03 - ClaimProcessing - Doc Source Member - IP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dceab08f-f046-4da5-9283-23d0af97ef76</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/ION04 - ClaimProcessing - Doc Source Provider - OP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1750944b-eca6-485e-9684-105ed69bc666</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/ION05 - ClaimProcessing - Doc Source Client - OP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d91781f5-0346-4eae-b221-216e42ac1b0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/ION06 - ClaimProcessing - Doc Source Member - OP</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
