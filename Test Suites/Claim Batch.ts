<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Claim Batch</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3c504143-5ad4-4f8c-98ab-e79bfa851c0c</testSuiteGuid>
   <testCaseLink>
      <guid>fdc9d6c8-3b8c-4d1f-be9c-8ac37fd84921</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim - Batch/KAO - Approved_Correction - Discount</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f425dacf-9584-47e3-a1ab-ee813bba5a62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim - Batch/KAO - Approved_Correction - Discount dan Stamp</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1df09209-5af9-405a-b37b-c21e9546fb17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim - Batch/KAO - Approved_Correction - Stamp</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
