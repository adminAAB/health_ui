<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Policy</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e4ae5de4-6538-4840-93de-f482abfd4b3c</testSuiteGuid>
   <testCaseLink>
      <guid>5430ad95-58d8-42bf-a6ca-4fda9805c8b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Policy/Create New IAS Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50936646-d5d6-4c73-a184-5544ec3e2e1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Policy/IAS Edit Policy Import Member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31cb5ecb-224b-4a0b-b8b4-508478fe1882</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Policy/Endorse IAS Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d2606ad-a93c-4a95-a162-221176416f86</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Policy/Create New INS Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d4ce5033-8bf9-4312-a57e-308083ac7d4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Policy/INS Edit Policy Import Member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>244c04f2-7ac6-437c-962c-d8f2b9cd0ed0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Policy/Endorse INS Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c9df232-5e95-48f7-b3f8-a21af939db0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Policy/Diagnosis Exclusion Policy</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
