<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Document Registration</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8eca6de3-2f09-48a0-8c51-8f72b5b9fe7b</testSuiteGuid>
   <testCaseLink>
      <guid>24cc3be0-474d-4582-9b0a-524c2b8968d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim - Document Registration/Member/WIY - New Claim_Edit_Select - IP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d4ecf09-db1f-4ae6-8b56-4384c21130cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim - Document Registration/Member/WIY - New Claim_Edit_Select - OP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40ed191a-b206-4afc-9459-f04398ecbb88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim - Document Registration/Member/WIY - NewClaim - Followup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26ecee7d-c153-40b1-bcbc-00ade1cbae17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim - Document Registration/Member/WIY - NewClaim_Edit_Select - MA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd81e207-0085-462e-98ec-a5a606ddcb82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim - Document Registration/Provider/KAO - NewClaim - Followup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa94fa39-c206-4026-aa40-ea9562204a4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim - Document Registration/Provider/KAO - NewClaim_Edit_Select - IP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f578464-c89b-41f4-86b4-59623c689352</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim - Document Registration/Provider/KAO - NewClaim_Edit_Select - MA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d5b19af-e1ec-417d-9843-844d6c5c1d62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim - Document Registration/Provider/KAO - NewClaim_Edit_Select - OP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f37e9789-f3d3-4049-a3e3-18eaf2e37398</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim - Document Registration/Provider/KAO - NewClaim_Edit_Select - Prepost</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d8a6f7f-9afa-4a58-af39-10c01674c658</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim - Document Registration/Provider/KAO - NewClaim_Edit_Select - Suspect Double</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
