<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Product</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>940f2c91-405a-4c1b-96e7-4240c50c22e1</testSuiteGuid>
   <testCaseLink>
      <guid>1387960c-259f-484f-b3cc-1c6c833515eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Product/Import Product</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87597a35-9def-4fd6-b4e4-46854be4b0de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Product/Add Delete Product Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43fd990b-51da-46a6-aa13-e5eadaf29c2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Product/Edit Product Info</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
