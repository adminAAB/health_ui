<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Membership Inquiry</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0f4299d1-bf66-47b2-a1d1-46e9a22d5703</testSuiteGuid>
   <testCaseLink>
      <guid>9247f575-52e7-4c52-a6a9-030c3607a95f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/DMW03 - MI - UpdateAccountInfo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a8ca75e-82e7-4d18-b12b-3d8d4cf70c6d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/DMW05 - MI - UpdatePhoneEmail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d8e70a4-e243-4665-90f0-eb5e9d53669e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/DMW12 - Membership Inquiry</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
