<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Membership Task List</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ba7958f7-26a6-4744-bf46-020d57f1c991</testSuiteGuid>
   <testCaseLink>
      <guid>08ecbd0c-21e8-4546-97b4-a0a4f8f5746e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Membership Task List/ION01 - MembershipTaskList - Unreg Member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>55fc21a8-e5b9-450a-b38c-e6580e0fda3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Membership Task List/ION02 - MembershipTaskList - Unreg Account</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
