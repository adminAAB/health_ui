<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Claim Assigment Maintenance</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>aaac85ee-99d8-4012-a84a-1d7e25cfcfb5</testSuiteGuid>
   <testCaseLink>
      <guid>abe43759-58ee-4dbd-b647-20134f04bfdc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Maintenance - Claim Assignment Maintenance/ION01 - Claim Assignment Maintenance - Back</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>055b0d1a-e011-43ce-aba0-caa036b8ce39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Maintenance - Claim Assignment Maintenance/ION02 - Claim Assignment Maintenance - Save - All</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a485a417-55d9-4dc4-8573-9417dbf2e225</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Maintenance - Claim Assignment Maintenance/ION03 - Claim Assignment Maintenance - Save - Cashless</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83e6f99b-1fad-423e-b4c6-cdfccdfea2a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Maintenance - Claim Assignment Maintenance/ION04 - Claim Assignment Maintenance - Save - Reimburse</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
