import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'),
	[	('Username') : 'YMU', //Input UserName
		('Password') : 'Password95' //Input Password
	], FailureHandling.STOP_ON_FAILURE)

//Pilih Menu
WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'),
	[	('MenuHeader') : 'Claim', //Pilih Menu Claim
		('SubMenu') : 'Claim Processing' //Pilih Menu Claim Processing
	], FailureHandling.STOP_ON_FAILURE)


WebUI.setText(findTestObject('Web/Health/Claim Processing/Txt_DocumentNo'), '1122127')
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Btn_Search'))

GEN5.ProcessingCommand()

//////Klik Checkbox Select Claim di Grid//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Chk_SelectClaim'))

//////Klik Button Edit//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Btn_Edit'))
GEN5.ProcessingCommand()

ArrayList ClaimInfo2 = [findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_DocumentNo'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_RegistrationNo'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_BoxNo'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_RegID'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_DocumentSource'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_ClaimStatus'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_MemberName'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_MemberNo'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_Classification'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_EmployeeClassification'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentPeriodStart'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentPeriodEnd'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_ClaimType'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_ProviderName'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_Doctor'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_DoctorSpecialty'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_ProductType'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AppropriateRBClass'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AppropriateRBRate'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentRBClass'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentRBRate'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_RoomAvailability'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_RoomOption'),
	//findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_UpgradeClass'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_Remarks'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_PayTo'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AccountNo'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AccountName'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AccountBank'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_BankBranch'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_OtherReason'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_PaymentStatus'),
	findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreamentRemarks')
	]

ArrayList Field2 = GEN5.getFieldsValue(ClaimInfo2)

String Txt_ClaimStatus_Array2 = 'Verified'
Field2.set(5, Txt_ClaimStatus_Array2)

String Txt_AppropriateRBRate_Array2 = Field2[18].replace(',','')
Field2.set(18, Txt_AppropriateRBRate_Array2)

String Txt_TreatmentRBRate_Array2 = Field2[20].replace(',','')
Field2.set(20, Txt_TreatmentRBRate_Array2)


String Txt_AccountName_Array2 = Field2[26].trim()
Field2.set(26, Txt_AccountName_Array2)

String Txt_OtherReason2 = Field2[29].replace('\t','').replace('\r', '').replace('\n', '')
Field2.set(29, Txt_OtherReason2)
println Txt_OtherReason2

String Txt_PaymentStatus_Array2 = 'Outstanding'
Field2.set(30, Txt_PaymentStatus_Array2)

String Query_ClaimInfo_ClaimH = "SELECT tdrd.DocumentNo AS DocumentNo, tdr.RegistrationNo AS RegistrationNo, tdrd.BoxNo AS BoxNo, ch.RegID, CASE WHEN tdr.DocumentSource = 'P' THEN 'Provider' WHEN tdr.DocumentSource = 'C' THEN 'Client' WHEN tdr.DocumentSource = 'M' THEN 'Member' END AS DocumentSource, CASE WHEN ch.Status = 'V' THEN 'Verified' WHEN ch.Status = 'R' THEN 'Approved' END AS ClaimStatus, pm.Name AS MemberName, pm.MemberNo AS MemberNo, pc.Description AS Classification, cc.client_ClassDescription AS EmployeeClassification, FORMAT (CAST(ch.Start AS DATE), 'dd/MMM/yyyy') AS TreatmentStart, FORMAT (CAST(ch.Finish AS DATE), 'dd/MMM/yyyy') AS TreatmentEnd,CASE WHEN ch.ClaimType = 'C' THEN 'Cashless'	WHEN ch.ClaimType = 'R' THEN 'Reimburse' END AS ClaimType,ch.TreatmentPlace AS ProviderName,ch.Doctor AS DoctorName,ch.DoctorSpecialty AS DoctorSpecialty,pt.Description AS ProductType,ch.RB_Class AS AppropriateRBClass,ch.AR_Rate AS AppropriateRBRate,ch.TR_Class AS TreatmentRBClass,ch.TR_Amount AS TreatmentRBRate,CASE WHEN ch.RoomAvailability='AV' THEN 'Room Available' WHEN ch.RoomAvailability='RNA' THEN 'Room Not Available' WHEN ch.RoomAvailability='RF' THEN 'Room Is Full' ELSE '' END AS RoomAvailability,CASE WHEN ch.RoomOption='ONPLAN' THEN 'On Plan' WHEN ch.RoomOption='NAPS' THEN 'NAPS' WHEN ch.RoomOption='APS' THEN 'APS' ELSE '' END AS RoomOption,ch.Remarks,CASE WHEN ch.Payto='P' THEN 'Provider' WHEN ch.Payto='M' THEN 'Member' WHEN ch.Payto='C' THEN 'Client' END AS PayTo,ch.AccountNo,ch.BankAccount,ch.BankName,ch.BankBranch,REPLACE(REPLACE(REPLACE(ch.reason, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') AS OtherReason,'Outstanding' AS PaymentStatus,ch.TreatmentRemarks FROM dbo.tbl_disp_registration_document AS tdrd INNER JOIN dbo.ClaimH AS ch ON tdrd.ClaimNo=ch.ClaimNo INNER JOIN dbo.tbl_disp_registration AS tdr ON tdr.RegistrationNo = tdrd.RegistrationNo INNER JOIN dbo.Policy_Member AS pm ON ch.MNO=pm.MNo INNER JOIN dbo.Policy AS p ON p.PNO=pm.PNO INNER JOIN dbo.Policy_Classification AS pc ON p.PNO=pc.PNO AND pc.CLASSNO=pm.CLASSNO left JOIN dbo.ClientClassification AS cc ON pc.Client_ClassNo=cc.Client_ClassNo AND cc.ClientID = p.ClientID INNER JOIN dbo.ProductType AS pt ON pt.ProductType=ch.ProductType WHERE tdrd.DocumentNo=1122127"
GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_ClaimInfo_ClaimH, Field2)


