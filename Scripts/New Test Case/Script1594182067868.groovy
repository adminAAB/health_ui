node('QCatalon-Win10-15'){
	
		stage('delete folder'){
				dir('C:\\Users\\Catalon15\\jenkins\\workspace\\A2isHealth-UI'){
					deleteDir()
					}
				}
			
		stage('Git Checkout') {
				dir('C:\\Users\\Catalon15\\jenkins\\workspace\\A2isHealth-UI') {
				git 'https://github.com/QCatalon/Health'
					  }
				  }
		 
		  stage('Write File') {
			  writeFile file: 'C:\\Users\\Catalon15\\jenkins\\workspace\\A2isHealth-UI\\Plugins\\URLConfigGen5Health.txt', text: 'https://gen5-qc.asuransiastra.com/health'
		  }
	
			try {
				stage('Claim Batch - Approved - Correction - Discount') {
					dir('E:\\Katalon_Studio_Windows_64-6.3.3') {
						bat label: '', script: 'katalon -noSplash -runMode=console -projectPath="C:\\Users\\Catalon15\\jenkins\\workspace\\A2isHealth-UI\\A2ISHealth.prj" -retry=0 -testSuitePath="Test Suites/Claim Batch/Approved - Correction - Discount" -executionProfile="default" -browserType="Chrome"'
						
						
					}
						def e2e = build job:'Build', propagate: false
						result = e2e.result
						if (result.equals("SUCCESS")) {
							build 'Results'
						}
						else {
							sh "exit 1" // this fails the stage
							echo "Cannot deploy without successful build"
						}
					}
				}
				catch (e) {
					result = "FAIL"
				}
			
			try {
				stage('Claim Batch - Approved - Correction - Discount dan Stamp') {
					dir('E:\\Katalon_Studio_Windows_64-6.3.3') {
						bat label: '', script: 'katalon -noSplash -runMode=console -projectPath="C:\\Users\\Catalon15\\jenkins\\workspace\\A2isHealth-UI\\A2ISHealth.prj" -retry=0 -testSuitePath="Test Suites/Claim Batch/Approved - Correction - Discount dan Stamp" -executionProfile="default" -browserType="Chrome"'
						
						
					}
						def e2e = build job:'Build', propagate: false
						result = e2e.result
						if (result.equals("SUCCESS")) {
							build 'Results'
						}
						else {
							sh "exit 1" // this fails the stage
							echo "Cannot deploy without successful build"
						}
					}
				}
				catch (e) {
					result = "FAIL"
				}
			
			
			try {
				stage('Claim Batch - Approved - Correction - Stamp') {
					dir('E:\\Katalon_Studio_Windows_64-6.3.3') {
						bat label: '', script: 'katalon -noSplash -runMode=console -projectPath="C:\\Users\\Catalon15\\jenkins\\workspace\\A2isHealth-UI\\A2ISHealth.prj" -retry=0 -testSuitePath="Test Suites/Claim Batch/Approved - Correction - Stamp" -executionProfile="default" -browserType="Chrome"'
						
						
					}
						def e2e = build job:'Build', propagate: false
						result = e2e.result
						if (result.equals("SUCCESS")) {
							build 'Results'
						}
						else {
							sh "exit 1" // this fails the stage
							echo "Cannot deploy without successful build"
						}
					}
				}
				catch (e) {
					result = "FAIL"
				}
				
			try {
				stage('Create IAS Policy') {
					dir('E:\\Katalon_Studio_Windows_64-6.3.3') {
						bat label: '', script: 'katalon -noSplash -runMode=console -projectPath="C:\\Users\\Catalon15\\jenkins\\workspace\\A2isHealth-UI\\A2ISHealth.prj" -retry=0 -testSuitePath="Test Suites/Create IAS Policy" -executionProfile="default" -browserType="Chrome"'
						
						
					}
						def e2e = build job:'Build', propagate: false
						result = e2e.result
						if (result.equals("SUCCESS")) {
							build 'Results'
						}
						else {
							sh "exit 1" // this fails the stage
							echo "Cannot deploy without successful build"
						}
					}
				}
				catch (e) {
					result = "FAIL"
				}
			
			try {
				stage('Create New INS Policy') {
					dir('E:\\Katalon_Studio_Windows_64-6.3.3') {
						bat label: '', script: 'katalon -noSplash -runMode=console -projectPath="C:\\Users\\Catalon15\\jenkins\\workspace\\A2isHealth-UI\\A2ISHealth.prj" -retry=0 -testSuitePath="Test Suites/Create New INS Policy" -executionProfile="default" -browserType="Chrome"'
						
						
					}
						def e2e = build job:'Build', propagate: false
						result = e2e.result
						if (result.equals("SUCCESS")) {
							build 'Results'
						}
						else {
							sh "exit 1" // this fails the stage
							echo "Cannot deploy without successful build"
						}
					}
				}
				catch (e) {
					result = "FAIL"
				}

			try {
				stage('Diagnosis Category'){
					dir('E:\\Katalon_Studio_Windows_64-6.3.3') {
						bat label: '', script: 'katalon -noSplash -runMode=console -projectPath="C:\\Users\\Catalon15\\jenkins\\workspace\\A2isHealth-UI\\A2ISHealth.prj" -retry=0 -testSuitePath="Test Suites/Diagnosis Category" -executionProfile="default" -browserType="Chrome"'
						
									
					}
						def e2e = build job:'Build', propagate: false
						result = e2e.result
						if (result.equals("SUCCESS")) {
							build 'Results'
						}
						else {
							sh "exit 1" // this fails the stage
							echo "Cannot deploy without successful build"
						}
					}
				}
				catch (e) {
					result = "FAIL"
				}
				
			try {
				stage('Diagnosis Question'){
					dir('E:\\Katalon_Studio_Windows_64-6.3.3') {
						bat label: '', script: 'katalon -noSplash -runMode=console -projectPath="C:\\Users\\Catalon15\\jenkins\\workspace\\A2isHealth-UI\\A2ISHealth.prj" -retry=0 -testSuitePath="Test Suites/Diagnosis Question" -executionProfile="default" -browserType="Chrome"'
						
									
					}
						def e2e = build job:'Build', propagate: false
						result = e2e.result
						if (result.equals("SUCCESS")) {
							build 'Results'
						}
						else {
							sh "exit 1" // this fails the stage
							echo "Cannot deploy without successful build"
						}
					}
				}
				catch (e) {
					result = "FAIL"
				}
	
}