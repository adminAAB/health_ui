import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.health.health as health
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

ArrayList randomDate = health.RandomDate()

//Scenario ini sudah mencakup NEW CLAIM - EDIT CLAIM - SELECT CLAIM
WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'),
	 [('Username') : 'KAO' ,  // Username Login
	 ('Password') : 'Password95'] ,  // Password Login
	 FailureHandling.STOP_ON_FAILURE)

//ini adalah contoh dinamis Pilih Menu dan SubMenu
WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'),
	 [('MenuHeader') : 'Claim',
	 ('SubMenu') : 'Batch'],
	 FailureHandling.STOP_ON_FAILURE)

//Correction Claim Batch - Sehingga Pilih Batch = Approved
WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Batch/Edit Claim Batch'),
	 [('BatchStatusA') : 'Approved' ,
	 ('BatchStatusO') : 'Open' ,
	 ('Discount') : '250000' ,
	 ('Stamp') : 'Rp. 6,000.00'],
	 FailureHandling.STOP_ON_FAILURE)


WebUI.closeBrowser()

