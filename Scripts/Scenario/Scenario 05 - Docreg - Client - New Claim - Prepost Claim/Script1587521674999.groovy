import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'LUR', ('Password') : 'Password95'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), [('Parent') : 'Claim', ('Submenu') : 'Document Registration'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Choose Document Source'), [('DocSource') : 'Client'
		, ('ClientName') : 'Asuransi Astra Buana'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Batch Client'), [('DeliveryNo') : 'Delivery01', ('HospitalInvoiceNo') : 'Hospital01'
		, ('DeliveryReceiveDate') : '11/Apr/2020', ('GarmedReceiveDate') : '12/Apr/2020'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Search and Select Member'), [('MemberNo') : 'R/00100830'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Client/New Claim/New Prepost'), [('MemberNo') : 'R/00100830'
		, ('ProductType') : 'Inpatient', ('TotalBilled') : '1250000', ('RoomOption') : 'On Plan', ('DiagnosisName') : 'CHOLERA'
		, ('TreatmentStart') : '03/Apr/2020', ('TreatmentFinish') : '04/Apr/2020', ('OverseasProvider') : 'No', ('ClaimType') : 'Reimburse'
		, ('ProviderName') : 'MRCCC (Siloam Hospitals Semanggi)', ('ProviderRoomList') : 'Standard', ('DocNoPrepost') : '1122357'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Summary Documents'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Client/Edit Claim/Edit Prepost'), [('UpdateTotalBilled') : '1500000'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Send Batch'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Choose Document Source'), [('DocSource') : 'Client'
		, ('ClientName') : 'Asuransi Astra Buana'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Batch Client'), [('DeliveryNo') : 'Delivery02', ('HospitalInvoiceNo') : 'Hospital02'
		, ('DeliveryReceiveDate') : '13/Apr/2020', ('GarmedReceiveDate') : '14/Apr/2020'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Client/Select Claim/Select Prepost'), [('MemberNo') : 'R/00100830'
		, ('Type') : 'DocNo IP - DocSource Client - Prepost', ('No') : '4'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Send Batch'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Exit-Application'), [:], FailureHandling.STOP_ON_FAILURE)

