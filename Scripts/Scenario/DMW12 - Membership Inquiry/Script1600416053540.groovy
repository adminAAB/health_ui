import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'ABF' //Input UserName
        , ('Password') : 'ITG@nt1P455QC' //Input Password
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/ChooseMenu'), [('MenuHeader') : 'Membership' //Pilih Menu
        , ('SubMenu') : 'Inquiry' //Pilih Sub Menu
        , ('LabelHalaman') : 'Membership Inquiry' //Assert Label Halaman
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Membership Inquiry/Search Member Null'), [('ClientNameNull') : 'Example Client' //Set CLient Name
        , ('EmployeeIDNull') : '' //Set Employee ID
        , ('MemberNameNull') : 'Example Member' //Set Member Name
        , ('MemberNoNull') : '' //Set Member Name
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Membership Inquiry/Search Member'), [('ClientName') : 'Asuransi Astra Buana' //Set CLient Name
        , ('EmployeeID') : '' //Set Employee ID
        , ('MemberName') : 'ANDRY FEBIAN' //Set Member Name
        , ('MemberNo') : '' //Set Member Name
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Membership Inquiry/Check Member Info'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Membership Inquiry/Check Benefit Info'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Membership Inquiry/Check Claim Info'), [('ProductType') : 'Inpatient'], FailureHandling.STOP_ON_FAILURE)

