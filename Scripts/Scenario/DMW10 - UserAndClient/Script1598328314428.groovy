import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'ABF' //Input UserName
        , ('Password') : 'ITG@nt1P455QC' //Input Password
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/ChooseMenu'), [('MenuHeader') : 'Maintenance' //Pilih Menu
        , ('SubMenu') : 'User & Client' //Pilih Sub Menu
        , ('LabelHalaman') : 'User & Client Maintenance' //Assert Label Halaman
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/UserAndClient/AddUserClient'), [('User') : 'ION' //Set user
        , ('Client') : 'Asuransi Astra Buana' //Set Client
        , ('Start') : '1/Jul/2020' //Set Assignment start
        , ('Finish') : '30/Nov/2020' //Set Assignment finish
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/UserAndClient/SearchUserClient'), [('User') : GlobalVariable.UserClient //Set user
        , ('Client') : 'Asuransi Astra Buana' //Set Client
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/UserAndClient/EditUserClient'), [('UserEdited') : 'DSJ' //Set user
        , ('ClientEdited') : 'PT Bank Permata Tbk' //Set Client
        , ('StartEdited') : '1/Aug/2020' //Set Assignment start
        , ('FinishEdited') : '30/Sep/2020' //Set Assignment finish
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/UserAndClient/SearchUserClient'), [('User') : GlobalVariable.UserClientEdited //Set user
        , ('Client') : 'PT Bank Permata Tbk' //Set Client
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/UserAndClient/DeleteUserClient'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/UserAndClient/SearchUserClientNull'), [('User') : GlobalVariable.UserClient //Set user
        , ('Client') : 'Asuransi Astra Buana' //Set Client	
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/UserAndClient/AddUserClientNull'), [:], FailureHandling.STOP_ON_FAILURE)

