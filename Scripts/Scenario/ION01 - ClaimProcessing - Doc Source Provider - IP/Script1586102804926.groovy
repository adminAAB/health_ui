import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

String Username
String Password

if (GEN5.verifyStaging())
{
	Username = 'ABF'
	Password = 'P@ssw0rd'
}
else
{
	Username = 'ABF'
	Password = 'ITG@nt1P455QC'
}


//Login
WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), 
	[	('Username') : Username, //Input UserName
		('Password') : Password //Input Password
    ], FailureHandling.STOP_ON_FAILURE)

//Pilih Menu
WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), 
	[	('MenuHeader') : 'Claim', //Pilih Menu Claim 
		('SubMenu') : 'Claim Processing' //Pilih Menu Claim Processing
    ], FailureHandling.STOP_ON_FAILURE)

//Input Claim Processing
WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Processing/Claim Processing'), 
	[	//('DocNo') : '1122120', //diaktifkan hanya untuk testing ( input docno nya langsung dr scenario, tidak dr db LITT)
		('DocNoType') : 'DocNo IP - DocSource Provider - On Plan',
		('UsernameReassign') : 'Andry Febian',
		('DoctorName') : 'Budi Rahardjo', 
		('Remarks') : 'Remarks - Create by Automate - ION', 
		('MedicalTreatment') : 'Adenoidektomi',
		//('TotalBill') : '2000',
		('FileName') : 'business.jpg',
		('CekGridDepan') : '1', //jika 1 maka akan dijalankan CekGridDepan, jika 0 tidak akan dijalankan CekGridDepan
		('CompareTDRD') : '1', //jika 1 maka akan dijalankan CompareTDRD, jika 0 tidak akan dijalankan CompareTDRD 
		('UploadFileDoc') : '0' //jika 1 maka akan dijalankan UploadFileDoc, jika 0 tidak akan dijalankan UploadFileDoc
     ])

//Logout
WebUI.callTestCase(findTestCase('Pages/Web/Health/LogOut/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

