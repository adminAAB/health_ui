import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.health.health as health
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.keyword.GEN5 as GEN5

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), 
	 [('Username') : 'KAO' ,  // Username Login
     ('Password') : 'ITG@nt1P455QC'] ,  // Password Login
     FailureHandling.STOP_ON_FAILURE)

//ini adalah contoh dinamis Pilih Menu dan SubMenu
WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), 
	 [('MenuHeader') : 'Maintenance', 
	 ('SubMenu') : 'Doctor'], 
     FailureHandling.STOP_ON_FAILURE)

//ADD
query = "select * from dbo.DataHealth where Type='ID Doctor' "

number = ((GEN5.getValueDatabase('172.16.94.48', 'LiTT', query, 'value')) as Integer)

println(number)

NoId = (number + 1)

println(NoId)

query2 = "update dbo.DataHealth set value = $NoId where Type = 'ID Doctor'"

GEN5.updateValueDatabase('172.16.94.48', 'LiTT', query2)

NoId.toString()

//Scenario Add Doctor
WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Doctor/Add Doctor'), 
	 [('Doctor') : 'KhairunnisaA' + NoId , 
	  ('Rating') : '5 - Excellent' ,
	  ('Specialty') : 'Dokter Umum' , 
	  ('Provider') : 'RS Hermina Depok'], 
     FailureHandling.STOP_ON_FAILURE)

//Scenario Edit Doctor
WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Doctor/Edit Doctor'),
	[('Doctor1') : GlobalVariable.Doctor,
	('Rating') : '4 - Good',
	('Specialty') : 'Dokter Gigi' ,
	('Provider') : 'RS Hermina Medan'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

