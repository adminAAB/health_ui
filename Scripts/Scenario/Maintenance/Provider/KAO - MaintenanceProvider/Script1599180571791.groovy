import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.health.health as health
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.eclipse.persistence.internal.identitymaps.NoIdentityMap as NoIdentityMap
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

//Login
WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'KAO' // Username Login
        , ('Password') : 'ITG@nt1P455QC'] // Password Login
    , FailureHandling.STOP_ON_FAILURE)

//ini adalah contoh dinamis Pilih Menu dan SubMenu
WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), [('MenuHeader') : 'Maintenance', ('SubMenu') : 'Provider'], 
    FailureHandling.STOP_ON_FAILURE)

//ADD
query = 'select * from dbo.DataHealth where Type=\'Provider\' '

number = ((GEN5.getValueDatabase('172.16.94.48', 'LiTT', query, 'value')) as Integer)

println(number)

NoId = (number + 1)

println(NoId)

query2 = "update dbo.DataHealth set value = $NoId where Type = 'Provider'"

GEN5.updateValueDatabase('172.16.94.48', 'LiTT', query2)

NoId.toString()

//ADD
query = "select * from dbo.DataHealth where Type='Provider' "

number = ((GEN5.getValueDatabase('172.16.94.48', 'LiTT', query, 'value')) as Integer)

println(number)

NoId = (number + 1)

println(NoId)

query2 = "update dbo.DataHealth set value = $NoId where Type = 'Provider'"

GEN5.updateValueDatabase('172.16.94.48', 'LiTT', query2)

NoId.toString()

//Add Provider
//Tab Provider Info
WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Add Provider Info'),
	[   ('ProviderID') : '2909' + NoId ,
		('ProviderName') : 'KAO Beauty Clinic' + NoId ,
		('Address') : 'Jalan Iskandar Muda, Jaksel',
		('City') : 'Kota Administrasi Jakarta Selatan',
		('Country') : 'Indonesia',
		('Latitude') : '-6.219278',
		('Longitude') : '106.817036',
		('TreatmentMedical') : 'Willing',
		('ProviderType') : 'Apotek',
		('ProviderClass') : 'Tipe A',
		('Rating') : '5 - Excellent',
		('ContractType') : 'Surat Keterangan Rekanan',
		('MOUNumber') : '178/HID-PM/Asuransi Astra/XII/',
		('StartDate') : '01/Jan/2020',
		('EndDate') : '31/Dec/2020',
		('TaxID') : '11223344',
		('DiscLeadTime') : '14',
		('DiscRefund') : '20',
		('RebateGM') : '2.5',
		('RebateMember') : '2',
		('DiscCondition') : '2.5% dari total tagihan per perawatan',
		('AdminFee') : '2',
		('AdminFeeMax') : '15',
		('PaymentDue') : '30',
		('LateCharge') : '5',
		('OpenHours') : '08:00',
		('CloseHours') : '17:00'], 
	FailureHandling.STOP_ON_FAILURE)

//Tab Contacts provider
WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Add Contacts'), 
	[('Function') : 'Finance', 
		('Name_Contacts') : 'Dwi Susanto', 
		('Email_Contacts') : 'dwisant@gmail.com', 
		('Phone_Contacts') : '082125364411',
		('Ext_Contacts') : '122', 
		('MobilePhone_Contacts') : '082125364411', 
		('Fax_Contacts') : '015'], 
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Add Rooms'), 
	[('StartDate') : '01/Jan/2020', 
	 ('ProviderRoom') : 'Beauty Specilaty', 
	 ('RoomName') : 'VIP', 
	 ('RoomCategory') : 'Perawatan', 
	 ('TreatmentType') : 'Special', 
	 ('RoomRate') : '1250000', 
	 ('NumberOfBed') : '1', 
	 ('Max') : '1'], 
 	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Add Accounts'), 
	[('BankName') : 'MANDIRI', 
	 ('AccountNo') : '90008989767', 
	 ('AccountName') : 'Ibu Khairunnisa A', 
	 ('BankBranch') : 'JAKARTA'], 
 	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Add Facilities dan Specialties'), 
	[('Facilities') : 'Kecantikan', 
	 ('Specialties') : 'Spesialis'], 
 	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Add Document Exclusion'), 
	[('DocExclusion') : 'Resume Medis Asli'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Add Documents'),
	[('File_Document') : 'Dokumen-KAO.pdf',
	 ('DocType') : 'MOU',
	 ('ContractType') : GlobalVariable.ContractType,
	 ('DocNumber') : '001',
	 ('StartDate') : '20/Sep/2020',
	 ('EndDate') : '29/Sep/2020'],
 	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Add Doctors'), 
	[('Doctors') : 'Karina'], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)
WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Btn_Save_Provider'))
GEN5.ProcessingCommand()


//Scenario Edit Provider Maintenance
WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Edit Provider Info'),
	[('ProviderName') : GlobalVariable.ProviderName,
	 ('ContractType') : 'PKS Garda Medika',
	 ('StartDate') : '01/Feb/2020',
	 ('EndDate') : '01/Jan/2021',
	 ('TaxID') : '250920',
	 ('DiscLeadTime') : '7',
	 ('DiscRefund') : '25'],
	 FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Edit Contacts'),
	[('Function') : 'Account Manager',
	 ('Name_Contacts') : 'Khairunnisa Aritonang',
	 ('Email_Contacts'): 'karitonang@gmail.com',
	 ('Phone_Contacts'): '085362663368',
	 ('Ext_Contacts')  : '121',
	 ('MobilePhone_Contacts') : '085362663368',
	 ('Fax_Contacts')  : '001'],
	 FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Edit Rooms'),
	[('ProviderRoom') : 'VIP Room',
	 ('RoomRate') : '1550000',
	 ('StartDate') : GlobalVariable.StartDate ],
	 FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Edit Accounts'),
	[('BankName') : 'BNI',
	 ('AccountNo') : '40005409889',
	 ('AccountName') : 'Khairunnisa Aritonang',
	 ('BankBranch') : 'Ancol'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Edit Facilities dan Specialties'),
	[('Facilities') : 'Kecantikan',
	 ('Specialties'): 'Spesialis'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Edit Document Exclusion'),
	[('DocExclusion') : 'Form Reimburse'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Edit Documents'),
	[:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Maintenance/Provider/Edit Doctors'),
	[('Doctors') : 'Moh Alif Cindi'], FailureHandling.STOP_ON_FAILURE)


WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Btn_Save_Provider'))
GEN5.ProcessingCommand()


