import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'ABF' //Input UserName
        , ('Password') : 'ITG@nt1P455QC' //Input Password
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/ChooseMenu'), [('MenuHeader') : 'Membership' //Pilih Menu
        , ('SubMenu') : 'Inquiry' //Pilih Sub Menu
        , ('LabelHalaman') : 'Membership Inquiry' //Assert Label Halaman
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Membership Inquiry/Search Member'), [('ClientName') : 'Asuransi Astra Buana' //Set CLient Name
        , ('EmployeeID') : '' //Set Employee ID
        , ('MemberName') : 'ANDRY FEBIAN' //Set Member Name
        , ('MemberNo') : '' //Set Member Name
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Membership Inquiry/Update Account Null'), [('AccountNameNull') : '' //Set Account Name
        , ('AccountNoNull') : '' //Set Account No
	], FailureHandling.STOP_ON_FAILURE)

query = 'select * from dbo.Otosales where Parameters=\'MI Update Account Info\''

number = ((GEN5.getValueDatabase('172.16.94.48', 'LiTT', query, 'Value')) as Integer)

println(number)

Num = (number + 1)

println(Num)

query2 = "update dbo.Otosales set Value = $Num where Parameters = 'MI Update Account Info'"

GEN5.updateValueDatabase('172.16.94.48', 'LiTT', query2)

Num.toString()

AccountName = ('ANDRY' + Num)

AccountNo = ('ABF' + Num)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Membership Inquiry/Update Account Info Member'), [('BankName') : 'BANK PERMATA' //Set Bank Name
        , ('AccountName') : AccountName //Set Account Name
        , ('AccountNo') : AccountNo //Set Account No
    ], FailureHandling.STOP_ON_FAILURE)

