import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'ABF' //Input UserName
        , ('Password') : 'ITG@nt1P455QC' //Input Password
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/ChooseMenu'), [('MenuHeader') : 'Maintenance' //Pilih Menu
        , ('SubMenu') : 'Profile' //Pilih Sub Menu
        , ('LabelHalaman') : 'Profile Maintenance' //Assert Label Halaman
    ], FailureHandling.STOP_ON_FAILURE)

//ADD
query = 'select * from dbo.DataHealth where Type=\'ProfileName\''

number = ((GEN5.getValueDatabase('172.16.94.48', 'LiTT', query, 'value')) as Integer)

println(number)

NoId = (number + 1)

println(NoId)

query2 = "update dbo.DataHealth set value = $NoId where Type = 'ProfileName'"

GEN5.updateValueDatabase('172.16.94.48', 'LiTT', query2)

NoId.toString()

WebUI.callTestCase(findTestCase('Pages/Web/Health/Profile Maintenance/AddGeneralInfo'), [('Name') : 'Big Golden ' + NoId //Set Nama Profile
        , ('Salutation') : 'PT.' //Pilih Salutation
        , ('CompanyType') : 'Private' //Pilih CompanyType
        , ('LineOfBussiness') : 'Agent' //Pilih LineOfBussiness
        , ('ClaimPriority') : '1000' //Set Claim Priority
        , ('Address') : 'Jl. TB Simatupang' //Pilih City
        , ('City') : 'Kota Administrasi Jakarta Selatan' //Pilih City
        , ('Country') : 'Indonesia' //Pilih Country
        , ('UserDoctor') : 'YMU' //Set Doctor
        , ('Marketing') : 'DUW' //Set Marketing
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Profile Maintenance/SearchProfile'), [('ProfileName') : GlobalVariable.ProfileName], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Profile Maintenance/EditGeneralInfo'), [('Name') : 'Big Golden Edited ' + 
        NoId //Set Nama Profile
        , ('Salutation') : 'PT.' //Pilih Salutation
        , ('CompanyType') : 'Private' //Pilih CompanyType
        , ('LineOfBussiness') : 'Agent' //Pilih LineOfBussiness
        , ('ClaimPriority') : '1500' //Set Claim Priority
        , ('Address') : 'Jl. TB Simatupang No.2' //Pilih City
        , ('City') : 'Kota Administrasi Jakarta Timur' //Pilih City
        , ('Country') : 'Indonesia' //Pilih Country
        , ('UserDoctor') : 'NMZ' //Set Doctor
        , ('Marketing') : 'ALR' //Set Marketing	
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Profile Maintenance/AddEditDeleteContact'), [('Function') : 'Finance' //Set Function
        , ('Name') : 'Devi 1' //Set Name
        , ('Email') : 'dlismawati@asuransiastra.com' //Set Email
        , ('MobilePhone') : '08123456789' //Set Mobile Phone
        , ('Function2') : 'Direktur' //Set Function
        , ('Name2') : 'Devi 2' //Set Name
        , ('Email2') : 'dlismawati@asuransiastra.com' //Set Email
        , ('MobilePhone2') : '08123456788' //Set Mobile Phone
        , ('ContactDeleted') : 'Devi 1' //Set ContactDeleted
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Profile Maintenance/AddEditDeleteAccount'), [('Bank') : 'BANK BNI' //Set Bank
        , ('AccountNo') : '16111996001' //Set AccountNo
        , ('AccountName') : 'Devi bank 1' //Set AccountName
        , ('Bank2') : 'BANK BRI SYARIAH' //Set Bank
        , ('AccountNo2') : '16111996002' //Set AccountNo
        , ('AccountName2') : 'Devi bank 2' //Set AccountName		
        , ('AccountDeleted') : 'BANK BNI' //Set AccountDeleted
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Profile Maintenance/AddEditDeleteReferenceBook'), [('Referencebook') : 'ReferenceBook.pdf' //Upload reference book
        , ('Description') : 'This is the description 1' // Set Description
        , ('EffectivePeriod') : '01/Oct/2020' // Set EffectivePeriod
        , ('ExpireDate') : '30/Dec/2020' // Set ExpireDate
        , ('Referencebook2') : 'ReferenceBook2.pdf' //Upload reference book
        , ('Description2') : 'This is the description 2' // Set Description
        , ('EffectivePeriod2') : '02/Oct/2020' // Set EffectivePeriod
        , ('ExpireDate2') : '31/Dec/2020' // Set ExpireDate
        , ('BookDeleted') : 'ReferenceBook.pdf' // Set ExpireDate
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Profile Maintenance/AddEditDeleteEmpClassification'), [('Classification1') : 'Golongan 1' //Set Classification1
        , ('Classification2') : 'Golongan 2' // Set Classification2
        , ('ClassificationDeleted') : 'Golongan 1' // Set ClassificationDeleted
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ButtonSave'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.callTestCase(findTestCase('Pages/Web/Health/Profile Maintenance/SearchProfile'), [('ProfileName') : GlobalVariable.ProfileNameEdited], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Profile Maintenance/RoomAndBoard'), [('EffectiveDate') : '01/Nov/2020' //Set EffectiveDate
        , ('Scheme') : 'Garda Medika RB Class' // Set Scheme
        , ('GMRnBClass') : 'VIP' // Set GMRnBClass
		, ('GMRnBClass2') : 'Akasia' // Set GMRnBClass
	], FailureHandling.STOP_ON_FAILURE)


