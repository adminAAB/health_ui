import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'ABF' //Input UserName
        , ('Password') : 'ITG@nt1P455QC' //Input Password
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Home/MenuHeader', [('MenuHeader') : 'Product']), FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/ChooseMenu'), [('MenuHeader') : 'Product' //Pilih Menu
        , ('SubMenu') : 'Product Type' //Pilih Sub Menu
        , ('LabelHalaman') : 'Product Type' //Assert Label Halaman
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/ProductType/AddProductTypeNull'), [('ProductTypeIDNull') : '' //Set ProductTypeID
        , ('ProductTypeNameNull') : '' //Set ProductTypeName
    ], FailureHandling.STOP_ON_FAILURE)

//ADD
query = 'select * from dbo.DataHealth where Type=\'ID Product Type\''

number = ((GEN5.getValueDatabase('172.16.94.48', 'LiTT', query, 'value')) as Integer)

println(number)

NoId = (number + 1)

println(NoId)

query2 = "update dbo.DataHealth set value = $NoId where Type = 'ID Product Type'"

GEN5.updateValueDatabase('172.16.94.48', 'LiTT', query2)

NoId.toString()

WebUI.callTestCase(findTestCase('Pages/Web/Health/ProductType/AddProductType'), [('ProductTypeID') : 'DMW PT ' + NoId //Set ProductTypeID
        , ('ProductTypeName') : 'Product DMW ' + NoId //Set ProductTypeName
        , ('AdmedikaCoverageCode') : '02' //Set Admedika Coverage Code
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/ProductType/SearchProductTypeNull'), [('ProductTypeNull') : 'ProductTypeNull'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/ProductType/SearchProductType'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/ProductType/EditProductTypeNull'), [('ProductTypeNameNull') : ''], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/ProductType/EditProductType'), [('ProductTypeNameEdited') : 'Product DMW ' + NoId + ' Edited' //Set ProductTypeName
        , ('AdmedikaCoverageCodeEdited') : '10' //Set Admedika Coverage Code
	], FailureHandling.STOP_ON_FAILURE)

