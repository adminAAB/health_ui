import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.keyword.GEN5 as GEN5

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'LUR', ('Password') : 'Password95'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), [('Parent') : 'Claim', ('Submenu') : 'Document Registration'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Choose Document Source'), [('DocSource') : 'Client'
        , ('ClientName') : 'Asuransi Astra Buana'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Batch Client'), [('DeliveryNo') : 'Delivery01', ('HospitalInvoiceNo') : 'Hospital01'
        , ('DeliveryReceiveDate') : '14/Jan/2020', ('GarmedReceiveDate') : '16/Jan/2020'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Search and Select Member'), [('MemberNo') : 'A/00117056'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Client/New Claim/New Claim'), [('MemberNo') : 'A/00117056'
        , ('ProductType') : 'Inpatient', ('TotalBilled') : '1250000', ('RoomOption') : 'On Plan', ('DiagnosisName') : 'CHOLERA'
        , ('TreatmentStart') : '01/May/2020', ('TreatmentFinish') : '02/May/2020', ('OverseasProvider') : 'No', ('ClaimType') : 'Reimburse'
        , ('ProviderName') : 'MRCCC (Siloam Hospitals Semanggi)', ('ProviderRoomList') : 'Standard'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Summary Documents'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Client/Edit Claim/Edit Claim'), [('UpdateTotalBilled') : '1500000'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Send Batch'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Choose Document Source'), [('DocSource') : 'Client'
        , ('ClientName') : 'Asuransi Astra Buana'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Batch Client'), [('DeliveryNo') : 'Delivery02', ('HospitalInvoiceNo') : 'Hospital02'
        , ('DeliveryReceiveDate') : '07/May/2020', ('GarmedReceiveDate') : '09/May/2020'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Client/Select Claim/Select Claim'), [('MemberNo') : 'A/00117056'
        , ('Type') : 'DocNo IP - DocSource Client', ('No') : '1'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Send Batch'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Exit-Application'), [:], FailureHandling.STOP_ON_FAILURE)

