import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'ABF' //Input UserName
        , ('Password') : 'ITG@nt1P455QC' //Input Password
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/ChooseMenu'), [('MenuHeader') : 'Settlement' //Pilih Menu
        , ('SubMenu') : 'Note Inquiry' //Pilih Sub Menu
        , ('LabelHalaman') : 'Note Inquiry' //Assert Label Halaman
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Note Inquiry/SearchNote'), [('ClientName') : 'Asuransi Astra Buana' //Set Client Name
        , ('NoteNo') : '00081/DN/JKT00/04/20' //Set Note No
        , ('DocNo') : '1119147' //Set Document No
        , ('GroupingNo') : '00156/RQ/EXC/GM/04/20' //Set Grouping No
        , ('PeriodFrom') : '1/Apr/2020' //Set Grouping No
        , ('PeriodTo') : '30/Apr/2020' //Set Grouping No
        //, ('NoteType') : 'All' //Set Grouping No
        //, ('SettledStatus') : 'Outstanding' //Set Grouping No
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Note Inquiry/ViewNote'), [:], FailureHandling.STOP_ON_FAILURE)

