import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

String Username
String Password

if (GEN5.verifyStaging()) {
    Username = 'ABF'

    Password = 'P@ssw0rd'
} else {
    Username = 'ABF'

    Password = 'ITG@nt1P455QC'
}

//Login
WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : Username //Input UserName
        , ('Password') : Password //Input Password
    ], FailureHandling.STOP_ON_FAILURE)

//Pilih Menu
WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), [('MenuHeader') : 'Membership' //Pilih Menu Claim
        , ('SubMenu') : 'Task List' //Pilih Menu Claim Registration
    ], FailureHandling.STOP_ON_FAILURE)


String MemberName = 'ION NEW MEMBER'
String ClientName = 'Asuransi Astra'
String EmpID = '2177'

String InvalidMemberName = 'ION NEW MEMBER 123'
String InvalidClientName = 'Asuransi Astra 123'
String InvalidEmpID = '2177123'
String InvalidDocNo = 'R1234'

String Registered_EmpID = '1135'
String Registered_MemberName = 'lianasari'
String Registered_MemberNo = 'L/00134'

String Registered_MemberNameSorting = 'Budiman'

//Call Page Membership Tasklist
WebUI.callTestCase(findTestCase('Pages/Web/Health/Membership Tasklist/Membership Tasklist'),
	 [('MemberName'):MemberName,
	  ('ClientName'):ClientName,
	  ('EmpID'):EmpID,
	  ('InvalidMemberName'):InvalidMemberName,
	  ('InvalidClientName'):InvalidClientName,
	  ('InvalidEmpID'):InvalidEmpID,
	  ('InvalidDocNo'):InvalidDocNo,
	  ('Registered_EmpID'): Registered_EmpID,
	  ('Registered_MemberName'): Registered_MemberName ,
	  ('Registered_MemberNo'): Registered_MemberNo,
	  ('Registered_MemberNameSorting'): Registered_MemberNameSorting	
	], FailureHandling.STOP_ON_FAILURE)

//Logout
WebUI.callTestCase(findTestCase('Pages/Web/Health/LogOut/Logout'), [:], FailureHandling.STOP_ON_FAILURE)



