import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'ABF' //Input UserName
        , ('Password') : 'ITG@nt1P455QC' //Input Password
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/ChooseMenu'), [('MenuHeader') : 'Maintenance' //Pilih Menu
        , ('SubMenu') : 'Diagnosis' //Pilih Sub Menu
        , ('LabelHalaman') : 'Diagnosis Maintenance' //Assert Label Halaman
    ], FailureHandling.STOP_ON_FAILURE)

//ADD
query = 'select * from dbo.DataHealth where Type=\'ID Diagnosis\''

number = ((GEN5.getValueDatabase('172.16.94.48', 'LiTT', query, 'value')) as Integer)

println(number)

NoId = (number + 1)

println(NoId)

query2 = "update dbo.DataHealth set value = $NoId where Type = 'ID Diagnosis'"

GEN5.updateValueDatabase('172.16.94.48', 'LiTT', query2)

NoId.toString()

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis/AddDiagnosis'), [('ID') : 'DMW ' + NoId //Set ID Diagnosis
        , ('Name') : 'Diagnosis ' + NoId //Set Nama Diagnosis
        , ('Description') : 'Description Diagnosis' //Set Deskripsi Diagnosis
        , ('MedicalTreatment') : 'excision' //Add Medical Treatment
        , ('DiagnosisCategory') : 'infeksi' //Add Diagnosis Category
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis/AddDiagnosisNull'), [('IDNull') : '' //Set ID Diagnosis
        , ('NameNull') : '' //Set Nama Diagnosis
        , ('DescriptionNull') : '' //Set Deskripsi Diagnosis	
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis/FilterDiagnosis'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis/SearchDiagnosis'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis/EditDiagnosis'), [ //('ID') : 'DMW ' + NoId //Set ID Diagnosis
        ('NameEdited') : ('Diagnosis ' + NoId) + ' Edited' //Set Nama Diagnosis
        , ('DescriptionEdited') : 'Description Diagnosis Edited' //Set Deskripsi Diagnosis
        , ('MedicalTreatmentEdited') : 'Coagulation' //Add Medical Treatment
        , ('DiagnosisCategoryEdited') : 'alcoholism' //Add Diagnosis Category
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis/SearchDiagnosisNull'), [('DiagnosisNull') : 'DMW Null' //Set ID Diagnosis
	], FailureHandling.STOP_ON_FAILURE)

