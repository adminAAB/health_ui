import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'LUR', ('Password') : 'Password95'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), [('Parent') : 'Claim', ('Submenu') : 'Document Registration'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Choose Document Source'), [('DocSource') : 'Client'
        , ('ClientName') : 'Asuransi Astra Buana'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Batch Client'), [('DeliveryNo') : 'GARMED2019', ('HospitalInvoiceNo') : 'HOSPITAL2019'
        , ('DeliveryReceiveDate') : '09/Jun/2020', ('GarmedReceiveDate') : '11/Jun/2020'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Search and Select Member'), [('MemberNo') : 'A/00117056'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Client/New Claim/New Claim'), [('MemberNo') : 'A/00117056'
        , ('ProductType') : 'Outpatient', ('TreatmentStart') : '06/Jun/2020', ('TreatmentFinish') : '06/Jun/2020', ('TotalBilled') : '500000'
        , ('DiagnosisName') : 'CHOLERA', ('OverseasProvider') : 'No', ('ClaimType') : 'Reimburse', ('ProviderName') : 'RS Mitra Keluarga Bekasi Barat'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Summary Documents'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Client/Edit Claim/Edit Claim'), [('UpdateTotalBilled') : '700000'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Send Batch'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Choose Document Source'), [('DocSource') : 'Client'
        , ('ClientName') : 'Asuransi Astra Buana'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Batch Client'), [('DeliveryNo') : 'GARMED2020', ('HospitalInvoiceNo') : 'HOSPITAL2020'
        , ('DeliveryReceiveDate') : '19/Jun/2020', ('GarmedReceiveDate') : '21/Jun/2020'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Client/Select Claim/Select Claim'), [('MemberNo') : 'A/00117056'
        , ('Type') : 'DocNo OP - DocSource Client', ('No') : '2'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Send Batch'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Exit-Application'), [:], FailureHandling.STOP_ON_FAILURE)

