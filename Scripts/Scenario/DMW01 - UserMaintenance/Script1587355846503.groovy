import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'ABF' //Input UserName
        , ('Password') : 'ITG@nt1P455QC' //Input Password
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/ChooseMenu'), [('MenuHeader') : 'Maintenance' //Pilih Menu
        , ('SubMenu') : 'User' //Pilih Sub Menu
        , ('LabelHalaman') : 'User Maintenance' //Assert Label Halaman
    ], FailureHandling.STOP_ON_FAILURE)

query = 'select * from dbo.Otosales where Parameters=\'User Maintenance\''

number = ((GEN5.getValueDatabase('172.16.94.48', 'LiTT', query, 'Value')) as Integer)

println(number)

NoId = (number + 1)

println(NoId)

query2 = "update dbo.Otosales set Value = $NoId where Parameters = 'User Maintenance'"

GEN5.updateValueDatabase('172.16.94.48', 'LiTT', query2)

NoId.toString()

Id = ('CEK' + NoId)

WebUI.callTestCase(findTestCase('Pages/Web/Health/User Maintenance/AddUserBack'), [('UserId') : Id //Input UserId
        , ('Username') : 'Example Name' //Input Username
        , ('Email') : 'example@test.com' //Input Email
        , ('UserType') : 'Branch Staff All' //Pilih UserType
        , ('Title') : 'Example Title' //Pilih UserType
        , ('filename') : 'ExampleTTD.jpeg' //Set file signature name
        , ('Role') : 'SUPERVISOR' //Set Role	
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/User Maintenance/AddUser'), [('UserId') : Id //Input UserId
        , ('Username') : 'Example Name' //Input Username
        , ('Email') : 'example@test.com' //Input Email
        , ('UserType') : 'Branch Staff All' //Pilih UserType
        , ('Title') : 'Example Title' //Pilih UserType
        , ('filename') : 'ExampleTTD.jpeg' //Set file signature name
        , ('Role') : 'SUPERVISOR' //Set Role
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/User Maintenance/SearchUserNull'), [('UsernameOrId') : 'DMWTEST' //Input Username atau UserId
        , ('UserType') : 'All' //Pilih UserType	
	], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/User Maintenance/SearchUser'), [('UsernameOrId') : Id //Input Username atau UserId
        , ('UserType') : 'All' //Pilih UserType
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/User Maintenance/EditUserBack'), [('Username') : 'Example Name edited' //Input Username
        , ('Email') : 'editedexample@test.com' //Input Email
        , ('UserType') : 'Accounting Supervisor' //Pilih UserType
        , ('Title') : '' //Pilih UserType
        , ('filename') : 'ktp.jpg' //Set file signature name
        , ('Role') : 'SUPERVISOR' //Select Role to delete
        , ('RoleEdited') : 'Chatbot User' //Set new role	
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/User Maintenance/EditUser' //[('UsernameOrId') : Id Select User hasil search
        ), [('Username') : 'Example Name edited' //Input Username
        , ('Email') : 'editedexample@test.com' //Input Email
        , ('UserType') : 'Accounting Supervisor' //Pilih UserType
        , ('Title') : '' //Pilih UserType
        , ('filename') : 'ktp.jpg' //Set file signature name
        , ('Role') : 'SUPERVISOR' //Select Role to delete
        , ('RoleEdited') : 'Chatbot User' //Set new role
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/User Maintenance/RequiredFields'), [:], FailureHandling.STOP_ON_FAILURE)

