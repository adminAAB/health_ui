import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'ABF' //Input UserName
        , ('Password') : 'ITG@nt1P455QC' //Input Password
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/ChooseMenu'), [('MenuHeader') : 'Maintenance' //Pilih Menu
        , ('SubMenu') : 'Diagnosis Category' //Pilih Sub Menu
        , ('LabelHalaman') : 'Diagnosis Category Maintenance' //Assert Label Halaman
    ], FailureHandling.STOP_ON_FAILURE)

//ADD
query = 'select * from dbo.Otosales where Parameters=\'Diagnosis Category\''

number = ((GEN5.getValueDatabase('172.16.94.48', 'LiTT', query, 'Value')) as Integer)

println(number)

NoId = (number + 1)

println(NoId)

query2 = "update dbo.Otosales set Value = $NoId where Parameters = 'Diagnosis Category'"

GEN5.updateValueDatabase('172.16.94.48', 'LiTT', query2)

NoId.toString()

DiagnosisCategory = ('Diagnosis Category' + NoId)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis Category/AddDiagnosisCategory'), [('DiagnosisCategory') : DiagnosisCategory //Set Diagnosis Category Name
        , ('DefaultCoverage') : 'Guaranteed but Uncovered' //Set Coverage
        , ('Payer') : 'Employee' //Set Payer
        , ('Confirmation') : 'Yes' //Set COnfirmation
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis Category/FilterDiagnosisCategory'), [('DefaultCoverage') : 'Covered' //Set Coverage
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis Category/SearchDiagnosisCategory'), [:], FailureHandling.STOP_ON_FAILURE)

//Edit
query = 'select * from dbo.Otosales where Parameters=\'Diagnosis Category\''

number = ((GEN5.getValueDatabase('172.16.94.48', 'LiTT', query, 'Value')) as Integer)

println(number)

NoId = (number + 1)

println(NoId)

query2 = "update dbo.Otosales set Value = $NoId where Parameters = 'Diagnosis Category'"

GEN5.updateValueDatabase('172.16.94.48', 'LiTT', query2)

NoId.toString()

DiagnosisCategory2 = ('Diagnosis Category' + NoId)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis Category/EditDiagnosisCategory'), [('DiagnosisCategory2') : DiagnosisCategory2 //Set Diagnosis Category Name
        , ('DefaultCoverage') : 'Uncovered' //Set Coverage
        , ('Payer') : 'Employee' //Set Payer
        , ('Confirmation') : 'No' //Set COnfirmation
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis Category/AddDiagnosisCategoryNull'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis Category/SearchDiagnosisCategory2'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis Category/EditDiagnosisCategoryNull'), [('DiagnosisCategoryNull') : '' //Set Diagnosis Category Name 
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Diagnosis Category/SearchDiagnosisCategoryNull'), [:], FailureHandling.STOP_ON_FAILURE)

