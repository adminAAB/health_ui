import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.keyword.GEN5

String Username
String Password

if (GEN5.verifyStaging())
{
	Username = 'ABF'
	Password = 'P@ssw0rd'
}
else
{
	Username = 'ABF'
	Password = 'ITG@nt1P455QC'
}


// lanjut reference no yah..

GlobalVariable.CR_UserName = Username
GlobalVariable.CR_DocSource = 'Client'
GlobalVariable.CR_ClientName = 'PT Essence Indonesia'
GlobalVariable.CR_ProviderName = 'RS Mitra Keluarga Depok'
GlobalVariable.CR_ReferenceNo = 'Automate-10'
GlobalVariable.CR_MemberNo = 'W/00030949'
GlobalVariable.CR_TreatmentStart = '01/Jul/2020'
GlobalVariable.CR_TreatmentEnd = '01/Jul/2020'
GlobalVariable.CR_ProductType = 'Outpatient'
GlobalVariable.CR_isPrepost = false
GlobalVariable.CR_DiagnosisID = 'A00'
GlobalVariable.CR_Diagnosis = 'CHOLERA'
GlobalVariable.CR_Billed = '2500602'
GlobalVariable.CR_Doctor = 'Adi Gunadi'
GlobalVariable.CR_RoomType_TreatmentRBClass = ''
GlobalVariable.CR_RoomOption = ''
GlobalVariable.CR_PayTo2 = 'Client'
GlobalVariable.CR_CobBpjs = 'false'
GlobalVariable.CR_CashPlanBpjs = 'true'
GlobalVariable.CR_DoubleInsured = 'false'
GlobalVariable.CR_PerawatanKhusus = 'true'
GlobalVariable.CR_PasienMeninggal = 'false'
GlobalVariable.CR_TrafficAccident = 'true'
GlobalVariable.CR_CTScan = 'true'
GlobalVariable.CR_MRI = 'true'
GlobalVariable.CR_PA = 'false'
GlobalVariable.CR_Lab = 'false'
GlobalVariable.CR_Rontgen = 'false'
GlobalVariable.CR_USG = 'true'
GlobalVariable.CR_SRIC = 'false'
GlobalVariable.CR_isUploadDoc = true
GlobalVariable.CR_Completeness = ['All Documents Valid:Lengkap', 'Registered Member:Lengkap', 'Registered PayTo Account:Lengkap', 'Eligible Product:Lengkap']
GlobalVariable.CR_SoftCopyFileName='business.jpg'
GlobalVariable.CR_isSendBatch = true
GlobalVariable.CR_isSuspectDouble = false
GlobalVariable.CR_CheckSearchMember = 'No'
GlobalVariable.CR_isOverseasProvider = 'No'
GlobalVariable.CR_isUnregMember = 'No'
GlobalVariable.CR_RemarksFollowUp = ''

//Login
WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : Username //Input UserName
		, ('Password') : Password //Input Password
	], FailureHandling.STOP_ON_FAILURE)

//Pilih Menu
WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), [('MenuHeader') : 'Claim' //Pilih Menu Claim
		, ('SubMenu') : 'Claim Registration' //Pilih Menu Claim Registration
	], FailureHandling.STOP_ON_FAILURE)


//Create New Claim lalu pilih Doc Source
WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Registration/Create New Claim - Pilih Doc Source'), [('CR_DocumentSource') : GlobalVariable.CR_DocSource
		, ('ProviderName') : GlobalVariable.CR_ProviderName,('ClientName'):GlobalVariable.CR_ClientName], FailureHandling.STOP_ON_FAILURE)


//Create New Claim
WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Registration/Batch - Doc Source Client/Create New Claim'),[:], FailureHandling.STOP_ON_FAILURE)



WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Registration/HL.CLM.REG.C'),
		[:], FailureHandling.STOP_ON_FAILURE)


//Logout
WebUI.callTestCase(findTestCase('Pages/Web/Health/LogOut/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

