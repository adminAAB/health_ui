import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.keyword.GEN5

String Username
String Password

if (GEN5.verifyStaging())
{
	Username = 'ABF'
	Password = 'P@ssw0rd'
}
else
{
	Username = 'ABF'
	Password = 'ITG@nt1P455QC'
}

GlobalVariable.CR_UserName = Username
GlobalVariable.CR_DocSource = 'Provider'
GlobalVariable.CR_ProviderName = 'RS Mitra Keluarga Depok'
GlobalVariable.CR_HospitalInvNo = 'Automate-02'
GlobalVariable.CR_Discount = '25000'
GlobalVariable.CR_Stamp = '0'
GlobalVariable.CR_MemberNo = 'N/00263'
GlobalVariable.CR_ClientName = 'Asuransi Astra Buana'
GlobalVariable.CR_EmpID = '1067'
GlobalVariable.CR_MemberName = 'NURSEHAN'
GlobalVariable.CR_CheckSearchDiagnosis = 'Yes'
GlobalVariable.CR_isNewClaim_Close = 'Yes'


//Login
WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : Username //Input UserName
		, ('Password') : Password //Input Password
	], FailureHandling.STOP_ON_FAILURE)

//Pilih Menu
WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), [('MenuHeader') : 'Claim' //Pilih Menu Claim
		, ('SubMenu') : 'Claim Registration' //Pilih Menu Claim Registration
	], FailureHandling.STOP_ON_FAILURE)


//Create New Claim lalu pilih Doc Source
WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Registration/Create New Claim - Pilih Doc Source'), [('CR_DocumentSource') : GlobalVariable.CR_DocSource
		, ('ProviderName') : GlobalVariable.CR_ProviderName,('isOverseasProvider'):GlobalVariable.CR_isOverseasProvider], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Registration/Batch - Doc Source Provider/Create New Claim'),
	[:], FailureHandling.STOP_ON_FAILURE)

//Masuk ke Form HL.CLM.REG.C lalu langsung close
WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Registration/HL.CLM.REG.C_Close'),
		[:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Registration/Batch - Doc Source Provider/Close Batch'),
	[:], FailureHandling.STOP_ON_FAILURE)



//Logout
WebUI.callTestCase(findTestCase('Pages/Web/Health/LogOut/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

