import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.keyword.GEN5

String Username
String Password

if (GEN5.verifyStaging())
{
	Username = 'ABF'
	Password = 'P@ssw0rd'
}
else
{
	Username = 'ABF'
	Password = 'ITG@nt1P455QC'
}


GlobalVariable.CR_UserName = Username
GlobalVariable.CR_DocSource = 'Provider'
GlobalVariable.CR_ProviderName = 'RS Mitra Keluarga Depok'
GlobalVariable.CR_HospitalInvNo = 'Automate-03'
GlobalVariable.CR_Discount = '0'
GlobalVariable.CR_Stamp = '6000'
GlobalVariable.CR_MemberNo = 'N/00263'
GlobalVariable.CR_ClientName = 'Asuransi Astra Buana'
GlobalVariable.CR_TreatmentStart = '27/Jun/2020'
GlobalVariable.CR_TreatmentEnd = '28/Jun/2020'
GlobalVariable.CR_ProductType = 'Inpatient'
GlobalVariable.CR_isPrepost = false
GlobalVariable.CR_DiagnosisID = 'A00'
GlobalVariable.CR_Diagnosis = 'CHOLERA'
GlobalVariable.CR_Billed = '2500402'
GlobalVariable.CR_Doctor = 'Adi Gunadi'
GlobalVariable.CR_RoomType_TreatmentRBClass = 'Kelas I'
GlobalVariable.CR_RoomOption = 'APS'
GlobalVariable.CR_CobBpjs = 'false'
GlobalVariable.CR_CashPlanBpjs = 'false'
GlobalVariable.CR_DoubleInsured = 'false'
GlobalVariable.CR_PerawatanKhusus = 'false'
GlobalVariable.CR_PasienMeninggal = 'false'
GlobalVariable.CR_TrafficAccident = 'false'
GlobalVariable.CR_CTScan = 'false'
GlobalVariable.CR_MRI = 'false'
GlobalVariable.CR_PA = 'false'
GlobalVariable.CR_Lab = 'false'
GlobalVariable.CR_Rontgen = 'false'
GlobalVariable.CR_USG = 'false'
GlobalVariable.CR_SRIC = 'false'
GlobalVariable.CR_isUploadDoc = false
GlobalVariable.CR_Completeness = ['All Documents Valid:Lengkap', 'Registered Member:Lengkap', 'Registered PayTo Account:Lengkap', 'Eligible Product:Lengkap']
GlobalVariable.CR_SoftCopyFileName='business.jpg'
GlobalVariable.CR_isSendBatch = true
GlobalVariable.CR_isSuspectDouble = false
GlobalVariable.CR_ChangeAccount = 'No'
GlobalVariable.CR_ClearSearchClaim = 'No'
GlobalVariable.CR_CheckSearchMember = 'No'
GlobalVariable.CR_isOverseasProvider = 'No'
GlobalVariable.CR_isUnregMember = 'No'
GlobalVariable.CR_RemarksFollowUp = ''

//Login
WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : Username //Input UserName
		, ('Password') : Password //Input Password
	], FailureHandling.STOP_ON_FAILURE)

//Pilih Menu
WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), [('MenuHeader') : 'Claim' //Pilih Menu Claim
		, ('SubMenu') : 'Claim Registration' //Pilih Menu Claim Registration
	], FailureHandling.STOP_ON_FAILURE)


//Create New Claim lalu pilih Doc Source
WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Registration/Create New Claim - Pilih Doc Source'), [('CR_DocumentSource') : GlobalVariable.CR_DocSource
		, ('ProviderName') : GlobalVariable.CR_ProviderName,('isOverseasProvider'):GlobalVariable.CR_isOverseasProvider], FailureHandling.STOP_ON_FAILURE)


//Create New Claim
WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Registration/Batch - Doc Source Provider/Create New Claim'),
		[:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Registration/HL.CLM.REG.C'),
		[:], FailureHandling.STOP_ON_FAILURE)




//Logout
WebUI.callTestCase(findTestCase('Pages/Web/Health/LogOut/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

