import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'ABF' //Input UserName
        , ('Password') : 'ITG@nt1P455QC' //Input Password
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/ChooseMenu'), [('MenuHeader') : 'Maintenance' //Pilih Menu
        , ('SubMenu') : 'Diagnosis Question' //Pilih Sub Menu
        , ('LabelHalaman') : 'Diagnosis Question' //Assert Label Halaman
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/DiagnosisQuestion/AddDiagnosisQuestionNull'), [('DiagnosisGroupNew') : 'Diagnosis New' //Input Diagnosis Group
        , ('Coverage') : 'By Diagnosis Question' //Set Coverage
        , ('DiagnosisList') : 'Diagnosis Automation' //Set Diagnosis List
    ], FailureHandling.STOP_ON_FAILURE)

query = 'select * from dbo.Otosales where Parameters=\'Diagnosis Group\''

number = ((GEN5.getValueDatabase('172.16.94.48', 'LiTT', query, 'Value')) as Integer)

println(number)

NoId = (number + 1)

println(NoId)

query2 = "update dbo.Otosales set Value = $NoId where Parameters = 'Diagnosis Group'"

GEN5.updateValueDatabase('172.16.94.48', 'LiTT', query2)

NoId.toString()

DiagnosisId = ('Diagnosis DMW' + NoId)

WebUI.callTestCase(findTestCase('Pages/Web/Health/DiagnosisQuestion/AddDiagnosisQuestion'), [('DiagnosisGroup') : DiagnosisId //Input Diagnosis Group
        , ('Coverage') : 'By Diagnosis Question' //Set Coverage
        , ('AdditionalInfo') : 'Info' //Set Additional Info
        , ('AdditionalDocument') : 'Hasil CT Scan' //Set Additional Document
        , ('DiagnosisList') : 'Diagnosis Automation' //Set Diagnosis List
        , ('Question') : 'Question A' //Set Question
        , ('CoverageQuestionYes') : 'Covered' //Set Coverage Yes
        , ('CoverageQuestionNo') : 'Confirm' //Set Coverage No
        , ('CoverageQuestionUnknown') : 'Uncovered' //Set Coverage Unknown
        , ('QuestionAddInfo') : 'Info question' //Set Question Additional Info
        , ('QuestionAddDoc') : 'Billing Perawatan Asli' //Set Question Additional Document
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/DiagnosisQuestion/SearchDiagnosisGroup'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/DiagnosisQuestion/EditDiagnosisQuestionNull'), [('DiagnosisGroupNull') : '' //Input Diagnosis Group
        , ('DiagnosisGroupNew') : 'Diagnosis New' //Input Diagnosis Group
        , ('DiagnosisList') : 'Diagnosis Automation' //Set Diagnosis List
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/DiagnosisQuestion/SearchDiagnosisGroup'), [:], FailureHandling.STOP_ON_FAILURE)

query3 = 'SELECT TOP 1* FROM dbo.Diagnosis order by CreatedDate desc'

String Diagnosis = GEN5.getValueDatabase('172.16.94.70', 'SEA', query3, 'Name')

WebUI.callTestCase(findTestCase('Pages/Web/Health/DiagnosisQuestion/EditDiagnosisQuestion' //[('DiagnosisGroup') : DiagnosisId //Input Diagnosis Group
        ), [('Coverage') : 'By Diagnosis Question' //Set Coverage
        , ('AdditionalInfo') : 'Info Edited' //Set Additional Info
        , ('AdditionalDocument') : 'Hasil MRI' //Set Additional Document
        , ('DiagnosisList') : Diagnosis //Set Diagnosis List
        , ('Question') : 'Question B' //Set Question
        , ('CoverageQuestionYes') : 'Covered' //Set Coverage Yes
        , ('CoverageQuestionNo') : 'Confirm' //Set Coverage No
        , ('CoverageQuestionUnknown') : 'Uncovered' //Set Coverage Unknown
        , ('QuestionAddInfo') : 'Info question edited' //Set Question Additional Info
        , ('QuestionAddDoc') : 'Billing Perbandingan' //Set Question Additional Document
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/DiagnosisQuestion/SearchDiagnosisGroupNull'), [('DiagnosisGroupNull') : 'null' //Input Diagnosis Group 
	], FailureHandling.STOP_ON_FAILURE)

