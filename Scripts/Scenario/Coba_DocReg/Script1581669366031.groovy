import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Login
WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'),
	[
	('Username') : 'GHE',  			//Input UserName
	('Password'): 'Password95'      //Input Password
	]
, FailureHandling.STOP_ON_FAILURE)


//Pilih Menu
WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'),
	[
	('MenuHeader') : 'Membership',        //Pilih Menu Claim
	('SubMenu'): 'Inquiry'  //Pilih Menu Doc Reg
	]
, FailureHandling.STOP_ON_FAILURE)




//Pilih Doc Source
//WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Choose Document Source'),
//	[
//	('DocSource') : 'Client'
//	]
//, FailureHandling.STOP_ON_FAILURE)

//Logout
WebUI.callTestCase(findTestCase('Pages/Web/Health/LogOut/Logout'), [:], FailureHandling.STOP_ON_FAILURE)
