import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String Username = 'RJW'
String Password = 'Password95'
GlobalVariable.Tipe = 'TS39'
CustomKeywords.'healthKeyword.utilityDB.updateParam1'(GlobalVariable.Tipe)

String MasterDataQuery = "SELECT * FROM dbo.HealthDocumentReg WHERE isRUn = 0 AND Tipe='"+GlobalVariable.Tipe+"'"
def Counter = com.keyword.UI.getValueDatabase(GlobalVariable.LiTTURL, 'LiTT', MasterDataQuery,'Counter')
GlobalVariable.Counter = Counter

GlobalVariable.CurrentDate = new Date().format("dd/MMM/yyyy")
println (GlobalVariable.CurrentDate)

WebUI.callTestCase(findTestCase('Pages/Web/Health/General Usage/Check System Property'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Login/Login'),
	['Username':Username,
	 'Password':Password]
	, FailureHandling.STOP_ON_FAILURE)

WebUI.maximizeWindow()

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Home/Menu Product Import'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Product/View Product Plan'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Product/Edit Benefit List'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Product/Edit Premium'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/General Usage/Exit-Application'), [:], FailureHandling.STOP_ON_FAILURE)
