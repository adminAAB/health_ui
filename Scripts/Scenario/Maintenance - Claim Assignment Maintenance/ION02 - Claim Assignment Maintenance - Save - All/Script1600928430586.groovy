import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5


String Username

String Password

if (GEN5.verifyStaging()) {
	Username = 'ABF'

	Password = 'P@ssw0rd'
} else {
	Username = 'ABF'

	Password = 'ITG@nt1P455QC'
}

String UserID_ClaimAnalist = 'IDT'
String UserName_ClaimAnalist = 'Dini Putry Destianty'
String ClaimType = 'All'
String ProductType = 'All'
String PrepostClaim = 'All'
String HolistikProvider = 'All'
String OverseasProvider = 'All'
String Client = 'All'
String CancelSave = 'No'
String Action = 'Save' //diisi Save atau Back
String CheckSearch = 'No'
String UbahDoctor = 'No'
String NamaDoctor = ''
String UbahPICMedcare = 'No'
String PICMedcare = ''
String DeleteRow = 'Yes'

//Login
WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : Username //Input UserName
		, ('Password') : Password //Input Password
	], FailureHandling.STOP_ON_FAILURE)

//Pilih Menu
WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), [('MenuHeader') : 'Maintenance' //Pilih Menu Claim
		, ('SubMenu') : 'Claim Assignment' //Pilih Menu Claim Registration
	], FailureHandling.STOP_ON_FAILURE)

//Call Test Case Claim Assignment
WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Assignment Maintenance/Claim Assignment Maintenance'),
	[('UserID_ClaimAnalist') : UserID_ClaimAnalist,
	 ('UserName_ClaimAnalist') : UserName_ClaimAnalist,
	 ('ClaimType') : ClaimType,
	 ('ProductType') : ProductType,
	 ('PrepostClaim') : PrepostClaim,
	 ('HolistikProvider') : HolistikProvider,
	 ('OverseasProvider') : OverseasProvider,
	 ('Client') : Client,
	 ('CancelSave') : CancelSave,
	 ('Action') : Action,
	 ('CheckSearch') : CheckSearch,
	 ('UbahDoctor') : UbahDoctor,
	 ('NamaDoctor') : NamaDoctor,
	 ('UbahPICMedcare') : UbahPICMedcare,
	 ('PICMedcare') : PICMedcare,
	 ('DeleteRow') : DeleteRow
	], FailureHandling.STOP_ON_FAILURE)


//Logout
WebUI.callTestCase(findTestCase('Pages/Web/Health/LogOut/Logout'), [:], FailureHandling.STOP_ON_FAILURE)


