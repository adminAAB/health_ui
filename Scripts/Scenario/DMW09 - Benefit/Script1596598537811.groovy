import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'ABF' //Input UserName
        , ('Password') : 'ITG@nt1P455QC' //Input Password
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Home/MenuHeader', [('MenuHeader') : 'Product']), FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/ChooseMenu'), [('MenuHeader') : 'Product' //Pilih Menu
        , ('SubMenu') : 'Benefit' //Pilih Sub Menu
        , ('LabelHalaman') : 'Benefit Maintenance' //Assert Label Halaman
    ], FailureHandling.STOP_ON_FAILURE)

//ADD
query = 'select * from dbo.DataHealth where Type=\'ID Benefit\''

number = ((GEN5.getValueDatabase('172.16.94.48', 'LiTT', query, 'value')) as Integer)

println(number)

NoId = (number + 1)

println(NoId)

query2 = "update dbo.DataHealth set value = $NoId where Type = 'ID Benefit'"

GEN5.updateValueDatabase('172.16.94.48', 'LiTT', query2)

NoId.toString()

WebUI.callTestCase(findTestCase('Pages/Web/Health/Benefit/AddBenefit'), [('BenefitID') : 'Benefit ' + NoId //Set Benefit ID
        , ('BenefitName') : 'Benefit DMW ' + NoId //Set Benefit Name
        , ('Unit') : 'Per Tahun' //Set Unit
        , ('ProductType') : 'IP' //Set ProductType
        , ('Level') : 'SubBenefit' //Set Level
        , ('ParentBenefit') : 'AHM-DSP-01' //Set Parent Benefit
        , ('LimitBenefit') : '30' //Set Limit Benefit
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Benefit/AddBenefitNull'), [('Level') : 'SubBenefit' //Set Level
        , ('LimitBenefit') : '400' //Set Limit Benefit
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Benefit/SearchBenefit'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Benefit/EditBenefitNull'), [('BenefitNameNull') : '' //Set Benefit Name
        , ('UnitNull') : 'Please select one' //Set Unit
        , ('LevelNull') : 'Please select one' //Set Level
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Benefit/EditBenefit'), [('BenefitNameEdited') : ('Benefit DMW ' + NoId) + 
        'Edited' //Set Benefit Name
        , ('UnitEdited') : 'Per Kejadian' //Set Unit
        , ('ProductTypeEdited') : 'OP' //Set ProductType
        , ('LevelEdited') : 'SubBenefit' //Set Level
        , ('ParentBenefitEdited') : 'AHM-DSP-05' //Set Parent Benefit
        , ('LimitBenefitEdited') : '50' //Set Limit Benefit
    ], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Benefit/SearchBenefitNull'), [('BenefitNull') : 'Benefit Null'], FailureHandling.STOP_ON_FAILURE)

