import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String Username = 'ABF'
String Password = 'ITG@nt1P455QC'
GlobalVariable.Tipe = 'TS53'
CustomKeywords.'healthKeyword.utilityDB.updateParam1'(GlobalVariable.Tipe)
CustomKeywords.'healthKeyword.utilityDB.updateCounter'(GlobalVariable.Tipe)
CustomKeywords.'healthKeyword.utilityDB.updateParam4'(GlobalVariable.Counter)

WebUI.callTestCase(findTestCase('Pages/Web/Health/General Usage/Check System Property'), [:], FailureHandling.STOP_ON_FAILURE)

String MasterPolicyQuery = "SELECT * FROM dbo.HealthPolicyInfo WHERE isRUn = 0 AND Tipe='"+GlobalVariable.Tipe+"'"
def Counter = com.keyword.UI.getValueDatabase(GlobalVariable.LiTTURL, 'LiTT', MasterPolicyQuery,'Counter')

GlobalVariable.Counter = Counter

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Login/Login'),
	['Username':Username,
	 'Password':Password]
	, FailureHandling.STOP_ON_FAILURE)

WebUI.maximizeWindow()

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Home/Menu Policy'), [:],FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Create New Policy'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Import Classification'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/View Product'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Compare Policy Info'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Compare Classification'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Compare Product'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/General Usage/Exit-Application'), [:], FailureHandling.STOP_ON_FAILURE)