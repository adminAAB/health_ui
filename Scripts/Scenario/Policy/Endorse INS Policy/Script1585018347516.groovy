import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/General Usage/Check System Property'), [:],FailureHandling.STOP_ON_FAILURE)
String Username = 'ABF'
String Password = 'ITG@nt1P455QC'
GlobalVariable.Tipe = "TS55"
CustomKeywords.'healthKeyword.utilityDB.updateParam1'(GlobalVariable.Tipe)

String MasterPolicyQuery = "SELECT * FROM dbo.HealthPolicyInfo WHERE isRUn = 0 AND Tipe='"+GlobalVariable.Tipe+"'"
def idPolicy = com.keyword.UI.getValueDatabase(GlobalVariable.LiTTURL, 'LiTT', MasterPolicyQuery,'id')
CustomKeywords.'healthKeyword.utilityDB.updateParam2'(idPolicy)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Login/Login'),
	['Username':Username,
	 'Password':Password]
	, FailureHandling.STOP_ON_FAILURE)

WebUI.maximizeWindow()

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Home/Menu Policy'), [:],FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/View Policy Info'), [:],FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Endorsement Policy'), [:],FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Add Member Manual'), [:],FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Change Member Manual'), [:],FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Change Classification Member'), [:],FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Terminate Member'), [:],FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Calculate Policy'), [:],FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Compare Premium'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Approve Policy'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/General Usage/Exit-Application'), [:], FailureHandling.STOP_ON_FAILURE)
