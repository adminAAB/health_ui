import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), [('Username') : 'LUR', ('Password') : 'Password95'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), [('Parent') : 'Maintenance', ('Submenu') : 'Medical Treatment'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Medical Treatment/Medical Treatment'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Medical Treatment/Medical Treatment Details'), [('TreatmentNameENG') : 'Etherophobia'
        , ('TreatmentNameIND') : 'Both Sides'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Medical Treatment/Add Medical Treatment Diagnosis'), [('AddDiagnosis') : 'CHOLERA'
        , ('ODS') : 'No', ('CCApprovalLimit') : '500000', ('Coverage') : 'By Treatment Question', ('QuestionYes') : 'Confirm'
        , ('QuestionNo') : 'Covered', ('QuestionUnknown') : 'Uncovered', ('QuestionDescription') : 'Pertanyaan 1', ('QuestionAdditionalInfo') : 'Konfirm Dokter'
        , ('QuestionAdditionalDocuments') : 'Billing Perbandingan'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Medical Treatment/Edit Medical Treatment'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Medical Treatment/Edit Medical Treatment Details'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Medical Treatment/Edit Diagnosis'), [('Coverage') : 'Covered', ('AdditionalInfo') : 'Konfirmasi Kembali ke Dokter'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Exit-Application'), [:], FailureHandling.STOP_ON_FAILURE)

