import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.health.health as health

ArrayList randomDate = health.RandomDate()

//Scenario ini sudah mencakup NEW CLAIM - EDIT CLAIM - SELECT CLAIM
WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), 
	[('Username') : 'KAO', 								// Username Login 
	('Password') : 'Password95'], 						// Password Login
    FailureHandling.STOP_ON_FAILURE)

//ini adalah contoh dinamis Pilih Menu dan SubMenu
WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), 
	[('MenuHeader') : 'Claim', 
	('SubMenu') : 'Document Registration'], 
    FailureHandling.STOP_ON_FAILURE)

//Pilih Docsource Provider
WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Choose Document Source'), 
	[('DocSource') : 'Provider', 						// Choose Provider, Member or Client
    ('Provider') : 'MRCC', 								// Choose Hospital for Provider
	('EmpID') : '', 									// Write Employee ID or let it empty to skip
	('MemberName') : '', 								// Write Member Name or let it empty to skip
	('Client') : '', 									// Write Client or let it empty to skip
    ('MemberNo') : 'D/00060556'], 						// Write Member No or let it empty to skip
    FailureHandling.STOP_ON_FAILURE)

//Call testcase batch provider
WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Create Claim All'), 
	[('DeliveryNo') : 'ClaimProv', 						// Free Text
	('InvoiceNo') : 'ClaimProvMA', 						// Free Text
	('StartDate') : randomDate[0], 						// Random start date
	('EndDate') : randomDate[1],						// Random End Date
	('Diagnosis') : 'A00', 								// Diagnosis ex: A00 for Cholera
	('ProductType') : 'Maternity', 						// Option : Inpatient, Outpatient, Maternity etc
	('Billed') : '9500000', 							// Free text number
	('TreatmentRB') : true, 							// True to open Combobox Treatment RB Class
	('Room') : 'VIP', 									// Option : President Suite, Executive Suite, VIP Superior, VIP, Deluxe etc
	('RoomOpt') : true, 								// True to open Combobox Room Option
	('RoomOption') : 'NAPS'], 							// Choose RoomOption
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Save Claim'), 
	[('EmpID') : GlobalVariable.EmpID, 
	('MemberNo') : GlobalVariable.MemberNo, 
	('MemberName') : GlobalVariable.MemberName, 
	('Client') : GlobalVariable.Client, 
	('StartDate') : GlobalVariable.StartDate, 
	('EndDate') : GlobalVariable.EndDate, 
	('Diagnosis') : GlobalVariable.Diagnosis, 
	('ProductType') : GlobalVariable.ProductType, 
	('Billed') : GlobalVariable.Billed, 
	('Room') : GlobalVariable.Room, 
	('RoomOption') : GlobalVariable.RoomOpt], 
    FailureHandling.STOP_ON_FAILURE)

//Edit Claim
WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Edit Claim All'), 
	[('EmpID') : '', 									// Write to edit Employee ID or let it empty to skip
    ('MemberNo') : '', 									// Write to edit Member No or let it empty to skip
    ('MemberName') : '', 								// Write to edit Member Name or let it empty to skip
    ('Client') : '', 									// Write to edit Client or let it empty to skip
    ('Billed') : '12500000', 							// Write to edit edited Bill, stay empty to skip
	('DeliveryNo') : '', 								// Write to edit Delivery No, stay empty to skip
    ('StartDate') : '',									// edit start date
    ('EndDate') : '', 									// edit End Date
    ('Diagnosis') : '', 								// Edit Diagnosis
    ('ProductType') : '', 								// Edit product Type
    ('Room') : '', 										// Edit Room
    ('RoomOption') : ''], 								// Edit RoomOption
    FailureHandling.STOP_ON_FAILURE)

//Select Claim
WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Select Claim All'), 
	[('DocSource') : GlobalVariable.DocSource,
    ('Provider') : GlobalVariable.Provider, 
	('EmpID') : GlobalVariable.EmpID, 
	('MemberNo') : GlobalVariable.MemberNo,
    ('MemberName') : GlobalVariable.MemberName, 
	('Client') : GlobalVariable.Client,
	('GLNo') : '', 										// Search using GLNo, skip if it's empty
    ('DeliveryNo') : GlobalVariable.DeliveryNo, 
	('InvoiceNo') : GlobalVariable.InvoiceNo, 
	('Type') : 'DocNo MA - DocSource Provider'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

