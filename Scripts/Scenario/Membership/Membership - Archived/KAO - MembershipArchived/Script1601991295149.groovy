import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

//Login
WebUI.callTestCase(findTestCase('Pages/Web/Health/Login/Login'), 
	[('Username') : 'KAO' // Username Login
        , ('Password') : 'ITG@nt1P455QC'] // Password Login
    , FailureHandling.STOP_ON_FAILURE)

//ini adalah contoh dinamis Pilih Menu dan SubMenu
WebUI.callTestCase(findTestCase('Pages/Web/Health/Home/Menu'), 
	[('MenuHeader') : 'Membership', 
	 ('SubMenu') : 'Membership - Archived'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Membership/Membership - Archived/Search Member'), 
	[('ClientName') : 'Denso Indonesia' //Set CLient Name
        , ('EmployeeID') : '' //Set Employee ID
        , ('MemberName') : 'ADITYO PRASETYA' //Set Member Name
        , ('MemberNo') : ''] //Set Member No
    , FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Membership/Membership - Archived/Restore Member'), 
	[:], FailureHandling.STOP_ON_FAILURE)

