import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

GlobalVariable.UserId = UserId

WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/ButtonAddUser'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldUserId'), UserId, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldUsername'), Username, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldEmail'), Email, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldAddUserType'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldAddTextUserType'), UserType, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldAddTextUserType'), Keys.chord(Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldTitle'), Title, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/ButtonBrowse'), FailureHandling.STOP_ON_FAILURE)

GEN5.UploadFile2(filename)

WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/ButtonAddRole'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldAddRole'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldAddTextRole'), Role, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldAddTextRole'), Keys.chord(Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/ButtonSaveRole'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/ButtonSave'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

String query = "select * from sysuser where id='"+GlobalVariable.UserId+"'"

String dbname = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Name")

String dbemail = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Email")

String dbtitle = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Title")

String query2 = "SELECT su.Name,SU.Type,SUT.Description FROM dbo.SysUser SU INNER JOIN dbo.SysUserType SUT ON SUT.Type = SU.Type WHERE SU.ID = '"+GlobalVariable.UserId+"'"

String dbusertype = GEN5.getValueDatabase("172.16.94.70", "SEA", query2, "Description")

String query3 = "SELECT UAR.RoleID, R.RoleName, UAR.RowStatus AS IsDeleted FROM General.UserApplicationRoles UAR INNER JOIN General.Roles R ON R.RoleID = UAR.RoleID WHERE UAR.UserID = '"+GlobalVariable.UserId+"'"

String dbrole = GEN5.getValueDatabase("172.16.94.74", "a2isAuthorizationDB", query3, "RoleName")

if ((Username == dbname) && (Email == dbemail) && (Title == dbtitle) && (UserType == dbusertype) && (Role == dbrole) ){
	KeywordUtil.markPassed("PASSED")
	}
else {
	KeywordUtil.markFailedAndStop("FAILED")
	}