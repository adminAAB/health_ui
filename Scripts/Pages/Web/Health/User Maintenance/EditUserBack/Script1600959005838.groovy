import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import org.openqa.selenium.Keys as Keys
import com.keyword.GEN5

WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/EditUser/UserSelected', [('UsernameOrId'): GlobalVariable.UsernameOrId]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/EditUser/ButtonEdit'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldUsername'), FailureHandling.STOP_ON_FAILURE)

if ((Username != null) || (Email != null) || (UserType != null) || (Title != null) || (filename != null) || (Role != null)){
	WebUI.setText(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldUsername'), Username, FailureHandling.STOP_ON_FAILURE)

	WebUI.setText(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldEmail'), Email, FailureHandling.STOP_ON_FAILURE)

	WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldAddUserType'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldAddTextUserType'), UserType, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.sendKeys(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldAddTextUserType'), Keys.chord(Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

	WebUI.setText(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldTitle'), Title, FailureHandling.STOP_ON_FAILURE)

	WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/ButtonBrowse'), FailureHandling.STOP_ON_FAILURE)
	
	GEN5.UploadFile2(filename)

	WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/EditUser/RoleSelected', [('Role'): Role]), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/EditUser/ButtonDeleteRole'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/ButtonAddRole'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldAddRole'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldAddTextRole'), RoleEdited, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.sendKeys(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/FieldAddTextRole'), Keys.chord(Keys.ENTER), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/ButtonSaveRole'), FailureHandling.STOP_ON_FAILURE)
}

WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/AddUser/ButtonBack'), FailureHandling.STOP_ON_FAILURE)