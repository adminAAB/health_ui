import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

GlobalVariable.UsernameOrId = UsernameOrId

WebUI.waitForElementPresent(findTestObject('Web/Health/User Maintenance/SearchUser/FieldSearchUsernameOrId'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Web/Health/User Maintenance/SearchUser/FieldSearchUsernameOrId'), UsernameOrId, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/User Maintenance/SearchUser/FieldSearchUserType'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Web/Health/User Maintenance/SearchUser/FieldTextUserType'), UserType, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Web/Health/User Maintenance/SearchUser/FieldTextUserType'), Keys.chord(Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/User Maintenance/ButtonSearch'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Web/Health/User Maintenance/SearchUser/UsernameOrId', [('UsernameOrId'):UsernameOrId] ), 0, FailureHandling.STOP_ON_FAILURE)