import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebUI.uploadFile(findTestObject('Web/Health/Product/Import/Btn_UploadAgeTable'), 'C:\\UploadA2ISHealth\\Age_Table.csv')

WebUI.uploadFile(findTestObject('Web/Health/Product/Import/Btn_UploadProductInfo'), 'C:\\UploadA2ISHealth\\Product_Info.csv')

WebUI.uploadFile(findTestObject('Web/Health/Product/Import/Btn_UploadBenefitList'), 'C:\\UploadA2ISHealth\\Benefit_List.csv')

WebUI.click(findTestObject('Web/Health/Product/Import/Btn_SubmitUpload'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

//WebUI.click(findTestObject('Web/Health/Product/Import/Btn_DownloadImportedProduct'))
//WebUI.delay(1)
//CustomKeywords.'healthKeyword.general.AllowMultipleFileDownload'()
//
//WebUI.delay(10)
WebUI.click(findTestObject('Web/Health/Product/Import/Btn_CloseImport'))
WebUI.delay(1)