import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def productPlan

if (GlobalVariable.ProductPlan!=""){
	 productPlan = GlobalVariable.ProductPlan
}else{
	 productPlan = 'HS-AUTOMATE-1300-'+GlobalVariable.Counter
}
WebUI.click(findTestObject('Web/Health/Product/Product Plan/Tab_ProductPlan'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebUI.setText(findTestObject('Web/Health/Product/Product Plan/Txt_ProductPlan'), productPlan)

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Btn_SearchProductPlan'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Lst_ProductPlan',[('key'):productPlan]))

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Btn_ExportProductPLan'))
WebUI.delay(2)