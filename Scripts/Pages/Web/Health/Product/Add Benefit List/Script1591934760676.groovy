import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


def benefitID = 'HS02'
GlobalVariable.TempBenefitID = benefitID
def seqNo = '2'
def occurenceAmount = '40000000'
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Product/Product Plan/Tab_BenefitList'))
WebUI.delay(2)

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Btn_AddBenefitList'))
WebUI.delay(2)
WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Exp_BenefitId'))
WebUI.delay(1)
CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Health/Product/Product Plan/Benefit List/Txt_SearchBenefitId'), benefitID)
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Btn_SearchBenefitId'))
WebUI.delay(1)
CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Lst_BenefitId',[('key'):benefitID]))
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Btn_SelectLookupBenefit'))
WebUI.delay(2)
WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Txt_SeqNo'))
WebUI.delay(1)
WebUI.sendKeys(findTestObject('Web/Health/Product/Product Plan/Benefit List/Txt_SeqNo'), benefitID)
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Txt_OccurenceAmount'))
WebUI.delay(1)
WebUI.sendKeys(findTestObject('Web/Health/Product/Product Plan/Benefit List/Txt_OccurenceAmount'), occurenceAmount)
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Btn_SaveEditBenefit'))
WebUI.delay(1)

