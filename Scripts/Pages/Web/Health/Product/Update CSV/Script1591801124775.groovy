import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

GlobalVariable.Tipe = 'TS39'
CustomKeywords.'healthKeyword.utilityDB.updateParam1'(GlobalVariable.Tipe)
CustomKeywords.'healthKeyword.utilityDB.updateCounter'(GlobalVariable.Tipe)
CustomKeywords.'healthKeyword.utilityDB.updateParam4'(GlobalVariable.Counter)

String MasterDataQuery = "SELECT * FROM dbo.HealthDocumentReg WHERE isRUn = 0 AND Tipe='"+GlobalVariable.Tipe+"'"
def Counter = com.keyword.UI.getValueDatabase(GlobalVariable.LiTTURL, 'LiTT', MasterDataQuery,'Counter')
GlobalVariable.Counter = Counter
//WebUI.callTestCase(findTestCase('Test Cases/General/Check System Property'), [:], FailureHandling.STOP_ON_FAILURE)
println GlobalVariable.SystemUser
//CustomKeywords.'csvThing.productInfo.csvSum'()
//CustomKeywords.'csvThing.benefitList.csvSum'()
//CustomKeywords.'csvThing.ageTable.csvSum'()

CustomKeywords.'csvThing.CSVProcessor.csvSumProductInfo'()
CustomKeywords.'csvThing.CSVProcessor.csvSumBenefitList'()
CustomKeywords.'csvThing.CSVProcessor.csvSumAgeTable'()

println ("product plannya segini "+GlobalVariable.ProductPlan)
CustomKeywords.'healthKeyword.utilityDB.updateParam5'(GlobalVariable.ProductPlan)