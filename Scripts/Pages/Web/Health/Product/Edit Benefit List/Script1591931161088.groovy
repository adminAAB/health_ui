import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


WebUI.click(findTestObject('Web/Health/Product/Product Plan/Tab_BenefitList'))

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Lst_BenefitList',[('key'):'Hospital Room and Board']))

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Btn_EditBenefitList'))

WebUI.clearText(findTestObject('Web/Health/Product/Product Plan/Benefit List/Txt_OccurenceAmount'))

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Txt_OccurenceAmount'))

WebUI.sendKeys(findTestObject('Web/Health/Product/Product Plan/Benefit List/Txt_OccurenceAmount'), '2000000')

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Btn_SaveEditBenefit'))

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Tab_BenefitList'))

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Lst_BenefitList',[('key'):'Miscellaneous Hospital Services']))

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Btn_EditBenefitList'))

WebUI.clearText(findTestObject('Web/Health/Product/Product Plan/Benefit List/Txt_OccurenceAmount'))

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Txt_OccurenceAmount'))

WebUI.sendKeys(findTestObject('Web/Health/Product/Product Plan/Benefit List/Txt_OccurenceAmount'), '2000000')

WebUI.click(findTestObject('Web/Health/Product/Product Plan/Benefit List/Btn_SaveEditBenefit'))