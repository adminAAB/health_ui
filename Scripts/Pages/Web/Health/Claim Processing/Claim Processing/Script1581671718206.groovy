import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

GEN5.ProcessingCommand()

//////Input Doc No//////
String DocNo

if (GEN5.verifyStaging())
{
	DocNo = GEN5.getValueDatabase("172.16.94.48", "LiTT", "SELECT TOP 1 * FROM DataHealth_Staging WHERE ApplicationName='Gen5Health' AND RowStatus=0 AND Type='"+DocNoType+"' order by id asc", "Value")
}
else
{
	DocNo = GEN5.getValueDatabase("172.16.94.48", "LiTT", "SELECT TOP 1 * FROM DataHealth WHERE ApplicationName='Gen5Health' AND RowStatus=0 AND Type='"+DocNoType+"' order by id asc", "Value")
}



WebUI.setText(findTestObject('Web/Health/Claim Processing/Txt_DocumentNo'), DocNo)

//////Klik Button Search//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Btn_Search')) 

GEN5.ProcessingCommand()

//// Awal CekGridDepan
if (CekGridDepan=='1') 
{

	//////Compare Grid Depan//////
	
ArrayList Row_Grid = GEN5.getAllRowsValue(findTestObject('Object Repository/Web/Health/Claim Processing/Grid_Claim'), "Document No",DocNo)

String Billed = Row_Grid[16].replace('Rp. ','').replace(',','')
Row_Grid.set(16, Billed)

String DocNo_Array = Row_Grid[1].trim()
Row_Grid.set(1, DocNo_Array)

String CNO_Array = Row_Grid[2].trim()
Row_Grid.set(2, CNO_Array)

String ClaimNo_Array = Row_Grid[3].trim()
Row_Grid.set(3, ClaimNo_Array)

String Source_Array = Row_Grid[4].trim()
Row_Grid.set(4, Source_Array)

String AgingTreatmentFinish_Array = Row_Grid[5].trim()
Row_Grid.set(5, AgingTreatmentFinish_Array)

String AgingRegisterDocument_Array = Row_Grid[6].trim()
Row_Grid.set(6, AgingRegisterDocument_Array)

String MemberName_Array = Row_Grid[7].trim()
Row_Grid.set(7, MemberName_Array)

String MemberNo_Array = Row_Grid[8].trim()
Row_Grid.set(8, MemberNo_Array)

String Client_Array = Row_Grid[9].trim()
Row_Grid.set(9, Client_Array)

String ProviderName_Array = Row_Grid[10].trim()
Row_Grid.set(10, ProviderName_Array)

String ClaimType_Array = Row_Grid[11].trim()
Row_Grid.set(11, ClaimType_Array)

String TreatmentStart_Array = Row_Grid[12].trim()
Row_Grid.set(12, TreatmentStart_Array)

String TreatmentFinish_Array = Row_Grid[13].trim()
Row_Grid.set(13, TreatmentFinish_Array)

String Status_Array = Row_Grid[14].trim()
Row_Grid.set(14, Status_Array)

String ProductType_Array = Row_Grid[15].trim()
Row_Grid.set(15, ProductType_Array)

String Billed_Array = Row_Grid[16].trim()
Row_Grid.set(16, Billed_Array)

String HospitalInv_Array = Row_Grid[17].trim()
Row_Grid.set(17, HospitalInv_Array)

String Receiver_Array = Row_Grid[18].trim()
Row_Grid.set(18, Receiver_Array)

String Query_Grid_Claim_TDRD = "SELECT '',tdrd.DocumentNo, 0 AS CNO, '' AS ClaimNo, CASE WHEN tdr.DocumentSource = 'P' THEN 'Provider' WHEN tdr.DocumentSource = 'C' THEN 'Client' WHEN tdr.DocumentSource = 'M' THEN 'Member' END AS SOURCE, DATEDIFF(DAY, TDRD.TreatmentEnd, GETDATE()) AS AgingTreatmentFinish, DATEDIFF(DAY, CompleteDate, GETDATE()) AS AgingRegisterDocument, pm.Name AS MemberName, pm.MemberNo AS MemberNo, p2.Name AS Client, tdrd.TreatmentPlace, CASE WHEN tdrd.ClaimType = 'C' THEN 'Cashless' WHEN tdrd.ClaimType = 'R' THEN 'Reimburse' END AS ClaimType, FORMAT (CAST(TDRD.TreatmentStart AS DATE), 'dd/MMM/yyyy') AS TDRD_TreatmentStart, FORMAT (CAST(TDRD.TreatmentEnd AS DATE), 'dd/MMM/yyyy') AS TDRD_TreatmentEnd, CASE WHEN tdrd.Status = 'SEND' THEN 'Document Sent' WHEN tdrd.Status = 'REGISTERED' THEN 'Document Registered' END AS STATUS, CASE WHEN tdrd.ProductType = 'IP' THEN 'Inpatient' WHEN tdrd.ProductType = 'OP' THEN 'Outpatient' WHEN tdrd.ProductType = 'MA' THEN 'Maternity' END AS ProductType, tdrd.TotalBill AS Billed, tdr.RefNo AS HosInvNo, su.name AS Recevier FROM dbo.tbl_disp_registration_document AS tdrd INNER JOIN dbo.tbl_disp_registration AS tdr ON tdr.RegistrationNo = tdrd.RegistrationNo INNER JOIN dbo.Policy_Member AS pm ON tdrd.MNO=pm.MNo INNER JOIN dbo.Policy AS p ON pm.PNO=p.PNO LEFT JOIN dbo.Profile AS p2 ON p.ClientID=p2.ID LEFT JOIN dbo.SysUser AS su ON su.ID=tdrd.Receiver WHERE tdrd.DocumentNo="+DocNo

GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_Grid_Claim_TDRD, Row_Grid)
} //// Akhir CekGridDepan

//////Klik Checkbox Select Claim di Grid//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Chk_SelectClaim'))
GEN5.ProcessingCommand()

//////Klik Button Re-Assign dan Re-Assign Claim//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Btn_Reassign'))
GEN5.ProcessingCommand()
WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Processing/PopUp Reassign/Txt_User'), UsernameReassign)
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/PopUp Reassign/Btn_Search'))

GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/PopUp Reassign/FirstRowGrid_UserReasign'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/PopUp Reassign/Btn_Select'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/PopUp Reassign/Btn_X'))


//////Klik Checkbox Select Claim di Grid//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Chk_SelectClaim'))
GEN5.ProcessingCommand()

//////Klik Button Edit//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Btn_Edit'))
GEN5.ProcessingCommand()


//// Awal CompareTDRD
if (CompareTDRD=='1')
{
	
	//////Tampung dan Compare dengan TDRD untuk field Claim Info yang selalu ada di Cashless dan Reimburse//////
	//////yg belum di compare krn blm tau querynya :
	////  -R&B Scheme
	////  -RB Limit
	
ArrayList ClaimInfo1 = [findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_DocumentNo'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_RegistrationNo'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_BoxNo'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_RegID'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_DocumentSource'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_ClaimStatus'), 
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_CreateDateBy'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_LastUpdateBy'), 
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_MemberName'), 
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_MemberNo'), 
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentPeriodStart'), 
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentPeriodEnd'), 
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_ClaimType'), 
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_ProviderName'), 
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_ProductType'), 
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentRBClass'), 
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentRBRate'), 
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_RoomOption'),			
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_PayTo'), 
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AccountNo'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AccountName'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AccountBank'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_BankBranch'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreamentRemarks'),
						]

ArrayList Field = GEN5.getFieldsValue(ClaimInfo1)

String Txt_TreatmentRBRate_Array = Field[16].replace(',','')
Field.set(16, Txt_TreatmentRBRate_Array)

String Txt_AccountName_Array = Field[20].trim()
Field.set(20, Txt_AccountName_Array)

String Txt_BankBranch = Field[22].trim()
Field.set(22, Txt_BankBranch)

String Query_ClaimInfo_TDRD = "SELECT tdrd.DocumentNo AS DocumentNo, tdr.RegistrationNo AS RegistrationNo, tdrd.BoxNo AS BoxNo, tdrd.RefBatchNo AS RegID, CASE WHEN tdr.DocumentSource = 'P' THEN 'Provider'  WHEN tdr.DocumentSource = 'C' THEN 'Client'  WHEN tdr.DocumentSource = 'M' THEN 'Member' END AS SOURCE, CASE WHEN tdrd.Status = 'SEND' THEN 'Document Sent' END AS STATUS, CONCAT (FORMAT (CAST(tdrd.CDate AS DATE), 'dd/MMM/yyyy'),'  /  ', tdrd.CUser) AS CreatedDateBy, CONCAT (FORMAT (CAST(tdrd.LDate AS DATE), 'dd/MMM/yyyy'),'  /  ', tdrd.LUser) AS ModifiedBy, pm.Name AS MemberName, pm.MemberNo AS MemberNo, FORMAT (CAST(TDRD.TreatmentStart AS DATE), 'dd/MMM/yyyy') AS TDRD_TreatmentStart, FORMAT (CAST(TDRD.TreatmentEnd AS DATE), 'dd/MMM/yyyy') AS TDRD_TreatmentEnd, CASE WHEN tdrd.ClaimType = 'C' THEN 'Cashless'  WHEN tdrd.ClaimType = 'R' THEN 'Reimburse' END AS ClaimType, tdrd.TreatmentPlace AS ProviderName,  CASE WHEN tdrd.ProductType = 'IP' THEN 'Inpatient'  WHEN tdrd.ProductType = 'OP' THEN 'Outpatient'  WHEN tdrd.ProductType = 'MA' THEN 'Maternity' END AS ProductType, COALESCE(tdrd.TreatmentRBClassDisp, '') AS TreatmentRBClass, CASE WHEN COALESCE(tdrd.TreatMentRBRateDisp, 0) = 0 THEN '' ELSE CAST(tdrd.TreatMentRBRateDisp AS VARCHAR(MAX)) END AS TreatmentRBRate , CASE WHEN tdrd.RoomOption='ONPLAN' THEN 'On Plan' WHEN tdrd.RoomOption='NAPS' THEN 'NAPS' WHEN tdrd.RoomOption='APS' THEN 'APS' ELSE '' END AS RoomOption,   CASE WHEN tdrd.PayToDisp = 'P' THEN 'Provider'  WHEN tdrd.PayToDisp = 'M' THEN 'Member'  WHEN tdrd.PayToDisp = 'C' THEN 'Client' END AS PayTo, tdrd.AccountNoDisp AS AccountNo, tdrd.BankAccountDisp AS AccountName, tdrd.BankNameDisp AS AccountBank, ISNULL( tdrd.BankBranchDisp,'' ) AS BankBranch, tdrd.Remarks AS TreatmentRemarks FROM dbo.tbl_disp_registration_document AS tdrd INNER JOIN dbo.tbl_disp_registration AS tdr ON tdr.RegistrationNo = tdrd.RegistrationNo INNER JOIN dbo.Policy_Member AS pm ON tdrd.MNO=pm.MNo INNER JOIN dbo.Policy AS p ON pm.PNO=p.PNO LEFT JOIN dbo.Profile AS p2 ON p.ClientID=p2.ID LEFT JOIN dbo.SysUser AS su ON su.ID=tdrd.Receiver WHERE tdrd.DocumentNo="+DocNo 
GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_ClaimInfo_TDRD, Field)


//////Tampung dan Compare dengan DB untuk field RBLimit//////  --> dipisah dari array diatas karena querynya beda  //sini-1
ArrayList ClaimInfo1c = [findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_RBLimit')]
ArrayList Field1C = GEN5.getFieldsValue(ClaimInfo1c)

String Txt_RBLimit_Array = Field1C[0].replace('Rp. ','').replace(',','').replace('.00', '')
Field1C.set(0, Txt_RBLimit_Array)

String Txt_Memberno_c = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_MemberNo'),'value', FailureHandling.STOP_ON_FAILURE)  
String Txt_TreatmentStart_c = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentPeriodStart'), 'value', FailureHandling.STOP_ON_FAILURE)



String Query_ClaimInfo_RBLimit = "SELECT TOP 1 CAST(PBS2.AmountLimit AS INT) FROM Policy P2 INNER JOIN Policy_Member PM2 ON P2.PNO = PM2.PNO INNER JOIN Policy_Member_Plan PMP2 ON PM2.PNO = PMP2.PNO AND PM2.MNO = PMP2.MNO AND PMP2.ExcludeF = 0 INNER JOIN Policy_Benefit_Schedule PBS2 ON PMP2.PNO = PBS2.PNO AND PMP2.PPlan = PBS2.PPlan AND PMP2.BAmount = PBS2.BAmount INNER JOIN Benefit B ON PBS2.BenefitID = B.BenefitID AND B.IsRoomAndBoard = 1 WHERE PM2.TType <> 'R' AND PM2.MemberNo = '"+Txt_Memberno_c+"' AND CAST('"+Txt_TreatmentStart_c+"' AS DATE) BETWEEN CAST(PM2.PDate AS DATE) AND CAST(PM2.PPDate AS DATE) ORDER BY PM2.MNo DESC"
GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_ClaimInfo_RBLimit, Field1C)

//////Tampung dan Compare dengan TDRD untuk field yang dinamis antara ClaimType cashless dan reimburse//////
String Txt_ClaimType_Array = Field[12]
if (Txt_ClaimType_Array=='Cashless')
{
	////  -COB BPJS (cashless)
	////  -Perawatan Khusus (R. Isolasi, HCU, ICU, PICU, NICU (Cashless)
	////  -Traffic Accident (Cashless)
	
	ArrayList ClaimInfo1A = [findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_CobBpjs'),
							 findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_PerawatanKhusus'),
							 findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_TrafficAccident')]

	ArrayList result1A = new ArrayList()
	 
	for (int a = 0 ; a < ClaimInfo1A.size() ; a++) {
		boolean checkbox = WebUI.verifyElementChecked(ClaimInfo1A[a], 1, FailureHandling.OPTIONAL)
		
		if (checkbox) {
			result1A.add('1')
		} else {
			result1A.add('0')
		}
	}

		
	String Query_ClaimInfo_TDRD_Flag = "SELECT CASE WHEN COALESCE(tdrd.isCobBPJS, 0) = 0 THEN '0' ELSE CAST(tdrd.isCobBPJS AS VARCHAR(MAX)) END AS isCobBPJS , CASE WHEN COALESCE(tdrd.isSpecialTreatment, 0) = 0 THEN '0' ELSE CAST(tdrd.isSpecialTreatment AS VARCHAR(MAX)) END AS isSpecialTreatment , CASE WHEN COALESCE(tdrd.isTrafficAccident, 0) = 0 THEN '0' ELSE CAST(tdrd.isTrafficAccident AS VARCHAR(MAX)) END AS isTrafficAccident FROM dbo.tbl_disp_registration_document AS tdrd WHERE DocumentNo="+DocNo
		
	GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_ClaimInfo_TDRD_Flag, result1A)
	
}
else if (Txt_ClaimType_Array=='Reimburse')
{
	////  -Cashplan BPJS (reimburse)
	////  -Double Insured (reimburse)
	////  -Perawatan Khusus (R. Isolasi, HCU, ICU, PICU, NICU (reimburse)
	////  -Pasien Meniggal (reimburse)
	////  -Traffic Accident (reimburse)
	
	ArrayList ClaimInfo1A = [findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_CashPlanBpjs'),
							 findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_DoubleInsured'),
							 findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_PerawatanKhusus'),
							 findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_PasienMeniggal'),
							 findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_TrafficAccident')
							]
	
	ArrayList result1A = new ArrayList()
	
	
	for (int a = 0 ; a < ClaimInfo1A.size() ; a++) {
		boolean checkbox = WebUI.verifyElementChecked(ClaimInfo1A[a], 1, FailureHandling.OPTIONAL)
		
		if (checkbox) {
			result1A.add('1')
		} else {
			result1A.add('0')
		}
	}

		
	String Query_ClaimInfo_TDRD_Flag = "SELECT CASE WHEN COALESCE(tdrd.isCashPlanBPJS, 0) = 0 THEN '0' ELSE CAST(tdrd.isCashPlanBPJS AS VARCHAR(MAX)) END AS isCashPlanBPJS , CASE WHEN COALESCE(tdrd.isDoubleInsured, 0) = 0 THEN '0' ELSE CAST(tdrd.isDoubleInsured AS VARCHAR(MAX)) END AS isDoubleInsured , CASE WHEN COALESCE(tdrd.isSpecialTreatment, 0) = 0 THEN '0' ELSE CAST(tdrd.isSpecialTreatment AS VARCHAR(MAX)) END AS isSpecialTreatment , CASE WHEN COALESCE(tdrd.isDeceased, 0) = 0 THEN '0'  ELSE CAST(tdrd.isDeceased AS VARCHAR(MAX)) END AS isDeceased , CASE WHEN COALESCE(tdrd.isTrafficAccident, 0) = 0 THEN '0'  ELSE CAST(tdrd.isTrafficAccident AS VARCHAR(MAX)) END AS isTrafficAccident FROM dbo.tbl_disp_registration_document AS tdrd WHERE DocumentNo="+DocNo
	
	GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_ClaimInfo_TDRD_Flag, result1A)
	
}


//////Tampung dan Compare dengan table ClaimAdditionalInfo untuk Additional Info//////
////  urutan get dr UI dibawah ini harus sama dengan urutan di query (by AdditionalInfoID)
///   CLAB, CMRI, CPA, CRONT, CTSCN, CUSG 

ArrayList ClaimInfo1B = [findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_PemeriksaanLab'),
						 findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_MRI'),
						 findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_PA'),
						 findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_Rotgen'),
						 findTestObject('Web/Health/Claim Processing/Tab Claim Info/Chk_SuratRujukanInhouseClinic'),
						 findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_CtScan'),
						 findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Chk_USG')
   						]


ArrayList result1B = new ArrayList()

for (int a = 0 ; a < ClaimInfo1B.size() ; a++) {
	boolean checkbox = WebUI.verifyElementChecked(ClaimInfo1B[a], 1, FailureHandling.OPTIONAL)
	
	if (checkbox) {
		result1B.add('1')
	} else {
		result1B.add('0')
	}
}


//diperbaiki yah
//String Query_ClaimInfo_TDRD_AdditionalInfo = "SELECT Status FROM dbo.ClaimAdditionalInfo AS cai WHERE DocumentNo="+DocNo+" ORDER BY AdditionalInfoID"

//ArrayList database2 = GEN5.getOneColumnDatabase("172.16.94.70", "SEA",  Query_ClaimInfo_TDRD_AdditionalInfo,   "Status")


//for (int a = 0 ; a < database2.size() ; a++) {
//	if (database2[a]=='true') {
//		database2.set(a,'1')
//	} else {
//		database2.set(a,'0')
//	}
//}


//int i
//for (i = 0 ; i < result1B.size() ; i++) {
//	if (database2[i] == result1B[i]) {
//		KeywordUtil.markPassed("Value " + result1B[i] +" from Array same with Database.")
//	} else {
//		KeywordUtil.markFailedAndStop("Value from Array = " + result1B[i] + " has different Value from database = " + database2[i])
//	}
//}

WebUI.delay(10)

} //// Akhir CompareTDRD




////Cek Value 'Doctor' dan Isi jika 'Doctor' kosong//////
String Value_Doctor = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_Doctor'), 'value')
if ((Value_Doctor == "") || (Value_Doctor == null)) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_Doctor'))
	GEN5.ProcessingCommand()
	
	boolean checkbox_SelectedProvider = WebUI.verifyElementChecked(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/PopUp Doctor/Chk_SelectedProviderOnly'), 1, FailureHandling.OPTIONAL)
	
	if (checkbox_SelectedProvider) {
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/PopUp Doctor/Chk_SelectedProviderOnly'))
		GEN5.ProcessingCommand()
	} 

	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/PopUp Doctor/Txt_DoctorName'), DoctorName)
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/PopUp Doctor/Btn_Search'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/PopUp Doctor/FirstRowGrid_Doctor'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/PopUp Doctor/Btn_Select'))
	GEN5.ProcessingCommand()
}



////// disini
//////jika Product Type = IP dan tgl treatment > 1 day dan Appropriate kosong maka pilih room paling atas//////
	String Value_ProductType = WebUI.getText(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_ProductType'))
	println Value_ProductType
	
	String Value_TreatmentStart = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentPeriodStart'), 'value')
	println Value_TreatmentStart
	
	String Value_TreatmentEnd = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentPeriodEnd'), 'value')
	println Value_TreatmentEnd
	
	String Value_Appropriate = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AppropriateRBClass'), 'value')
	println Value_Appropriate

	if (Value_ProductType=='Inpatient' && Value_TreatmentStart != Value_TreatmentEnd && Value_Appropriate=='')
    {
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_AppropriateRBClass'))
		GEN5.ProcessingCommand()
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Pop Up Appropriate RB Class/Row_1_Grid_ProviderRoomList'))
		GEN5.ProcessingCommand()
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Pop Up Appropriate RB Class/Btn_Select'))
	
		String Value_Appropriate2 = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AppropriateRBClass'), 'value')
		println Value_Appropriate2
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_TreatmentRBClass'))
		GEN5.ProcessingCommand()
		GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Pop Up Treatment RB Class/Grid Provider Room List'), 'Room Type', Value_Appropriate2)
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Pop Up Treatment RB Class/Btn_Select'))
		GEN5.ProcessingCommand()
	}
	if ((Value_ProductType=='Inpatient') && (Value_TreatmentStart != Value_TreatmentEnd) && (Value_Appropriate!=''))
    {
		println Value_Appropriate
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_TreatmentRBClass'))
		GEN5.ProcessingCommand()
		GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Pop Up Treatment RB Class/Grid Provider Room List'), 'Room Type', Value_Appropriate)
		GEN5.ProcessingCommand()
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Pop Up Treatment RB Class/Btn_Select'))
		GEN5.ProcessingCommand()
	}

	
	//////Klik Tab Claim Details////// --untuk test document centang dokumen yg ga kecentang, billed nya diganti dulu jadi 2000, doc no 1122120--
	//WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Claim Processing/Lbl_JudulClaim'), 2)
	//WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_CallHistory'))
	//WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_ClaimDetails'))



	//////Isi Billed HS01)////// --untuk test document centang dokumen yg ga kecentang, billed nya diganti dulu jadi 2000, doc no 1122120--
	//float Billed_HS01_A = Billed_HS01
	//WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Details/Txt_Billed'))
	//WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Details/Txt_Billed'), Billed_HS011)
	//WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Details/Grid_BenefitLimit'))

	
	
	//////Klik Tab Document////// --untuk test document--
	//WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Claim Processing/Lbl_JudulClaim'), 2)
	//WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_CallHistory'))
	//WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_ClaimDocuments'))
	
	//GEN5.tickAllUncheckedCheckboxInTable(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Documents/Grid_Documents'))
	
	
	//////Klik Tab Claim Info////// --untuk test document--
	//WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_ClaimInfo'))
	
	
	

//////Cek Value 'Remarks' dan Isi jika 'Remarks' kosong//////
String Value_Remarks = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_Remarks'), 'value')
if ((Value_Remarks == "") || (Value_Remarks == null)) {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_Remarks'), Remarks)	
}



//// Isi 'Medical Treatment'//////
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_Add_MedicalTreatment'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/PopUp_Add_MedicalTreatment/Txt_MedicalTreatment'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/PopUp_Add_MedicalTreatment/Lst_MedicalTreatment',[('MedicalTreatment'): MedicalTreatment]))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/PopUp_Add_MedicalTreatment/Btn_Select'))
	GEN5.ProcessingCommand()
	
	
	
//////Klik Tab Claim Details//////
	//WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Claim Processing/Lbl_JudulClaim'), 2)
	WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Claim Processing/Object_Atas'), 2)
	
	WebUI.delay(2)
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_CallHistory'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_ClaimDetails'))
	GEN5.ProcessingCommand()

//////Ambil nilai Billed dari TDRD//////
String TotalBill = GEN5.getValueDatabase("172.16.94.70", "SEA", "SELECT TotalBill FROM dbo.tbl_disp_registration_document AS tdrd WHERE DocumentNo="+DocNo, "TotalBill")

String TotalBill2 = TotalBill.replace('.00', '')
		



//////Isi Billed Benefit paling atas)//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Details/Txt_Billed'))
GEN5.ProcessingCommand()
WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Details/Txt_Billed'), TotalBill2)
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Details/Grid_BenefitLimit'))
GEN5.ProcessingCommand()


//////Compare HS01//////
String Value_Accepted_HS01 = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Details/Txt_Accepted'), 'value') 
String Value_Accepted_HS01_A = Value_Accepted_HS01.replace('Rp. ', '').replace(',', '').replace('.00', '') 


String Value_Unpaid_HS01 = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Details/Txt_Unpaid'), 'value') 
String Value_Unpaid_HS01_A = Value_Unpaid_HS01.replace('Rp. ', '').replace(',', '').replace('.00', '') 


String Value_ExcessTotal_HS01 = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Details/Txt_ExcessTotal'), 'value') 
String Value_ExcessTotal_HS01_A = Value_ExcessTotal_HS01.replace('Rp. ', '').replace(',', '').replace('.00', '') 
String Billed_HS01_A = TotalBill2

int Value_Accepted_HS01_int = Value_Accepted_HS01_A.toInteger()
int Value_Unpaid_HS01_int = Value_Unpaid_HS01_A.toInteger()
int Value_ExcessTotal_HS01_int = Value_ExcessTotal_HS01_A.toInteger()
int Billed_HS01_int = Billed_HS01_A.toInteger()


if (Billed_HS01_int == Value_Accepted_HS01_int + Value_Unpaid_HS01_int + Value_ExcessTotal_HS01_int  ){
	KeywordUtil.markPassed('Perhitungan Benar') 
 }
else {
	KeywordUtil.markFailed('Perhitungan Salah')
 }

//////Klik Tab Claim Documents////// 
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_ClaimDocuments'))
GEN5.ProcessingCommand()

if (UploadFileDoc=='1')
{
//////Upload Document di Tab Document//////  
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Documents/Btn_Upload'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Documents/PopUp Upload/Checkbox_DocumentPertama'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Documents/PopUp Upload/Btn_Browse'))
GEN5.ProcessingCommand()
GEN5.UploadFile2(FileName)
GEN5.ProcessingCommand()


WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Documents/PopUp Upload/Btn_UploadFile'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Documents/PopUp Upload/Btn_Close_PopUp_UploadFileSucess'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Documents/Grid_Document_RowPertama'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Documents/Btn_View'))
GEN5.ProcessingCommand()

	
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Documents/Btn_ShowHideImage'))
GEN5.ProcessingCommand()

}

GEN5.ProcessingCommand()
GEN5.tickAllCheckboxInTable(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Documents/Grid_Documents'))
GEN5.ProcessingCommand()


//////Klik Tab Completeness Status//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_CompletenessStatus'))
GEN5.ProcessingCommand()

//////Klik Tab Claim Info//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_ClaimInfo'))
GEN5.ProcessingCommand()
WebUI.delay(5)
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_GenerateOtherReason'))
GEN5.ProcessingCommand()



//////Tampung Field UI sebelum Save//////
ArrayList ClaimInfo2 = [findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_DocumentNo'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_RegistrationNo'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_BoxNo'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_RegID'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_DocumentSource'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_ClaimStatus'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_MemberName'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_MemberNo'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_Classification'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_EmployeeClassification'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentPeriodStart'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentPeriodEnd'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_ClaimType'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_ProviderName'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_Doctor'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_DoctorSpecialty'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_ProductType'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AppropriateRBClass'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AppropriateRBRate'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentRBClass'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreatmentRBRate'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_RoomAvailability'),					
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_RoomOption'),
						//findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_UpgradeClass'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_Remarks'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Btn_PayTo'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AccountNo'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AccountName'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_AccountBank'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_BankBranch'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_OtherReason'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_PaymentStatus'),
						findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Info/Txt_TreamentRemarks')
						]

ArrayList Field2 = GEN5.getFieldsValue(ClaimInfo2)

String Txt_ClaimStatus_Array2 = 'Verified'
Field2.set(5, Txt_ClaimStatus_Array2)

String Txt_AppropriateRBRate_Array2 = Field2[18].replace(',','')
Field2.set(18, Txt_AppropriateRBRate_Array2)

String Txt_TreatmentRBRate_Array2 = Field2[20].replace(',','')
Field2.set(20, Txt_TreatmentRBRate_Array2)


String Txt_AccountName_Array2 = Field2[26].trim()
Field2.set(26, Txt_AccountName_Array2)

String Txt_BankBranch2 = Field2[28].trim()
Field2.set(28, Txt_BankBranch2)

String Txt_OtherReason2 = Field2[29].replace('\t','').replace('\r', '').replace('\n', '')
Field2.set(29, Txt_OtherReason2)

String Txt_PaymentStatus_Array2 = 'Outstanding'
Field2.set(30, Txt_PaymentStatus_Array2)



//////Klik Button Save//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Btn_Save'))
	
GEN5.ProcessingCommand()

boolean PopUpPrepost = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Claim Processing/Pop Up Reference Claim Prepost/Lbl_PopUpPrePost'), 2, FailureHandling.OPTIONAL)
if (PopUpPrepost==true)
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Pop Up Reference Claim Prepost/Btn_NotPrepost'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Btn_Save'))
	GEN5.ProcessingCommand()
}
	

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Pop Up Save Sucess/Btn_X'))
	
GEN5.ProcessingCommand()

//////Compare Value UI yang sudah di tampung sebelum save dengan DB//////
String Query_ClaimInfo_ClaimH = "SELECT tdrd.DocumentNo AS DocumentNo, tdr.RegistrationNo AS RegistrationNo, tdrd.BoxNo AS BoxNo, ch.RegID, CASE WHEN tdr.DocumentSource = 'P' THEN 'Provider' WHEN tdr.DocumentSource = 'C' THEN 'Client' WHEN tdr.DocumentSource = 'M' THEN 'Member' END AS DocumentSource, CASE WHEN ch.Status = 'V' THEN 'Verified' WHEN ch.Status = 'R' THEN 'Approved' END AS ClaimStatus, pm.Name AS MemberName, pm.MemberNo AS MemberNo, pc.Description AS Classification, CASE WHEN COALESCE(cc.Client_ClassNo, 0) = 0 THEN '' ELSE CAST(cc.client_ClassDescription AS VARCHAR(MAX)) END AS EmployeeClassification, FORMAT (CAST(ch.Start AS DATE), 'dd/MMM/yyyy') AS TreatmentStart, FORMAT (CAST(ch.Finish AS DATE), 'dd/MMM/yyyy') AS TreatmentEnd,CASE WHEN ch.ClaimType = 'C' THEN 'Cashless'	WHEN ch.ClaimType = 'R' THEN 'Reimburse' END AS ClaimType,ch.TreatmentPlace AS ProviderName,ch.Doctor AS DoctorName,ch.DoctorSpecialty AS DoctorSpecialty,pt.Description AS ProductType,COALESCE(ch.RB_Class, '') AS AppropriateRBClass,CASE WHEN COALESCE(ch.AR_Rate, 0) = 0 THEN '' ELSE CAST(ch.AR_Rate AS VARCHAR(MAX)) END AS AppropriateRBRate,COALESCE(ch.TR_Class, '') AS TreatmentRBClass,CASE WHEN COALESCE(ch.TR_Amount, 0) = 0 THEN '' ELSE CAST(ch.TR_Amount AS VARCHAR(MAX)) END AS TreatmentRBRate ,CASE WHEN ch.RoomAvailability='AV' THEN 'Room Available' WHEN ch.RoomAvailability='RNA' THEN 'Room Not Available' WHEN ch.RoomAvailability='RF' THEN 'Room Is Full' ELSE '' END AS RoomAvailability,CASE WHEN ch.RoomOption='ONPLAN' THEN 'On Plan' WHEN ch.RoomOption='NAPS' THEN 'NAPS' WHEN ch.RoomOption='APS' THEN 'APS' ELSE '' END AS RoomOption,ch.Remarks,CASE WHEN ch.Payto='P' THEN 'Provider' WHEN ch.Payto='M' THEN 'Member' WHEN ch.Payto='C' THEN 'Client' END AS PayTo,ch.AccountNo,ch.BankAccount,ch.BankName,ch.BankBranch,REPLACE(REPLACE(REPLACE(ch.reason, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') AS OtherReason,'Outstanding' AS PaymentStatus,ch.TreatmentRemarks FROM dbo.tbl_disp_registration_document AS tdrd INNER JOIN dbo.ClaimH AS ch ON tdrd.ClaimNo=ch.ClaimNo INNER JOIN dbo.tbl_disp_registration AS tdr ON tdr.RegistrationNo = tdrd.RegistrationNo INNER JOIN dbo.Policy_Member AS pm ON ch.MNO=pm.MNo INNER JOIN dbo.Policy AS p ON p.PNO=pm.PNO INNER JOIN dbo.Policy_Classification AS pc ON p.PNO=pc.PNO AND pc.CLASSNO=pm.CLASSNO left JOIN dbo.ClientClassification AS cc ON pc.Client_ClassNo=cc.Client_ClassNo AND cc.ClientID = p.ClientID INNER JOIN dbo.ProductType AS pt ON pt.ProductType=ch.ProductType WHERE tdrd.DocumentNo="+DocNo
GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_ClaimInfo_ClaimH, Field2)


//////Klik Checkbox Select Claim di Grid//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Chk_SelectClaim'))
GEN5.ProcessingCommand()

//////Klik Button Edit//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Btn_Edit'))
GEN5.ProcessingCommand()



//////Klik Tab Document//////
WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Claim Processing/Lbl_JudulClaim'), 2)
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_CallHistory'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_ClaimDocuments'))
GEN5.ProcessingCommand()
GEN5.tickAllCheckboxInTable(findTestObject('Object Repository/Web/Health/Claim Processing/Tab Claim Documents/Grid_Documents'))
GEN5.ProcessingCommand()

//////Klik Tab Completeness Status//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_CompletenessStatus'))
GEN5.ProcessingCommand()

//////Klik Tab Claim Info//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Tab_ClaimInfo'))
GEN5.ProcessingCommand()

//////Buat Aray untuk Cek ClaimStatus dan PaymentStatus after Approved//////
ArrayList Field3 = ['Approved','Outstanding']

//////Klik Button Approve//////
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Btn_Approve'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Popup Approve Confirmation/Btn_Yes'))
GEN5.ProcessingCommand()


boolean PopUpSuspectFraud = WebUI.waitForElementVisible(findTestObject('Object Repository/Old Stuffs/Claim/Claim Processing/Pop Up Fraud Suspect/PopUp_FraudSuspect'), 2, FailureHandling.OPTIONAL)
if (PopUpSuspectFraud==true)
{
	WebUI.click(findTestObject('Object Repository/Old Stuffs/Claim/Claim Processing/Pop Up Fraud Suspect/Btn_Approve'))
	GEN5.ProcessingCommand()
}

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Popup Approve Confirmation/Popup View Claim Acceptance/Btn_Approve'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Processing/Popup Approve Sucess/Btn_X'))
GEN5.ProcessingCommand()

//////Compare Value UI yang sudah di tampung sebelum save dengan DB//////
String Query_ClaimInfo_ClaimH_Approve = "SELECT CASE WHEN ch.Status = 'V' THEN 'Verified' WHEN ch.Status = 'R' THEN 'Approved' END AS ClaimStatus, 'Outstanding' AS PaymentStatus FROM dbo.tbl_disp_registration_document AS tdrd INNER JOIN dbo.ClaimH AS ch ON tdrd.ClaimNo=ch.ClaimNo  WHERE tdrd.DocumentNo="+DocNo     
GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_ClaimInfo_ClaimH_Approve, Field3)

String query_update = 'UPDATE dbo.DataHealth SET RowStatus = 1 WHERE value = \'' + DocNo + '\''

GEN5.updateValueDatabase("172.16.94.48", "LiTT", query_update)

