import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Product/Product Type/IconAddProductType'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Product Type/IconAddProductType'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Product/Product Type/FieldProductTypeID'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Product Type/FieldProductTypeID'), ProductTypeIDNull, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Product Type/FieldProductTypeName'), ProductTypeNameNull, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Product Type/ButtonSaveProductType'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Product/Product Type/RequiredProductTypeID'), 2, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Product/Product Type/RequiredProductTypeName'), 2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Product Type/ButtonCancelProductType'), FailureHandling.STOP_ON_FAILURE)


