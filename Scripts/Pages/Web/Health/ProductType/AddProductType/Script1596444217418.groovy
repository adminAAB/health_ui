import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

GlobalVariable.ProductTypeID = ProductTypeID

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Product/Product Type/IconAddProductType'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Product Type/IconAddProductType'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Product/Product Type/FieldProductTypeID'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Product Type/FieldProductTypeID'), ProductTypeID, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Product Type/FieldProductTypeName'), ProductTypeName, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Product Type/FieldAdmedikaCoverageCode'), AdmedikaCoverageCode, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Product Type/ButtonSaveProductType'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Product Type/IconCloseSuccessAdd'), FailureHandling.STOP_ON_FAILURE)

String query = "SELECT * FROM dbo.ProductType WHERE ProductType='"+GlobalVariable.ProductTypeID+"'"

String dbProductTypeName = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Description")
println(dbProductTypeName)

String query2 = "SELECT * FROM MappingAdMedika Where GMCode ='"+GlobalVariable.ProductTypeID+"'"

String dbAdmedikaCovCode = GEN5.getValueDatabase("172.16.94.70", "SEA", query2, "AMCode")
println(dbAdmedikaCovCode)

if ((ProductTypeName == dbProductTypeName) && (AdmedikaCoverageCode == dbAdmedikaCovCode)){
	KeywordUtil.markPassed("PASSED")
	}
else {
	KeywordUtil.markFailedAndStop("FAILED")
	}

