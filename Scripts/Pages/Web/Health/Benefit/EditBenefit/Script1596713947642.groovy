import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/IconEditBenefit'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldBenefitName'), BenefitNameEdited, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldUnit'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/UnitSelected',[('Unit'): UnitEdited]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit//ArrowFieldUnit'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/IconSearchProductType'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldSearchProductType'), ProductTypeEdited, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ButtonSearchProductType'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ProductTypeSelected', [('ProductType'):ProductTypeEdited]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ButtonSelectProductType'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldLevel'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/LevelSelected',[('Level'): LevelEdited]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit//ArrowFieldLevel'), FailureHandling.STOP_ON_FAILURE)

if (LevelEdited == 'SubBenefit'){

	WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/IconSearchParentBenefit'), FailureHandling.STOP_ON_FAILURE)
	
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldSearchParentBenefit'), ParentBenefitEdited, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ButtonSearchParentBenefit'), FailureHandling.STOP_ON_FAILURE)
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ParentBenefitSelectedEdited', [('ParentBenefit'): ParentBenefitEdited]), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ButtonSelectParentBenefit'), FailureHandling.STOP_ON_FAILURE)

	WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldLimitBenefit'), LimitBenefitEdited, FailureHandling.STOP_ON_FAILURE)
}

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ButtonSaveBenefitDetails'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/IconCloseSuccessAdd'), FailureHandling.STOP_ON_FAILURE)

String query = "SELECT * FROM dbo.Benefit WHERE BenefitID='"+GlobalVariable.BenefitID+"'"

String dbBenefitName = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Name")
println(dbBenefitName)

String dbUnit = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Unit")
println(dbUnit)

String dbProductType = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "ProductType")
println(dbProductType)

String dbParentBenefit = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "pBenefitID")
println(dbParentBenefit)

dbLimitBenefit = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "PctLimit")
println(dbLimitBenefit)

if (LevelEdited == 'Benefit'){
	if ((BenefitNameEdited == dbBenefitName) && (UnitEdited == dbUnit)  && (ProductTypeEdited == dbProductType)){
		KeywordUtil.markPassed("PASSED")
		}
	else {
		KeywordUtil.markFailedAndStop("FAILED")
		}
}

if (LevelEdited == 'SubBenefit'){
	if ((BenefitNameEdited == dbBenefitName) && (UnitEdited == dbUnit)  && (ProductTypeEdited == dbProductType) && (ParentBenefitEdited == dbParentBenefit) ){
		KeywordUtil.markPassed("PASSED")
		}
	else {
		KeywordUtil.markFailedAndStop("FAILED")
		}
}