import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

GlobalVariable.BenefitID = BenefitID

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/IconAddBenefit'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldBenefitID'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldBenefitID'), BenefitID, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldBenefitName'), BenefitName, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldUnit'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/UnitSelected',[('Unit'): Unit]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit//ArrowFieldUnit'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/IconSearchProductType'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldSearchProductType'), ProductType, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ButtonSearchProductType'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ProductTypeSelected', [('ProductType'):ProductType]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ButtonSelectProductType'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldLevel'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/LevelSelected',[('Level'): Level]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit//ArrowFieldLevel'), FailureHandling.STOP_ON_FAILURE)

if (Level == 'SubBenefit'){

	WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/IconSearchParentBenefit'), FailureHandling.STOP_ON_FAILURE)
	
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldSearchParentBenefit'), ParentBenefit, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ButtonSearchParentBenefit'), FailureHandling.STOP_ON_FAILURE)
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ParentBenefitSelected', [('ParentBenefit'): ParentBenefit]), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ButtonSelectParentBenefit'), FailureHandling.STOP_ON_FAILURE)

	WebUI.setText(findTestObject('Object Repository/Web/Health/Product/Benefit/FieldLimitBenefit'), LimitBenefit, FailureHandling.STOP_ON_FAILURE)
}

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/CheckboxAddActive'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/ButtonSaveBenefitDetails'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Product/Benefit/IconCloseSuccessAdd'), FailureHandling.STOP_ON_FAILURE)

String query = "SELECT * FROM dbo.Benefit WHERE BenefitID='"+GlobalVariable.BenefitID+"'"

String dbBenefitName = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Name")
println(dbBenefitName)

String dbUnit = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Unit")
println(dbUnit)

String dbProductType = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "ProductType")
println(dbProductType)

String dbParentBenefit = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "pBenefitID")
println(dbParentBenefit)

dbLimitBenefit = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "PctLimit")
println(dbLimitBenefit)

if (Level == 'Benefit'){
	if ((BenefitName == dbBenefitName) && (Unit == dbUnit)  && (ProductType == dbProductType)){
		KeywordUtil.markPassed("PASSED")
		}
	else {
		KeywordUtil.markFailedAndStop("FAILED")
		}
} 

if (Level == 'SubBenefit'){
	if ((BenefitName == dbBenefitName) && (Unit == dbUnit)  && (ProductType == dbProductType) && (ParentBenefit == dbParentBenefit) ){
		KeywordUtil.markPassed("PASSED")
		}
	else {
		KeywordUtil.markFailedAndStop("FAILED")
		}
}