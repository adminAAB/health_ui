import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

GlobalVariable.UserClientEdited = UserEdited

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/UserSearchSelected', [('User'):GlobalVariable.UserClient]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconEdit'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/UserAndClient/IconSave'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconSearchUserID'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/UserAndClient/FieldLookUpUserIdName'), UserEdited, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/ButtonSearchUserIdName'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/UserIdSelected', [('UserIdName'):UserEdited]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/ButtonSelectUserIdName'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconSearchClientID'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/UserAndClient/FieldLookUpClient'), ClientEdited, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/ButtonSearchClient'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/ClientSelected', [('Client'):ClientEdited]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/ButtonSelectClient'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconAssignmentStart'), FailureHandling.STOP_ON_FAILURE)

GEN5.DatePicker(StartEdited, findTestObject('Object Repository/Web/Health/UserAndClient/StartSelected'))

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconAssignmentFinish'), FailureHandling.STOP_ON_FAILURE)

GEN5.DatePicker(FinishEdited, findTestObject('Object Repository/Web/Health/UserAndClient/FinishSelected'))

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconSave'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconCloseSuccessAdd'), FailureHandling.STOP_ON_FAILURE)

String query = "SELECT * FROM UserClient WHERE UserID = '$UserEdited'"

String dbID = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "UserID")

String query2 = "SELECT Name FROM Profile WHERE ID IN(SELECT ClientID FROM UserClient WHERE UserID ='$UserEdited')"

String dbClientName = GEN5.getValueDatabase("172.16.94.70", "SEA", query2, "Name")

if ((UserEdited == dbID) && (ClientEdited == dbClientName)){
	KeywordUtil.markPassed("PASSED")
	}
else {
	KeywordUtil.markFailedAndStop("FAILED")
	}

