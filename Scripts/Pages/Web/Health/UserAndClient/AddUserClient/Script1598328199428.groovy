import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

GlobalVariable.UserClient = User

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconAdd'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/UserAndClient/IconSave'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconSearchUserID'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/UserAndClient/FieldLookUpUserIdName'), User, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/ButtonSearchUserIdName'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/UserIdSelected', [('UserIdName'):User]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/ButtonSelectUserIdName'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconSearchClientID'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/UserAndClient/FieldLookUpClient'), Client, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/ButtonSearchClient'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/ClientSelected', [('Client'):Client]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/ButtonSelectClient'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconAssignmentStart'), FailureHandling.STOP_ON_FAILURE)

GEN5.DatePicker(Start, findTestObject('Object Repository/Web/Health/UserAndClient/StartSelected'))

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconAssignmentFinish'), FailureHandling.STOP_ON_FAILURE)

GEN5.DatePicker(Finish, findTestObject('Object Repository/Web/Health/UserAndClient/FinishSelected'))

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconSave'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/UserAndClient/IconCloseSuccessAdd'), FailureHandling.STOP_ON_FAILURE)

String query = "SELECT * FROM UserClient WHERE UserID = '$User'"

String dbID = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "UserID")

String query2 = "SELECT Name FROM Profile WHERE ID IN(SELECT ClientID FROM UserClient WHERE UserID ='$User')"

String dbClientName = GEN5.getValueDatabase("172.16.94.70", "SEA", query2, "Name")

if ((User == dbID) && (Client == dbClientName)){
	KeywordUtil.markPassed("PASSED")
	}
else {
	KeywordUtil.markFailedAndStop("FAILED")
	}

