import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import org.openqa.selenium.Keys as Keys
import com.keyword.GEN5

GlobalVariable.DiagnosisCategory2 = DiagnosisCategory2

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/DiagnosisCategorySelected', [('DiagnosisCategory'):GlobalVariable.DiagnosisCategory]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/IconEdit'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Diagnosis Category/FieldDiagnosisCategory'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis Category/FieldDiagnosisCategory'), DiagnosisCategory2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/FieldDefaultCoverage'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/DefaultCoverageSelected', [('DefaultCoverage'):DefaultCoverage]), FailureHandling.STOP_ON_FAILURE)

if(DefaultCoverage == 'Guaranteed but Uncovered'){
	WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/FieldPayer'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/PayerSelected', [('Payer'):Payer]), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/FieldNeedConfirmation'), FailureHandling.STOP_ON_FAILURE)

	WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/NeedConfirmationSelected', [('Confirmation'):Confirmation]), FailureHandling.STOP_ON_FAILURE)
}

if(DefaultCoverage == 'Uncovered'){
	WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/FieldNeedConfirmation'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/NeedConfirmationSelected', [('Confirmation'):Confirmation]), FailureHandling.STOP_ON_FAILURE)
}

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/ButtonSave'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/IconCloseAfterEdit'), FailureHandling.STOP_ON_FAILURE)

String query = "SELECT * FROM dbo.DiagnosisCategory WHERE Description = '"+GlobalVariable.DiagnosisCategory2+"'"

String dbDiagnosisCategory = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Description")
println(dbDiagnosisCategory)

String dbDefaultCoverage = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "DefaultCoverage")
println(dbDefaultCoverage)

String dbPayer = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Payer")
println(dbPayer)

if(DefaultCoverage == 'Covered'){
	if ((DiagnosisCategory2 == dbDiagnosisCategory) && (DefaultCoverage == dbDefaultCoverage)){
		KeywordUtil.markPassed("PASSED")
		}
	else {
		KeywordUtil.markFailedAndStop("FAILED")
		}
}

if(DefaultCoverage == 'Guaranteed but Uncovered'){
	if ((DiagnosisCategory2 == dbDiagnosisCategory) && (DefaultCoverage == dbDefaultCoverage) && (Payer == dbPayer)){
		KeywordUtil.markPassed("PASSED")
		}
	else {
		KeywordUtil.markFailedAndStop("FAILED")
		}
}

if(DefaultCoverage == 'Uncovered'){
	if ((DiagnosisCategory2 == dbDiagnosisCategory) && (DefaultCoverage == dbDefaultCoverage) && ('Employee' == dbPayer)){
		KeywordUtil.markPassed("PASSED")
		}
	else {
		KeywordUtil.markFailedAndStop("FAILED")
		}
}


