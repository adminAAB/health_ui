import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import org.openqa.selenium.Keys as Keys
import com.keyword.GEN5

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/ButtonReset'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis Category/FieldSearchDiagnosisCategory'), GlobalVariable.DiagnosisCategory, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/ButtonSearch'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Diagnosis Category/NoDataFound'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/IconCloseAfterEdit'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/ButtonClose'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Category/ButtonYesCloseApplication'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Diagnosis Category/LabelMenu'), 3, FailureHandling.STOP_ON_FAILURE)
