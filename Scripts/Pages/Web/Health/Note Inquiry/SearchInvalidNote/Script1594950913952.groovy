import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

//Search Client - Cancel
WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/IconSearchClient'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ButtonCancelSearchClient'), FailureHandling.STOP_ON_FAILURE)

String Client = WebUI.getText(findTestObject('Object Repository/Web/Health/Note Inquiry/FieldClient'), FailureHandling.STOP_ON_FAILURE)

println(Client)

if (Client == ''){
	KeywordUtil.markPassed("PASSED")
	}
	else {
	KeywordUtil.markFailedAndStop("FAILED")
}

//Search Invalid Cient
WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/IconSearchClient'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Note Inquiry/FieldClientName'), InvalidClient, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ButtonSearchClientName'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.waitForElementNotPresent(findTestObject('Object Repository/Web/Health/Note Inquiry/InvalidClientSelected'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ButtonCancelSearchClient'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ButtonReset'), FailureHandling.STOP_ON_FAILURE)

//Search Invalid NoteNo
WebUI.setText(findTestObject('Object Repository/Web/Health/Note Inquiry/FieldNoteNo'), InvalidNoteNo, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ButtonSearch'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.waitForElementNotPresent(findTestObject('Object Repository/Web/Health/Note Inquiry/InvalidNoteNoSelected'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ButtonReset'), FailureHandling.STOP_ON_FAILURE)

//Search Invalid DocumentNo
WebUI.setText(findTestObject('Object Repository/Web/Health/Note Inquiry/FieldDocNo'), InvalidDocNo, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ButtonSearch'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.waitForElementNotPresent(findTestObject('Object Repository/Web/Health/Note Inquiry/InvalidDocNoSelected'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ButtonReset'), FailureHandling.STOP_ON_FAILURE)

//Search Invalid GroupingNo
WebUI.setText(findTestObject('Object Repository/Web/Health/Note Inquiry/FieldGroupingNo'), InvalidGroupingNo, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ButtonSearch'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.waitForElementNotPresent(findTestObject('Object Repository/Web/Health/Note Inquiry/InvalidGroupingNoSelected'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ButtonReset'), FailureHandling.STOP_ON_FAILURE)





