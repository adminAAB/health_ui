import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.util.KeywordUtil
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Note Inquiry/NoteNoSelected'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/NoteNoSelected', [('NoteNo'):GlobalVariable.NoteNo]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/IconViewNote'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

String NoteNo = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Note Inquiry/PopupTextarea'),'value', FailureHandling.STOP_ON_FAILURE).substring(10, 30)

String NoteProductionDate = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Note Inquiry/PopupTextarea'),'value', FailureHandling.STOP_ON_FAILURE).substring(107, 118)

String NoteType = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Note Inquiry/PopupTextarea'),'value', FailureHandling.STOP_ON_FAILURE).substring(131, 137)

String dbNoteNo = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "NoteNo")

String dbNoteProductionDate = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Note Production Date")

String dbNoteType = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Note Type")

if ((NoteNo == dbNoteNo) && (NoteProductionDate == dbNoteProductionDate) && (NoteType == dbNoteType)){
	KeywordUtil.markPassed("PASSED")
	}
	else {
	KeywordUtil.markFailedAndStop("FAILED")
}
