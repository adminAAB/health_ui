import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

GlobalVariable.NoteNo = NoteNo

if ((ClientName != null) && (NoteNo != null) && (DocNo != null) && (GroupingNo != null) && (PeriodFrom != null) && (PeriodTo != null)){
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/IconSearchClient'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Note Inquiry/FieldClientName'), ClientName, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ButtonSearchClientName'), FailureHandling.STOP_ON_FAILURE)
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ClientNameSelected', [('ClientName'):ClientName]), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ButtonSelectClientName'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Note Inquiry/FieldNoteNo'), NoteNo, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Note Inquiry/FieldDocNo'), DocNo, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Note Inquiry/FieldGroupingNo'), GroupingNo, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/IconPeriodFrom'), FailureHandling.STOP_ON_FAILURE)
	
	GEN5.DatePicker(PeriodFrom, findTestObject('Web/Health/Note Inquiry/DateSelectedFrom'))
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/IconPeriodTo'), FailureHandling.STOP_ON_FAILURE)
	
	GEN5.DatePicker(PeriodTo, findTestObject('Web/Health/Note Inquiry/DateSelectedTo'))
		
	WebUI.click(findTestObject('Object Repository/Web/Health/Note Inquiry/ButtonSearch'), FailureHandling.STOP_ON_FAILURE)
	
	GEN5.ProcessingCommand()

}





