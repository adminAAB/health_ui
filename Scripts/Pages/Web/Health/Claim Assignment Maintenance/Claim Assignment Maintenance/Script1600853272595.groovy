import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5



if (CheckSearch=='Yes')
{
	//Search by Doctor
	WebUI.click(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Btn_Clear'))
	GEN5.ProcessingCommand()
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Txt_Doctor'), DoctorNameSearch)
	WebUI.click(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Btn_Search'))
	GEN5.ProcessingCommand()
	
	//ArrayList Grid4 = GEN5.getAllColumnValue(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Grid_ClaimAssignment'), "Doctor")
	
	QueryGrid4= "select su1.name from Sysuser as su left join Sysuser as su1 on su.Superior=su1.ID where su1.Name='"+DoctorNameSearch+"'"
	ArrayList ListDoctor = GEN5.getOneColumnDatabase("172.16.94.70", "SEA", QueryGrid4,"name")
	
	GEN5.CompareColumnsValue(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Grid_ClaimAssignment'),    "Doctor", ListDoctor)
	
	
	//Search by User ID
	WebUI.click(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Btn_Clear'))
	GEN5.ProcessingCommand()
	WebUI.setText(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Txt_UserID'), UserID_ClaimAnalist)
	WebUI.click(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Btn_Search'))
	GEN5.ProcessingCommand()
	 
	
	ArrayList Grid1 = GEN5.getAllRowsValue(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Grid_ClaimAssignment'), "User ID", UserID_ClaimAnalist)
	String UserID1 = Grid1[0].trim()
	Grid1.set(0, UserID1)
	
	String UserName1 = Grid1[1].trim()
	Grid1.set(1, UserName1)
	
	String Doctor1 = Grid1[2].trim()
	Grid1.set(2, Doctor1)
	
	String PICMedcare1 = Grid1[3].trim()
	Grid1.set(3, PICMedcare1)
	
	String Rules1 = Grid1[4].trim()
	Grid1.set(4, Rules1)
	
	
	QueryGrid= "select su.ID,su.Name,su1.Name,	case when su.isPICMedcare=1 then 'Yes' else 'No' end as PICMedcare,1 from Sysuser as su left join Sysuser as su1 on su.Superior=su1.ID where su.ID='"+UserID_ClaimAnalist+"'"
	GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryGrid, Grid1)
	
	
	
	//Search by User Name
	WebUI.click(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Txt_UserID'), UserName_ClaimAnalist)
	WebUI.click(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Btn_Search'))
	GEN5.ProcessingCommand()
	
	ArrayList Grid2 = GEN5.getAllRowsValue(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Grid_ClaimAssignment'), "User ID", UserID_ClaimAnalist)
	String UserID2 = Grid2[0].trim()
	Grid2.set(0, UserID2)
	
	String UserName2 = Grid2[1].trim()
	Grid2.set(1, UserName2)
	
	String Doctor2 = Grid2[2].trim()
	Grid2.set(2, Doctor2)
	
	String PICMedcare2 = Grid2[3].trim()
	Grid2.set(3, PICMedcare2)
	
	String Rules2 = Grid2[4].trim()
	Grid2.set(4, Rules2)



	QueryGrid= "select su.ID,su.Name,su1.Name,	case when su.isPICMedcare=1 then 'Yes' else 'No' end as PICMedcare,1 from Sysuser as su left join Sysuser as su1 on su.Superior=su1.ID where su.ID='"+UserID_ClaimAnalist+"'"
	GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryGrid, Grid1)

}
else if (CheckSearch=='No')
{
	//Search by User ID
	WebUI.click(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Btn_Clear'))
	GEN5.ProcessingCommand()
	WebUI.setText(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Txt_UserID'), UserID_ClaimAnalist)
	WebUI.click(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Btn_Search'))
	GEN5.ProcessingCommand()
}

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Row1_Grid'))
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Btn_Edit'))
GEN5.ProcessingCommand()

if (UbahDoctor=='Yes')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/Btn_Doctor'))
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/List_Doctor',[('NamaDoctor') : NamaDoctor]))
	GEN5.ProcessingCommand()
}

if (UbahPICMedcare=='Yes')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/Btn_PICMedcare'))
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/List_PICMedcare',[('PICMedcare') : PICMedcare]))
	GEN5.ProcessingCommand()
	
}

if (DeleteRow=='Yes')
{
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/Chk_Row1_Grid'))
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/Btn_Delete'))
GEN5.ProcessingCommand()
}


WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/Btn_Add'))
GEN5.ProcessingCommand()

if (CancelSave=='Yes')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/Btn_Cancel'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/Btn_Add'))
	GEN5.ProcessingCommand()
}


WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/Btn_ClaimType'))
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/List_ClaimType',[('ClaimType') : ClaimType]))
GEN5.ProcessingCommand()




WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/Btn_ProductType'))
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/List_ProductType',[('ProductType') : ProductType]))
GEN5.ProcessingCommand()



WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/Btn_PrepostClaim'))
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/List_PrepostClaim',[('PrepostClaim') : PrepostClaim]))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/Btn_HolistikProvider'))
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/List_HolistikProvider',[('HolistikProvider') : HolistikProvider]))
GEN5.ProcessingCommand()


WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/Btn_OverseasProvider'))
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/List_OverseasProvider',[('OverseasProvider') : OverseasProvider]))
GEN5.ProcessingCommand()


WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/Btn_Client'))
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/List_Client',[('Client') : Client]))
GEN5.ProcessingCommand()

if (Client=='Specified')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/Chk_Row1_GridClient'))
	WebUI.delay(3)
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/Chk_Row1_GridClient'))
	WebUI.delay(3)
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/Chk_Row1_GridClient'))
	WebUI.delay(3)
	
	String ClientName = WebUI.getText(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/Row1_NamaClient_GridClient'))
	println ClientName
	Client = ClientName
}

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/PopUp Add Rule/Btn_Save'))
GEN5.ProcessingCommand()

if (Action == 'Save')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/Btn_Save'))
	GEN5.ProcessingCommand()
	
	ArrayList Input = [ClaimType,ProductType,PrepostClaim,HolistikProvider,OverseasProvider,Client]
	
	QueryAfterSave= "select CASE WHEN ClaimType='C' THEN 'Cashless' WHEN ClaimType='R' THEN 'Reimburse' WHEN ClaimType='All' THEN 'All' END AS ClaimType,ProductType,PrePost,ProviderHolistic,ProviderOverseas,CASE WHEN ClientID='All' THEN 'All' ELSE p.name END AS ClientID from MappingClaimAssignment as mca  left join profile as p on mca.ClientID=p.id where UserID='"+UserID_ClaimAnalist+"'"
	GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryAfterSave, Input)
	
	if ((UbahDoctor=='Yes') || (UbahPICMedcare=='Yes'))
	{
		ArrayList Grid3 = GEN5.getAllRowsValue(findTestObject('Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Grid_ClaimAssignment'), "User ID", UserID_ClaimAnalist)
		String UserID3 = Grid3[0].trim()
		Grid3.set(0, UserID3)
		
		String UserName3 = Grid3[1].trim()
		Grid3.set(1, UserName3)
		
		String Doctor3 = Grid3[2].trim()
		Grid3.set(2, Doctor3)
		
		String PICMedcare3 = Grid3[3].trim()
		Grid3.set(3, PICMedcare3)
		
		String Rules3 = Grid3[4].trim()
		Grid3.set(4, Rules3)
		
		
		QueryGrid3= "select su.ID,su.Name,su1.Name,	case when su.isPICMedcare=1 then 'Yes' else 'No' end as PICMedcare,1 from Sysuser as su left join Sysuser as su1 on su.Superior=su1.ID where su.ID='"+UserID_ClaimAnalist+"'"
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryGrid3, Grid3)
	}
		
}
else if (Action == 'Back')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/HL.MNT.CAM.B/Btn_Back'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Btn_Close'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Assignment Maintenance/HL.MNT.CAM.A/Btn_Yes_Close'))
}






//WebUI.click()
//WebUI.click()
//WebUI.click()





