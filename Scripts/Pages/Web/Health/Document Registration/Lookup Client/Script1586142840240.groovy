import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

WebUI.click(findTestObject('Web/Health/Document Registration/Member/Search Member/BTN LookupMember'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Client/Search Client/TXT Client Name'), Client)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Search Client/BTN Search Client'))

GEN5.ProcessingCommand()

GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Document Registration/Client/Search Client/TABLE Search Client'), "Client Name", Client)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Search Client/BTN Select Client'))

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Search Client/BTN Next'))













