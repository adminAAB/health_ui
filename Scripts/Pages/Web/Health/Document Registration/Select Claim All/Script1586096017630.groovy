import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

if (GlobalVariable.DocSource == 'Provider') {
	WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Select Claim Provider'),
		[('DocSource') : DocSource,
		('Provider') : Provider,
		('EmpID') : EmpID,
		('MemberNo') : MemberNo,
		('MemberName') : MemberName,
		('Client') : Client,
		('GLNo') : GLNo, 									
		('DeliveryNo') : DeliveryNo,
		('InvoiceNo') : InvoiceNo,
		('Type') : Type],
		FailureHandling.STOP_ON_FAILURE)
	
} else if (GlobalVariable.DocSource == 'Member'){
	WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Select Claim Member'),
		[('DocSource') : DocSource,
		('Provider') : Provider,
		('EmpID') : EmpID,
		('MemberNo') : MemberNo,
		('MemberName') : MemberName,
		('Client') : Client,
		('Type') : Type],
		FailureHandling.STOP_ON_FAILURE)
	
} else if (GlobalVariable.DocSource == 'Client') {
	WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Select Claim Client'),
		[('DocSource') : DocSource,
		('Provider') : Provider,
		('EmpID') : EmpID,
		('MemberNo') : MemberNo,
		('MemberName') : MemberName,
		('Client') : Client,
		('DeliveryNo') : DeliveryNo,
		('InvoiceNo') : InvoiceNo,
		('Type') : Type],
		FailureHandling.STOP_ON_FAILURE)
}