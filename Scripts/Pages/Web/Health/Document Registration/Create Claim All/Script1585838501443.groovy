import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Call testcase batch provider
if (GlobalVariable.DocSource == 'Provider') {
	WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Batch Provider'),
		[('DeliveryNo') : DeliveryNo, 				
		('InvoiceNo') : InvoiceNo], 				
		FailureHandling.STOP_ON_FAILURE)

	//Create Claim
	WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Create Claim Provider'),
		[('StartDate') : StartDate, 				
		('EndDate') : EndDate,			
		('Diagnosis') : Diagnosis, 		
		('ProductType') : ProductType, 	
		('Billed') : Billed, 			
		('TreatmentRB') : TreatmentRB, 	
		('Room') : Room, 				
		('RoomOpt') : RoomOpt, 			
		('RoomOption') : RoomOption], 	
		FailureHandling.STOP_ON_FAILURE)

} else if (GlobalVariable.DocSource == 'Member') {
	WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Create Claim Member'),
		[('DeliveryNo') : DeliveryNo,
		('StartDate') : StartDate,
		('EndDate') : EndDate,
		('Diagnosis') : Diagnosis,
		('ProductType') : ProductType,
		('Billed') : Billed,
		('TreatmentRB') : TreatmentRB,
		('Room') : Room,
		('RoomOpt') : RoomOpt,
		('RoomOption') : RoomOption],
		FailureHandling.STOP_ON_FAILURE)
	
} else if (GlobalVariable.DocSource == 'Client') {
	WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Batch Client'),
		[('DeliveryNo') : DeliveryNo,
		('InvoiceNo') : InvoiceNo,
		('StartDate') : StartDate,
		('EndDate') : EndDate],
		FailureHandling.STOP_ON_FAILURE)
	
	WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Client Search Member'),
		[('EmpID') : GlobalVariable.EmpID,
		('MemberName') : GlobalVariable.MemberName,
		('MemberNo'): GlobalVariable.MemberNo],
		FailureHandling.STOP_ON_FAILURE)
	
	WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Create Claim Client'),
		[('StartDate') : StartDate,
		('EndDate') : EndDate,
		('Diagnosis') : Diagnosis,
		('ProductType') : ProductType,
		('Billed') : Billed,
		('TreatmentRB') : TreatmentRB,
		('Room') : Room,
		('RoomOpt') : RoomOpt,
		('RoomOption') : RoomOption],
		FailureHandling.STOP_ON_FAILURE)
}
