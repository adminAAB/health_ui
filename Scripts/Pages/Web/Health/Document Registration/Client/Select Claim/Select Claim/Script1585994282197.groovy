import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

WebUI.setText(findTestObject('Web/Health/Document Registration/Client/Lookup Search Member/Txt_memberNo'), GlobalVariable.MemberNo)
WebUI.click(findTestObject('Web/Health/Document Registration/Client/Lookup Search Member/Btn_searchMember'))
GEN5.ProcessingCommand()
GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Document Registration/Client/Lookup Search Member/Tbl_lstMember'), 'Member No', GlobalVariable.MemberNo)
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Lookup Search Member/Btn_selectMember'))
GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Document Registration/Client/Lookup Search Member/Lookup Search Claim/Tbl_lstClaim'), "No", No)
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Lookup Search Member/Lookup Search Claim/Btn_selectClaim'))

if (GlobalVariable.ProductType == 'Inpatient' || GlobalVariable.ProductType == 'Maternity') {	
	//berikan arraylist utk mengambil nilai2 claim
	ArrayList CreateClaimIP =
	[
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_memberName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_providerName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentStart'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentFinish'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_overseasProvider'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_productType'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_diagnosisName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_totalBilled'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentRBClass'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentRBRate'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_roomOption'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_claimType'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_payTo'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankBranch'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountNo'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountName')]
	 
	//Script Untuk Convert
	ArrayList Field = GEN5.getFieldsValue(CreateClaimIP)
	String TreatStart = Field[2].replaceAll('/' , ' ')
	Field.set(2, TreatStart)
	String TreatFin = Field[3].replaceAll('/' , ' ')
	Field.set(3, TreatFin)
	String Billed = Field[7].replaceAll(',' , '')
	Field.set(7, Billed)
	String RBRate = Field[9].replaceAll(',' , '')
	Field.set(9, RBRate)
	String RoomOption = Field[10].replaceAll(' ' , '').toUpperCase()
	Field.set(10, RoomOption)

} else if (GlobalVariable.ProductType == 'Outpatient') {
	ArrayList CreateClaimOP =
	[
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_memberName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_providerName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentStart'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentFinish'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_overseasProvider'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_productType'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_diagnosisName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_totalBilled'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_claimType'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_payTo'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankBranch'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountNo'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountName')]
	 
	//Script Untuk Convert
	ArrayList Field = GEN5.getFieldsValue(CreateClaimOP)
	String TreatStart = Field[2].replaceAll('/' , ' ')
	Field.set(2, TreatStart)
	String TreatFin = Field[3].replaceAll('/' , ' ')
	Field.set(3, TreatFin)
	String Billed = Field[7].replaceAll(',' , '')
	Field.set(7, Billed)
}	
	GlobalVariable.Field
	
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Btn_next'))
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Documents/Btn_save'))
GEN5.ProcessingCommand()
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Summary Documents/Btn_ok'))
GEN5.Sleep(2)

//// ambil nilai docno dari popup
//ArrayList variable = GEN5.getPopUpText(findTestObject('Web/Health/Document Registration/Client/Summary Documents/Btn_PopupdocNo'))
//println variable
//GlobalVariable.DocumentNo = variable[0]

// compare database
if (GlobalVariable.ProductType == 'Inpatient' || GlobalVariable.ProductType == 'Maternity') {
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA",
		QueryDocRegIP.replace("_VariableDocNo_", GlobalVariable.DocumentNo), GlobalVariable.Field)
} else if (GlobalVariable.ProductType == 'Outpatient') {
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA",
		QueryDocRegOP.replace("_VariableDocNo_", GlobalVariable.DocumentNo), GlobalVariable.Field)
}
WebUI.delay(5)

// ambil docno lalu lempar ke LiTT
List<String> variable = GEN5.getPopUpText(findTestObject('Object Repository/Web/Health/Document Registration/Client/Summary Documents/Btn_PopupdocNo'))
GEN5.InsertIntoDataHealth(Type, "Gen5Health", variable[0])
GlobalVariable.DocumentNo = variable[0]

// close popup
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Summary Documents/Btn_closePopupdocno'))

WebUI.delay(2)