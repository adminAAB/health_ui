import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

// memilih member
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Btn_lookupMember'))
WebUI.setText(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Member/Txt_memberNo'), MemberNo)
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Member/Btn_search'))
GEN5.ProcessingCommand()
GEN5.ClickExpectedRow(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Member/Tbl_lstMember'), 'Member No', MemberNo)
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Member/Btn_select'))
GlobalVariable.MemberNo = MemberNo

// menentukan treatment start dan finish
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Btn_dateTreatmentStart'))
GEN5.DatePicker(TreatmentStart, findTestObject('Web/Health/Document Registration/Client/Claim/Date_treatmentStart'))
GlobalVariable.TreatmentStart =TreatmentStart
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Btn_dateTreatmentFinish'))
GEN5.DatePicker(TreatmentFinish, findTestObject('Web/Health/Document Registration/Client/Claim/Date_treatmentFinish'))
GlobalVariable.TreatmentFinish = TreatmentFinish

// menentukan provider
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Btn_lookupProvider'))
WebUI.setText(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Provider/Txt_providerName'), ProviderName)
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Provider/Btn_searchProvider'))
GEN5.ClickExpectedRow(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Provider/Tbl_lstProvider'), 'Provider Name', ProviderName)
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Provider/Btn_selectProvider'))
GlobalVariable.ProviderName = ProviderName

// menentukan overseas provider
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Btn_overseasProvider'))
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lst_overseasProvider', [('OverseasProvider') : OverseasProvider]))
GlobalVariable.OverseasProvider = OverseasProvider

// menentukan product type
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Btn_dropdownProductType'))
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lst_ProductType',[('ProductType') : ProductType]))
GlobalVariable.ProductType = ProductType

if (ProductType == 'Inpatient' || ProductType == 'Maternity') {
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Btn_lookupDiagnosis'))
	WebUI.setText(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Diagnosis/Txt_diagnosisName'), DiagnosisName)
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Diagnosis/Btn_searchDiagnosis'))
	GEN5.ProcessingCommand()
	GEN5.ClickExpectedRow(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Diagnosis/Tbl_lstDiagnosis'), 'Diagnosis Name', DiagnosisName)
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Diagnosis/Btn_selectDiagnosis'))
	GlobalVariable.DiagnosisName = DiagnosisName
	WebUI.setText(findTestObject('Web/Health/Document Registration/Client/Claim/Txt_totalBilled'), TotalBilled)
	GlobalVariable.TotalBilled = TotalBilled
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Treatment RB Class/Btn_treatmentRBClass'))
	GEN5.ClickExpectedRow(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Treatment RB Class/Tbl_lstTreatmentRB'), 'Room Type', ProviderRoomList)
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Treatment RB Class/Btn_selectProviderList'))
	GlobalVariable.ProviderRoomList = ProviderRoomList
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Btn_roomOption'))
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lst_roomOption', [('RoomOption') : RoomOption]))
	GlobalVariable.RoomOption = RoomOption
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Btn_claimType'))
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lst_claimType', [('ClaimType') : ClaimType]))
	GlobalVariable.ClaimType = ClaimType
}else if (ProductType == 'Outpatient') {
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Btn_lookupDiagnosis'))
	WebUI.setText(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Diagnosis/Txt_diagnosisName'), DiagnosisName)
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Diagnosis/Btn_searchDiagnosis'))
	GEN5.ProcessingCommand()
	GEN5.ClickExpectedRow(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Diagnosis/Tbl_lstDiagnosis'), 'Diagnosis Name', DiagnosisName)
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lookup Diagnosis/Btn_selectDiagnosis'))
	GlobalVariable.DiagnosisName = DiagnosisName
	WebUI.setText(findTestObject('Web/Health/Document Registration/Client/Claim/Txt_totalBilled'), TotalBilled)
	GlobalVariable.TotalBilled = TotalBilled
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Btn_claimType'))
	GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lst_claimType', [('ClaimType') : ClaimType]))
	GlobalVariable.ClaimType = ClaimType
}

GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Btn_referenceClaim'))
GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Lookup Reference Claim/Tbl_referenceClaim'), "Document No", DocNoPrepost)
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Lookup Reference Claim/Btn_select'))

if (ProductType == 'Inpatient' || ProductType == 'Maternity') {	
	//berikan arraylist utk mengambil nilai2 claim
	ArrayList CreateClaimIP =
	[
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_memberName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_providerName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentStart'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentFinish'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_overseasProvider'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_productType'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_diagnosisName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_totalBilled'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentRBClass'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentRBRate'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_roomOption'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_referenceClaim'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_claimType'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_payTo'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankBranch'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountNo'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountName')]
	 
	//Script Untuk Convert
	ArrayList Field = GEN5.getFieldsValue(CreateClaimIP)
	String TreatStart = Field[2].replaceAll('/' , ' ')
	Field.set(2, TreatStart)
	String TreatFin = Field[3].replaceAll('/' , ' ')
	Field.set(3, TreatFin)
	String Billed = Field[7].replaceAll(',' , '')
	Field.set(7, Billed)
	String RBRate = Field[9].replaceAll(',' , '')
	Field.set(9, RBRate)
	String RoomOption = Field[10].replaceAll(' ' , '').toUpperCase()
	Field.set(10, RoomOption)

} else if (ProductType == 'Outpatient') {
	ArrayList CreateClaimOP =
	[
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_memberName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_providerName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentStart'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentFinish'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_overseasProvider'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_productType'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_diagnosisName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_totalBilled'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_claimType'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_payTo'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankBranch'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountNo'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountName')]
	 
	//Script Untuk Convert
	ArrayList Field = GEN5.getFieldsValue(CreateClaimOP)
	String TreatStart = Field[2].replaceAll('/' , ' ')
	Field.set(2, TreatStart)
	String TreatFin = Field[3].replaceAll('/' , ' ')
	Field.set(3, TreatFin)
	String Billed = Field[7].replaceAll(',' , '')
	Field.set(7, Billed)
}	
	GlobalVariable.Field

GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Btn_next'))
GEN5.ProcessingCommand()