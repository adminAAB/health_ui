import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

GEN5.ClickExpectedRow(findTestObject('Web/Health/Document Registration/Client/Batch Client/Lst_editClaim'), "Document No", GlobalVariable.DocumentNo)
WebUI.delay(5)
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Batch Client/Btn_editClaim'))
GEN5.ProcessingCommand()


WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_totalBilled'), UpdateTotalBilled)
//GlobalVariable.TotalBilled = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_totalBilled'), 'value')
//
//if(TotalBilled == GlobalVariable.TotalBilled) {
//	GlobalVariable.TotalBilled = GlobalVariable.TotalBilled + 100000
//	println GlobalVariable.TotalBilled
//	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_totalBilled'), GlobalVariable.TotalBilled)
//}
//	
//berikan arraylist utk mengambil nilai2 claim
if (GlobalVariable.ProductType == 'Inpatient' || GlobalVariable.ProductType == 'Maternity') {	
	//berikan arraylist utk mengambil nilai2 claim
	ArrayList CreateClaimIP =
	[
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_memberName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_providerName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentStart'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentFinish'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_overseasProvider'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_productType'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_diagnosisName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_totalBilled'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentRBClass'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentRBRate'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_roomOption'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_referenceClaim'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_claimType'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_payTo'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankBranch'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountNo'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountName')]
	 
	//Script Untuk Convert
	ArrayList Field = GEN5.getFieldsValue(CreateClaimIP)
	String TreatStart = Field[2].replaceAll('/' , ' ')
	Field.set(2, TreatStart)
	String TreatFin = Field[3].replaceAll('/' , ' ')
	Field.set(3, TreatFin)
	String Billed = Field[7].replaceAll(',' , '')
	Field.set(7, Billed)
	String RBRate = Field[9].replaceAll(',' , '')
	Field.set(9, RBRate)
	
} else if (GlobalVariable.ProductType == 'Outpatient') {
	ArrayList CreateClaimOP =
	[
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_memberName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_providerName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentStart'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentFinish'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_overseasProvider'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_productType'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_diagnosisName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_totalBilled'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_claimType'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Lst_payTo'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankName'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankBranch'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountNo'),
	findTestObject('Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountName')]
 
	//Script Untuk Convert
	ArrayList Field = GEN5.getFieldsValue(CreateClaimOP)
	String TreatStart = Field[2].replaceAll('/' , ' ')
	Field.set(2, TreatStart)
	String TreatFin = Field[3].replaceAll('/' , ' ')
	Field.set(3, TreatFin)
	String Billed = Field[7].replaceAll(',' , '')
	Field.set(7, Billed)
}
GlobalVariable.Field

GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Btn_next'))
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Documents/Btn_save'))
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Summary Documents/Btn_ok'))
GEN5.Sleep(2)

// compare database
if (GlobalVariable.ProductType == 'Inpatient' || GlobalVariable.ProductType == 'Maternity') {
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA",
		QueryDocRegPrepost.replace("_VariableDocNo_", GlobalVariable.DocumentNo), GlobalVariable.Field)
} else if (GlobalVariable.ProductType == 'Outpatient') {
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA",
		QueryDocRegOP.replace("_VariableDocNo_", GlobalVariable.DocumentNo), GlobalVariable.Field)
}
WebUI.delay(5)
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Summary Documents/Btn_closePopupdocno'))