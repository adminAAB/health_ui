import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

GEN5.ProcessingCommand()

boolean checkButtonSend = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Send'), 1, FailureHandling.OPTIONAL)

if (checkButtonSend) {
	WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Send'), 1)
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Send'))
	
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Yes'))
	
	GEN5.ProcessingCommand()
	
	WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX_Succes'), 10, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.delay(3)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX_Succes'))
	
	GEN5.ProcessingCommand()
}

GlobalVariable.DocSource = DocSource

GlobalVariable.MemberNo = MemberNo

GlobalVariable.Provider = Provider

GlobalVariable.EmpID = EmpID

GlobalVariable.MemberName = MemberName

GlobalVariable.Client = Client

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Btn_DocumentSource'))

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Lst_DocumentSource', [('DocSource') : GlobalVariable.DocSource]))

if (DocSource == 'Provider') {
    WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Lookup Provider'), 
		[('Provider') : Provider], 
        FailureHandling.STOP_ON_FAILURE)
	
} else if (DocSource == 'Member') {
	WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Lookup Member'), 
		[('EmpID') : EmpID, 								
		('MemberName') : MemberName, 						
		('Client') : Client, 								
		('MemberNo'): MemberNo], 
		FailureHandling.STOP_ON_FAILURE)
	
} else if (DocSource == 'Client') {
	WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Lookup Client'),
		[('Client') : Client],
		FailureHandling.STOP_ON_FAILURE)

}



