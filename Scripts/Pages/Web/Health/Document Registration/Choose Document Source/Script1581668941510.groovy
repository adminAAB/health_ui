import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

GEN5.ProcessingCommand()

//memilih docsource
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Btn_DocumentSource'))
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/List_DocumentSource',[('DocSource') : DocSource]))
GlobalVariable.DocSource

// mencari client
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Lookup Client/Btn_lookUp_Client'))
WebUI.setText(findTestObject('Web/Health/Document Registration/Client/Lookup Client/Txt_ClientName'), ClientName)
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Lookup Client/Btn_searchClient'))

// memilih client
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Lookup Client/span_Client'))
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Lookup Client/Btn_selectClient'))
GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Document Source - Client/Btn_next'))
GlobalVariable.ClientName