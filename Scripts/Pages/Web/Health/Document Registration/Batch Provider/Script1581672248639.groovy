import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5


WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Batch Provider/Txt_DeliveryNo'), DeliveryNo)
GlobalVariable.DeliveryNo = DeliveryNo

WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Batch Provider/Txt_HospitalInvNo'), InvoiceNo)
GlobalVariable.InvoiceNo = InvoiceNo

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Batch Provider/Btn_Add'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Batch Provider/Btn_Search'))

GEN5.ProcessingCommand()  //Processing Command disini akan nge handle smpe proses processing command tsb selesai

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Batch Provider/Btn_NewClaim'))

GEN5.ProcessingCommand()