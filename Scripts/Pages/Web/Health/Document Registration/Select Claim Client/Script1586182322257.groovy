import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Choose Document Source'),
	[('DocSource') : DocSource,
	('Provider') : Provider,
	('EmpID') : EmpID,
	('MemberName') : MemberName,
	('Client') : Client,
	('MemberNo') : MemberNo],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Batch Client'),
	[('DeliveryNo') : DeliveryNo,
	('InvoiceNo') : InvoiceNo,
	('StartDate') : GlobalVariable.StartDate,
	('EndDate') : GlobalVariable.EndDate],
	FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

if (EmpID != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Client/Select Claim/TXT Emp ID'), EmpID)
}

if (MemberNo != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Client/Select Claim/TXT MemberNo'), MemberNo)
}

if (MemberName != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Client/Select Claim/TXT Member Name'), MemberName)
}

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Create Claim/BTN Search Member'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Search Member/BTN Choose Result'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Search Member/BTN Select'))

GEN5.ProcessingCommand()

GEN5.ClickExpectedRowWithNext(findTestObject('Object Repository/Web/Health/Document Registration/Client/Select Claim/TABLE Select Claim'), "Treatment Start", GlobalVariable.StartDate, findTestObject('Object Repository/Web/Health/Document Registration/Client/Select Claim/BTN Next Table'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Select Claim/BTN Select Table'))

GEN5.ProcessingCommand()

if (GlobalVariable.ProductType == 'Inpatient') {
	//berikan arraylist utk mengambil nilai2 claim
	ArrayList CreateClaimIP =
	[
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_memberName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_providerName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentStart'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentFinish'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_overseasProvider'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_productType'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_diagnosisName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_totalBilled'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentRBClass'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentRBRate'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_roomOption'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_claimType'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_payTo'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankBranch'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountNo'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountName')]
	 
	//Script Untuk Convert
	ArrayList Field = GEN5.getFieldsValue(CreateClaimIP)
	
	String TreatStart = Field[2].replaceAll('/' , ' ')
	Field.set(2, TreatStart)
	
	String TreatFin = Field[3].replaceAll('/' , ' ')
	Field.set(3, TreatFin)
	
	String Billed = Field[7].replaceAll(',' , '')
	Field.set(7, Billed)
	
	if (Field[9].toString() == '') {
		Field.set(9, '0')
	}
	
	String RBRate = Field[9].replaceAll(',' , '')
	Field.set(9, RBRate)
	
	String RoomOption = Field[10].replaceAll(' ' , '').toUpperCase()
	Field.set(10, RoomOption)
	
	GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryDocRegIP.replace("_VariableDocNo_", GlobalVariable.Docno), Field)

} else if (GlobalVariable.ProductType == 'Outpatient') {
	ArrayList CreateClaimOP =
	[
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_memberName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_providerName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentStart'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentFinish'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_overseasProvider'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_productType'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_diagnosisName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_totalBilled'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_claimType'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_payTo'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankBranch'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountNo'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountName')]
	 
	//Script Untuk Convert
	ArrayList Field = GEN5.getFieldsValue(CreateClaimOP)
	String TreatStart = Field[2].replaceAll('/' , ' ')
	Field.set(2, TreatStart)
	
	String TreatFin = Field[3].replaceAll('/' , ' ')
	Field.set(3, TreatFin)
	
	String Billed = Field[7].replaceAll(',' , '')
	Field.set(7, Billed)
	
	GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryDocRegOP.replace("_VariableDocNo_", GlobalVariable.Docno), Field)
}

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Next'))

GEN5.ProcessingCommand()

boolean Prepost = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp Prepost/PopUp_Prepost'), 2, FailureHandling.OPTIONAL)

if (Prepost == true) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp Prepost/Btn_NotPrepost'))
}

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Save'))

GEN5.ProcessingCommand()

//Suspect Double
boolean SuspectDouble = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp_Suspect'), 2, FailureHandling.OPTIONAL)
println(SuspectDouble)

if (SuspectDouble == true)
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Close_Suspect'))
	WebUI.delay(2)
}

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_OK'))

GEN5.ProcessingCommand()

//Muncul Pop Up Docno

List<String> variable = GEN5.getPopUpText (findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp_Docno'))

GEN5.InsertIntoDataHealth(Type, "Gen5Health", variable[0])


//Close Pop Up Docno
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX'))

WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Send'), 1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Send'))

boolean checkButtonYes = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Yes'), 2, FailureHandling.OPTIONAL)

int count = 1
while (!checkButtonYes && count < 6) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Send'))
	
	KeywordUtil.markWarning("Button Send can't be Clicked " + count + " Time(s)")
	
	checkButtonYes = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Yes'), 2, FailureHandling.OPTIONAL)
	
	count++
}

if (count == 6) {
	KeywordUtil.markFailedAndStop("ERROR - Button Send is not Clickable")
}

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Yes'))

GEN5.ProcessingCommand()

WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX_Succes'), 10, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX_Succes'), 1)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX_Succes'))

GEN5.ProcessingCommand()
















