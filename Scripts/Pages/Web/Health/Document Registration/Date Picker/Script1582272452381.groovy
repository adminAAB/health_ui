import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_TreatmentStart'))

GEN5.DatePicker(StartDate , findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_Date'))

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_TreatmentEnd'))

GEN5.DatePicker(EndDate , findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_EndDate'))

WebUI.delay(1)