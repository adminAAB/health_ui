import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

if (GlobalVariable.DocSource == 'Provider' || GlobalVariable.DocSource == 'Client') {
	WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Edit Claim Provider'),
		[('EmpID') : EmpID, 								// Write to edit Employee ID or let it empty to skip
		('MemberNo') : MemberNo, 							// Write to edit Member No or let it empty to skip
		('MemberName') : MemberName, 						// Write to edit Member Name or let it empty to skip
		('Client') : Client, 								// Write to edit Client or let it empty to skip
		('Billed') : Billed, 								// Write to edit edited Bill, stay empty to skip
		('StartDate') : StartDate,							// edit start date
		('EndDate') : EndDate, 								// edit End Date
		('Diagnosis') : Diagnosis, 							// Edit Diagnosis
		('ProductType') : ProductType, 						// Edit product Type
		('Room') : Room, 									// Edit Room
		('RoomOption') : RoomOption], 						// Edit RoomOption
		FailureHandling.STOP_ON_FAILURE)

} else if (GlobalVariable.DocSource == 'Member') {
	WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Edit Claim Member'),
		[('DeliveryNo') : DeliveryNo, 						// Write to edit Delivery No, stay empty to skip
		('Billed') : Billed, 								// Write to edit edited Bill, stay empty to skip
		('StartDate') : StartDate,							// edit start date
		('EndDate') : EndDate, 								// edit End Date
		('Diagnosis') : Diagnosis, 							// Edit Diagnosis
		('ProductType') : ProductType, 						// Edit product Type
		('Room') : Room, 									// Edit Room
		('RoomOption') : RoomOption], 						// Edit RoomOption
		FailureHandling.STOP_ON_FAILURE)
}




