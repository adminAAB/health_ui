import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Choose Document Source'),
	[('DocSource') : DocSource,
	('Provider') : Provider,
	('EmpID') : EmpID,
	('MemberName') : MemberName,
	('Client') : Client,
	('MemberNo') : MemberNo],
	FailureHandling.STOP_ON_FAILURE)

if (GlobalVariable.ProductType == 'Inpatient') {
	ArrayList data=
	[findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Client Name'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Delivery No'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Delivery Receive Date'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field GarMed Receive Date'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Provider'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Overseas Provider'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Treatment Period From'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Treatment Period To'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Product Type'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Diagnosis'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Total Billed'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Treatment RB Class'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Treatment RB Rate'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Room Option'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Pay To'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Bank Name'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Bank Branch'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Account No'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Account Name')
	]
	//Script Untuk Compare
	ArrayList variabel = GEN5.getFieldsValue(data)
	
	String Dated1 = variabel[2].replaceAll('/' , ' ')
	variabel.set(2, Dated1)
	
	String Dated2 = variabel[3].replaceAll('/' , ' ')
	variabel.set(3, Dated2)
	
	String Dated3 = variabel[6].replaceAll('/' , ' ')
	variabel.set(6, Dated3)
	
	String Dated4 = variabel[7].replaceAll('/' , ' ')
	variabel.set(7, Dated4)
	
	String Billed = variabel[10].replaceAll(',' , '')
	variabel.set(10, Billed)
	
	String Rate = variabel[12].replaceAll(',' , '')
	variabel.set(12, Rate)
	
	String Branch = variabel[16].trim()
	variabel.set(16, Branch)
	
	GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryMember.replace("_test_", GlobalVariable.Docno), variabel)
	
} else if ((GlobalVariable.ProductType == 'Outpatient')) {
	ArrayList data=
	[findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Client Name'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Delivery No'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Delivery Receive Date'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field GarMed Receive Date'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Provider'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Overseas Provider'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Treatment Period From'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Treatment Period To'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Product Type'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Diagnosis'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Total Billed'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Pay To'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Bank Name'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Bank Branch'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Account No'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Account Name')
	]
	//Script Untuk Compare
	ArrayList variabel = GEN5.getFieldsValue(data)
	
	
	String Dated1 = variabel[2].replaceAll('/' , ' ')
	variabel.set(2, Dated1)
	
	String Dated2 = variabel[3].replaceAll('/' , ' ')
	variabel.set(3, Dated2)
	
	String Dated3 = variabel[6].replaceAll('/' , ' ')
	variabel.set(6, Dated3)
	
	String Dated4 = variabel[7].replaceAll('/' , ' ')
	variabel.set(7, Dated4)
	
	String Billed = variabel[10].replaceAll(',' , '')
	variabel.set(10, Billed)
	
	String Branch = variabel[13].trim()
	variabel.set(13, Branch)
	
	GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryMember2.replace("_test_", GlobalVariable.Docno), variabel)
}

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Next'), 1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Next'))

GEN5.ProcessingCommand()

boolean Prepost = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp Prepost/PopUp_Prepost'), 2, FailureHandling.OPTIONAL)

if (Prepost == true) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp Prepost/Btn_NotPrepost'))
	
	GEN5.ProcessingCommand()
}

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Save'))

GEN5.ProcessingCommand()

boolean SuspectDouble = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp_Suspect'), 2, FailureHandling.OPTIONAL)

if (SuspectDouble == true) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Close_Suspect'))
	
	GEN5.ProcessingCommand()
}

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_OK'))

GEN5.ProcessingCommand()

List<String> getDocNo = GEN5.getPopUpText (findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp_Docno'))

GEN5.InsertIntoDataHealth(Type, "Gen5Health", getDocNo[0])

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX'))











