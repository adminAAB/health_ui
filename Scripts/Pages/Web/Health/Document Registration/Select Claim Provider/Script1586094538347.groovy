import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Choose Document Source'), 
	[('DocSource') : DocSource, 
	('Provider') : Provider,
	('EmpID') : EmpID, 								
	('MemberName') : MemberName, 							
	('Client') : Client, 								
    ('MemberNo') : MemberNo], 					 
	FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Batch Provider/Txt_DeliveryNo'), DeliveryNo)

WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Batch Provider/Txt_HospitalInvNo'), InvoiceNo)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Batch Provider/Btn_Add'))

GEN5.ProcessingCommand()

//Muncul Pop Up Search Claim

if (EmpID != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Select Claim/Txt_EmpID'), EmpID)
} 

if (MemberNo != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Select Claim/Txt_MemberNo'), MemberNo)
}

if (MemberName != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Select Claim/Txt_MemberName'), MemberName)
}

if (GLNo != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Select Claim/Txt_GLNO'), GLNo)
}

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Batch Provider/Btn_Search'))

GEN5.ProcessingCommand()

GEN5.ClickExpectedRowWithNext(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Select Claim/table_SelectClaim'), "Treatment Start", GlobalVariable.StartDate, findTestObject('Object Repository/Web/Health/Document Registration/Provider/Select Claim/Button Table_SelectClaim'))
//WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Select Claim/Lst_SelectClaim'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Select Claim/Btn_Select'))

GEN5.ProcessingCommand()

//Variable Object Repository yang mau di Compare
if (GlobalVariable.ProductType == 'Inpatient' || GlobalVariable.ProductType == 'Maternity') {
	ArrayList DataClaim =
	[
	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_MemberName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Client'),
	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_TreatmentStart'),
	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_TreatmentEnd'),
	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_ProductType'),
	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Diagnosis'),
	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Billed'),
	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_TreatmentRBClass'),
	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_TreatmentRBRate'),
	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_RoomOption')
	]

//Script Untuk Compare
ArrayList Field = GEN5.getFieldsValue(DataClaim)
String Billed = Field[6].replaceAll(',' , '')
Field.set(6, Billed)

String Dated1 = Field[2].replaceAll('/' , ' ')
Field.set(2, Dated1)

String Dated2 = Field[3].replaceAll('/' , ' ')
Field.set(3, Dated2)

String RBRate = Field[8].replaceAll(',' , '')
Field.set(8, RBRate)


GEN5.compareRowDBtoArray("172.16.94.70", "SEA",
	QueryDocReg.replace("_test_", GlobalVariable.Docno), Field)

} else {
	ArrayList DataClaim =
	[
		findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_MemberName'),
		findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Client'),
		findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_TreatmentStart'),
		findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_TreatmentEnd'),
		findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_ProductType'),
		findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Diagnosis'),
		findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Billed'),
	]
	
	ArrayList Field = GEN5.getFieldsValue(DataClaim)
	String Billed = Field[6].replaceAll(',' , '')
	Field.set(6, Billed)
	
	String Dated1 = Field[2].replaceAll('/' , ' ')
	Field.set(2, Dated1)
	
	String Dated2 = Field[3].replaceAll('/' , ' ')
	Field.set(3, Dated2)
	
	GEN5.compareRowDBtoArray("172.16.94.70", "SEA",
		QueryDocReg2.replace("_test_", GlobalVariable.Docno), Field)
}



WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Next'))

GEN5.ProcessingCommand()

boolean Prepost = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp Prepost/PopUp_Prepost'), 2, FailureHandling.OPTIONAL)

if (Prepost == true) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp Prepost/Btn_NotPrepost'))
}

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Save'))

GEN5.ProcessingCommand()

//Suspect Double
boolean SuspectDouble = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp_Suspect'), 2, FailureHandling.OPTIONAL)
println(SuspectDouble)

if (SuspectDouble == true)
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Close_Suspect'))
	WebUI.delay(2)
}

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_OK'))

GEN5.ProcessingCommand()

//Muncul Pop Up Docno

List<String> variable = GEN5.getPopUpText (findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp_Docno'))

GEN5.InsertIntoDataHealth(Type, "Gen5Health", variable[0])


//Close Pop Up Docno
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX'))

WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Send'), 1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Send'))

boolean checkButtonYes = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Yes'), 2, FailureHandling.OPTIONAL)

int count = 1
while (!checkButtonYes && count < 6) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Send'))
	
	KeywordUtil.markWarning("Button Send can't be Clicked " + count + " Time(s)")
	
	checkButtonYes = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Yes'), 2, FailureHandling.OPTIONAL)
	
	count++
}

if (count == 6) {
	KeywordUtil.markFailedAndStop("ERROR - Button Send is not Clickable")
}

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Yes'))

GEN5.ProcessingCommand()

WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX_Succes'), 10, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX_Succes'))

GEN5.ProcessingCommand()



