import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

WebUI.callTestCase(findTestCase('Pages/Web/Health/Document Registration/Choose Document Source'),
	[('DocSource') : GlobalVariable.DocSource,
	('Provider') : GlobalVariable.Provider,
	('EmpID') : GlobalVariable.EmpID,
	('MemberName') : GlobalVariable.MemberName,
	('Client') : GlobalVariable.Client,
	('MemberNo') : GlobalVariable.MemberNo],
	FailureHandling.STOP_ON_FAILURE)

if (DeliveryNo != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/TXT DeliveryNo'), DeliveryNo)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date DeliveryReceive'))
}

if (StartDate != '') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date DeliveryReceive'))
	
	GEN5.DatePicker(StartDate, findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date Choose DeliveryReceive'))
}

if (EndDate != '') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date GardaMedDate'))
	
	GEN5.DatePicker(EndDate, findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date Choose GardaMedDate'))
	
}

if (ProductType != '') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_ProductType'))
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_ProductType', [
				('ProductType') : ProductType]))
}

if (Diagnosis != '') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_Diagnosis'))
	
	WebUI.delay(2)
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Txt_Diagnosis'), Diagnosis)
	GlobalVariable.Diagnosis = Diagnosis
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Btn_Search'))
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Lst_Diagnosis'))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Btn_Select'))
	
	WebUI.delay(2)
}

if (Billed != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Billed'), Billed)
}

if (Room != '') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_TreatmentRB'))
	
	GEN5.ProcessingCommand()
	
	GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Treatment RB/Lst_TreatmentRB'),
		'Room Type', Room)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Treatment RB/Btn_Select'))
}

if (RoomOption != '') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_RoomOption'))
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_RoomOption',
			[('RoomOption') : RoomOption]))
}

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Next'), 1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Next'))

GEN5.ProcessingCommand()

GEN5.tickAllCheckboxInTable(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Cbx_Document'))

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Save'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_OK'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX'))

GEN5.ProcessingCommand()








