import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

// set text
WebUI.setText(findTestObject('Web/Health/Document Registration/Client/Batch Client/Txt_deliveryNo'), DeliveryNo)
GlobalVariable.DeliveryNo

WebUI.setText(findTestObject('Web/Health/Document Registration/Client/Batch Client/Txt_hospInvoiceNo'), HospitalInvoiceNo)
GlobalVariable.HospitalInvoiceNo

// menentukan date
WebUI.click(findTestObject('Web/Health/Document Registration/Client/Batch Client/Btn_selectDeliveryDate'))
GEN5.DatePicker(DeliveryReceiveDate, findTestObject('Object Repository/Web/Health/Document Registration/Client/Batch Client/Date_DeliveryReceivedDate'))
WebUI.click(findTestObject('Web/Health/Document Registration/Client/Batch Client/Btn_selectGarMedReceiveDate'))
GEN5.DatePicker(GarmedReceiveDate, findTestObject('Object Repository/Web/Health/Document Registration/Client/Batch Client/Date_GarMedReceivedDate'))

WebUI.click(findTestObject('Web/Health/Document Registration/Client/Batch Client/Btn_newClaimLst'))