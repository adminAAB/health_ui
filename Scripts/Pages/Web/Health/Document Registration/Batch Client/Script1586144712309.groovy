import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

GEN5.ProcessingCommand()

// set text
WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Client/Create Claim/TXT Delivery No'), DeliveryNo)
GlobalVariable.DeliveryNo = DeliveryNo

WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Client/Create Claim/TXT Invoice No'), InvoiceNo)
GlobalVariable.InvoiceNo = InvoiceNo

// menentukan date
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Create Claim/DATE open Delivery Date'))

GEN5.DatePicker(StartDate, findTestObject('Object Repository/Web/Health/Document Registration/Client/Create Claim/DATE choose Delivery Date'))

GlobalVariable.StartDate = StartDate

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Create Claim/DATE open Garmed Date'))

GEN5.DatePicker(EndDate, findTestObject('Object Repository/Web/Health/Document Registration/Client/Create Claim/DATE choose Garmed Date'))

GlobalVariable.EndDate = EndDate

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Create Claim/BTN Add Claim'))




