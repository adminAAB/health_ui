import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

GEN5.ProcessingCommand()

// memilih member
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Btn_lookupMember'))

if (GlobalVariable.EmpID != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_EmpID'),
		GlobalVariable.EmpID)
}

if (GlobalVariable.MemberNo != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Client/Create Claim/TXT Member No'), GlobalVariable.MemberNo)
} 

if (GlobalVariable.MemberName != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_MemberName'),
		GlobalVariable.MemberName)
}

GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Lookup Member/Btn_search'))

GEN5.ProcessingCommand()

GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Lookup Member/Tbl_lstMember'), 'Member No',  GlobalVariable.MemberNo)

GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Lookup Member/Btn_select'))

// menentukan treatment start dan finish
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Btn_dateTreatmentStart'))

GEN5.DatePicker(StartDate, findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Date_treatmentStart'))

GlobalVariable.StartDate = StartDate

GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Btn_dateTreatmentFinish'))

GEN5.DatePicker(EndDate, findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Date_treatmentFinish'))

GlobalVariable.EndDate = EndDate

// menentukan provider
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Btn_lookupProvider'))

WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Lookup Provider/Txt_providerName'), GlobalVariable.Provider)

GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Lookup Provider/Btn_searchProvider'))

GEN5.ProcessingCommand()

GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Lookup Provider/Tbl_lstProvider'), 'Provider Name', GlobalVariable.Provider)

GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Lookup Provider/Btn_selectProvider'))

GEN5.ProcessingCommand()

// menentukan product type
GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Btn_dropdownProductType'))

GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Lst_ProductType',[('ProductType') : ProductType]))

GlobalVariable.ProductType = ProductType

WebUI.delay(1)

//Diagnosis
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_Diagnosis'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Txt_Diagnosis'), Diagnosis)
GlobalVariable.Diagnosis = Diagnosis

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Btn_Search'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Lst_Diagnosis'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Btn_Select'))

WebUI.delay(2)

//Billed
WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Billed'), Billed)

GlobalVariable.Billed = Billed

WebUI.delay(1)

if (TreatmentRB == true) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_TreatmentRB'))

	GEN5.ProcessingCommand()

	GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Treatment RB/Lst_TreatmentRB'),
		'Room Type', Room)
	
	GlobalVariable.Room = Room

	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Treatment RB/Btn_Select'))

	String[] RBRate = GEN5.getFieldsValue([findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_TreatmentRBRate')
			, findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Client')])

	HargaRB = RBRate
	GlobalVariable.HargaRB = HargaRB
}

WebUI.delay(2)

//Room Option
if (RoomOpt == true) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_RoomOption'))

	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_RoomOption',
			[('RoomOption') : RoomOption]))
	
	GlobalVariable.RoomOpt = RoomOption
}

//GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Btn_claimType'))
//
//GEN5.Click(findTestObject('Web/Health/Document Registration/Client/Claim/Lst_claimType', [('ClaimType') : ClaimType]))
//
//GlobalVariable.ClaimType = ClaimType

if (ProductType == 'Inpatient') {
	//berikan arraylist utk mengambil nilai2 claim
	ArrayList CreateClaimIP =
	[
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_memberName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_providerName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentStart'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentFinish'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_overseasProvider'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_productType'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_diagnosisName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_totalBilled'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentRBClass'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentRBRate'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_roomOption'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_claimType'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_payTo'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankBranch'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountNo'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountName')]
	 
	//Script Untuk Convert
	ArrayList Field = GEN5.getFieldsValue(CreateClaimIP)
	
	String TreatStart = Field[2].replaceAll('/' , ' ')
	Field.set(2, TreatStart)
	
	String TreatFin = Field[3].replaceAll('/' , ' ')
	Field.set(3, TreatFin)
	
	String Billed = Field[7].replaceAll(',' , '')
	Field.set(7, Billed)
	
	if (Field[9].toString() == '') {
		Field.set(9, '0')
	}
	
	String RBRate = Field[9].replaceAll(',' , '')
	Field.set(9, RBRate)
	
	String RoomOption = Field[10].replaceAll(' ' , '').toUpperCase()
	Field.set(10, RoomOption)
	
	GlobalVariable.FieldDataMember = Field

} else if (ProductType == 'Outpatient') {
	ArrayList CreateClaimOP =
	[
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_memberName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_providerName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentStart'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_treatmentFinish'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_overseasProvider'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_productType'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_diagnosisName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_totalBilled'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_claimType'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Lst_payTo'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankName'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_bankBranch'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountNo'),
	findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Comparing/Txt_accountName')]
	 
	//Script Untuk Convert
	ArrayList Field = GEN5.getFieldsValue(CreateClaimOP)
	String TreatStart = Field[2].replaceAll('/' , ' ')
	Field.set(2, TreatStart)
	
	String TreatFin = Field[3].replaceAll('/' , ' ')
	Field.set(3, TreatFin)
	
	String Billed = Field[7].replaceAll(',' , '')
	Field.set(7, Billed)
	
	GlobalVariable.FieldDataMember = Field
}

GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Claim/Btn_next'))

GEN5.ProcessingCommand()