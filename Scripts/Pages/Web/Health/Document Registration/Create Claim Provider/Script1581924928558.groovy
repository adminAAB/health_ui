import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.keyword.GEN5 as GEN5

//Search Member
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_Member'))

WebUI.delay(2)

if (GlobalVariable.EmpID != '') {
    WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_EmpID'), 
        GlobalVariable.EmpID)
}

if (GlobalVariable.MemberNo != '') {
    WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_MemberNo'), 
        GlobalVariable.MemberNo)
}

if (GlobalVariable.MemberName != '') {
    WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_MemberName'), 
        GlobalVariable.MemberName)
}

if (GlobalVariable.Client != '') {
    WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_Client'), 
        GlobalVariable.Client)
}

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Btn_Search'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Lst_Member'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Btn_Select'))

WebUI.delay(2)

//Treatment Period
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_TreatmentStart'))

GEN5.DatePicker(StartDate, findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_Date'))

GlobalVariable.StartDate = StartDate

WebUI.delay(1)

if (ProductType != 'Outpatient') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_TreatmentEnd'))
	
	GEN5.DatePicker(EndDate, findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_EndDate'))
	
	boolean checkDiagnosis = WebUI.waitForElementClickable(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_Diagnosis'), 2, FailureHandling.OPTIONAL)
	
	while (!checkDiagnosis) {
		WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_TreatmentEnd'))
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_TreatmentEnd'))
		
		GEN5.DatePicker(EndDate, findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_EndDate'))
		
		checkDiagnosis = WebUI.waitForElementClickable(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_Diagnosis'), 2, FailureHandling.OPTIONAL)
	}
}

GlobalVariable.EndDate = EndDate

WebUI.delay(1)

//Diagnosis
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_Diagnosis'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Txt_Diagnosis'), Diagnosis)
GlobalVariable.Diagnosis = Diagnosis

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Btn_Search'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Lst_Diagnosis'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Btn_Select'))

WebUI.delay(2)

//ProductType
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_ProductType'))

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_ProductType', [
            ('ProductType') : ProductType]))
GlobalVariable.ProductType = ProductType

GEN5.ProcessingCommand()

//Billed
WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Billed'), Billed)

GlobalVariable.Billed = Billed

WebUI.delay(1)

//Treatment RB Class & RB Rate
ArrayList HargaRB = new ArrayList()

if (TreatmentRB == true) {
    WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_TreatmentRB'))

    GEN5.ProcessingCommand()

    GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Treatment RB/Lst_TreatmentRB'), 
        'Room Type', Room)
	
	GlobalVariable.Room = Room

    WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Treatment RB/Btn_Select'))

    String[] RBRate = GEN5.getFieldsValue([findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_TreatmentRBRate')
            , findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Client')])

    HargaRB = RBRate
	GlobalVariable.HargaRB = HargaRB
}

WebUI.delay(1)

//Room Option
if (RoomOpt == true) {
    WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_RoomOption'))

    WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_RoomOption', 
            [('RoomOption') : RoomOption]))
	
	GlobalVariable.RoomOpt = RoomOption
}

//Next Claim
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Next'))

GEN5.ProcessingCommand()

