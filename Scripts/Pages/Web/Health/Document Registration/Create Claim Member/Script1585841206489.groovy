import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

GEN5.ProcessingCommand()

WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/TXT DeliveryNo'), DeliveryNo)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date DeliveryReceive'))

GEN5.DatePicker(StartDate, findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date Choose DeliveryReceive'))

GlobalVariable.StartDate = StartDate

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date GardaMedDate'))

GEN5.DatePicker(EndDate, findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date Choose GardaMedDate'))

GlobalVariable.EndDate = EndDate

boolean checkTreatment = WebUI.waitForElementClickable(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date TreatmentPeriodFrom'), 1, FailureHandling.OPTIONAL)

while (!checkTreatment) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date GardaMedDate'))
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date GardaMedDate'))
	
	GEN5.DatePicker(EndDate, findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date Choose GardaMedDate'))
	
	checkTreatment = WebUI.waitForElementClickable(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date TreatmentPeriodFrom'), 1, FailureHandling.OPTIONAL)
}

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date TreatmentPeriodFrom'))

GEN5.DatePicker(StartDate, findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date Choose TreatmentPeriodFrom'))

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date TreatmentPeriodTo'))

GEN5.DatePicker(EndDate, findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date Choose TreatmentPeriodTo'))

boolean checkProvider = WebUI.waitForElementClickable(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/BTN LookupProvider'), 1, FailureHandling.OPTIONAL)

while (!checkProvider) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date TreatmentPeriodTo'))
	
	GEN5.DatePicker(EndDate, findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/Date Choose TreatmentPeriodTo'))
	
	checkProvider = WebUI.waitForElementClickable(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/BTN LookupProvider'), 1, FailureHandling.OPTIONAL)
}

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Create Claim Member/BTN LookupProvider'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Search Provider/Txt_ProviderName'), GlobalVariable.Provider)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Search Provider/Btn_Search'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Search Provider/Lst_Provider'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Search Provider/Btn_Select'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_ProductType'))

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_ProductType', [
			('ProductType') : ProductType]))

GlobalVariable.ProductType = ProductType

GEN5.ProcessingCommand()

//Diagnosis
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_Diagnosis'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Txt_Diagnosis'), Diagnosis)
GlobalVariable.Diagnosis = Diagnosis

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Btn_Search'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Lst_Diagnosis'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Btn_Select'))

WebUI.delay(2)

//Billed
WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Billed'), Billed)

GlobalVariable.Billed = Billed

//Treatment RB Class & RB Rate
if (TreatmentRB == true) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_TreatmentRB'))

	GEN5.ProcessingCommand()

	GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Treatment RB/Lst_TreatmentRB'),
		'Room Type', Room)
	
	GlobalVariable.Room = Room

	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Treatment RB/Btn_Select'))

	String[] RBRate = GEN5.getFieldsValue([findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_TreatmentRBRate')
			, findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Client')])

	GlobalVariable.HargaRB = RBRate
}

//Room Option
if (RoomOpt == true) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_RoomOption'))

	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_RoomOption',
			[('RoomOption') : RoomOption]))
	
	GlobalVariable.RoomOpt = RoomOption
}

if (ProductType == 'Inpatient') {
	ArrayList data=
	[findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Client Name'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Delivery No'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Delivery Receive Date'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field GarMed Receive Date'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Provider'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Overseas Provider'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Treatment Period From'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Treatment Period To'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Product Type'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Diagnosis'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Total Billed'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Treatment RB Class'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Treatment RB Rate'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Room Option'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Pay To'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Bank Name'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Bank Branch'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Account No'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Account Name')
	]
	//Script Untuk Compare
	ArrayList variabel = GEN5.getFieldsValue(data)
	
	String Dated1 = variabel[2].replaceAll('/' , ' ')
	variabel.set(2, Dated1)
	
	String Dated2 = variabel[3].replaceAll('/' , ' ')
	variabel.set(3, Dated2)
	
	String Dated3 = variabel[6].replaceAll('/' , ' ')
	variabel.set(6, Dated3)
	
	String Dated4 = variabel[7].replaceAll('/' , ' ')
	variabel.set(7, Dated4)
	
	String Billed = variabel[10].replaceAll(',' , '')
	variabel.set(10, Billed)
	
	String Rate = variabel[12].replaceAll(',' , '')
	variabel.set(12, Rate)
	
	String Branch = variabel[16].trim()
	variabel.set(16, Branch)
	
	GlobalVariable.FieldDataMember = variabel
	
} else if ((ProductType == 'Outpatient')) {
	ArrayList data=
	[findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Client Name'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Delivery No'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Delivery Receive Date'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field GarMed Receive Date'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Provider'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Overseas Provider'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Treatment Period From'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Treatment Period To'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Product Type'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Diagnosis'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Total Billed'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Pay To'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Bank Name'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Bank Branch'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Account No'),
		findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Field Account Name')
	]
	//Script Untuk Compare
	ArrayList variabel = GEN5.getFieldsValue(data)
	
	
	String Dated1 = variabel[2].replaceAll('/' , ' ')
	variabel.set(2, Dated1)
	
	String Dated2 = variabel[3].replaceAll('/' , ' ')
	variabel.set(3, Dated2)
	
	String Dated3 = variabel[6].replaceAll('/' , ' ')
	variabel.set(6, Dated3)
	
	String Dated4 = variabel[7].replaceAll('/' , ' ')
	variabel.set(7, Dated4)
	
	String Billed = variabel[10].replaceAll(',' , '')
	variabel.set(10, Billed)
	
	String Branch = variabel[13].trim()
	variabel.set(13, Branch)
	
	GlobalVariable.FieldDataMember = variabel
}

//Next Claim
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Next'))

GEN5.ProcessingCommand()

//WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Save'))
//
//GEN5.ProcessingCommand()
//
//WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_OK'))
//
//GEN5.ProcessingCommand()
//
//WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX'))
//
//GEN5.ProcessingCommand()
















