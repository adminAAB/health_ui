import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.chrome.ChromeDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

WebUI.setText(findTestObject('Web/Health/Document Registration/Client/Lookup Search Member/Txt_memberNo'), MemberNo)
WebUI.click(findTestObject('Web/Health/Document Registration/Client/Lookup Search Member/Btn_searchMember'))
GEN5.ProcessingCommand()
WebUI.delay(1)

GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Document Registration/Client/Lookup Search Member/Tbl_lstMember'), 'Member No', MemberNo)

GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Lookup Search Member/Btn_selectMember'))

GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Lookup Search Member/Lookup Search Claim/Btn_searchClaim'))

GEN5.ProcessingCommand()

GEN5.Click(findTestObject('Object Repository/Web/Health/Document Registration/Client/Lookup Search Member/Lookup Search Claim/Btn_newClaim'))
