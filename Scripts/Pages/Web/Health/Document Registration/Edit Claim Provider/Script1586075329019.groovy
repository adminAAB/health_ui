import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5

WebUI.delay(2)

//Edit Claim
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Edit Claim/Lst_EditClaim'))

//GEN5.ProcessingCommand()
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Edit Claim/Btn_Edit'))

GEN5.ProcessingCommand()

if (EmpID != '' || MemberNo != '' || MemberName != '' || Client != '') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_Member'))
	
	WebUI.delay(2)
	
	if (EmpID != '') {
		WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_EmpID'),
			EmpID)
		GlobalVariable.EmpID = EmpID
	}
	
	if (MemberNo != '') {
		WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_MemberNo'),
			MemberNo)
	
		GlobalVariable.MemberNo = MemberNo
	}
	
	if (MemberName != '') {
		WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_MemberName'),
			MemberName)
		GlobalVariable.MemberName = MemberName
		
	}
	
	if (Client != '') {
		WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_Client'),
			Client)
		GlobalVariable.Client = Client
	}
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Btn_Search'))
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Lst_Member'))
	
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Btn_Select'))
	
	WebUI.delay(2)
}

if (StartDate != '') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_TreatmentStart'))
	
	GEN5.DatePicker(StartDate, findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_Date'))
}

if (EndDate != '') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_TreatmentEnd'))
	
	GEN5.DatePicker(EndDate, findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_EndDate'))
}

//Ubah Nilai Billed nya
if (Billed != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Billed'), Billed)
}

if (Diagnosis != '') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_Diagnosis'))
	
	WebUI.delay(2)
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Txt_Diagnosis'), Diagnosis)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Btn_Search'))
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Lst_Diagnosis'))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Diagnosis/Btn_Select'))
	
	WebUI.delay(2)
}

if (ProductType != '') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_ProductType'))
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_ProductType', [
				('ProductType') : ProductType]))
	GlobalVariable.ProductType = ProductType
	
	GEN5.ProcessingCommand()
}

if (Room != '') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_LookUp_TreatmentRB'))
	
		GEN5.ProcessingCommand()
	
		GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Treatment RB/Lst_TreatmentRB'),
			'Room Type', Room)
	
		WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Treatment RB/Btn_Select'))
}

if (RoomOption != '') {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_RoomOption'))
	
		WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_RoomOption',
				[('RoomOption') : RoomOption]))
}

GEN5.ProcessingCommand()

//ArrayList DataClaim = [
//	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_MemberName'),
//	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Client'),
//	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_TreatmentStart'),
//	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_TreatmentEnd'),
//	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_ProductType'),
//	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Diagnosis'),
//	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_Billed'),
//	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_TreatmentRBClass'),
//	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Txt_TreatmentRBRate'),
//	findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_RoomOption')
//	]


//Script Untuk Compare
//ArrayList Field = GEN5.getFieldsValue(DataClaim)
//String Billed = Field[6].replaceAll(',' , '')
//Field.set(6, Billed)
//
//String Dated1 = Field[2].replaceAll('/' , ' ')
//Field.set(2, Dated1)
//
//String Dated2 = Field[3].replaceAll('/' , ' ')
//Field.set(3, Dated2)
//
//String RBRate = Field[8].replaceAll(',' , '')
//Field.set(8, RBRate)
//
//
//GEN5.compareRowDBtoArray("172.16.94.70", "SEA", 
//	QueryDocReg.replace("_test_", GlobalVariable.Docno), Field)

//GEN5.CompareFieldtoDatabase(Field, "172.16.94.70", "SEA", 
//	QueryDocReg.replace("_test_", GlobalVariable.Docno))

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Next'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Save'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_OK'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX'))

WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Send'), 1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Send'))

boolean checkButtonYes = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Yes'), 2, FailureHandling.OPTIONAL)

int count = 1
while (!checkButtonYes && count < 6) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Send'))
	
	KeywordUtil.markWarning("Button Send can't be Clicked " + count + " Time(s)")
	
	checkButtonYes = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Yes'), 2, FailureHandling.OPTIONAL)
	
	count++
}

if (count == 6) {
	KeywordUtil.markFailedAndStop("ERROR - Button Send is not Clickable")
}

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Yes'))

GEN5.ProcessingCommand()

WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX_Succes'), 10, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX_Succes'), 1)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX_Succes'))

GEN5.ProcessingCommand()
