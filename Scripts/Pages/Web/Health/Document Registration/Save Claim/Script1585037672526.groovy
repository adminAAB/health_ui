import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

//Jika ada Pop Up PrepostClaim

boolean Prepost = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp Prepost/PopUp_Prepost'), 2, FailureHandling.OPTIONAL)

if (Prepost == true) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp Prepost/Btn_NotPrepost'))
}

GEN5.ProcessingCommand()

//Halaman Documents (Pake Custom Keywordnya mas cok
GEN5.tickAllCheckboxInTable(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Cbx_Document'))

GEN5.ProcessingCommand()

//Save Claim
WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Save'))

GEN5.ProcessingCommand()

//Jika ada Pop Up Suspect Double

boolean SuspectDouble = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp_Suspect'), 2, FailureHandling.OPTIONAL)

if (SuspectDouble == true) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Lst_Suspect'))
	WebUI.delay(2)
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Select_Suspect'))
	GEN5.ProcessingCommand()
	//Next Claim
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Next'))
	GEN5.ProcessingCommand()
	//Save Claim
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_Save'))
	GEN5.ProcessingCommand()
}


WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_OK'))

GEN5.ProcessingCommand()


//Jika ada Pop Up Followup

boolean FollowUp = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Document Registration/Provider/FollowUp/PopUp_FollowUp'), 2, FailureHandling.OPTIONAL)

if (FollowUp == true) {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/FollowUp/Txt_ContactPerson'), ContactPerson)
	WebUI.delay(2)
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/FollowUp/Cbx_Phone'))
	WebUI.delay(2)
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/FollowUp/Txt_Phone'), Phone)
	WebUI.delay(2)
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/FollowUp/Txt_Remarks'), Remarks)
	WebUI.delay(2)
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/FollowUp/Btn_Save'))
	WebUI.delay(2)
}



//Muncul Pop Up Docno

List<String> variable = GEN5.getPopUpText (findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/PopUp_Docno'))

GlobalVariable.Docno = variable[0]


//Compare Script Antara yang diInput melalui Scenario dengan yang ada diDB
//Dimana urutan Arraylist nya juga harus berurutan yang ada di Scenario
if (GlobalVariable.DocSource == 'Provider') {
	ArrayList compare = [GlobalVariable.EmpID, GlobalVariable.MemberNo, GlobalVariable.MemberName, GlobalVariable.Client]
	ArrayList prefix = ['PM.EmpID AS EMPID','PM.MemberNo AS MemberNo','PM.Name AS MemberName','P4.Name AS ClientName']
	String input
	String editQuery
	
	int v
	for (v = 0 ; v < compare.size() ; v++) {
		if (compare[v] != '') {
			input = compare[v]
			editQuery = prefix[v]
			break
		}
	}
	
	if (ProductType == 'Inpatient' || ProductType == 'Maternity') {
		ArrayList compareInputan = [input, GlobalVariable.HargaRB[1], StartDate.replace('/' , ' '), EndDate.replace('/' , ' '), ProductType, Diagnosis, Billed.replace(',' , ''), Room, GlobalVariable.HargaRB[0].replace(',' , ''), RoomOption]
	
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryDocReg.replace("_displayPrefix_", editQuery).replace("_test_", GlobalVariable.Docno), compareInputan)
		
		WebUI.delay(2)
	} else {
		ArrayList compareInputan = [input, StartDate.replace('/' , ' '), EndDate.replace('/' , ' '), ProductType, Diagnosis, Billed.replace(',' , '')]
		
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryDocReg2.replace("_displayPrefix_", editQuery).replace("_test_", GlobalVariable.Docno), compareInputan)
		
		WebUI.delay(2)
	}
} else if (GlobalVariable.DocSource == 'Member') {
	if (ProductType == 'Inpatient') {
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryMember.replace("_test_", GlobalVariable.Docno), GlobalVariable.FieldDataMember)
		GEN5.ProcessingCommand()
		
	} else if (ProductType == 'Outpatient') {
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryMember2.replace("_test_", GlobalVariable.Docno), GlobalVariable.FieldDataMember)
		GEN5.ProcessingCommand()
	}
} else if (GlobalVariable.DocSource == 'Client') {

	if (GlobalVariable.ProductType == 'Inpatient') {	
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryDocRegIP.replace("_VariableDocNo_", GlobalVariable.Docno), GlobalVariable.FieldDataMember)
		WebUI.delay(2)
		
	} else if (GlobalVariable.ProductType == 'Outpatient') {
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryDocRegOP.replace("_VariableDocNo_", GlobalVariable.Docno), GlobalVariable.FieldDataMember)
		WebUI.delay(2)
	}
}


WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Btn_CloseX'))

GEN5.ProcessingCommand()
