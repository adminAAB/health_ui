import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

ArrayList MemberName = GEN5.getFieldsValue([findTestObject('Object Repository/Web/Health/Document Registration/Member/Search Member/Field Member')])

WebUI.click(findTestObject('Web/Health/Document Registration/Member/Search Member/BTN LookupMember'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Web/Health/Document Registration/Btn_Next'), 3)

if (GlobalVariable.EmpID != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_EmpID'),
		GlobalVariable.EmpID)
}

if (GlobalVariable.MemberNo != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_MemberNo'),
		GlobalVariable.MemberNo)
}

if (GlobalVariable.MemberName != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_MemberName'),
		GlobalVariable.MemberName)
}

if (GlobalVariable.Client != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Provider/Create Claim/Search Member/Txt_Client'),
		GlobalVariable.Client)
}

//WebUI.setText(findTestObject('Web/Health/Document Registration/Member/Search Member/TXT MemberNo'), MemberNo)

WebUI.click(findTestObject('Web/Health/Document Registration/Member/Search Member/BTN Search Member'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Search Member/BTN Choose Result'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Search Member/BTN Select'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Btn_Next'))

GEN5.ProcessingCommand()

if (MemberName[0].toString() != '') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Document Registration/Member/Search Member/TXT Provider'), GlobalVariable.Provider)
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Button Search'))
	
	GEN5.ProcessingCommand()
	
	GEN5.ClickExpectedRowWithNext(findTestObject('Object Repository/Web/Health/Document Registration/Member/Search Member/TABLE Search Member'), "Treatment Start", GlobalVariable.StartDate, findTestObject('Object Repository/Web/Health/Document Registration/Member/Search Member/BTN Next Search Member'))
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/Search Member/BTN Select Search Member'))

} else {
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Button Search'))
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Document Registration/Member/New Claim/Button New Claim'))
}






