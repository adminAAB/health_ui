import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5


String DocNo

if (GEN5.verifyStaging())
{
	DocNo = GEN5.getValueDatabase("172.16.94.48", "LiTT", "select top 1 value from datahealth_staging where type='"+Type+"' and RowStatus='0' order by value asc", "Value")
}
else
{
	DocNo = GEN5.getValueDatabase("172.16.94.48", "LiTT", "select top 1 value from datahealth where type='"+Type+"' and RowStatus='0' order by value asc", "Value")
}

println DocNo


if (CheckSearchSorting=='Yes')
{

	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_DocumentNo'), InvalidMemberName)
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Search'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_MemberName'), InvalidClientName)
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Search'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_EmpID'), InvalidEmpID)
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Search'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_ClientName'), InvalidDocNo)
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Search'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_DocumentNo'), DocNo)
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_DocumentNo'), InvalidMemberName)
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Search'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_DocumentNo'), DocNo)
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_MemberName'), MemberName)
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_EmpID'), EmpID)
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_ClientName'), ClientName)
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Search'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_DocumentNo'), DocNo)
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Search'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_MemberName'), MemberName)
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Search'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_EmpID'), EmpID)
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Search'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_ClientName'), ClientName)
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Search'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_DocNo'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_DocNo'))
	GEN5.ProcessingCommand()
	
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_ClaimNo'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_ClaimNo'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_MemberName'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_MemberName'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_MemberNo'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_MemberNo'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_Client'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_Client'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_ProviderName'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_ProviderName'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_ClaimType'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_ClaimType'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_TreatmentStart'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_TreatmentStart'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_TreatmentFinish'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_TreatmentFinish'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_Status'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_Status'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_ProductType'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_ProductType'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_Billed'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_Billed'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_HospInvNo'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_HospInvNo'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_Receiver'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_Receiver'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_Source'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Sorting_Source'))
	GEN5.ProcessingCommand()
}

WebUI.delay(4)
WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Txt_DocumentNo'), DocNo)
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Search'))
GEN5.ProcessingCommand()

//compare grid

ArrayList Row_Grid = GEN5.getAllRowsValue(findTestObject('Object Repository/Web/Health/Membership Tasklist/Grid_MembershipTaskList'), "Document No",DocNo)


String DocNo_Array = Row_Grid[0].trim()
Row_Grid.set(0, DocNo_Array)

String ClaimNo_Array = Row_Grid[1].trim()
Row_Grid.set(1, ClaimNo_Array)

String MemberName_Array = Row_Grid[2].trim()
Row_Grid.set(2, MemberName_Array)

String MemberNo_Array = Row_Grid[3].trim()
Row_Grid.set(3, MemberNo_Array)

String Client_Array = Row_Grid[4].trim()
Row_Grid.set(4, Client_Array)

String ProviderName_Array = Row_Grid[5].trim()
Row_Grid.set(5, ProviderName_Array)

String ClaimType_Array = Row_Grid[6].trim()
Row_Grid.set(6, ClaimType_Array)

String TreatmentStart_Array = Row_Grid[7].trim()
Row_Grid.set(7, TreatmentStart_Array)

String TreatmentEnd_Array = Row_Grid[8].trim()
Row_Grid.set(8, TreatmentEnd_Array)

String Status_Array = Row_Grid[9].trim()
Row_Grid.set(9, Status_Array)

String ProductType_Array = Row_Grid[10].trim()
Row_Grid.set(10, ProductType_Array)

String Billed = Row_Grid[11].replace('Rp. ','').replace(',','').trim()
Row_Grid.set(11, Billed)

String HosInv_Array = Row_Grid[12].trim()
Row_Grid.set(12, HosInv_Array)

String Receiver_Array = Row_Grid[13].trim()
Row_Grid.set(13, Receiver_Array)

String Source_Array = Row_Grid[14].trim()
Row_Grid.set(14, Source_Array)

String Query_Grid_MembershipTaskList

if(Scenario=='UnregMember')
{
Query_Grid_MembershipTaskList = "select tdrd.DocumentNo,tdrd.ClaimNo,tdrdu.PatientName,'' as MemberNo,p.name as Client,p2.name as Provider,case when tdrd.claimType='C' then 'Cashless' else 'Reimburse' end as ClaimType,FORMAT (CAST(TDRD.TreatmentStart AS DATE), 'dd/MMM/yyyy') AS TreatmentStart,FORMAT (CAST(TDRD.TreatmentEnd AS DATE), 'dd/MMM/yyyy') AS TreatmentEnd,'Follow Up' as status,pt.description as ProductType,tdrd.totalbill as Billed,tdr.refNo,su.name as Receiver,'Dispatcher' as Source from tbl_disp_registration_document as tdrd left join tbl_disp_registration_unregmember as tdrdu on tdrd.DocumentNo=tdrdu.DocumentNo left join profile as p on tdrd.client=p.id left join profile as p2 on tdrd.Provider=p2.id left join ProductType as pt on tdrd.ProductType = pt.ProductType left join tbl_disp_registration as tdr on tdrd.registrationNo=tdr.registrationNo left join sysuser as su on tdrd.PICreceiver = su.id where tdrd.DocumentNo="+DocNo
}
else if(Scenario=='UnregAccount')
{
Query_Grid_MembershipTaskList = " select tdrd.DocumentNo,tdrd.ClaimNo,pm.Name,pm.MemberNo as MemberNo,p.name as Client,p2.name as Provider,case when tdrd.claimType='C' then 'Cashless' else 'Reimburse' end as ClaimType,FORMAT (CAST(TDRD.TreatmentStart AS DATE), 'dd/MMM/yyyy') AS TreatmentStart,FORMAT (CAST(TDRD.TreatmentEnd AS DATE), 'dd/MMM/yyyy') AS TreatmentEnd,'Follow Up' as status,pt.description as ProductType,tdrd.totalbill as Billed,tdr.refNo,su.name as Receiver,'Dispatcher' as Source from tbl_disp_registration_document as tdrd left join profile as p on tdrd.client=p.id left join profile as p2 on tdrd.Provider=p2.id left join ProductType as pt on tdrd.ProductType = pt.ProductType left join tbl_disp_registration as tdr on tdrd.registrationNo=tdr.registrationNo left join sysuser as su on tdrd.PICreceiver = su.id left join policy_member as pm on tdrd.mno=pm.mno where tdrd.DocumentNo="+DocNo
}

GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_Grid_MembershipTaskList, Row_Grid)
 //// Akhir CekGridDepan


WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Row1_Grid'))
WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Select'))
GEN5.ProcessingCommand()


WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Btn_Back'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Select'))
GEN5.ProcessingCommand()


if (CheckDetail=='Yes')
{
	
	
	
	if(Scenario=='UnregMember')
	{
		ArrayList DetailClaim = [	findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_ClientName'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_MemberName'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_DeliveryReceiveDate'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_GardaMedikaReceiveDate'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_Provider'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_TreatmentStart'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_TreatmentEnd'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Btn_ProductType'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_Diagnosis'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_TotalBilled'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_Receiver'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Btn_PayTo'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_AccountNo'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_BankName'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_AccountName'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_BankBranch')
			]

		ArrayList Field = GEN5.getFieldsValue(DetailClaim)
		
		String Billed2 = Field[9].replace('Rp. ','').replace(',','').trim()
		Field.set(9, Billed2)
		
		String Query_ClaimDetails = "select  p.name as ClientName, tdrdu.PatientName as MemberName,FORMAT (CAST(tdr.deliveryReceiveDate AS DATE), 'dd/MMM/yyyy') AS deliveryReceiveDate,FORMAT (CAST(tdr.documentReceiveDate AS DATE), 'dd/MMM/yyyy') AS documentReceiveDate,p2.name as Provider,FORMAT (CAST(TDRD.TreatmentStart AS DATE), 'dd/MMM/yyyy') AS TreatmentStart,FORMAT (CAST(TDRD.TreatmentEnd AS DATE), 'dd/MMM/yyyy') AS TreatmentEnd,pt.description as ProductType, d.description as Diagnosis,tdrd.totalbill as Billed,su.name as Receiver,CASE WHEN tdrd.PayToDisp='P' then 'Provider' WHEN tdrd.PayToDisp='C' then 'Client' else 'Member' end as PayTo,tdrd.AccountNoDisp, tdrd.BankNameDisp, tdrd.BankAccountDisp, tdrd.BankBranchDisp from tbl_disp_registration_document as tdrd left join tbl_disp_registration_unregmember as tdrdu on tdrd.DocumentNo=tdrdu.DocumentNo left join profile as p on tdrd.client=p.id left join profile as p2 on tdrd.Provider=p2.id left join ProductType as pt on tdrd.ProductType = pt.ProductType left join tbl_disp_registration as tdr on tdrd.registrationNo=tdr.registrationNo left join sysuser as su on tdrd.PICreceiver = su.id left join diagnosis as d on tdrd.diagnosisID=d.Diagnosis where tdrd.DocumentNo="+DocNo
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_ClaimDetails, Field)
	}
	else if(Scenario=='UnregAccount')
	{
		ArrayList DetailClaim = [	findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_ClientName'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_MemberName'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_MemberNo'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_EmpID'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_Sex'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_Birthdate'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_DeliveryReceiveDate'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_GardaMedikaReceiveDate'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_Provider'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_TreatmentStart'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_TreatmentEnd'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Btn_ProductType'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_Diagnosis'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_TotalBilled'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_Receiver'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Btn_PayTo'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_AccountNo'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_BankName'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_AccountName'),
			findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Txt_BankBranch')
			]

		ArrayList Field = GEN5.getFieldsValue(DetailClaim)
		
		String Billed2 = Field[13].replace('Rp. ','').replace(',','').trim()
		Field.set(13, Billed2)
		
		Query_ClaimDetails = "select  p.name as ClientName, pm.Name as MemberName,pm.memberNo,pm.EmpID,case when pm.Sex='F' then 'Female' else 'Male' end as Sex,FORMAT (CAST(pm.BirthDate AS DATE), 'dd/MMM/yyyy') AS Birthdate,FORMAT (CAST(tdr.deliveryReceiveDate AS DATE), 'dd/MMM/yyyy') AS deliveryReceiveDate,FORMAT (CAST(tdr.documentReceiveDate AS DATE), 'dd/MMM/yyyy') AS documentReceiveDate,p2.name as Provider,FORMAT (CAST(TDRD.TreatmentStart AS DATE), 'dd/MMM/yyyy') AS TreatmentStart,FORMAT (CAST(TDRD.TreatmentEnd AS DATE), 'dd/MMM/yyyy') AS TreatmentEnd,pt.description as ProductType, d.description as Diagnosis,tdrd.totalbill as Billed,su.name as Receiver,CASE WHEN tdrd.PayToDisp='P' then 'Provider' WHEN tdrd.PayToDisp='C' then 'Client' else 'Member' end as PayTo,tdrd.AccountNoDisp, tdrd.BankNameDisp, tdrd.BankAccountDisp, tdrd.BankBranchDisp from tbl_disp_registration_document as tdrd left join policy_member as pm on tdrd.mno=pm.mno left join profile as p on tdrd.client=p.id left join profile as p2 on tdrd.Provider=p2.id left join ProductType as pt on tdrd.ProductType = pt.ProductType left join tbl_disp_registration as tdr on tdrd.registrationNo=tdr.registrationNo left join sysuser as su on tdrd.PICreceiver = su.id left join diagnosis as d on tdrd.diagnosisID=d.Diagnosis where tdrd.DocumentNo="+DocNo
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_ClaimDetails, Field)
		
	}
	
	
	
}


if(Scenario=='UnregMember')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Btn_Search_Member'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_X'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Btn_Search_Member'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Txt_MemberName'), Registered_MemberNameSorting)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Sorting_PatientName'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Sorting_PatientName'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Sorting_EmpID'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Sorting_EmpID'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Sorting_MemberNo'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Sorting_MemberNo'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Sorting_ClientName'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Sorting_ClientName'))
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Sorting_Sex'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Sorting_Sex'))
	GEN5.ProcessingCommand()
	
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Sorting_BirthDate'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Sorting_BirthDate'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Txt_EmpID'), Registered_EmpID)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Txt_EmpID'), Registered_EmpID)
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Txt_MemberName'), Registered_MemberName)
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Txt_MemberNo'), Registered_MemberNo)
	GEN5.ProcessingCommand()
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Search'))
	GEN5.ProcessingCommand()
	
	
	//compare grid member
	
	ArrayList Row_Grid2 = GEN5.getAllRowsValue(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Grid_Member'), "No","1")
	
	String No_Array2 = Row_Grid2[0].trim()
	Row_Grid2.set(0, No_Array2)
	
	String PatientName_Array2 = Row_Grid2[1].trim()
	Row_Grid2.set(1, PatientName_Array2)
	
	String EmpID_Array2 = Row_Grid2[2].trim()
	Row_Grid2.set(2, EmpID_Array2)
	
	String MemberNo_Array2 = Row_Grid2[3].trim()
	Row_Grid2.set(3, MemberNo_Array2)
	
	String ClientName_Array2 = Row_Grid2[4].trim()
	Row_Grid2.set(4, ClientName_Array2)
	
	String MembershipType_Array2 = Row_Grid2[5].trim()
	Row_Grid2.set(5, MembershipType_Array2)
	
	String Sex_Array2 = Row_Grid2[6].trim()
	Row_Grid2.set(6, Sex_Array2)
	
	String BirthDate_Array2 = Row_Grid2[7].trim()
	Row_Grid2.set(7, BirthDate_Array2)
	
	String Query_Grid_Member = " select top 1 '1' as No, pm.Name, pm.EmpID, pm.MemberNo, p2.Name, case when pm.Membership='1. EMP' then 'Employee' when pm.Membership='2. SPO' then 'Spouse' else 'Child' end as MembershipType , case when pm.Sex='F' then 'Female' else 'Male' end as Sex,  FORMAT (CAST(pm.BirthDate AS DATE), 'dd/MMM/yyyy') AS BirthDate  from policy_member as pm  left join policy as p on pm.pno=p.pno  left join profile as p2 on p.ClientID=p2.ID  where pm.memberNo='"+Registered_MemberNo+"' and pm.EmpID='"+Registered_EmpID+"' and pm.Name='"+Registered_MemberName+"' order by pm.pno desc"
	GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_Grid_Member, Row_Grid2)
	 //// Akhir CekGridMember
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Txt_MemberNo'), Registered_MemberNo)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Row1_GridMember'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Member/Btn_Select'))
	GEN5.ProcessingCommand()
	
	
}

if(Scenario=='UnregAccount')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Btn_SearchAccount'))
	GEN5.ProcessingCommand()
		
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Account/Btn_X'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Btn_SearchAccount'))
	GEN5.ProcessingCommand()
		
	
	
	
	//compare grid account
	
	ArrayList Row_Grid3 = GEN5.getAllRowsValue(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Account/Grid_AccountList'), "No","1")
	
	String No_Array3 = Row_Grid3[0].trim()
	Row_Grid3.set(0, No_Array3)
	
	String AccountNo_Array3 = Row_Grid3[1].trim()
	Row_Grid3.set(1, AccountNo_Array3)
	
	String AccountName_Array3 = Row_Grid3[2].trim()
	Row_Grid3.set(2, AccountName_Array3)
	
	String BankName_Array3 = Row_Grid3[3].trim()
	Row_Grid3.set(3, BankName_Array3)
	
	String BankBranch_Array3 = Row_Grid3[4].trim()
	Row_Grid3.set(4, BankBranch_Array3)
	
	String Query_Grid_Account = "select '1' as no,pma.AccountNo,pma.AccountName,pma.bankName, case when  pma.bankBranch is null then b.bankHO else pma.bankBranch end as bankBranch from tbl_disp_registration_document as tdrd left join profile as p on tdrd.client=p.id left join profile as p2 on tdrd.Provider=p2.id left join ProductType as pt on tdrd.ProductType = pt.ProductType left join tbl_disp_registration as tdr on tdrd.registrationNo=tdr.registrationNo left join sysuser as su on tdrd.PICreceiver = su.id left join policy_member as pm on tdrd.mno=pm.mno left join policy_member_account as pma on pm.mno=pma.mno left join bank as b on b.bank=pma.bank where tdrd.DocumentNo="+DocNo
	
	
	GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_Grid_Account, Row_Grid3)
	 //// Akhir CekGridMember
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Account/Row1_GirdAccount'))
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Pop Up Search Account/Btn_Select'))
	GEN5.ProcessingCommand()
	
	
}

WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Claim Details/Btn_Save'))
GEN5.ProcessingCommand()

String query_update = 'UPDATE dbo.DataHealth SET RowStatus = 1 WHERE value = \'' + DocNo + '\''

GEN5.updateValueDatabase("172.16.94.48", "LiTT", query_update)

WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_Close'))
GEN5.ProcessingCommand()


WebUI.click(findTestObject('Object Repository/Web/Health/Membership Tasklist/Btn_YesClose'))
GEN5.ProcessingCommand()













