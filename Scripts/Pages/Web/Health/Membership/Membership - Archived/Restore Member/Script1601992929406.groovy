import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.util.KeywordUtil

WebUI.click(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Lst_Top1'))

WebUI.click(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Btn_Select'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()


String ClientName = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldClientName'), 'value')
String MemberNo = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldMemberNo'), 'value')
String MemberName = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldMemberName'), 'value')
String EmpID = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldEmpID'), 'value')
String Classification = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldClassification'), 'value')
String Area = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldArea'), 'value')
String MembershipType = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldMembershipType'), 'value')
String MembershipStatus = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldMembershipStatus'), 'value')
String Sex = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldSex'), 'value')
String CardNo = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldCardNo'), 'value')
String CardRequestDate =  WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldCardRequestDate'), 'value')
String CardSendDate = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldCardSendDate'), 'value')
String Age = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldAge'), 'value')
String MaritalStatus = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldMaritalStatus'), 'value')
String PhoneNo = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldPhone'), 'value')
String Email = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/Field_Email'), 'value')
String AccountBank = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldAccountBank'), 'value')
String AccountNo = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldAccountNo'), 'value')
String AccountName = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldAccountName'), 'value')
String CardPrintDate = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldCardPrintDate'), 'value')
String CardReceiveDate = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/FieldCardReceiveDate'), 'value')

ArrayList dataui = [ClientName,MemberNo,MemberName,EmpID,Classification,Area,MembershipType,MembershipStatus,Sex,CardNo,CardRequestDate,CardSendDate,Age,MaritalStatus,PhoneNo,Email,AccountBank,AccountNo,AccountName,CardPrintDate,CardReceiveDate]

println(dataui)

WebUI.delay(4)
WebUI.click(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/Btn_Restore'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/Btn_Yes'))
WebUI.delay(1)

boolean Restore = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/PopUp_WaitingRestore'), 2, FailureHandling.OPTIONAL)

if (Restore == true) {
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/Btn_RestoreX'))
	WebUI.delay(1)
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Details/Btn_Close'))
}else {
	WebUI.click(findTestObject('Object Repository/Web/Health/Membership/Membership - Archived/Btn_X'))
}






