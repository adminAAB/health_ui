import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import org.openqa.selenium.Keys as Keys
import com.keyword.GEN5

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Diagnosis/DiagnosisSelected', [('ID'):GlobalVariable.IDDiagnosis]), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/DiagnosisSelected', [('ID'):GlobalVariable.IDDiagnosis]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/IconEditDiagnosis'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis/FieldName'), NameEdited, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis/FieldDescription'), DescriptionEdited, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/MedicalTreatmentSelected', [('MedicalTreatmentDeleted'):GlobalVariable.MedicalTreatment]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/IconDeleteMedicalTreatment'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/ButtonYesDelete'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/IconAddMedicalTreatment'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/IconSearchMedicalTreatment'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis/FieldSearchMedicalTreatment'), MedicalTreatmentEdited, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/ButtonSearchMedicalTreatment'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/MedicalTreatmentSearchSelected', [('MedicalTreatment'):MedicalTreatmentEdited]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/ButtonSelectMedicalTreatment'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/ButtonSaveMedicalTreatment'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/DiagnosisCategorySelected', [('DiagnosisCategoryDeleted'):GlobalVariable.DiagnoisisCategory]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/IconDeleteDiagnosisCategory'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/ButtonYesDelete'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Diagnosis/IconAddDiagnosisCategory'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/IconAddDiagnosisCategory'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/IconSearchDiagnosisCategory'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis/FieldSearchDiagnosisCategory'), DiagnosisCategoryEdited, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/ButtonSearchDiagnosisCategory'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/DiagnosisCategorySearchSelected', [('DiagnosisCategory'):DiagnosisCategoryEdited]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/ButtonSelectDiagnosisCategory'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/ButtonSaveDiagnosisCategory'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/ButtonSaveDiagnosisDetails'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis/IconCloseSuccessAdd'), FailureHandling.STOP_ON_FAILURE)

String query = "SELECT * FROM dbo.Diagnosis WHERE Diagnosis= '"+GlobalVariable.IDDiagnosis+"'"

String dbNameEdited = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Name")
println(dbNameEdited)

String dbDescriptionEdited = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Description_IN")
println(dbDescriptionEdited)

String query2 = "SELECT * FROM dbo.MedicalTreatment WHERE MedicalTreatmentID IN (SELECT MedicalTreatmentID FROM dbo.DiagnosisTreatment WHERE Diagnosis IN (SELECT Diagnosis FROM dbo.Diagnosis WHERE Diagnosis= '"+GlobalVariable.IDDiagnosis+"'))"

String dbMedicalTreatmentEdited = GEN5.getValueDatabase("172.16.94.70", "SEA", query2, "Description")
println(dbMedicalTreatmentEdited)

String query3 = "SELECT * FROM dbo.DiagnosisCategory WHERE DiagnosisCategoryID IN (SELECT DiagnosisCategoryID FROM dbo.DiagnosisCategoryDiagnosis  WHERE Diagnosis IN (SELECT Diagnosis FROM dbo.Diagnosis WHERE Diagnosis='"+GlobalVariable.IDDiagnosis+"'))"

String dbDiagnosisCategoryEdited = GEN5.getValueDatabase("172.16.94.70", "SEA", query3, "Description")
println(dbDiagnosisCategoryEdited)

if ((NameEdited == dbNameEdited) && (DescriptionEdited == dbDescriptionEdited) && (MedicalTreatmentEdited == dbMedicalTreatmentEdited) & (DiagnosisCategoryEdited == dbDiagnosisCategoryEdited)){
	KeywordUtil.markPassed("PASSED")
	}
else {
	KeywordUtil.markFailedAndStop("FAILED")
	}

