import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import org.junit.After
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import groovy.time.TimeCategory
import com.keyword.*

WebDriver driver = DriverFactory.getWebDriver()
//def rowRegistration = findTestData("getHealthPremiRegistration").getAllData().size()
String PremiRegistrationQuery = "SELECT TransactionType, Classification, MembershipType, ProductType, NumOfMember, Premium FROM HealthPremiRegistration where Tipe =(SELECT param1 FROM dbo.HealthParam WHERE USER_ID='"+GlobalVariable.SystemUser+"')"
int rowRegistration = CustomKeywords.'healthKeyword.utilityDB.getCountRow'(PremiRegistrationQuery)
String PremiInstallmentQuery = "SELECT Description,DueDate,GrossPremiumFee FROM HealthPremiInstallment where Tipe =(SELECT param1 FROM dbo.HealthParam WHERE USER_ID='"+GlobalVariable.SystemUser+"')"
int rowInstallment = CustomKeywords.'healthKeyword.utilityDB.getCountRow'(PremiInstallmentQuery)

//buat compare premi registration
def Transtype
def Classification
def MemberType
def ProdType
def NoM
def Premi

//buat compare premi installment
def Description
def DueDate
def GrossPremiumFee
//buat compare premi yang ada commission
def GrossCommission
def VAT
def Tax

Date today = new Date()

WebUI.click(findTestObject('Web/Health/Policy/Tab_PolicyMenu',['key':'Premium']))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Policy/Premium/Chk_TransactionType'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Premium/Chk_Classification'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

if(GlobalVariable.Tipe =='TS55'){
	WebUI.click(findTestObject('Web/Health/Policy/Premium/Chk_MembershipType'))
	
	CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
	WebUI.delay(1)
}else if (GlobalVariable.Tipe =='PolicyEditEmployeeID'){
	WebUI.click(findTestObject('Web/Health/Policy/Premium/Chk_ProductType'))
	
	CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
	WebUI.delay(1)
}

//compare premi registration
CustomKeywords.'healthKeyword.utilityDB.getHealthPremiRegistration'()
WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)
if(GlobalVariable.Tipe =='TS54' || GlobalVariable.Tipe == 'TS58' || GlobalVariable.Tipe == 'PolicyEditEmployeeID'){
	for(int i = 0;i<rowRegistration;i++){
		Transtype = driver.findElement(By.xpath('//*[@id="TabPremiumViewTable"]/a2is-datatable/div[2]/div/table/tbody/tr['+(i+1)+']/td[1]/span')).text.toString()
		Classification = driver.findElement(By.xpath('//*[@id="TabPremiumViewTable"]/a2is-datatable/div[2]/div/table/tbody/tr['+(i+1)+']/td[2]/span')).text.toString()
		MemberType = driver.findElement(By.xpath('//*[@id="TabPremiumViewTable"]/a2is-datatable/div[2]/div/table/tbody/tr['+(i+1)+']/td[3]/span')).text.toString()
		ProdType = driver.findElement(By.xpath('//*[@id="TabPremiumViewTable"]/a2is-datatable/div[2]/div/table/tbody/tr['+(i+1)+']/td[4]/span')).text.toString()
		NoM = driver.findElement(By.xpath('//*[@id="TabPremiumViewTable"]/a2is-datatable/div[2]/div/table/tbody/tr['+(i+1)+']/td[5]/span')).text.toString()
		Premi = driver.findElement(By.xpath('//*[@id="TabPremiumViewTable"]/a2is-datatable/div[2]/div/table/tbody/tr['+(i+1)+']/td[6]/span')).text.toString()
		WebUI.verifyEqual(Transtype,GlobalVariable.compTransType[i])
		WebUI.verifyEqual(Classification,GlobalVariable.compClassification[i])
		WebUI.verifyEqual(MemberType,GlobalVariable.compMembershipType[i])
		WebUI.verifyEqual(ProdType,GlobalVariable.compProductType[i])
		WebUI.verifyEqual(NoM,GlobalVariable.compNumOfMember[i])
		WebUI.verifyEqual(Premi,GlobalVariable.compPremium[i])
	}
}
println "Compare Tab Premium Registration Beres"
WebUI.switchToDefaultContent()
WebUI.delay(1)

def compareDate = ""
use(TimeCategory,{
	today = new Date()
	compareDate = (today + 30.day)
	})
compareDate = compareDate.format("dd/MMM/yyyy")

//compare premi installment
CustomKeywords.'healthKeyword.utilityDB.getHealthPremiInstallment'()
WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)
WebUI.delay(1)
for(int i = 0;i<rowInstallment;i++){
	Description = driver.findElement(By.xpath('//*[@id="TabPremiumPremiumInstallmentTable"]/a2is-datatable/div[2]/div/table/tbody/tr['+(i+1)+']/td[1]/span')).text.toString()
	DueDate = driver.findElement(By.xpath('//*[@id="DueDateTxt_'+i+'"]')).getAttribute('value').toString()
	GrossPremiumFee = driver.findElement(By.xpath('//*[@id="GrossPremium'+i+'"]')).getAttribute('value').toString()
	if(GlobalVariable.Tipe == 'TS57'){
	GrossCommission = driver.findElement(By.xpath('//*[@id="TabPremiumPremiumInstallmentTable"]/a2is-datatable/div[2]/div/table/tbody/tr['+(i+1)+']/td[4]/span')).text.toString()
	VAT = driver.findElement(By.xpath('//*[@id="TabPremiumPremiumInstallmentTable"]/a2is-datatable/div[2]/div/table/tbody/tr['+(i+1)+']/td[5]/span')).text.toString()
	Tax = driver.findElement(By.xpath('//*[@id="TabPremiumPremiumInstallmentTable"]/a2is-datatable/div[2]/div/table/tbody/tr['+(i+1)+']/td[6]/span')).text.toString()
	}else if(GlobalVariable.Tipe == 'TS58'){
	GrossCommission = driver.findElement(By.xpath('//*[@id="TabPremiumPremiumInstallmentTable"]/a2is-datatable/div[2]/div/table/tbody/tr['+(i+1)+']/td[4]/span')).text.toString()
	VAT = driver.findElement(By.xpath('//*[@id="TabPremiumPremiumInstallmentTable"]/a2is-datatable/div[2]/div/table/tbody/tr['+(i+1)+']/td[5]/span')).text.toString()
	Tax = driver.findElement(By.xpath('//*[@id="TabPremiumPremiumInstallmentTable"]/a2is-datatable/div[2]/div/table/tbody/tr['+(i+1)+']/td[6]/span')).text.toString()
	}
	
	WebUI.verifyEqual(Description, GlobalVariable.compDescription[i])
	if(GlobalVariable.Tipe!='TS55' && GlobalVariable.Tipe!='TS58' && GlobalVariable.Tipe !='PolicyEditEmployeeID'){
		WebUI.verifyEqual(DueDate, GlobalVariable.compDueDate[i])
	}else{
		WebUI.verifyEqual(DueDate,compareDate)
	}
	WebUI.verifyEqual(GrossPremiumFee, GlobalVariable.compGrossPremiumFee[i])
}
println "Compare Tab Premium Installment Beres"
WebUI.switchToDefaultContent()

if(GlobalVariable.Tipe =='TS54'){
	WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Web/Health/Policy/Premium/Txt_TotalGrossPremium'), 'value'), 'Rp. 5,739,015.39')
	
}else if(GlobalVariable.Tipe == 'TS55'){
	WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Web/Health/Policy/Premium/Txt_TotalGrossPremium'), 'value'), 'Rp. 584,714.05')
	
}else if(GlobalVariable.Tipe == 'TS57'){
	WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Web/Health/Policy/Premium/Txt_TotalGrossPremium'), 'value'), 'Rp. 80,470,000.00')
	
	WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Web/Health/Policy/Premium/Txt_TotalGrossCommission'), 'value'),'Rp. -12,048,000.00')
	
}else if(GlobalVariable.Tipe == 'TS58'){
	WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Web/Health/Policy/Premium/Txt_TotalGrossPremium'), 'value'), 'Rp. 20,080,000.00')
	
	WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Web/Health/Policy/Premium/Txt_TotalGrossCommission'), 'value'),'Rp. -3,012,000.00')
}
// compare export premium
WebUI.click(findTestObject('Web/Health/Policy/Premium/Btn_Export'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
if(GlobalVariable.Tipe!='PolicyEditEmployeeID'){
	def currentPNO = CustomKeywords.'healthKeyword.utilityDB.getCurrentPNO'(GlobalVariable.PolicyNo)
	String todaysDate = today.format("yyyy/MM/dd")
	String nowTime = today.format("HH:mm")
	todaysDate = todaysDate.replaceAll("/","")
	nowTime = nowTime.replaceAll(":","")
	//def fileName = currentPNO+"."+todaysDate+"."+nowTime
	def fileName = currentPNO+"."+todaysDate
	
	WebUI.click(findTestObject('Web/Health/Policy/Premium/Export Premium Info/Btn_Process'))
	
	CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
	WebUI.delay(2)
	
//	CustomKeywords.'csvThing.CSVProcessor.compareExportPremium'(fileName)
}