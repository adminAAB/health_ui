import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

def memberChangeClass
def newClassification
if(GlobalVariable.Tipe == 'TS55'){
	memberChangeClass = 'ARI AUTOMATE'
	newClassification = 'Class 1 IP-500'
}else if(GlobalVariable.Tipe == 'TS58'){
	memberChangeClass = 'WAHYU AUTOMATE'
	newClassification = 'OP-150'
}
def effDate = '1/Jan/2020'
String[] Dates = effDate.split("/")
String Tanggal = Dates[0]
String Bulan = Dates[1]
String Tahun = Dates[2]
WebDriver driver = DriverFactory.getWebDriver()

WebUI.setText(findTestObject('Web/Health/Policy/Member/Txt_MemberName'),memberChangeClass)

//cek kalo current transactionnya ke ceklis
WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)
boolean currentTransaction = driver.findElement(By.xpath('//*[@id="TabMemberCurrentEndorsement"]/a2is-multi-check-dc/div[2]/div[1]/div/div/input')).isSelected()
WebUI.switchToDefaultContent()

if(currentTransaction){
	WebUI.click(findTestObject('Web/Health/Policy/Member/Chk_CurrentTransaction'))
	WebUI.delay(1)
}

WebUI.click(findTestObject('Web/Health/Policy/Member/Btn_Search'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Member/Lst_Member',['key':memberChangeClass]))

WebUI.click(findTestObject('Web/Health/Policy/Member/Btn_ChangeClassification'))

WebUI.click(findTestObject('Web/Health/Policy/Member/Change Classification/Txt_NewClassification'))

WebUI.click(findTestObject('Web/Health/Policy/Member/Change Classification/Lst_NewClassification',['key':newClassification]))

WebUI.click(findTestObject('Web/Health/Policy/Member/Change Classification/Btn_SetDate'))

CustomKeywords.'healthKeyword.utilityKeyword.setDatePicker'(effDate,'8')
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Policy/Member/Change Classification/Btn_SaveChangeClassification'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)