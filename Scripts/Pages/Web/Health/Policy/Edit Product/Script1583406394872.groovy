import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

String PolicyProductDetailQuery = "SELECT * FROM dbo.HealthPolicyProductDetails WHERE idPolicy=(SELECT param2 FROM dbo.HealthParam WHERE USER_ID='"+GlobalVariable.SystemUser+"')"
ArrayList PolicyProductDetails = com.keyword.UI.getOneRowDatabase(GlobalVariable.LiTTURL, 'LiTT', PolicyProductDetailQuery)

GlobalVariable.ProductType = PolicyProductDetails[3]
GlobalVariable.ReimbursementPercentage = PolicyProductDetails[4]
GlobalVariable.CustomOverallLimit =  PolicyProductDetails[5]
GlobalVariable.IndividualLimit =  PolicyProductDetails[6]
GlobalVariable.FamilyLimit =  PolicyProductDetails[7]
def NoRefund =  PolicyProductDetails[8]
def NoRefIfAlreadyClaim =  PolicyProductDetails[9]

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Web/Health/Policy/Tab_PolicyMenu',['key':'Product']))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Policy/Product/Exp_ProductType'))

WebUI.click(findTestObject('Web/Health/Policy/Product/Lst_ProductType',['key':GlobalVariable.ProductType]))

WebUI.click(findTestObject('Web/Health/Policy/Product/Btn_Search'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebDriver driver = DriverFactory.getWebDriver()
WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)

GlobalVariable.Classification1 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[1]/span')).text.toString()
WebUI.switchToDefaultContent()


WebUI.click(findTestObject('Web/Health/Policy/Product/Lst_Product',['key':GlobalVariable.Classification1]))

WebUI.click(findTestObject('Web/Health/Policy/Product/Btn_Edit'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

//WebUI.setText(findTestObject('Web/Health/Policy/Product/Product Info/Txt_ReimbursementPercentage'), GlobalVariable.reimbursementPercentage)
//WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Product/Product Info/Txt_CustomOverallLimit'))

WebUI.click(findTestObject('Web/Health/Policy/Product/Product Info/Lst_CustomOverallLimit',['key':GlobalVariable.CustomOverallLimit]))

WebUI.clearText(findTestObject('Web/Health/Policy/Product/Product Info/Txt_IndividualAL'))

WebUI.click(findTestObject('Web/Health/Policy/Product/Product Info/Txt_IndividualAL'))

WebUI.sendKeys(findTestObject('Web/Health/Policy/Product/Product Info/Txt_IndividualAL'), GlobalVariable.IndividualLimit)


WebUI.clearText(findTestObject('Web/Health/Policy/Product/Product Info/Txt_FamilyAL'))

WebUI.click(findTestObject('Web/Health/Policy/Product/Product Info/Txt_FamilyAL'))

WebUI.sendKeys(findTestObject('Web/Health/Policy/Product/Product Info/Txt_FamilyAL'), GlobalVariable.FamilyLimit)

WebUI.click(findTestObject('Web/Health/Policy/Product/Product Info/Txt_NoRefund'))

WebUI.click(findTestObject('Web/Health/Policy/Product/Product Info/Lst_NoRefund',['key':NoRefund]))

WebUI.click(findTestObject('Web/Health/Policy/Product/Product Info/Txt_NoRefundClaim'))

WebUI.click(findTestObject('Web/Health/Policy/Product/Product Info/Lst_NoRefundClaim',['key':NoRefIfAlreadyClaim]))


GlobalVariable.ProductPlan = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_PPlanId'),'value')
GlobalVariable.ProductPlanName = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_PPlanName'),'value')
GlobalVariable.ProductType = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_ProductType'),'value')
GlobalVariable.IndividualLimit = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_IndividualAL'), 'value')
GlobalVariable.FamilyLimit = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_FamilyAL'),'value')
GlobalVariable.NoRefund = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_NoRefund'),'value')
GlobalVariable.NoRefundClaim = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_NoRefundClaim'),'value')