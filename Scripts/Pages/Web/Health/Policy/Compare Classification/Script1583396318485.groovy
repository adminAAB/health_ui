import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebUI.click(findTestObject('Web/Health/Policy/Tab_PolicyMenu',['key':'Classification']))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebDriver driver = DriverFactory.getWebDriver()
WebUI.switchToFrame(findTestObject('Object Repository/frame'), 5)
def classification1 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[2]/span')).text.toString()
def classification2 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[2]/span')).text.toString()
def classification3 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[2]/span')).text.toString()
def empClassification1 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[3]/span')).text.toString()
def empClassification2 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[3]/span')).text.toString()
def empClassification3 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[3]/span')).text.toString()
def classProduct1 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[7]/span')).text.toString()
def classProduct2 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[7]/span')).text.toString()
def classProduct3 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[7]/span')).text.toString()
WebUI.switchToDefaultContent()


WebUI.verifyEqual(GlobalVariable.Classification1, classification1)
WebUI.verifyEqual(GlobalVariable.Classification2, classification2)
WebUI.verifyEqual(GlobalVariable.Classification3, classification3)
WebUI.verifyEqual(GlobalVariable.EmpClassification1, empClassification1)
WebUI.verifyEqual(GlobalVariable.EmpClassification2, empClassification2)
WebUI.verifyEqual(GlobalVariable.EmpClassification3, empClassification3)
WebUI.verifyEqual(GlobalVariable.ClassProduct1, classProduct1)
WebUI.verifyEqual(GlobalVariable.ClassProduct2, classProduct2)
WebUI.verifyEqual(GlobalVariable.ClassProduct3, classProduct3)
WebUI.verifyElementChecked(findTestObject('Web/Health/Policy/Classification/Chk_Emp1'), 1)
WebUI.verifyElementChecked(findTestObject('Web/Health/Policy/Classification/Chk_Emp2'), 1)
WebUI.verifyElementChecked(findTestObject('Web/Health/Policy/Classification/Chk_Emp3'), 1)
WebUI.verifyElementChecked(findTestObject('Web/Health/Policy/Classification/Chk_Spouse1'), 1)
WebUI.verifyElementChecked(findTestObject('Web/Health/Policy/Classification/Chk_Spouse2'), 1)
WebUI.verifyElementChecked(findTestObject('Web/Health/Policy/Classification/Chk_Spouse3'), 1)
WebUI.verifyElementChecked(findTestObject('Web/Health/Policy/Classification/Chk_Child1'), 1)
WebUI.verifyElementChecked(findTestObject('Web/Health/Policy/Classification/Chk_Child2'), 1)
WebUI.verifyElementChecked(findTestObject('Web/Health/Policy/Classification/Chk_Child3'), 1)

WebUI.scrollToElement(findTestObject('Web/Health/Policy/Lbl_Policy'), 2)

WebUI.switchToDefaultContent()

println "Compare Classification Beres"

//ini buat ngehapus policy classification biar gak numpuk
if(GlobalVariable.SystemUser=='RJW'||GlobalVariable.SystemUser=='OHS'){
	def fileName = 'C:\\HasilDownloadA2IS\\Policy Classification.csv'
	CustomKeywords.'healthKeyword.utilityKeyword.Delete_File'(fileName)
}else{
	def fileName = 'E:\\HasilDownloadA2IS\\Policy Classification.csv'
	CustomKeywords.'healthKeyword.utilityKeyword.Delete_File'(fileName)
}