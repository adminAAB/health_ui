import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebDriver driver = DriverFactory.getWebDriver()
def currentPNO = CustomKeywords.'healthKeyword.utilityDB.getCurrentPNO'(GlobalVariable.PolicyNo)
Date today = new Date()
String todaysDate = today.format("dd/MMM/yyyy")
def statProcess = ""
int maxLoop = 0


WebUI.click(findTestObject('Web/Health/Policy/Btn_ApprovePolicy'))
WebUI.delay(2)

WebUI.click(findTestObject('Web/Health/Policy/Approve Policy/Btn_Process'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

statProcess = CustomKeywords.'healthKeyword.utilityDB.getStatusProccess'()

if(statProcess!='2'){
	WebUI.click(findTestObject('Web/Health/Policy/Btn_CloseConfirm'))
	
	statProcess = CustomKeywords.'healthKeyword.utilityDB.getStatusProccess'()
	while(statProcess=='1' && maxLoop<100){
		println("BACKGROUND PROCESS MASIH BERJALAN")
		WebUI.delay(10)
		statProcess = CustomKeywords.'healthKeyword.utilityDB.getStatusProccess'()
		maxLoop++;
	}
	WebUI.delay(1)
	WebUI.click(findTestObject('Web/Health/Policy/Btn_ApprovePolicy'))
	
	WebUI.click(findTestObject('Web/Health/Policy/Approve Policy/Btn_Process'))
	
	CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
	WebUI.delay(1)
}

WebUI.click(findTestObject('Web/Health/Policy/Btn_CloseConfirm'))

WebUI.switchToFrame(findTestObject('Object Repository/frame'), 5)

if (GlobalVariable.Tipe == 'TS54'){
	def compPNO = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[1]/span')).text.toString()
	def compTransaction = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[2]/span')).text.toString()
	def compApprovedDate = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[3]/span')).text.toString()
	def compPIC = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[4]/span')).text.toString()
	def compReferenceNo = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[5]/span')).text.toString()
	def compDescription = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[6]/span')).text.toString()
	def compGrossPremium = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[7]/span')).text.toString()
	def compAdminFee = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[8]/span')).text.toString()
	def compNetPremium = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[9]/span')).text.toString()
	def compPayment = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[10]/span')).text.toString()
	
	WebUI.delay(1)
	WebUI.verifyEqual(compPNO, currentPNO)
	WebUI.verifyEqual(compTransaction, "New Policy")
	WebUI.verifyEqual(compApprovedDate, todaysDate)
	WebUI.verifyEqual(compPIC, "Arnold Yoab Wijaya")
	WebUI.verifyEqual(compReferenceNo,"")
	WebUI.verifyEqual(compDescription,"")
	WebUI.verifyEqual(compGrossPremium,"Rp. 5,639,015.39")
	WebUI.verifyEqual(compAdminFee,"Rp. 100,000.00")
	WebUI.verifyEqual(compNetPremium, "Rp. 5,739,015.39")
	WebUI.verifyEqual(compPayment,"0 of 2 Premium Installment Settled")
	println "COMPARE HISTORY BERES"
}else if (GlobalVariable.Tipe == 'TS55'){
	def compPNO = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[1]/span')).text.toString()
	def compTransaction = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[2]/span')).text.toString()
	def compApprovedDate = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[3]/span')).text.toString()
	def compPIC = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[4]/span')).text.toString()
	def compReferenceNo = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[5]/span')).text.toString()
	def compDescription = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[6]/span')).text.toString()
	def compGrossPremium = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[7]/span')).text.toString()
	def compAdminFee = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[8]/span')).text.toString()
	def compNetPremium = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[9]/span')).text.toString()
	def compPayment = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[10]/span')).text.toString()
	
	WebUI.delay(1)
	WebUI.verifyEqual(compPNO, currentPNO)
	WebUI.verifyEqual(compTransaction, "Endorsement - 1")
	WebUI.verifyEqual(compApprovedDate, todaysDate)
//	WebUI.verifyEqual(compPIC, "Winando Bimantoro")
	WebUI.verifyEqual(compReferenceNo,"Automate Registration, Change, and Termination")
	WebUI.verifyEqual(compDescription,"Change from "+GlobalVariable.SystemUser+" tanggal "+todaysDate)
	WebUI.verifyEqual(compGrossPremium,"Rp. 584,714.05")
	WebUI.verifyEqual(compAdminFee,"Rp. 0.00")
	WebUI.verifyEqual(compNetPremium, "Rp. 584,714.05")
	WebUI.verifyEqual(compPayment,"")
	println "COMPARE HISTORY BERES"
}else if (GlobalVariable.Tipe == 'TS57'){
	def compPNO = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[1]/span')).text.toString()
	def compTransaction = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[2]/span')).text.toString()
	def compApprovedDate = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[3]/span')).text.toString()
	def compPIC = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[4]/span')).text.toString()
	def compReferenceNo = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[5]/span')).text.toString()
	def compDescription = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[6]/span')).text.toString()
	def compGrossPremium = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[7]/span')).text.toString()
	def compAdminFee = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[8]/span')).text.toString()
	def compGrossCommission = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[9]/span')).text.toString()
	def compVAT = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[10]/span')).text.toString()
	def compTax = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[11]/span')).text.toString()
	def compNetPremium = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[12]/span')).text.toString()
	def compPaymentStatus = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[13]/span')).text.toString()
	
	WebUI.delay(1)
	WebUI.verifyEqual(compPNO, currentPNO)
	WebUI.verifyEqual(compTransaction, "New Policy")
	WebUI.verifyEqual(compApprovedDate, todaysDate)
	WebUI.verifyEqual(compPIC, "Arnold Yoab Wijaya")
	WebUI.verifyEqual(compReferenceNo, "")
	WebUI.verifyEqual(compDescription, "")
	WebUI.verifyEqual(compGrossPremium, "Rp. 80,320,000.00")
	WebUI.verifyEqual(compAdminFee, "Rp. 150,000.00")
	WebUI.verifyEqual(compGrossCommission, "Rp. -12,048,000.00")
	WebUI.verifyEqual(compVAT,"Rp. -1,204,800.00")
	WebUI.verifyEqual(compTax,"Rp. 0.00")
	WebUI.verifyEqual(compNetPremium, "Rp. 67,217,200.00")
	WebUI.verifyEqual(compPaymentStatus,"")
	println "COMPARE HISTORY BERES"
}else if (GlobalVariable.Tipe == 'TS58'){
	def compPNO = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[1]/span')).text.toString()
	def compTransaction = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[2]/span')).text.toString()
	def compApprovedDate = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[3]/span')).text.toString()
	def compPIC = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[4]/span')).text.toString()
	def compReferenceNo = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[5]/span')).text.toString()
	def compDescription = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[6]/span')).text.toString()
	def compGrossPremium = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[7]/span')).text.toString()
	def compAdminFee = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[8]/span')).text.toString()
	def compGrossCommission = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[9]/span')).text.toString()
	def compVAT = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[10]/span')).text.toString()
	def compTax = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[11]/span')).text.toString()
	def compNetPremium = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[12]/span')).text.toString()
	def compPaymentStatus = driver.findElement(By.xpath('//*[@id="TabPremiumHistoryTable"]/a2is-datatable/div[2]/div/table/tbody/tr/td[13]/span')).text.toString()
	
	WebUI.delay(1)
	WebUI.verifyEqual(compPNO, currentPNO)
	WebUI.verifyEqual(compTransaction, "Endorsement - 1")
	WebUI.verifyEqual(compApprovedDate, todaysDate)
//	WebUI.verifyEqual(compPIC, "Winando Bimantoro")
	WebUI.verifyEqual(compReferenceNo, "Automate Registration, Change, and Termination")
	WebUI.verifyEqual(compDescription, "Change from "+GlobalVariable.SystemUser+" tanggal "+todaysDate)
	WebUI.verifyEqual(compGrossPremium, "Rp. 20,080,000.00")
	WebUI.verifyEqual(compAdminFee, "Rp. 0.00")
	WebUI.verifyEqual(compGrossCommission, "Rp. -3,012,000.00")
	WebUI.verifyEqual(compVAT,"Rp. -301,200.00")
	WebUI.verifyEqual(compTax,"Rp. 0.00")
	WebUI.verifyEqual(compNetPremium, "Rp. 16,766,800.00")
	WebUI.verifyEqual(compPaymentStatus,"")
	println "COMPARE HISTORY BERES"
}
WebUI.switchToDefaultContent()
WebUI.delay(1)