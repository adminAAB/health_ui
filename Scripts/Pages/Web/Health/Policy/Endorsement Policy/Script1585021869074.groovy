import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def requester = GlobalVariable.SystemUser

WebUI.click(findTestObject('Web/Health/Policy/Btn_EndorsePolicy'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.setText(findTestObject('Web/Health/Policy/Endorse Policy/Txt_Requester'), requester)

WebUI.click(findTestObject('Web/Health/Policy/Endorse Policy/Exp_EndorsementType'))

WebUI.click(findTestObject('Web/Health/Policy/Endorse Policy/Lst_ComboSearchTop1',['key':'Change']))

WebUI.setText(findTestObject('Web/Health/Policy/Endorse Policy/Txt_ReferenceNo'), 'Automate Registration, Change, and Termination')

WebUI.click(findTestObject('Web/Health/Policy/Endorse Policy/Btn_FileUpload'))
WebUI.delay(2)
CustomKeywords.'healthKeyword.general.RewriteFileName'('Book1.pdf')
CustomKeywords.'healthKeyword.general.UploadFile'()
WebUI.delay(10)

WebUI.click(findTestObject('Web/Health/Policy/Endorse Policy/Btn_Process'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(2)
CustomKeywords.'healthKeyword.general.waitA2ISBlocker'()
WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Web/Health/Policy/Lbl_Policy'), 2)