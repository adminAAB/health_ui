import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

String PolicyProductDetailQuery = "SELECT * FROM dbo.HealthPolicyProductDetails WHERE idPolicy=(SELECT param2 FROM dbo.HealthParam WHERE USER_ID='"+GlobalVariable.SystemUser+"')"
ArrayList PolicyProductDetails = com.keyword.UI.getOneRowDatabase(GlobalVariable.LiTTURL, 'LiTT', PolicyProductDetailQuery)

GlobalVariable.CompBenefitId = PolicyProductDetails[10]
GlobalVariable.OccurencesAmount = PolicyProductDetails[11]
GlobalVariable.OccurencesFrequency = PolicyProductDetails[12]
GlobalVariable.RbAmountTolerance = PolicyProductDetails[13]
GlobalVariable.RbAmountToleranceDuration = PolicyProductDetails[14]
GlobalVariable.RbLvlTolerance = PolicyProductDetails[15]
GlobalVariable.RbLvlToleranceDuration = PolicyProductDetails[16]

WebDriver driver = DriverFactory.getWebDriver()

WebUI.click(findTestObject('Web/Health/Policy/Product/Tab_ProductMenu',['key':'Benefit List']))

WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Lst_Benefit',['key':GlobalVariable.CompBenefitId]))

WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Btn_EditBenefit'))

WebUI.clearText(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_OccurenceAmount'))

WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_OccurenceAmount'))

WebUI.sendKeys(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_OccurenceAmount'), GlobalVariable.OccurencesAmount)

WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_OccurenceFrequency'))

WebUI.clearText(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_OccurenceFrequency'))

WebUI.sendKeys(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_OccurenceFrequency'), GlobalVariable.OccurencesFrequency)

WebUI.scrollToElement(findTestObject('Web/Health/Policy/Product/Benefit List/Btn_SaveBenefit'), 2)


//ngecek checkbox di klik apa engga
WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)

boolean ChkBoxLvlTolerance = driver.findElement(By.xpath('//*[@id="RBLevelCHK"]/a2is-multi-check-nc/div[2]/div[1]/div/div/input')).isSelected()
boolean ChkBoxAmountTolerance = driver.findElement(By.xpath('//*[@id="RBAmountCHK"]/a2is-multi-check-nc/div[2]/div[1]/div/div/input')).isSelected()
WebUI.switchToDefaultContent()

if(GlobalVariable.Tipe == 'TS54'){
	if (!ChkBoxAmountTolerance){
		WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Chk_RBAmountTolerance'))
		
	}
	
	WebUI.clearText(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_AmountTolerance'))
	
	WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_AmountTolerance'))
	
	WebUI.sendKeys(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_AmountTolerance'), GlobalVariable.RbAmountTolerance)
	
	WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_ToleranceMaxDays'))
	
	WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Lst_TolerencaMaxDays',['key':GlobalVariable.RbAmountToleranceDuration]))
	
	if (!ChkBoxLvlTolerance){
		WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Chk_RBLevelTolerance'))
	
	}
	WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_Level'))
	WebUI.delay(1)
	WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Lst_Level',['key':GlobalVariable.RbLvlTolerance]))
	WebUI.delay(1)
	WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_LevelToleranceDays'))
	WebUI.delay(1)
	WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Lst_LevelToleranceDays',['key':GlobalVariable.RbLvlToleranceDuration]))
	WebUI.delay(1)
}

GlobalVariable.CompBenefitName = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_BenefitName'), 'value')
GlobalVariable.CompBenefitSequence = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_SequenceNo'), 'value')
GlobalVariable.OccurencesAmount = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_OccurenceAmount'), 'value')
GlobalVariable.OccurencesFrequency = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_OccurenceFrequency'), 'value')
GlobalVariable.ConfinementAmount = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_ConfinementAmount'), 'value')
GlobalVariable.ConfinementFrequency = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_ConfinementFrequency'), 'value')
GlobalVariable.YearlyAmount = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_YearlyAmount'), 'value')
GlobalVariable.YearlyFrequency = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_YearlyFrequency'), 'value')
if(GlobalVariable.Tipe == 'TS54'){
	GlobalVariable.RbAmountTolerance = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_AmountTolerance'), 'value')
	GlobalVariable.RbAmountToleranceDuration = WebUI.getText(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_ToleranceMaxDays')).toString()
	GlobalVariable.RbLvlTolerance = WebUI.getText(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_Level')).toString()
	GlobalVariable.RbLvlToleranceDuration = WebUI.getText(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_LevelToleranceDays')).toString()
}

WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Btn_SaveBenefit'))

WebUI.click(findTestObject('Web/Health/Policy/Product/Btn_SaveProduct'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Product/Btn_CloseConfirm'))