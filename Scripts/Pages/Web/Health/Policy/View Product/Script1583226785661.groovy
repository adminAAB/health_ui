import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

String MasterPolicyQuery = "SELECT * FROM dbo.HealthPolicyInfo WHERE isRUn = 0 AND Tipe='"+GlobalVariable.Tipe+"'"
ArrayList MasterPolicyData = com.keyword.UI.getOneRowDatabase(GlobalVariable.LiTTURL, 'LiTT', MasterPolicyQuery)
GlobalVariable.ProductType = MasterPolicyData[27]

WebUI.click(findTestObject('Web/Health/Policy/Tab_PolicyMenu',['key':'Product']))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()

WebUI.click(findTestObject('Web/Health/Policy/Product/Exp_ProductType'))

WebUI.click(findTestObject('Web/Health/Policy/Product/Lst_ProductType',['key':GlobalVariable.ProductType]))

WebUI.click(findTestObject('Web/Health/Policy/Product/Btn_Search'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()

WebDriver driver = DriverFactory.getWebDriver()
WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)
GlobalVariable.ProdClassification1 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[1]/span')).text.toString()
GlobalVariable.ProdClassification2 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[1]/span')).text.toString()
GlobalVariable.ProdClassification3 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[1]/span')).text.toString()
GlobalVariable.ProdType1 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[2]/span')).text.toString()
GlobalVariable.ProdType2 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[2]/span')).text.toString()
GlobalVariable.ProdType3 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[2]/span')).text.toString()
GlobalVariable.ProdPlan1 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[3]/span')).text.toString()
GlobalVariable.ProdPlan2 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[3]/span')).text.toString()
GlobalVariable.ProdPlan3 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[3]/span')).text.toString()
GlobalVariable.ProdDescription1 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[4]/span')).text.toString()
GlobalVariable.ProdDescription2 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[4]/span')).text.toString()
GlobalVariable.ProdDescription3 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[4]/span')).text.toString()
GlobalVariable.ProdAnnual1 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[5]/span')).text.toString()
GlobalVariable.ProdAnnual2 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[5]/span')).text.toString()
GlobalVariable.ProdAnnual3 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[5]/span')).text.toString()
WebUI.switchToDefaultContent()

WebUI.scrollToElement(findTestObject('Web/Health/Policy/Lbl_Policy'), 2)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('Web/Health/Policy/Btn_BackToPolicyMenu'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
