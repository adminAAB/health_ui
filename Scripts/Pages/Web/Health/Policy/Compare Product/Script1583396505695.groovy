import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory


//WebUI.delay(1)
//WebUI.setText(findTestObject('Web/Health/Policy/Txt_PolicyNo'), GlobalVariable.policyNo)
//WebUI.delay(1)
//
//WebUI.click(findTestObject('Web/Health/Policy/Btn_Search'))
//WebUI.delay(1)
//CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
//WebUI.delay(1)
//WebUI.click(findTestObject('Web/Health/Policy/Lst_PolicyInquiry',['key':GlobalVariable.policyNo]))
//WebUI.delay(1)
//WebUI.click(findTestObject('Web/Health/Policy/Btn_EditPolicy'))
//WebUI.delay(1)
//CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
//WebUI.delay(1)
//WebUI.switchToFrame(findTestObject('Object Repository/frame'), 5)
//WebUI.delay(1)
//WebUI.scrollToPosition(0, 0)
//WebUI.delay(1)
//WebUI.switchToDefaultContent()
//WebUI.delay(1)

String MasterPolicyQuery = "SELECT * FROM dbo.HealthPolicyInfo WHERE isRUn = 0 AND Tipe='"+GlobalVariable.Tipe+"'"
GlobalVariable.ProductType = com.keyword.UI.getValueDatabase(GlobalVariable.LiTTURL, 'LiTT', MasterPolicyQuery,'ProductType')

WebUI.click(findTestObject('Web/Health/Policy/Tab_PolicyMenu',['key':'Product']))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Policy/Product/Exp_ProductType'))

WebUI.click(findTestObject('Web/Health/Policy/Product/Lst_ProductType',['key':GlobalVariable.ProductType]))

WebUI.click(findTestObject('Web/Health/Policy/Product/Btn_Search'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebDriver driver = DriverFactory.getWebDriver()
println GlobalVariable.Tipe
if(GlobalVariable.Tipe == 'TS53'||GlobalVariable.Tipe == 'TS56') {
	WebUI.switchToFrame(findTestObject('Object Repository/frame'), 5)
	def prodClassification1 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[1]/span')).text.toString()
	def prodClassification2 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[1]/span')).text.toString()
	def prodClassification3 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[1]/span')).text.toString()
	def prodType1 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[2]/span')).text.toString()
	def prodType2 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[2]/span')).text.toString()
	def prodType3 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[2]/span')).text.toString()
	def prodPlan1 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[3]/span')).text.toString()
	def prodPlan2 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[3]/span')).text.toString()
	def prodPlan3 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[3]/span')).text.toString()
	def prodDescription1 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[4]/span')).text.toString()
	def prodDescription2 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[4]/span')).text.toString()
	def prodDescription3 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[4]/span')).text.toString()
	def prodAnnual1 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[5]/span')).text.toString()
	def prodAnnual2 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[5]/span')).text.toString()
	def prodAnnual3 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[5]/span')).text.toString()
	WebUI.switchToDefaultContent()
	WebUI.delay(1)
	
	WebUI.verifyEqual(GlobalVariable.ProdClassification1, prodClassification1)
	WebUI.verifyEqual(GlobalVariable.ProdClassification2, prodClassification2)
	WebUI.verifyEqual(GlobalVariable.ProdClassification3, prodClassification3)
	WebUI.verifyEqual(GlobalVariable.ProdType1, prodType1)
	WebUI.verifyEqual(GlobalVariable.ProdType2, prodType2)
	WebUI.verifyEqual(GlobalVariable.ProdType3, prodType3)
	WebUI.verifyEqual(GlobalVariable.ProdPlan1, prodPlan1)
	WebUI.verifyEqual(GlobalVariable.ProdPlan2, prodPlan2)
	WebUI.verifyEqual(GlobalVariable.ProdPlan3, prodPlan3)
	WebUI.verifyEqual(GlobalVariable.ProdDescription1, prodDescription1)
	WebUI.verifyEqual(GlobalVariable.ProdDescription2, prodDescription2)
	WebUI.verifyEqual(GlobalVariable.ProdDescription3, prodDescription3)
	WebUI.verifyEqual(GlobalVariable.ProdAnnual1, prodAnnual1)
	WebUI.verifyEqual(GlobalVariable.ProdAnnual2, prodAnnual2)
	WebUI.verifyEqual(GlobalVariable.ProdAnnual3, prodAnnual3)
	
	println "Compare Product Beres"
	
}else if(GlobalVariable.Tipe == 'TS54' || GlobalVariable.Tipe =='TS57'){

	WebUI.switchToFrame(findTestObject('Object Repository/frame'), 5)
	WebUI.delay(1)
	GlobalVariable.Classification1 = driver.findElement(By.xpath('//*[@id="TabProductSearchTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[1]/span')).text.toString()
	WebUI.switchToDefaultContent()
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Web/Health/Policy/Product/Lst_Product',['key':GlobalVariable.Classification1]))
	WebUI.delay(1)
	WebUI.click(findTestObject('Web/Health/Policy/Product/Btn_Edit'))
	WebUI.delay(1)
	CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
	WebUI.delay(1)
	
	def productPlan = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_PPlanId'),'value')
	def productPlanName = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_PPlanName'),'value')
//	def productType = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_ProductType'),'value')
	def IndividualLimit = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_IndividualAL'), 'value')
	def FamilyLimit = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_FamilyAL'),'value')
//	def reimbursementPercentage = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_ReimbursementPercentage'), 'value')
	def customOverallLimit = WebUI.getText(findTestObject('Web/Health/Policy/Product/Product Info/Txt_CustomOverallLimit')).toString()
	def productType = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_ProductType'),'value')
	def NoRefund = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_NoRefund'),'value')
	def NoRefundClaim = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Product Info/Txt_NoRefundClaim'),'value')
	
	WebUI.verifyEqual(GlobalVariable.ProductPlan, productPlan)
	WebUI.verifyEqual(GlobalVariable.ProductPlanName, productPlanName)
	WebUI.verifyEqual(GlobalVariable.IndividualLimit, IndividualLimit)
	WebUI.verifyEqual(GlobalVariable.FamilyLimit, FamilyLimit)
//	WebUI.verifyEqual(GlobalVariable.reimbursementPercentage, reimbursementPercentage)
	WebUI.verifyEqual(GlobalVariable.CustomOverallLimit, customOverallLimit)
	WebUI.verifyEqual(GlobalVariable.ProductType, productType)
	WebUI.verifyEqual(GlobalVariable.NoRefund, NoRefund)
	WebUI.verifyEqual(GlobalVariable.NoRefundClaim, NoRefundClaim)
	WebUI.delay(1)
	println "Compare Beres"	
}