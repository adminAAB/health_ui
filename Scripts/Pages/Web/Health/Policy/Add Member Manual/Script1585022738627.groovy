import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.stringtemplate.v4.compiler.STParser.memberExpr_return

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

Date today = new Date()

def MemberName
def Classification
def Sex
def BirthDate
def MaritalStatus
def Phone
def Email
def AccountNo
def EffectiveDate

if (GlobalVariable.Tipe == 'TS55'){
	MemberName = "ADI AUTOMATE"
	Classification = "Class 3 IP-1000"
	Sex = "Male"
	BirthDate = "1/Jan/1990"
	MaritalStatus = "Single"
	Phone = "009988776655"
	Email = "afebian@asuransiastra.com"
	AccountNo = "112233445566"
	EffectiveDate = "1/Jan/2020"
}else if(GlobalVariable.Tipe == 'TS58'){
	MemberName = "RUDI AUTOMATE"
	Classification = "OP-150"
	Sex = "Male"
	BirthDate = "1/Jan/1990"
	MaritalStatus = "Single"
	Phone = "009988776666"
	Email = "febian@asuransiastra.com"
	AccountNo = "112233445555"
	EffectiveDate = "1/Jan/2020"
}

String[] Dates = BirthDate.split("/")
String Tanggal = Dates[0]
String Bulan = Dates[1]
String Tahun = Dates[2]
WebDriver driver = DriverFactory.getWebDriver()
def HealthParamQuery = "SELECT * FROM HealthParam where User_Id = '"+GlobalVariable.SystemUser+"'"
ArrayList GetHealthParam = com.keyword.UI.getOneRowDatabase(GlobalVariable.LiTTURL, 'LiTT', HealthParamQuery)
GlobalVariable.compEmpId = GetHealthParam[5]


//GlobalVariable.compEmpId = findTestData("getHealthParam").getValue(6, 1)

WebUI.click(findTestObject('Web/Health/Policy/Tab_PolicyMenu',['key':'Member']))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

//cek kalo current transactionnya ceklis
WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)

boolean currentTransaction = driver.findElement(By.xpath('//*[@id="TabMemberCurrentEndorsement"]/a2is-multi-check-dc/div[2]/div[1]/div/div/input')).isSelected()
WebUI.switchToDefaultContent()

if(!currentTransaction){
	WebUI.click(findTestObject('Web/Health/Policy/Member/Chk_CurrentTransaction'))
}

WebUI.click(findTestObject('Web/Health/Policy/Member/Btn_Search'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Member/Btn_AddMember'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Exp_MembershipType'))

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Lst_MembershipType',['key':'Employee']))

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Txt_EmployeeID'), GlobalVariable.compEmpId)

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Txt_MemberName'),MemberName)

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Txt_Classification'))

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Lst_ComboSearchTop1',['key':Classification]))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Txt_Sex'))

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Lst_Sex',['key':Sex]))

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Btn_DoBDatePicker'))

CustomKeywords.'healthKeyword.utilityKeyword.setDatePicker'(BirthDate,'9')
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Txt_MaritalStatus'))

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Lst_MaritalStatus',['key':'Single']))

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Btn_EffDatePicker'))

CustomKeywords.'healthKeyword.utilityKeyword.setDatePicker'(EffectiveDate,'10')

//WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Txt_ImportantPerson'))
//
//WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Lst_ImportantPerson',['key':'Yes']))

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Txt_Area'), 'HO')

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Txt_Phone'),Phone)

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Txt_Email'), Email)


WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Btn_ExpAccountBank'))

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Bank/Txt_BankName'),'Bank Central Asia')

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Bank/Btn_Search'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Bank/Lst_BankTop1'))

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Bank/Btn_Select'))

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Txt_AccountName'), MemberName)

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Txt_AccountNo'), AccountNo)

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Btn_Save'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Btn_CloseConfirm'))
