import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String MasterPolicyQuery = "SELECT * FROM dbo.HealthParam WHERE User_Id='"+GlobalVariable.SystemUser+"'"
GlobalVariable.PolicyNo = com.keyword.UI.getValueDatabase(GlobalVariable.LiTTURL, 'LiTT', MasterPolicyQuery, 'Param5')


WebUI.setText(findTestObject('Web/Health/Policy/Txt_PolicyNo'), GlobalVariable.PolicyNo)

WebUI.click(findTestObject('Web/Health/Policy/Btn_Search'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Lst_PolicyInquiry',['key':GlobalVariable.PolicyNo]))

WebUI.click(findTestObject('Web/Health/Policy/Btn_EditPolicy'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.scrollToElement(findTestObject('Web/Health/Policy/Lbl_Policy'), 2)

GlobalVariable.ClientName = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_Client'),'value')
GlobalVariable.PremiumRefundAdj = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_PremiumRefund'),'value')