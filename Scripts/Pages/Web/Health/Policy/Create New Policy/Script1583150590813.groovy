import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String MasterPolicyQuery = "SELECT * FROM dbo.HealthPolicyInfo WHERE isRUn = 0 AND Tipe='"+GlobalVariable.Tipe+"'"
ArrayList MasterPolicyData = com.keyword.UI.getOneRowDatabase(GlobalVariable.LiTTURL, 'LiTT', MasterPolicyQuery)

GlobalVariable.ClientId = MasterPolicyData[2]
GlobalVariable.OrderType = MasterPolicyData[5]
GlobalVariable.Branch = MasterPolicyData[6]
GlobalVariable.Department = MasterPolicyData[7]
GlobalVariable.ClassOfBusiness = MasterPolicyData[8]
GlobalVariable.Segment = MasterPolicyData[9]
GlobalVariable.SegmentOfBusiness = MasterPolicyData[10]
GlobalVariable.Channel = MasterPolicyData[11]
GlobalVariable.SeparationExcess = 'No'
GlobalVariable.ExcessMBP = MasterPolicyData[16]
GlobalVariable.AppropriateRBMustBeHigher = 'No'
GlobalVariable.FamilyPlan = MasterPolicyData[17]
GlobalVariable.MaxChildren = MasterPolicyData[18]
GlobalVariable.MaxBirthDelivery = MasterPolicyData[19]
GlobalVariable.ChildAgeLimit = MasterPolicyData[20]
GlobalVariable.MaxBackDate = '999'
GlobalVariable.ReimbursementLTT = '14'
GlobalVariable.InsuranceType = MasterPolicyData[12]
GlobalVariable.PremiumRefund = MasterPolicyData[21]
GlobalVariable.Payment = MasterPolicyData[22]
GlobalVariable.AdminFee = MasterPolicyData[23]
GlobalVariable.UseAdmedika = 'Yes'
GlobalVariable.PolicyNo = (MasterPolicyData[24]+GlobalVariable.Counter)
GlobalVariable.StartEffDate = '01/Jan/2020'
GlobalVariable.EndEffDate = '31/Dec/2020'

if(GlobalVariable.Tipe == 'TS56'){
	GlobalVariable.SourceOfBusiness = MasterPolicyData[15]
	GlobalVariable.FeePerMember = MasterPolicyData[25]
	GlobalVariable.RemainingDepositNotif = MasterPolicyData[26]
}

println "isi Policy No = "+GlobalVariable.PolicyNo

CustomKeywords.'healthKeyword.utilityDB.updateParam5'(GlobalVariable.PolicyNo)
WebUI.click(findTestObject('Web/Health/Policy/Btn_CreateNewPolicy'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)

WebUI.scrollToPosition(0, 0)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('Web/Health/Policy/Exp_Client'))

WebUI.setText(findTestObject('Web/Health/Policy/Policy Info/Client Search/Txt_ClientName'), GlobalVariable.ClientId)

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Client Search/Btn_SearchClient'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Client Search/Lst_ClientTop1'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Client Search/Btn_SelectClient'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Txt_OrderType'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_OrderType',['key':GlobalVariable.OrderType]))

WebUI.setText(findTestObject('Web/Health/Policy/Policy Info/Txt_PolicyNo'), GlobalVariable.PolicyNo)

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Exp_StartEff'))

CustomKeywords.'healthKeyword.utilityKeyword.setDatePicker'(GlobalVariable.StartEffDate,'16')

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Exp_EndEff'))

CustomKeywords.'healthKeyword.utilityKeyword.setDatePicker'(GlobalVariable.EndEffDate,'17')

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Exp_Branch'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_ComboSearchTop1',['key':GlobalVariable.Branch]))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Txt_Department'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_Department',['key':GlobalVariable.Department]))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Txt_ClassOfBusiness'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_ClassOfBusiness',['key':GlobalVariable.ClassOfBusiness]))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Txt_Segment'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_Segment',['key':GlobalVariable.Segment]))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Txt_SegmentOfBusiness'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_SegmentOfBusiness',['key':GlobalVariable.SegmentOfBusiness]))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Txt_Channel'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_Channel',['key':GlobalVariable.Channel]))

if(GlobalVariable.Channel == 'Direct Broker'){
	WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Exp_BusinessSource'))

	WebUI.setText(findTestObject('Web/Health/Policy/Policy Info/Search Business Source/Txt_BusinessSource'),GlobalVariable.SourceOfBusiness)
	
	WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Search Business Source/Btn_Search'))
	
	CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Search Business Source/Lst_BusinessSourceTop1',['key':GlobalVariable.SourceOfBusiness]))
	
	WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Search Business Source/Btn_Select'))
}
WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)

WebUI.scrollToPosition(0, 0)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Txt_SeparationExcess'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_SeparationExcess',['key':GlobalVariable.SeparationExcess]))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Txt_AppropriateRBMustBeHigher'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_AppropriateRBMustBeHigher',['key':GlobalVariable.AppropriateRBMustBeHigher]))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Txt_FamilyPlan'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_FamilyPlan',['key':GlobalVariable.FamilyPlan]))

WebUI.setText(findTestObject('Web/Health/Policy/Policy Info/Txt_MaxChildren'),GlobalVariable.MaxChildren)

WebUI.setText(findTestObject('Web/Health/Policy/Policy Info/Txt_MaxBirthDelivery'), GlobalVariable.MaxBirthDelivery)

WebUI.setText(findTestObject('Web/Health/Policy/Policy Info/Txt_ChildAgeLimit'), GlobalVariable.ChildAgeLimit)

WebUI.setText(findTestObject('Web/Health/Policy/Policy Info/Txt_MaxBackDated'), GlobalVariable.MaxBackDate)

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Exp_MaxBackDatedType'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_MaxBackDatedType',['key':'Working Days']))

WebUI.setText(findTestObject('Web/Health/Policy/Policy Info/Txt_ReimbursementLTT'),GlobalVariable.ReimbursementLTT)

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Exp_ReimbursementLTTType'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_ReimbursementLTTType',['key':'Working Days']))


WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Txt_InsuranceType'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_InsuranceType',['key':GlobalVariable.InsuranceType]))

if(GlobalVariable.InsuranceType == 'Insurance Services'){
	WebUI.setText(findTestObject('Web/Health/Policy/Policy Info/Txt_FeePerMember'), GlobalVariable.FeePerMember)
	
	WebUI.setText(findTestObject('Web/Health/Policy/Policy Info/Txt_RemainDepNotif'), GlobalVariable.RemainingDepositNotif)
	
	WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Txt_DepositType'))
	
	WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_DepositType',['key':'Deposit (Tanam)']))
}

WebUI.setText(findTestObject('Web/Health/Policy/Policy Info/Txt_PremiumRefund'), GlobalVariable.PremiumRefund)

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Txt_Payment'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_Payment',['key':GlobalVariable.Payment]))

WebUI.setText(findTestObject('Web/Health/Policy/Policy Info/Txt_AdminFee'), GlobalVariable.AdminFee)

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Txt_UseAdmedika'))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Lst_UseAdmedika',['key':GlobalVariable.UseAdmedika]))

WebUI.click(findTestObject('Web/Health/Policy/Policy Info/Btn_SavePolicyInfo'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Policy/Btn_CloseConfirm'))

GlobalVariable.AdminFee = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_AdminFee'),'value')
if(GlobalVariable.Channel=='Direct Broker'){
	GlobalVariable.FeePerMember = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_FeePerMember'), 'value')
	GlobalVariable.BusinessSourceType =  WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_BusinessSourceType'))
	GlobalVariable.BusinessSourceFee = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_BusinessSourceFee'),'value')
	GlobalVariable.CommissionOnEndorsement = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_CommOnEndorsement'))
}
GlobalVariable.ClientId = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_Client'),'value')
if(GlobalVariable.InsuranceType == 'Insurance Services'){
	GlobalVariable.FeePerMember = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_FeePerMember'), 'value')
}

WebUI.scrollToElement(findTestObject('Web/Health/Policy/Lbl_Policy'), 2)

WebUI.switchToDefaultContent()
