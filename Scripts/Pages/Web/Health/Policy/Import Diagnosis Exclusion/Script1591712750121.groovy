import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

int currentPNO = CustomKeywords.'healthKeyword.utilityDB.getCurrentPNO'(GlobalVariable.PolicyNo)
def savedate = '11/May/2020'

WebUI.click(findTestObject('Object Repository/Web/Health/Policy/Diagnosis Exclusion/Btn_Import'))

WebUI.click(findTestObject('Object Repository/Web/Health/Policy/Diagnosis Exclusion/Import Prompt/Btn_DatePicker'))
CustomKeywords.'healthKeyword.utilityKeyword.setDatePicker'(savedate,'15')

WebUI.delay(1)
WebUI.click(findTestObject('Object Repository/Web/Health/Policy/Diagnosis Exclusion/Import Prompt/Btn_ImportFile'))
WebUI.delay(1)
CustomKeywords.'healthKeyword.general.RewriteFileName'('DiagnosisExclusion.csv')
WebUI.delay(1)
CustomKeywords.'healthKeyword.general.UploadFile'()
WebUI.delay(10)
WebUI.click(findTestObject('Object Repository/Web/Health/Policy/Diagnosis Exclusion/Import Prompt/Btn_Process'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Object Repository/Web/Health/Policy/Diagnosis Exclusion/Btn_CloseConfirm'))

WebUI.click(findTestObject('Object Repository/Web/Health/Policy/Diagnosis Exclusion/Txt_EffDate'))

WebUI.click(findTestObject('Object Repository/Web/Health/Policy/Diagnosis Exclusion/Lst_EffDate',['key':savedate]))
CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

int count = CustomKeywords.'healthKeyword.utilityDB.getCountDiagnosisExclusion'(currentPNO, savedate)
println count
CustomKeywords.'healthKeyword.utilityDB.getDiagnosisExclusion'(currentPNO, savedate)

for(int i = 0;i<count;i++){
	WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Policy/Diagnosis Exclusion/Grid_DxCoverage',['key':i]),'value'), GlobalVariable.DxCoverage[i])
	WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Policy/Diagnosis Exclusion/Grid_DxPayer',['key':i]),'value'), GlobalVariable.DxPayer[i])
	WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Policy/Diagnosis Exclusion/Grid_DxConfirmation',['key':i]),'value'), GlobalVariable.DxConfirmation[i])
}
WebUI.delay(1)