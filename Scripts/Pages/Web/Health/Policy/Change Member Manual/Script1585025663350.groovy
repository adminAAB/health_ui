import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.stringtemplate.v4.compiler.STParser.memberExpr_return

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

Date today = new Date()

def MemberName
def Classification
def Sex
def BirthDate
def MaritalStatus
def Phone
def Email
def NewBank
def AccountNo

if (GlobalVariable.Tipe == 'TS55'){
	MemberName = "SANTI AUTOMATE"
	Classification = "Class 1 IP-500"
	Sex = "Female"
	BirthDate = "1/Jan/2001"
	MaritalStatus = "Married"
	Phone = "001122334455"
	Email = "febian@asuransiastra.com"
	AccountNo = "998877665544"
}else if(GlobalVariable.Tipe == 'TS58'){
	MemberName = "RIRI AUTOMATE"
	Classification = "OP-170"
	Sex = "Female"
	BirthDate = "1/Jan/2001"
	MaritalStatus = "Married"
	Phone = "001122334444"
	Email = "snurohmah@asuransiastra.com"
	AccountNo = "998877665533"
}
String[] Dates = BirthDate.split("/")
String Tanggal = Dates[0]
String Bulan = Dates[1]
String Tahun = Dates[2]
WebDriver driver = DriverFactory.getWebDriver()
WebUI.click(findTestObject('Web/Health/Policy/Member/Btn_Search'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Policy/Member/Lst_Member',['key':GlobalVariable.compEmpId]))

WebUI.click(findTestObject('Web/Health/Policy/Member/Btn_EditMember'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Txt_MemberName'),MemberName)

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Txt_Classification'))

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Lst_ComboSearchTop1',['key':Classification]))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Txt_Sex'))

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Lst_Sex',['key':Sex]))


WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Btn_DoBDatePicker'))

CustomKeywords.'healthKeyword.utilityKeyword.setDatePicker'(BirthDate,'9')
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Txt_MaritalStatus'))

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Lst_MaritalStatus',['key':MaritalStatus]))

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Btn_EffDatePicker'))

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Txt_Area'), 'Jakarta')

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Txt_Phone'),Phone)

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Txt_Email'), Email)


WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Btn_ExpAccountBank'))

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Bank/Txt_BankName'),'Bank Mandiri')

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Bank/Btn_Search'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Bank/Lst_BankTop1'))

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Bank/Btn_Select'))


WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Txt_AccountName'), MemberName)

WebUI.setText(findTestObject('Web/Health/Policy/Member/Add Member/Txt_AccountNo'), AccountNo)

WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Chk_DentalCover'))


WebUI.click(findTestObject('Web/Health/Policy/Member/Add Member/Btn_Save'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Btn_CloseConfirm'))
