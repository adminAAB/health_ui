import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

def fileName = "Import_Member_"+GlobalVariable.Tipe+".csv"
//String PolicyProductDetailQuery = "SELECT * FROM dbo.HealthPolicyProductDetails WHERE idPolicy=(SELECT param2 FROM dbo.HealthParam WHERE USER_ID='"+GlobalVariable.SystemUser+"')"
String MasterPolicyQquery = "SELECT * FROM dbo.HealthPolicyInfo WHERE isRUn = 0 AND Tipe='"+GlobalVariable.Tipe+"'"
String importOption = com.keyword.UI.getValueDatabase(GlobalVariable.LiTTURL, 'LiTT', MasterPolicyQquery, 'ImportType')

//sementara begini dulu, belum tau kedepannya ada lagi apa engga opsi topUpWithdraw
def topUpWithdraw = "80000000"


WebDriver driver = DriverFactory.getWebDriver()

WebUI.click(findTestObject('Web/Health/Policy/Tab_PolicyMenu',['key':'Member']))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
CustomKeywords.'healthKeyword.general.RewriteFileName'(fileName)

WebUI.click(findTestObject('Web/Health/Policy/Member/Btn_Import'))

WebUI.click(findTestObject('Web/Health/Policy/Member/Import Member/Txt_Option'))

WebUI.click(findTestObject('Web/Health/Policy/Member/Import Member/Lst_Option',['key':importOption]))

if(importOption == 'Import and Calculate'){
	WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)
	
	boolean ChkTopUpWithdraw = driver.findElement(By.xpath('//*[@id="TabImportMemberCombo"]/a2is-multi-check-nc/div[2]/div[1]/div/div[4]/input')).isSelected()
	WebUI.switchToDefaultContent()
	if(!ChkTopUpWithdraw){
		WebUI.click(findTestObject('Web/Health/Policy/Member/Import Member/Chk_TopUpWithdraw'))
	}
	WebUI.setText(findTestObject('Web/Health/Policy/Member/Import Member/Txt_TopUpWithdraw'),topUpWithdraw)
}

WebUI.click(findTestObject('Web/Health/Policy/Member/Import Member/Btn_ImportFile'))
CustomKeywords.'csvThing.CSVProcessor.importMemberPolicy'()
WebUI.delay(1)
CustomKeywords.'healthKeyword.general.UploadFile'()
WebUI.delay(20)

WebUI.click(findTestObject('Web/Health/Policy/Member/Import Member/Btn_Process'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(2)
WebUI.click(findTestObject('Web/Health/Policy/Member/Btn_CloseConfirm'))

WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)

if(GlobalVariable.Tipe!='PolicyNegate'){
	boolean ChkCurrentTrx = driver.findElement(By.xpath('//*[@id="TabMemberCurrentEndorsement"]/a2is-multi-check-dc/div[2]/div[1]/div/div/input')).isSelected()
	WebUI.switchToDefaultContent()
		
	if(!ChkCurrentTrx){
		WebUI.click(findTestObject('Web/Health/Policy/Member/Chk_CurrentTransaction'))
	}
	WebUI.click(findTestObject('Web/Health/Policy/Member/Btn_Search'))
	
	CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Web/Health/Policy/Member/Chk_AllEmpId'))
	
	WebUI.click(findTestObject('Web/Health/Policy/Member/Btn_Export'))
	
	CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Web/Health/Policy/Member/Export Member/Rad_CurrentTransaction'))
	
	WebUI.click(findTestObject('Web/Health/Policy/Member/Export Member/Txt_Delivery'))
	
	WebUI.click(findTestObject('Web/Health/Policy/Member/Export Member/Lst_Delivery',['key':'Download']))
	
	WebUI.click(findTestObject('Web/Health/Policy/Member/Export Member/Btn_Process'))
	
	CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
	WebUI.delay(1)
}

Date today = new Date()
String todaysDate = today.format("yyyy/MM/dd")
String nowTime = today.format("HH:mm")
todaysDate = todaysDate.replaceAll("/","")
nowTime = nowTime.replaceAll(":","")
//def policyMemberName = GlobalVariable.clientName+"."+GlobalVariable.policyNo+"."+todaysDate+"."+nowTime

def policyMemberName = GlobalVariable.ClientName+"."+GlobalVariable.PolicyNo+"."+todaysDate
if(!GlobalVariable.Tipe=='PolicyNegate'){
CustomKeywords.'csvThing.CSVProcessor.compareImportMemberPolicy'(policyMemberName)
println "COMPARE BERES"
}

if(importOption != 'Import and Calculate'){
	WebUI.callTestCase(findTestCase('Test Cases/Pages/Web/Health/Policy/Calculate Policy'), [:], FailureHandling.STOP_ON_FAILURE)
}
