import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebDriver driver = DriverFactory.getWebDriver()

WebUI.click(findTestObject('Web/Health/Policy/Product/Tab_ProductMenu',['key':'Benefit List']))

WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Lst_Benefit',['key':GlobalVariable.CompBenefitId]))

WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Btn_EditBenefit'))

def compBenefitId = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_BenefitId'),'value')
def compBenefitName = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_BenefitName'), 'value')
def compBenefitSequence = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_SequenceNo'), 'value')
def occurencesAmount = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_OccurenceAmount'), 'value')
def occurencesFrequency = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_OccurenceFrequency'), 'value')
def confinementAmount = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_ConfinementAmount'), 'value')
def confinementFrequency = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_ConfinementFrequency'), 'value')
def yearlyAmount = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_YearlyAmount'), 'value')
def yearlyFrequency = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_YearlyFrequency'), 'value')
def rbAmountTolerance
def rbAmountToleranceDuration
def rbLvlTolerance
def rbLvlToleranceDuration
if(GlobalVariable.Tipe == 'TS54'){
	rbAmountTolerance = WebUI.getAttribute(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_AmountTolerance'), 'value')
	rbAmountToleranceDuration = WebUI.getText(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_ToleranceMaxDays')).toString()
	rbLvlTolerance = WebUI.getText(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_Level')).toString()
	rbLvlToleranceDuration = WebUI.getText(findTestObject('Web/Health/Policy/Product/Benefit List/Txt_LevelToleranceDays')).toString()
}

WebUI.verifyEqual(GlobalVariable.CompBenefitId, compBenefitId)
WebUI.verifyEqual(GlobalVariable.CompBenefitName, compBenefitName)
WebUI.verifyEqual(GlobalVariable.CompBenefitSequence, compBenefitSequence)
WebUI.verifyEqual(GlobalVariable.OccurencesAmount, occurencesAmount)
WebUI.verifyEqual(GlobalVariable.OccurencesFrequency, occurencesFrequency)
WebUI.verifyEqual(GlobalVariable.ConfinementAmount, confinementAmount)
WebUI.verifyEqual(GlobalVariable.ConfinementFrequency, confinementFrequency)
WebUI.verifyEqual(GlobalVariable.YearlyAmount, yearlyAmount)
WebUI.verifyEqual(GlobalVariable.YearlyFrequency, yearlyFrequency)
if(GlobalVariable.Tipe == 'TS54'){
	WebUI.verifyEqual(GlobalVariable.RbAmountTolerance, rbAmountTolerance)
	WebUI.verifyEqual(GlobalVariable.RbAmountToleranceDuration, rbAmountToleranceDuration)
	WebUI.verifyEqual(GlobalVariable.RbLvlTolerance, rbLvlTolerance)
	WebUI.verifyEqual(GlobalVariable.RbLvlToleranceDuration, rbLvlToleranceDuration)
}


println "Compare Beres"

WebUI.click(findTestObject('Web/Health/Policy/Product/Benefit List/Btn_CloseBenefitPopup'))

WebUI.click(findTestObject('Web/Health/Policy/Product/Btn_Back'))
