import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.setText(findTestObject('Web/Health/Policy/Txt_PolicyNo'), GlobalVariable.PolicyNo)

WebUI.click(findTestObject('Web/Health/Policy/Btn_Search'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

WebUI.click(findTestObject('Web/Health/Policy/Lst_PolicyInquiry',['key':GlobalVariable.PolicyNo]))

WebUI.click(findTestObject('Web/Health/Policy/Btn_EditPolicy'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

def clientId = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_Client'),'value')
def orderType = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_OrderType'))
def branch = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_Branch'))
def department = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_Department'))
def classOfBusiness = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_ClassOfBusiness'))
def segment = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_Segment'))
def segmentOfBusiness = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_SegmentOfBusiness'))
def channel = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_Channel'))
def separationExcess = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_SeparationExcess'))
//def excessMBP = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_ExcessMBP'))
def appropriateRBMustBeHigher = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_AppropriateRBMustBeHigher'))
def familyPlan = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_FamilyPlan'))
def maxChildren = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_MaxChildren'),'value')
def maxBirthDelivery = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_MaxBirthDelivery'),'value')
def childAgeLimit = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_ChildAgeLimit'), 'value')
def maxBackDate = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_MaxBackDated'),'value')
def reimbursementLTT = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_ReimbursementLTT'),'value')
def insuranceType = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_InsuranceType'))
def premiumRefund = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_PremiumRefund'),'value')
def payment = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_Payment'))
def adminFee = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_AdminFee'),'value')
def useAdmedika = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_UseAdmedika'))
def policyNo = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_PolicyNo'),'value')
def startEffDate = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_StartEffDate'),'value')
def endEffDate = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_EndEffDate'),'value')
def feePerMember = ""
def businessSourceType = ""
def businessSourceFee = ""
def commissionOnEndorsement = ""
if(GlobalVariable.Channel=='Direct Broker'){
	feePerMember = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_FeePerMember'), 'value')
	businessSourceType =  WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_BusinessSourceType'))
	businessSourceFee = WebUI.getAttribute(findTestObject('Web/Health/Policy/Policy Info/Txt_BusinessSourceFee'),'value')
	commissionOnEndorsement = WebUI.getText(findTestObject('Web/Health/Policy/Policy Info/Txt_CommOnEndorsement'))
}

//println "clientId " +clientId
//println "orderType " +orderType
//println "branch " +branch
//println "department " +department
//println "classOfBusiness " +classOfBusiness
//println "segment " +segment
//println "segmentOfBusiness " +segmentOfBusiness
//println "channel " +channel
//println "separationExcess " +separationExcess
//println "excessMBP " +excessMBP
//println "familyPlan " +familyPlan
//println "maxChildren " +maxChildren
//println "maxBirthDelivery " +maxBirthDelivery
//println "childAgeLimit " +childAgeLimit
//println "maxBackDate " +maxBackDate
//println "reimbursementLTT " +reimbursementLTT
//println "insuranceType " +insuranceType
//println "premiumRefund " +premiumRefund
//println "payment " +payment
//println "adminFee " +adminFee
//println "useAdmedika " +useAdmedika
//println "policyNo " +policyNo
//println "startEffDate " +startEffDate
//println "endEffDate " +endEffDate

WebUI.verifyEqual(GlobalVariable.ClientId,clientId)
WebUI.verifyEqual(GlobalVariable.OrderType,orderType)
WebUI.verifyEqual(GlobalVariable.Branch,branch)
WebUI.verifyEqual(GlobalVariable.Department,department)
WebUI.verifyEqual(GlobalVariable.ClassOfBusiness,classOfBusiness)
WebUI.verifyEqual(GlobalVariable.Segment,segment)
WebUI.verifyEqual(GlobalVariable.SegmentOfBusiness,segmentOfBusiness)
WebUI.verifyEqual(GlobalVariable.Channel,channel)
WebUI.verifyEqual(GlobalVariable.SeparationExcess,separationExcess)
//WebUI.verifyEqual(GlobalVariable.excessMBP,excessMBP)
WebUI.verifyEqual(GlobalVariable.FamilyPlan,familyPlan)
WebUI.verifyEqual(GlobalVariable.MaxChildren,maxChildren)
WebUI.verifyEqual(GlobalVariable.MaxBirthDelivery,maxBirthDelivery)
WebUI.verifyEqual(GlobalVariable.ChildAgeLimit,childAgeLimit)
WebUI.verifyEqual(GlobalVariable.MaxBackDate,maxBackDate)
WebUI.verifyEqual(GlobalVariable.ReimbursementLTT,reimbursementLTT)
WebUI.verifyEqual(GlobalVariable.InsuranceType,insuranceType)
WebUI.verifyEqual(GlobalVariable.PremiumRefund,premiumRefund)
WebUI.verifyEqual(GlobalVariable.Payment,payment)
WebUI.verifyEqual(GlobalVariable.AdminFee,adminFee)
WebUI.verifyEqual(GlobalVariable.UseAdmedika,useAdmedika)
WebUI.verifyEqual(GlobalVariable.PolicyNo,policyNo)
WebUI.verifyEqual(GlobalVariable.StartEffDate,startEffDate)
WebUI.verifyEqual(GlobalVariable.EndEffDate,endEffDate)
if(GlobalVariable.Channel=='Direct Broker'){
	WebUI.verifyEqual(GlobalVariable.FeePerMember, feePerMember)
	WebUI.verifyEqual(GlobalVariable.BusinessSourceType, businessSourceType)
	WebUI.verifyEqual(GlobalVariable.BusinessSourceFee, businessSourceFee)
	WebUI.verifyEqual(GlobalVariable.CommissionOnEndorsement, commissionOnEndorsement)
}

WebUI.scrollToElement(findTestObject('Web/Health/Policy/Lbl_Policy'), 2)

WebUI.switchToDefaultContent()

