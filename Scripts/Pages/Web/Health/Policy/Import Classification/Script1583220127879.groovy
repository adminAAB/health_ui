import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.keyword.*

WebUI.click(findTestObject('Web/Health/Policy/Tab_PolicyMenu',['key':'Classification']))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()

WebUI.click(findTestObject('Web/Health/Policy/Classification/Btn_Import'))
WebUI.delay(1)
//CustomKeywords.'healthKeyword.general.RewriteFileName'('Import_Policy_Classification_'+GlobalVariable.Tipe+'.csv')
//WebUI.delay(1)
//CustomKeywords.'healthKeyword.general.UploadFile'()
GEN5.UploadFile2('Import_Policy_Classification_'+GlobalVariable.Tipe+'.csv')

WebUI.delay(10)

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)
WebUI.click(findTestObject('Web/Health/Policy/Classification/Btn_Download'))
WebUI.delay(2)
WebUI.click(findTestObject('Web/Health/Policy/Classification/Btn_CloseImportPopUp'))
WebUI.delay(2)

//compare import dsini
CustomKeywords.'csvThing.CSVProcessor.compareImportPolicyClassification'()
println "Compare Import Selesai"
WebUI.delay(1)

//compare export dsini
//WebUI.click(findTestObject('Web/Health/Policy/Classification/Btn_Export'))
//WebUI.delay(1)
//CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
//WebUI.delay(1)

//persiapan compare UI
WebDriver driver = DriverFactory.getWebDriver()
WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 5)
GlobalVariable.Classification1 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[2]/span')).text.toString()
GlobalVariable.Classification2 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[2]/span')).text.toString()
GlobalVariable.Classification3 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[2]/span')).text.toString()
GlobalVariable.EmpClassification1 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[3]/span')).text.toString()
GlobalVariable.EmpClassification2 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[3]/span')).text.toString()
GlobalVariable.EmpClassification3 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[3]/span')).text.toString()
GlobalVariable.ClassProduct1 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[7]/span')).text.toString()
GlobalVariable.ClassProduct2 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[2]/td[7]/span')).text.toString()
GlobalVariable.ClassProduct3 = driver.findElement(By.xpath('//*[@id="ClassificationTable"]/a2is-datatable/div[2]/div/table/tbody/tr[3]/td[7]/span')).text.toString()

WebUI.switchToDefaultContent()
WebUI.delay(1)