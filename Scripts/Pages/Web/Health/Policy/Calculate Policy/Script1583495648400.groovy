import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebDriver driver = DriverFactory.getWebDriver()
def topUpWithdraw
def statProcess = ""
int maxLoop = 0

WebUI.click(findTestObject('Web/Health/Policy/Btn_CalculatePolicy'))

if(GlobalVariable.Tipe == 'TS58'){
	topUpWithdraw = "20000000"
	WebUI.click(findTestObject('Web/Health/Policy/Member/Calculate Policy/Chk_TopUpWithdraw'))
	WebUI.setText(findTestObject('Web/Health/Policy/Member/Calculate Policy/Txt_TopUpWithdraw'),topUpWithdraw)
}
WebUI.click(findTestObject('Web/Health/Policy/Member/Calculate Policy/Btn_Calculate'))

CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
WebUI.delay(1)

statProcess = CustomKeywords.'healthKeyword.utilityDB.getStatusProccess'()
WebUI.delay(1)
if(statProcess!='2'){
	WebUI.click(findTestObject('Web/Health/Policy/Btn_CloseConfirm'))
	WebUI.delay(1)
	statProcess = CustomKeywords.'healthKeyword.utilityDB.getStatusProccess'()
	while(statProcess=='1' && maxLoop<100){
		println("BACKGROUND PROCESS MASIH BERJALAN")
		WebUI.delay(10)
		statProcess = CustomKeywords.'healthKeyword.utilityDB.getStatusProccess'()
		maxLoop++;
	}
	WebUI.click(findTestObject('Web/Health/Policy/Btn_CalculatePolicy'))
	
	WebUI.click(findTestObject('Web/Health/Policy/Member/Calculate Policy/Btn_Calculate'))
	
	CustomKeywords.'healthKeyword.general.waitProcessingCommand2'()
	WebUI.delay(1)
}

WebUI.click(findTestObject('Web/Health/Policy/Btn_CloseConfirm'))

WebUI.scrollToElement(findTestObject('Web/Health/Policy/Lbl_Policy'), 2)