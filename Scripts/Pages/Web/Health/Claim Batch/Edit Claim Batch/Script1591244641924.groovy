import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

GEN5.ProcessingCommand()

GlobalVariable.BatchStatusA = BatchStatusA
GlobalVariable.StampBatch = Stamp

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/Btn_BatchStatus'))

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/Lst_BatchStatus', [('BatchStatus') : GlobalVariable.BatchStatusA]))

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/Btn_Search'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/Select_BatchNo'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/Btn_Edit'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/PopUp ClaimBatch/Btn_Correction'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/PopUp ClaimBatch/PopUp Confirmation/Btn_Yes'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/PopUp ClaimBatch/PopUp Confirmation WriteOff/Btn_No'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/Btn_Close(X)'))

WebUI.delay(2)

GlobalVariable.BatchStatusO = BatchStatusO

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/Btn_BatchStatus'))

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/Lst_BatchStatus', [('BatchStatus') : GlobalVariable.BatchStatusO]))

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/Btn_Search'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/Select_BatchNo'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/Btn_Edit'))

WebUI.delay(2)

if(Discount != '0') { 

GEN5.DeleteWrite(findTestObject('Object Repository/Web/Health/Claim Batch/PopUp ClaimBatch/Txt_Discount'), Discount)

}

WebUI.delay(3)

if(Stamp != '0') {

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/PopUp ClaimBatch/Btn_Stamp'))

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/PopUp ClaimBatch/Lst_Stamp', [('Stamp') : GlobalVariable.StampBatch]))

}

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/PopUp ClaimBatch/Btn_Approved'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/PopUp ClaimBatch/PopUp Confirmation/Btn_Yes'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Batch/Btn_Close(X)'))
