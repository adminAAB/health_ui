import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/FieldGroupingType'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/GroupingTypeSelected', [('GroupingType'):GroupingType]), FailureHandling.STOP_ON_FAILURE)

if (GroupingType == 'Premium'){

	WebUI.setText(findTestObject('Object Repository/Web/Health/Grouping Inquiry/FieldGroupingNo'), GroupingNoPremium, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/ButtonSearch'), FailureHandling.STOP_ON_FAILURE)
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/GroupingSelected', [('GroupingNo'):GroupingNoPremium]), FailureHandling.STOP_ON_FAILURE)

	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/IconEdit'), FailureHandling.STOP_ON_FAILURE)
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/IconAddNote'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/ButtonSearchNote'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/NoteSelected'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/ButtonSaveNote'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/NoteSelectedDeleted'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/IconDeleteNote'), FailureHandling.STOP_ON_FAILURE)
	
	}

if (GroupingType == 'Excess'){
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Grouping Inquiry/FieldGroupingNo'), GroupingNoExcess, FailureHandling.STOP_ON_FAILURE)
		
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/ButtonSearch'), FailureHandling.STOP_ON_FAILURE)
		
	GEN5.ProcessingCommand()
		
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/GroupingSelected', [('GroupingNo'):GroupingNoExcess]), FailureHandling.STOP_ON_FAILURE)

	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/IconEdit'), FailureHandling.STOP_ON_FAILURE)
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/IconAddNoteExcess'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/ButtonSearchNoteExcess'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/NoteSelectedExcess'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/ButtonSaveNoteExcess'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/NoteSelectedDeleted'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/IconDeleteNoteExcess'), FailureHandling.STOP_ON_FAILURE)
	
	}

WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/ButtonSaveGrouping'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Grouping Inquiry/ButtonSaveRemarks'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()



