import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import org.openqa.selenium.Keys as Keys
import com.keyword.GEN5

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/DiagnosisGroupSelected', [('DiagnosisGroup'): GlobalVariable.DiagnosisGroup]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/IconEditDiagnosisGroup'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldCoverage'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldCoverageSelected', [('Coverage'):Coverage]), FailureHandling.STOP_ON_FAILURE)

if ((Coverage == 'Covered') || (Coverage == 'Confirm')){
	WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldAdditionalInfo'), AdditionalInfo, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldAdditionalDocument'), AdditionalDocument, FailureHandling.STOP_ON_FAILURE)

	WebUI.sendKeys(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldAdditionalDocument'), Keys.chord(Keys.ENTER), FailureHandling.STOP_ON_FAILURE)
}

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/DiagnosisListSelected'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/IconDeleteDiagnosisList'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/IconAddDiagnosisList'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldAddDiagnosisList'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldSetDiagnosisList'), DiagnosisList, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldSetDiagnosisList'), Keys.chord(Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/ButtonSaveDiagnosisList'), FailureHandling.STOP_ON_FAILURE)

if (Coverage == 'By Diagnosis Question'){
		
	WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/IconAddDiagnosisQuestion'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldQuestionEdited'), Question, FailureHandling.STOP_ON_FAILURE)

	WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldQuestionYesEdited'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldQuestionYesSelected', [('CoverageQuestionYes'): CoverageQuestionYes]), FailureHandling.STOP_ON_FAILURE)

	WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldQuestionNoEdited'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldQuestionNoSelected', [('CoverageQuestionNo'): CoverageQuestionNo]), FailureHandling.STOP_ON_FAILURE)

	WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldQuestionUnknownEdited'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldQuestionUnknownSelected', [('CoverageQuestionUnknown'): CoverageQuestionUnknown]), FailureHandling.STOP_ON_FAILURE)

	WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldQuestionAdditionalInfoEdited'), QuestionAddInfo, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldQuestionAdditionalDocuments'), QuestionAddDoc, FailureHandling.STOP_ON_FAILURE)

	WebUI.sendKeys(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldQuestionAdditionalDocuments'), Keys.chord(Keys.ENTER), FailureHandling.STOP_ON_FAILURE)
	
}

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/ButtonSaveDiagnosisQuestions'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

String query = "SELECT * FROM DiagnosisGroup WHERE DiagnosisGroupID ='"+GlobalVariable.DiagnosisGroup+"'"

String dbDiagnosisGroup = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "DiagnosisGroupID")
println(dbDiagnosisGroup)

String dbCoverage = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Coverage")
println(dbCoverage)

if ((Coverage == 'Covered') || (Coverage == 'Confirm')){
	
	String dbAdditionalInfo = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "AdditionalInfo")
	println(dbAdditionalInfo)
	
	String dbAdditionalDocument = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "AdditionalDocuments")
	println(dbAdditionalDocument)
	
	if ((DiagnosisGroup == dbDiagnosisGroup) && (Coverage == dbCoverage) && (AdditionalInfo == dbAdditionalInfo)){
		KeywordUtil.markPassed("PASSED")
		}
	else {
		KeywordUtil.markFailedAndStop("FAILED")
		}
}

if (Coverage == 'By Diagnosis Question'){
	
	String query2 = "SELECT * FROM DiagnosisQuestion WHERE DiagnosisGroupID = '"+GlobalVariable.DiagnosisGroup+"' AND isActive='1'"
	
	String dbQuestion = GEN5.getValueDatabase("172.16.94.70", "SEA", query2, "QuestionDescription")
	println(dbQuestion)
	
	String dbCoverageQuestionYes = GEN5.getValueDatabase("172.16.94.70", "SEA", query2, "TrueResult")
	println(dbCoverageQuestionYes)
	
	String dbCoverageQuestionNo = GEN5.getValueDatabase("172.16.94.70", "SEA", query2, "FalseResult")
	println(dbCoverageQuestionNo)
	
	String dbCoverageQuestionUnknown = GEN5.getValueDatabase("172.16.94.70", "SEA", query2, "NoResult")
	println(dbCoverageQuestionUnknown)
	
	String dbQuestionAddInfo = GEN5.getValueDatabase("172.16.94.70", "SEA", query2, "AdditionalInfo")
	println(dbQuestionAddInfo)
	
	String dbQuestionAddDoc = GEN5.getValueDatabase("172.16.94.70", "SEA", query2, "AdditionalDocuments")
	println(dbQuestionAddDoc)
	
	if ((GlobalVariable.DiagnosisGroup == dbDiagnosisGroup) && (Question == dbQuestion)  && (CoverageQuestionYes == dbCoverageQuestionYes) && (CoverageQuestionNo == dbCoverageQuestionNo) && (CoverageQuestionUnknown == dbCoverageQuestionUnknown) && (QuestionAddInfo == dbQuestionAddInfo)){
		KeywordUtil.markPassed("PASSED")
		}
	else {
		KeywordUtil.markFailedAndStop("FAILED")
		}
}


