import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import org.openqa.selenium.Keys as Keys
import com.keyword.GEN5

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/IconAddDiagnosisGroup'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

//Save Without add Diagnosis Group name
WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/ButtonSaveDiagnosisQuestions'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Diagnosis Question/DiagnosisGroupRequired'), 3, FailureHandling.STOP_ON_FAILURE)

//Set Diagnosis Group name
WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldDiagnosisGroup'), DiagnosisGroupNew, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldCoverage'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldCoverageSelected', [('Coverage'):Coverage]), FailureHandling.STOP_ON_FAILURE)

//Save Without add diagnosis list
WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/ButtonSaveDiagnosisQuestions'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Diagnosis Question/DiagnosisListRequired'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/IconCloseDiagnosisListNull'), FailureHandling.STOP_ON_FAILURE)

//Set Diagnosis List
WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/IconAddDiagnosisList'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldAddDiagnosisList'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldSetDiagnosisList'), DiagnosisList, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Object Repository/Web/Health/Diagnosis Question/FieldSetDiagnosisList'), Keys.chord(Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/ButtonSaveDiagnosisList'), FailureHandling.STOP_ON_FAILURE)

//Save Without Diagnosis Question
WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/ButtonSaveDiagnosisQuestions'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Diagnosis Question/DiagnosisQuestionRequired'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/IconCloseDiagnosisQuestionNull'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Diagnosis Question/ButtonBackDiagnosisDetails'), FailureHandling.STOP_ON_FAILURE)

