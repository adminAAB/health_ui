import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.junit.After

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Txt_DxGroupName'), DxGroupName)

//menambah 1 Diagnosis List
GEN5.Click(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Btn_addDxList'))	
WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 0, FailureHandling.STOP_ON_FAILURE)
//GEN5.ComboBox('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Lst_addDxList', SearchDxList)
GEN5.ComboBoxSearch(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Lst_addDxList'), findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Txt_searchDxList') , SearchDxList)
WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)
GEN5.Click(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Btn_saveDxList'))

// menambah 1 Diagnosis Question
GEN5.Click(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Btn_addDxQuestion'))
WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Txt_DxQuestionDesc'), DxQuestionDesc)
GEN5.Click(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Lst_DxQuestionYes'))
WebUI.selectOptionByValue(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Lst_DxQuestionYes'), DxQuestionYes, true)
GEN5.Click(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Lst_DxQuestionNo'))
WebUI.selectOptionByValue(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Lst_DxQuestionNo'), DxQuestionNo, true)
GEN5.Click(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Lst_DxQuestionUnknown'))
WebUI.selectOptionByValue(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Lst_DxQuestionUnknown'), DxQuestionUnknown, true)
WebUI.setText(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Lst_DxQuestionAdditionalInfo'), DxQuestionAdditionalInfo)

// simpan diagnosis group
GEN5.Click(findTestObject('Object Repository/Web/Health/Diagnosis Question/Diagnosis Question Details/Btn_saveDxGroup'))