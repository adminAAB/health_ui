import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

// memilih diagnosis
WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 0, FailureHandling.STOP_ON_FAILURE)
GEN5.ComboBoxSearch(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Btn_diagnosisList'), findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Txt_searchDiagnosis'), AddDiagnosis)
GlobalVariable.AddDiagnosis = AddDiagnosis

// menentukan DOS
GEN5.ComboBox(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Btn_ods'), ODS)
//GEN5.Click(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Btn_ods'))
//GEN5.Click(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Lst_ods'), [('ODS') : ODS])

// menentukan CC Approval Limit
WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)
WebUI.setText(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Txt_ccApprovalLimit'), CCApprovalLimit)

// menentukan coverage
WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 0, FailureHandling.STOP_ON_FAILURE)
GEN5.ComboBox(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Btn_coverage'), Coverage)
//GEN5.Click(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Btn_coverage'))
//GEN5.Click(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Lst_coverage'), [('Coverage') : Coverage])
GlobalVariable.Coverage = Coverage

WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)

if (Coverage == 'Covered' || Coverage == 'Confirm') {
	WebUI.setText(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Txt_additionalInfo'))
	//GEN5.Click(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Btn_additionalDocuments'))
	GEN5.Click(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Btn_save'))
} 
else if (Coverage == 'By Treatment Question') {	
// By Treatment Question
	GEN5.Click(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Btn_addTreatmentQuestion'))
	WebUI.setText(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Txt_questionDescription'), QuestionDescription)
	WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 0, FailureHandling.STOP_ON_FAILURE)
	GEN5.ComboBox(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Btn_questionYes'), QuestionYes)
	GEN5.ComboBox(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Btn_questionNo'), QuestionNo)
	GEN5.ComboBox(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Btn_questionUnknown'), QuestionUnknown)
	WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)
	WebUI.setText(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Txt_questionAdditionalInfo'), QuestionAdditionalInfo)
	//WebUI.switchToFrame(findTestObject('Object Repository/Frame'), 0)
	//GEN5.ComboBoxSearch(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Btn_questionAdditionalDocuments'), findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Txt_questionAdditionalDocuments'), QuestionAdditionalDocuments)
	GEN5.Click(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Add Diagnosis/Btn_save'))
}
GEN5.Click(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Btn_save'))
GEN5.Click(findTestObject('Object Repository/Web/Health/Medical Treatment/Medical Treatment Details/Btn_closePopupSuccess'))
		