import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

WebUI.waitForElementPresent(findTestObject('Web/Health/Membership Inquiry/MemberSelected', [('MemberName'):GlobalVariable.MemberName]), 3, FailureHandling.STOP_ON_FAILURE)

MemberNo = WebUI.getText(findTestObject('Web/Health/Membership Inquiry/FieldGetMemberNo'), FailureHandling.STOP_ON_FAILURE)

println(MemberNo)

WebUI.click(findTestObject('Web/Health/Membership Inquiry/MemberSelected', [('MemberName'):GlobalVariable.MemberName]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/Membership Inquiry/ButtonSelect'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.scrollToElement(findTestObject('Web/Health/Membership Inquiry/ButtonUpdateForm'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/Membership Inquiry/ButtonUpdateForm'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Web/Health/Membership Inquiry/LabelPopup'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Web/Health/Membership Inquiry/FieldAccountName'), AccountNameNull, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Web/Health/Membership Inquiry/FieldAccountNo'), AccountNoNull, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/Membership Inquiry/ButtonUpdatePopup'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Membership Inquiry/RequiredAccountName'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health/Membership Inquiry/RequiredAccountNo'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Membership Inquiry/ButtonCancelUpdate'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Membership Inquiry/ButtonCloseDetails'), FailureHandling.STOP_ON_FAILURE)
