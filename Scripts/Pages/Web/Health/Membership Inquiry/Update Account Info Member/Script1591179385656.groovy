import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

WebUI.waitForElementPresent(findTestObject('Web/Health/Membership Inquiry/MemberSelected', [('MemberName'):GlobalVariable.MemberName]), 3, FailureHandling.STOP_ON_FAILURE)

MemberNo = WebUI.getText(findTestObject('Web/Health/Membership Inquiry/FieldGetMemberNo'), FailureHandling.STOP_ON_FAILURE)

println(MemberNo)

WebUI.click(findTestObject('Web/Health/Membership Inquiry/ButtonSelect'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.scrollToElement(findTestObject('Web/Health/Membership Inquiry/ButtonUpdateForm'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/Membership Inquiry/ButtonUpdateForm'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Web/Health/Membership Inquiry/LabelPopup'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/Membership Inquiry/IconSearchAccountBank'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Web/Health/Membership Inquiry/FieldBankName'), BankName, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/Membership Inquiry/ButtonSearchBankName'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/Membership Inquiry/BankSelected', [('BankName'):BankName]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/Membership Inquiry/ButtonSelectBankName'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Web/Health/Membership Inquiry/FieldAccountName'), AccountName, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Web/Health/Membership Inquiry/FieldAccountNo'), AccountNo, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/Membership Inquiry/ButtonUpdatePopup'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Web/Health/Membership Inquiry/SuccessPopup'), 3, FailureHandling.STOP_ON_FAILURE)

String query = "Select * from fn_GetAccountInfo('$MemberNo')"

String bankname = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "BankName")

println(bankname)

String accountname = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "AccountName")

println(accountname)

String accountno = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "AccountNo")

println(accountno)

if ((BankName == bankname) && (AccountName == accountname) && (AccountNo == accountno) ){
	KeywordUtil.markPassed("PASSED")
	}
else {
	KeywordUtil.markFailedAndStop("FAILED")
	}
