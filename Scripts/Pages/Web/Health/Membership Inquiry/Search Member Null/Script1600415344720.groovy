import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5

if((ClientNameNull != null) && (EmployeeIDNull != null) && (MemberNameNull != null) && (MemberNoNull != null)){
	WebUI.setText(findTestObject('Web/Health/Membership Inquiry/FieldClientName'), ClientNameNull, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Web/Health/Membership Inquiry/FieldEmployeeID'), EmployeeIDNull, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Web/Health/Membership Inquiry/FieldMemberName'), MemberNameNull, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Web/Health/Membership Inquiry/FieldMemberNo'), MemberNoNull, FailureHandling.STOP_ON_FAILURE)
}

WebUI.click(findTestObject('Web/Health/Membership Inquiry/ButtonSearch'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.waitForElementNotPresent(findTestObject('Web/Health/Membership Inquiry/MemberSelected', [('MemberName'):MemberNameNull]), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/Membership Inquiry/ButtonClear'), FailureHandling.STOP_ON_FAILURE)
