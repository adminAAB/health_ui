import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

WebUI.waitForElementPresent(findTestObject('Web/Health/MI - Update Info Member/MemberSelected', [('MemberName'):GlobalVariable.MemberName]), 0, FailureHandling.STOP_ON_FAILURE)

MemberNo = WebUI.getText(findTestObject('Web/Health/MI - Update Info Member/FieldGetMemberNo'), FailureHandling.STOP_ON_FAILURE)

println(MemberNo)

WebUI.click(findTestObject('Web/Health/MI - Update Info Member/MemberSelected', [('MemberName'):GlobalVariable.MemberName]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/MI - Update Info Member/ButtonSelect'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.scrollToElement(findTestObject('Web/Health/MI - Update Info Member/ButtonUpdateForm'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/MI - Update Info Member/ButtonUpdateForm'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Web/Health/MI - Update Info Member/LabelPopup'), 3, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/MI - Update Info Member/FieldPhone'), Phone, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/MI - Update Info Member/FieldEmail'), Email, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Membership Inquiry/FieldCategory'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Membership Inquiry/CategorySelected',[('Category'):CategoryMember]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/MI - Update Info Member/ButtonUpdatePopup'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Web/Health/MI - Update Info Member/SuccessPopup'), 3, FailureHandling.STOP_ON_FAILURE)

String query = "SELECT TOP 1 MNo, CorrespondenceID_Claim AS Phone, Email FROM dbo.Policy_Member WHERE MemberNo = '$MemberNo'"

String query2 = "SELECT TOP 1 * FROM Policy_Member_Category WHERE MemberNo = '$MemberNo'"

String dbphone = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Phone")

println(dbphone)

String dbemail = GEN5.getValueDatabase("172.16.94.70", "SEA", query, "Email")

println(dbemail)

String dbcategory = GEN5.getValueDatabase("172.16.94.70", "SEA", query2, "CategoryCode")

println(dbcategory)

if ((Phone == dbphone) && (Email == dbemail) && (CategoryMember == dbcategory)){
	KeywordUtil.markPassed("PASSED")
	}
else {
	KeywordUtil.markFailedAndStop("FAILED")
	}