import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil

import com.keyword.GEN5

//klik Tab Document
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab_Document'))
GEN5.ProcessingCommand()
GEN5.tickAllCheckboxInTable(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Grid_Document'))
GEN5.ProcessingCommand()

if (GlobalVariable.CR_isUploadDoc==true)
{
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Btn_Upload'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Pop Up Upload SoftCopy/Chk_Row1_Grid'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Pop Up Upload SoftCopy/Btn_Browse'))
GEN5.ProcessingCommand()

GEN5.UploadFile2(GlobalVariable.CR_SoftCopyFileName)
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Pop Up Upload SoftCopy/Btn_Upload'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Pop Up Upload FIle Success/Button X'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Row1_GridDocument'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Btn_ViewFile'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Btn_ShowHideImage'))
GEN5.ProcessingCommand()

}



