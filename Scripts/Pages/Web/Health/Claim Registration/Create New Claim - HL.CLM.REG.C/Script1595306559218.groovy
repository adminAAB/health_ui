import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil

import com.keyword.GEN5


//klik Tab Claim Info
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab_ClaimInfo'))
GEN5.ProcessingCommand()


//input Delivery Received Date jika Doc Source Member
if (DocSource == 'Member')
{
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_DeliveryReceivedDate'))
GEN5.ProcessingCommand()

GEN5.DatePicker(DeliveryReceivedDate, findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/DatePicker_DeliveryReceivedDate'))
GEN5.ProcessingCommand()
}

//input member
if (DocSource == 'Client' || DocSource == 'Provider')
{
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_SearchMember'))
WebUI.delay(3)

if (CheckSearchMember=='Yes')
{
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_EmpID'), EmpID)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_MemberName'), MemberName)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Clear'))
	GEN5.ProcessingCommand()
		
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_ClientName'), ClientName)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_EmpID'), EmpID)
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_MemberName'), MemberName)
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_ClientName'), ClientName)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Clear'))
	GEN5.ProcessingCommand()
	
		
}

	if (UnregMember == 'No')
		{
			WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_MemberNo'), MemberNo)
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Search'))
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Row1_GridMember'))
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Select'))
			GEN5.ProcessingCommand()
		}
	else if (UnregMember == 'Yes')
		{
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Search'))
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_UnregMember'))
			GEN5.ProcessingCommand()	
			
			WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Txt_MemberName'), MemberName)
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_SearchClient'))
			GEN5.ProcessingCommand()
			
			WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Client/Txt_ClientName'), ClientName)
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Client/Btn_Search'))
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Client/Row1_GridClient'))
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Client/Btn_Select'))
			GEN5.ProcessingCommand()
											
		}
}

//input provider jika doc source client dan member

if (DocSource == 'Client' || DocSource == 'Member') 
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_Provider'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Provider/Txt_ProviderName'), ProviderName)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Provider/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Provider/Row1_GridProvider'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Provider/Btn_Select'))
	GEN5.ProcessingCommand()

}


//input Treatment Start dan Treatment End
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_TreatmentStart'))
GEN5.ProcessingCommand()
GEN5.DatePicker(TreatmentStart, findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/DatePicker_TreatmentStart'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_TreatmentEnd'))
GEN5.ProcessingCommand()
GEN5.DatePicker(TreatmentEnd, findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/DatePicker_TreatmentEnd'))
GEN5.ProcessingCommand()


//input Product Type
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_ProductType'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_ProductType',[('ProductType') : ProductType]))
GEN5.ProcessingCommand()

//input Diagnosis
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_Diagnosis'))
GEN5.ProcessingCommand()
WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search and Select Diagnosis/Txt_Diagnosis'), Diagnosis)
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search and Select Diagnosis/Btn_Search'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search and Select Diagnosis/Row1_GridDiagnosis'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search and Select Diagnosis/Btn_Select'))
GEN5.ProcessingCommand()

//input Billed
WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Txt_Billed'), Billed)
GEN5.ProcessingCommand()

//input Doctor
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_SearchDoctor'))
GEN5.ProcessingCommand()
WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Doctor/Txt_DoctorName'), Doctor)
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Doctor/Btn_Search'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Doctor/Row1_GridDoctor'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Doctor/Btn_Select'))
GEN5.ProcessingCommand()

//input TreatmentRBClass
if (ProductType=='Inpatient' || ProductType=='Maternity' )
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_TreatmentRBClass'))
	GEN5.ProcessingCommand()

	if (ProductType=='Maternity')
	{
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Provider Room List/Btn_TreatmentType'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Provider Room List/List_TreatmentType',[('TreatmentType'):TreatmentType]))
		GEN5.ProcessingCommand()
	}
	
	GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Provider Room List/Grid Provider Room List'), 'Room Type', RoomType_TreatmentRBClass)
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Provider Room List/Btn_Select'))
	GEN5.ProcessingCommand()
}


//input Room Option
if (ProductType=='Inpatient' || ProductType=='Maternity' )
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_RoomOption'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_RoomOption',[('RoomOption') : RoomOption]))
	GEN5.ProcessingCommand()
}


if (DocSource == 'Client')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_Payto'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_Payto',[('PayTo') : PayTo]))
	GEN5.ProcessingCommand()

}


//input COB BPJS
if (CobBpjs=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_CobBpjs'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_CobBpjs',[('isCobBpjs'): 'Yes']))
	GEN5.ProcessingCommand()
}

//input CashPlan BPJS
if (CashPlanBpjs=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_CashPlanBpjs'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_CashPlanBpjs',[('isCashPlanBpjs'): 'Yes']))
	GEN5.ProcessingCommand()
}

//input Double Insured
if (DoubleInsured=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_DoubleInsured'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_CashPlanBpjs',[('isCashPlanBpjs'): 'Yes']))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_DoubleInsured',[('isDoubleInsured'):'Yes']))
	GEN5.ProcessingCommand()
}


//input Perawatan Khusus
if (PerawatanKhusus=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_PerawatanKhusus'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_PerawatanKhusus',[('isPerawatanKhusus'): 'Yes']))
	GEN5.ProcessingCommand()
}


//input Pasien Meninggal
if (PasienMeninggal=='true')
{	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_PasienMeninggal'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_PasienMeninggal',[('isPasienMinggal'): 'Yes']))
	GEN5.ProcessingCommand()
}

//input Traffic Accident
if (TrafficAccident=='true')
{	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_TrafficAccident'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_TraficAccident',[('isTraficAccident'): 'Yes']))
	GEN5.ProcessingCommand()
}

//input Additional Info - CT Scan
if (CTScan=='true')
{	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_CTSCan'))
	GEN5.ProcessingCommand()
}


//input Additional Info - MRI
if (MRI=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_MRI'))
	GEN5.ProcessingCommand()
}

//input Additional Info - PA
if (PA=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_PA'))
	GEN5.ProcessingCommand()
}


//input Additional Info - Lab
if (Lab=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_Lab'))
	GEN5.ProcessingCommand()
}


//input Additional Info - Rontgen
if (Rontgen=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_Rontgen'))
	GEN5.ProcessingCommand()
}


//input Additional Info - USG
if (USG=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_USG'))
	GEN5.ProcessingCommand()
}


//input Additional Info - SRIC
if (SRIC=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_SRIC'))
	GEN5.ProcessingCommand()
}


String PatientName = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Txt_MemberName'), 'value')
String ClientName = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Txt_ClientName'), 'value')
String PayTo = WebUI.getText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Txt_Payto'))
String AccountNo = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Txt_AccountNo'), 'value')
String BankName = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Txt_BankName'), 'value')
String AccountName = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Txt_AccountName'), 'value')
String ClaimType = WebUI.getText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_ClaimType'))

println ClaimType



WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Object_Atas'), 2)
GEN5.ProcessingCommand()

//klik Tab Document
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab_Document'))
GEN5.ProcessingCommand()
GEN5.tickAllCheckboxInTable(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Grid_Document'))
GEN5.ProcessingCommand()

if (isUploadDoc==true)
{
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Btn_Upload'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Pop Up Upload SoftCopy/Chk_Row1_Grid'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Pop Up Upload SoftCopy/Btn_Browse'))
GEN5.ProcessingCommand()

GEN5.UploadFile2(SoftCopyFileName)
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Pop Up Upload SoftCopy/Btn_Upload'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Pop Up Upload FIle Success/Button X'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Row1_GridDocument'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Btn_ViewFile'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Document/Btn_ShowHideImage'))
GEN5.ProcessingCommand()

}

//klik Tab Completeness Status
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab_ComplenessStatus'))
GEN5.ProcessingCommand()

ArrayList CompletenessUI = GEN5.HealthCheckStatus(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Completeness Status/Grid_CompletenessStatus'), "Completeness Criteria", "Status")
println (CompletenessUI)
println (Completeness)

if (CompletenessUI == Completeness  ){
	KeywordUtil.markPassed('Completeness sesuai')
 }
else {
	KeywordUtil.markFailed('Completeness tidak sesuai')
 }


//klik Button Save
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Btn_Save'))
GEN5.ProcessingCommand()

boolean PopUpSuspectDouble = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Label_SuspectDouble'), 2, FailureHandling.OPTIONAL)
if (PopUpSuspectDouble==true && isSuspectDouble==true)
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/PopUp Suspect Double/Row1_GridSuspectDouble'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/PopUp Suspect Double/Btn_Select'))
	GEN5.ProcessingCommand()
	
}
else if (PopUpSuspectDouble==true && isSuspectDouble==false)
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/PopUp Suspect Double/Btn_Close'))
	GEN5.ProcessingCommand()
}

boolean PopUp30CalendarDays = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/PopUp 30 Calendar Days/Label_30CalendarDays'), 2, FailureHandling.OPTIONAL)
if (PopUp30CalendarDays==true)
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/PopUp 30 Calendar Days/Btn_OK'))
	GEN5.ProcessingCommand()
}



if (UnregMember=='No')
{
	//generate BoxNo jika Boxno Kosong
	String BoxNo = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_BoxNo'), 'value')
	if (BoxNo == '')
	{
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Btn_GenerateBoxNo'))
		GEN5.ProcessingCommand()
	}
}

//compare field Tab Claim Info dengan Pop Up Summary

String Summary_PatientName = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_Summary_PatientName'), 'value')
String Summary_DocumentSource = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_Summary_DocumentSource'), 'value')
String Summary_Diagnosis = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_Summary_Diagnosis'), 'value')
String Summary_ProductType = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_Summary_ProductType'), 'value')
String Summary_ClientName = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_Summary_ClientName'), 'value')
String Summary_ProviderName = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_Summary_ProviderName'), 'value')
String Summary_TreatmentDate = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_Summary_TreatmentDate'), 'value')
String Summary_HospitaInvoiceNo = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_Summary_HospitalInvoiceNo'), 'value') 
String Summary_Billed = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_Summary_Billed'), 'value')
String Summary_Payto = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_Summary_PayTo'), 'value')
String Summary_AccountNo = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_Summary_AccountNo'), 'value')
String Summary_BankName = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_Summary_BankName'), 'value')
String Summary_AccountName = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_Summary_AccountName'), 'value')
String Summary_BoxNo = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Txt_BoxNo'), 'value')




if (Summary_PatientName == PatientName  ){
	KeywordUtil.markPassed('Patient Name di Summary match dengan Patient Name di ClaimInfo')
 }
else {
	KeywordUtil.markFailed('Patient Name di Summary tidak match dengan Patient Name di ClaimInfo')
 }

if (Summary_DocumentSource == DocSource  ){
	KeywordUtil.markPassed('Doc Source di Summary match dengan Doc Source di ClaimInfo')
 }
else {
	KeywordUtil.markFailed('Doc Source di Summary tidak match dengan Doc Source di ClaimInfo')
 }


if (Summary_Diagnosis == Diagnosis  ){
	KeywordUtil.markPassed('Diagnosis di Summary match dengan Diagnosis di ClaimInfo')
 }
else {
	KeywordUtil.markFailed('Diagnosis di Summary tidak match dengan Diagnosis di ClaimInfo')
 }


if (Summary_ProductType == ProductType  ){
	KeywordUtil.markPassed('Product Type di Summary match dengan Product Type di ClaimInfo')
 }
else {
	KeywordUtil.markFailed('Product Type di Summary tidak match dengan Product Type di ClaimInfo')
 }


if (Summary_ClientName == ClientName  ){
	KeywordUtil.markPassed('Client Name di Summary match dengan Client Name di ClaimInfo')
 }
else {
	KeywordUtil.markFailed('Client Name di Summary tidak match dengan Client Name di ClaimInfo')
 }


if (Summary_ProviderName == ProviderName  ){
	KeywordUtil.markPassed('Provider Name di Summary match dengan Provider Name di ClaimInfo')
 }
else {
	KeywordUtil.markFailed('Provider Name di Summary tidak match dengan Provider Name di ClaimInfo')
 }


String TreatmentDate = TreatmentStart+' to '+TreatmentEnd

if (Summary_TreatmentDate == TreatmentDate  ){
	KeywordUtil.markPassed('Treatment Date di Summary match dengan Treatment Date di ClaimInfo')
 }
else {
	KeywordUtil.markFailed('Treatment Date di Summary tidak match dengan Treatment Date di ClaimInfo')
 }

String Nomor

if (DocSource == 'Client')
{
	Nomor = ReferenceNo
	if (Summary_HospitaInvoiceNo == Nomor  ){
		KeywordUtil.markPassed('Reference No di Summary match dengan Reference No di ClaimInfo')
	 }
	else {
		KeywordUtil.markFailed('Reference No di Summary tidak match dengan Reference No di ClaimInfo')
	 }
	
}
else if (DocSource == 'Provider')
{
	Nomor = HospitalInvNo
	
	if (Summary_HospitaInvoiceNo == Nomor  ){
		KeywordUtil.markPassed('Hospital Invoice No di Summary match dengan Hospital Invoice No di ClaimInfo')
	 }
	else {
		KeywordUtil.markFailed('Hospital Invoice No di Summary tidak match dengan Hospital Invoice No di ClaimInfo')
	 }
}
else
{
	Nomor = ''
}




String Summary_Billed2 = Summary_Billed.replace('Rp. ','').replace(',','').replace('.00','')

if (Summary_Billed2 == Billed  ){
	KeywordUtil.markPassed('Billed di Summary match dengan Billed di ClaimInfo')
 }
else {
	KeywordUtil.markFailed('Billed di Summary tidak match dengan Billed di ClaimInfo')
 }


if (Summary_Payto == PayTo  ){
	KeywordUtil.markPassed('Pay To di Summary match dengan Pay To di ClaimInfo')
 }
else {
	KeywordUtil.markFailed('Pay To di Summary tidak match dengan Pay To di ClaimInfo')
 }


if (Summary_AccountNo == AccountNo  ){
	KeywordUtil.markPassed('Account No di Summary match dengan Account No di ClaimInfo')
 }
else {
	KeywordUtil.markFailed('Account No di Summary tidak match dengan Account No di ClaimInfo')
 }

if (Summary_BankName == BankName  ){
	KeywordUtil.markPassed('Bank Name di Summary match dengan Bank Name di ClaimInfo')
 }
else {
	KeywordUtil.markFailed('Bank Name di Summary tidak match dengan Bank Name di ClaimInfo')
 }


if (Summary_AccountName == AccountName  ){
	KeywordUtil.markPassed('Account Name di Summary match dengan Account Name di ClaimInfo')
 }
else {
	KeywordUtil.markFailed('Account Name di Summary tidak match dengan Account Name di ClaimInfo')
 }



//klik button OK
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Summary/Btn_OK'))
GEN5.ProcessingCommand()
WebUI.delay(2)

String DocNo
String SendTo


today = new Date()
today2 = today.format('yyyy-MM-dd')
println (today2)

String pcb_ReimbursementF

if (ClaimType == 'Reimburse')
{
	pcb_ReimbursementF = '1'
}
else if (ClaimType == 'Cashless')
{
	pcb_ReimbursementF = '0'
}


println (pcb_ReimbursementF)

String pcb_DiscountF
String PCB_AUser = ''
String PCB_ADate = ''
String PCB_AutoApprovalStatus = ''
String PCB_AutoApprovalDate = ''



String Stamp2
String Discount2
String AccountNo_PCB
String AccountName_PCB
String BankName_PCB

	if (DocSource=='Provider')
	{
		Stamp2=Stamp
		Discount2=Discount
		AccountNo_PCB = AccountNo
		AccountName_PCB = AccountName
		BankName_PCB = BankName
	}
	else
	{
		Stamp2='0'
		Discount2='0'
		AccountNo_PCB = ''
		AccountName_PCB = ''
		BankName_PCB = ''
	}



if (Discount2!='0')
{
	pcb_DiscountF = '1'
}
else
{
	pcb_DiscountF = '0'
}
	
///unregMember=No
if (UnregMember=='No')
{
	
	List<String> Hasil = GEN5.getPopUpText(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Saved Document No/Label_DocNo'))
	println (Hasil)
	DocNo = Hasil[0]
	SendTo = Hasil[1]
	println (DocNo)
	println (SendTo)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Saved Document No/Button X'))
	GEN5.ProcessingCommand()
	
	String SendToID = GEN5.getValueDatabase("172.16.94.70", "SEA","SELECT ID FROM dbo.SysUser AS su WHERE Name='"+SendTo+"'","ID")
	
	
	
	//Compare UI dengan DB
	
		
	ArrayList Inputan
	
	if (DocSource=='Provider' || DocSource=='Client')
	{
	           Inputan = [Username,
						 'REG',
						 'REGISTERED',
						 ClientName,
						 PatientName,
						 TreatmentStart,
						 TreatmentEnd,
						 Billed,
						 Username,
						 SendToID,
						 Username,
						 today2,
						 ProviderName,
						 SendToID,
						 Summary_BoxNo,
						 DiagnosisID,
						 ProductType,
						 RoomOption,
						 today2,
						 today2,
						 CobBpjs,
						 CashPlanBpjs,
						 DoubleInsured,
						 PerawatanKhusus,
						 PasienMeninggal,
						 TrafficAccident,
						 ClaimType,
						 BankName,
						 AccountName,
						 AccountNo,
						 PayTo,
						 ClaimType,
						 DocSource,
						 today2,
						 Username,
						 today2,
						 Nomor,
						 Billed,
						 today2,
						 today2,
						 '1',
						 Billed,
						 pcb_ReimbursementF,
						 today2,
						 Username,
						 Nomor,
						 pcb_DiscountF,
						 Discount2,
						 'O',
						 PCB_AUser,
						 PCB_ADate,
						 Stamp2,
						 AccountNo_PCB,
						 AccountName_PCB,
						 BankName_PCB,
						 PCB_AutoApprovalStatus,
						 PCB_AutoApprovalDate	
						]
	}
	else if (DocSource=='Member')
	{
	           Inputan = [Username,
						 'SANA',
						 'SEND',
						 ClientName,
						 PatientName,
						 TreatmentStart,
						 TreatmentEnd,
						 Billed,
						 Username,
						 SendToID,
						 Username,
						 today2,
						 ProviderName,
						 SendToID,
						 Summary_BoxNo,
						 DiagnosisID,
						 ProductType,
						 RoomOption,
						 today2,
						 today2,
						 CobBpjs,
						 CashPlanBpjs,
						 DoubleInsured,
						 PerawatanKhusus,
						 PasienMeninggal,
						 TrafficAccident,
						 ClaimType,
						 BankName,
						 AccountName,
						 AccountNo,
						 PayTo,
						 ClaimType,
						 DocSource,
						 today2,
						 Username,
						 today2,
						 Nomor,
						 Billed,
						 today2
						]
	}
	
	
	
	
		String Query_TDRD
		
		if (DocSource=='Provider')
		{
		Query_TDRD = "SELECT TDRD.Dispatcher AS TDRD_Dispatcher,TDRD.DocumentStatus AS TDRD_DocumentStatus,TDRD.Status AS TDRD_Status,P4.Name AS ClientName,PM.Name AS MemberName,FORMAT (CAST(TDRD.TreatmentStart AS DATE), 'dd/MMM/yyyy') AS TDRD_TreatmentStart,FORMAT (CAST(TDRD.TreatmentEnd AS DATE), 'dd/MMM/yyyy') AS TreatmentEnd,TDR.TotalBill AS TDR_TotalBill,TDRD.Sender AS TDRD_Sender,TDRD.Receiver AS TDRD_Receiver,TDRD.CUser AS TDRD_CUser,CAST(TDRD.CDate AS DATE) AS TDRD_CDate,TDRD.TreatmentPlace AS TDRD_TreatmentPlace,TDRD.PIC AS TDRD_PIC,TDRD.BoxNo AS TDRD_BoxNo,TDRD.DiagnosisID AS TDRD_DiagnosisID,pt.Description AS TDRD_ProductType,CASE WHEN tdrd.RoomOption='ONPLAN' THEN 'On Plan'  WHEN tdrd.RoomOption='NAPS' THEN 'NAPS'  WHEN tdrd.RoomOption='APS' THEN 'APS' ELSE '' END AS TDRD_RoomOption,TDRD.DeliveryReceiveDate AS TDRD_DeliveryReceiveDate,TDRD.ReceiveDate AS TDRD_ReceiveDate,CASE WHEN TDRD.isCobBPJS=1 THEN 'true'  ELSE 'false' END AS TDRD_isCobBPJS,CASE WHEN TDRD.isCashPlanBPJS=1 THEN 'true' ELSE 'false' END AS TDRD_isCashPlanBPJS,CASE WHEN TDRD.isDoubleInsured=1 THEN 'true' ELSE 'false' END AS TDRD_isDoubleInsured,CASE WHEN TDRD.isSpecialTreatment=1 THEN 'true' ELSE  'false' END AS TDRD_isSpecialTreatment,CASE WHEN TDRD.isDeceased=1 THEN 'true' ELSE 'false' END AS TDRD_isDeceased,CASE WHEN TDRD.isTrafficAccident=1 THEN 'true' ELSE 'false' END AS TDRD_isTrafficAccident,CASE WHEN tdrd.ClaimType = 'C' THEN 'Cashless' WHEN tdrd.ClaimType = 'R' THEN 'Reimburse'END AS TDRD_ClaimType,TDRD.BankNameDisp AS TDRD_BankNameDisp,TDRD.BankAccountDisp AS TDRD_BankAccountDisp,TDRD.AccountNoDisp AS TDRD_AccountNoDisp,CASE WHEN tdrd.PayToDisp = 'P' THEN 'Provider' WHEN tdrd.PayToDisp = 'M' THEN 'Member' WHEN tdrd.PayToDisp = 'C' THEN 'Client'END AS TDRD_PayTo,CASE WHEN tdr.ClaimType = 'C' THEN 'Cashless' WHEN tdr.ClaimType = 'R' THEN 'Reimburse'END AS TDR_ClaimType,CASE WHEN TDR.DocumentSource = 'P' THEN 'Provider' WHEN TDR.DocumentSource = 'M' THEN 'Member' WHEN TDR.DocumentSource = 'C' THEN 'Client'END AS TDR_DocumentSource,CAST(TDR.DocumentReceiveDate AS DATE) AS TDR_DocumentReceiveDate,TDR.CUser AS TDR_CUser,CAST(TDR.CDate AS DATE) AS TDR_CDate,TDR.RefNo AS TDR_RefNo,TDR.TotalBill AS TDR_TotalBill,TDR.DeliveryReceiveDate AS TDR_DeliveryReceiveDate,CAST(PCB.ReceiveDate AS DATE) AS PCB_ReceiveDate,PCB.TotalqtyClaim AS PCB_TotalqtyClaim,CAST(PCB.TotalClaimAmount AS INTEGER) AS PCB_TotalClaimAmount,PCB.ReimbursementF AS pcb_ReimbursementF,CAST(PCB.FDATE AS DATE) AS PCB_FDATE,PCB.FUser AS PCB_FUser,PCB.RefNo AS PCB_RefNo,PCB.DiscountF AS PCB_DiscountF,CAST(PCB.Discount AS INTEGER) AS PCB_Discount,PCB.Status AS PCB_Status,COALESCE(PCB.AUser,'') AS PCB_AUser,COALESCE(FORMAT(PCB.ADate, 'yyyy-MM-dd'), '') AS PCB_ADate,CAST(PCB.Stamp AS INTEGER) AS PCB_Stamp,COALESCE(PCB.AccountNo,'') AS PCB_AccountNo,COALESCE(PCB.BankAccount,'') AS PCB_BankAccount,COALESCE(PCB.BankName,'') AS PCB_BankName,ISNULL(PCB.AutoApprovalStatus,'') AS PCB_AutoApprovalStatus,COALESCE(FORMAT(PCB.AutoApprovalDate, 'yyyy-MM-dd'), '') AS PCB_AutoApprovalDate FROM dbo.tbl_disp_registration_document AS TDRD LEFT JOIN dbo.tbl_disp_registration AS TDR ON TDR.RegistrationNo = TDRD.RegistrationNo AND TDR.Provider = TDRD.Provider LEFT JOIN dbo.Post_Claim_Batch AS PCB ON TDR.RefBatchNo=CAST(pcb.RegID AS VARCHAR(10))LEFT JOIN dbo.Provider AS P ON pcb.ProviderID=p.PNO LEFT JOIN dbo.Profile AS P2 ON P.ID = P2.ID AND P2.Name = TDRD.TreatmentPlace AND P2.ID = PCB.Source LEFT JOIN dbo.Policy_Member AS PM ON TDRD.MNO = PM.MNo LEFT JOIN dbo.Policy AS P3 ON PM.PNO = P3.PNO LEFT JOIN dbo.Profile AS P4 ON P3.ClientID = P4.ID LEFT JOIN dbo.ProductType AS pt ON pt.ProductType = TDRD.ProductType WHERE TDRD.DocumentNo="+DocNo //1126486
		}
		else if (DocSource=='Client')
		{
		Query_TDRD = "SELECT TDRD.Dispatcher AS TDRD_Dispatcher,TDRD.DocumentStatus AS TDRD_DocumentStatus,TDRD.Status AS TDRD_Status,P4.Name AS ClientName,PM.Name AS MemberName,FORMAT (CAST(TDRD.TreatmentStart AS DATE), 'dd/MMM/yyyy') AS TDRD_TreatmentStart,FORMAT (CAST(TDRD.TreatmentEnd AS DATE), 'dd/MMM/yyyy') AS TreatmentEnd,TDR.TotalBill AS TDR_TotalBill,TDRD.Sender AS TDRD_Sender,TDRD.Receiver AS TDRD_Receiver,TDRD.CUser AS TDRD_CUser,CAST(TDRD.CDate AS DATE) AS TDRD_CDate,TDRD.TreatmentPlace AS TDRD_TreatmentPlace,TDRD.PIC AS TDRD_PIC,TDRD.BoxNo AS TDRD_BoxNo,TDRD.DiagnosisID AS TDRD_DiagnosisID,pt.Description AS TDRD_ProductType,CASE WHEN tdrd.RoomOption='ONPLAN' THEN 'On Plan'  WHEN tdrd.RoomOption='NAPS' THEN 'NAPS'  WHEN tdrd.RoomOption='APS' THEN 'APS' ELSE '' END AS TDRD_RoomOption,TDRD.DeliveryReceiveDate AS TDRD_DeliveryReceiveDate,TDRD.ReceiveDate AS TDRD_ReceiveDate,CASE WHEN TDRD.isCobBPJS=1 THEN 'true'  ELSE 'false' END AS TDRD_isCobBPJS,CASE WHEN TDRD.isCashPlanBPJS=1 THEN 'true' ELSE 'false' END AS TDRD_isCashPlanBPJS,CASE WHEN TDRD.isDoubleInsured=1 THEN 'true' ELSE 'false' END AS TDRD_isDoubleInsured,CASE WHEN TDRD.isSpecialTreatment=1 THEN 'true' ELSE  'false' END AS TDRD_isSpecialTreatment,CASE WHEN TDRD.isDeceased=1 THEN 'true' ELSE 'false' END AS TDRD_isDeceased,CASE WHEN TDRD.isTrafficAccident=1 THEN 'true' ELSE 'false' END AS TDRD_isTrafficAccident,CASE WHEN tdrd.ClaimType = 'C' THEN 'Cashless' WHEN tdrd.ClaimType = 'R' THEN 'Reimburse'END AS TDRD_ClaimType,TDRD.BankNameDisp AS TDRD_BankNameDisp,TDRD.BankAccountDisp AS TDRD_BankAccountDisp,TDRD.AccountNoDisp AS TDRD_AccountNoDisp,CASE WHEN tdrd.PayToDisp = 'P' THEN 'Provider' WHEN tdrd.PayToDisp = 'M' THEN 'Member' WHEN tdrd.PayToDisp = 'C' THEN 'Client'END AS TDRD_PayTo,CASE WHEN tdr.ClaimType = 'C' THEN 'Cashless' WHEN tdr.ClaimType = 'R' THEN 'Reimburse'END AS TDR_ClaimType,CASE WHEN TDR.DocumentSource = 'P' THEN 'Provider' WHEN TDR.DocumentSource = 'M' THEN 'Member' WHEN TDR.DocumentSource = 'C' THEN 'Client'END AS TDR_DocumentSource,CAST(TDR.DocumentReceiveDate AS DATE) AS TDR_DocumentReceiveDate,TDR.CUser AS TDR_CUser,CAST(TDR.CDate AS DATE) AS TDR_CDate,TDR.RefNo AS TDR_RefNo,TDR.TotalBill AS TDR_TotalBill,TDR.DeliveryReceiveDate AS TDR_DeliveryReceiveDate,CAST(PCB.ReceiveDate AS DATE) AS PCB_ReceiveDate,PCB.TotalqtyClaim AS PCB_TotalqtyClaim,CAST(PCB.TotalClaimAmount AS INTEGER) AS PCB_TotalClaimAmount,PCB.ReimbursementF AS pcb_ReimbursementF,CAST(PCB.FDATE AS DATE) AS PCB_FDATE,PCB.FUser AS PCB_FUser,PCB.RefNo AS PCB_RefNo,PCB.DiscountF AS PCB_DiscountF,CAST(PCB.Discount AS INTEGER) AS PCB_Discount,PCB.Status AS PCB_Status,COALESCE(PCB.AUser,'') AS PCB_AUser,COALESCE(FORMAT(PCB.ADate, 'yyyy-MM-dd'), '') AS PCB_ADate,CAST(PCB.Stamp AS INTEGER) AS PCB_Stamp,COALESCE(PCB.AccountNo,'') AS PCB_AccountNo,COALESCE(PCB.BankAccount,'') AS PCB_BankAccount,COALESCE(PCB.BankName,'') AS PCB_BankName,ISNULL(PCB.AutoApprovalStatus,'') AS PCB_AutoApprovalStatus,COALESCE(FORMAT(PCB.AutoApprovalDate, 'yyyy-MM-dd'), '') AS PCB_AutoApprovalDate FROM dbo.tbl_disp_registration_document AS TDRD LEFT JOIN dbo.tbl_disp_registration AS TDR ON TDR.RegistrationNo = TDRD.RegistrationNo AND TDR.Client = TDRD.Client LEFT JOIN dbo.Post_Claim_Batch AS PCB ON TDR.RefBatchNo=CAST(pcb.RegID AS VARCHAR(10))LEFT JOIN dbo.Provider AS P ON pcb.ProviderID=p.PNO LEFT JOIN dbo.Profile AS P2 ON P.ID = P2.ID AND P2.Name = TDRD.TreatmentPlace AND P2.ID = PCB.Source LEFT JOIN dbo.Policy_Member AS PM ON TDRD.MNO = PM.MNo LEFT JOIN dbo.Policy AS P3 ON PM.PNO = P3.PNO LEFT JOIN dbo.Profile AS P4 ON P3.ClientID = P4.ID LEFT JOIN dbo.ProductType AS pt ON pt.ProductType = TDRD.ProductType WHERE TDRD.DocumentNo="+DocNo  //1127110
		}
		else if (DocSource=='Member')
		{
		Query_TDRD = "SELECT TDRD.Dispatcher AS TDRD_Dispatcher,TDRD.DocumentStatus AS TDRD_DocumentStatus,TDRD.Status AS TDRD_Status,P4.Name AS ClientName,PM.Name AS MemberName,FORMAT (CAST(TDRD.TreatmentStart AS DATE), 'dd/MMM/yyyy') AS TDRD_TreatmentStart,FORMAT (CAST(TDRD.TreatmentEnd AS DATE), 'dd/MMM/yyyy') AS TreatmentEnd,TDR.TotalBill AS TDR_TotalBill,TDRD.Sender AS TDRD_Sender,TDRD.Receiver AS TDRD_Receiver,TDRD.CUser AS TDRD_CUser,CAST(TDRD.CDate AS DATE) AS TDRD_CDate,TDRD.TreatmentPlace AS TDRD_TreatmentPlace,TDRD.PIC AS TDRD_PIC,TDRD.BoxNo AS TDRD_BoxNo,TDRD.DiagnosisID AS TDRD_DiagnosisID,pt.Description AS TDRD_ProductType,CASE WHEN tdrd.RoomOption='ONPLAN' THEN 'On Plan'  WHEN tdrd.RoomOption='NAPS' THEN 'NAPS'  WHEN tdrd.RoomOption='APS' THEN 'APS' ELSE '' END AS TDRD_RoomOption,TDRD.DeliveryReceiveDate AS TDRD_DeliveryReceiveDate,TDRD.ReceiveDate AS TDRD_ReceiveDate,CASE WHEN TDRD.isCobBPJS=1 THEN 'true'  ELSE 'false' END AS TDRD_isCobBPJS,CASE WHEN TDRD.isCashPlanBPJS=1 THEN 'true' ELSE 'false' END AS TDRD_isCashPlanBPJS,CASE WHEN TDRD.isDoubleInsured=1 THEN 'true' ELSE 'false' END AS TDRD_isDoubleInsured,CASE WHEN TDRD.isSpecialTreatment=1 THEN 'true' ELSE  'false' END AS TDRD_isSpecialTreatment,CASE WHEN TDRD.isDeceased=1 THEN 'true' ELSE 'false' END AS TDRD_isDeceased,CASE WHEN TDRD.isTrafficAccident=1 THEN 'true' ELSE 'false' END AS TDRD_isTrafficAccident,CASE WHEN tdrd.ClaimType = 'C' THEN 'Cashless' WHEN tdrd.ClaimType = 'R' THEN 'Reimburse'END AS TDRD_ClaimType,TDRD.BankNameDisp AS TDRD_BankNameDisp,TDRD.BankAccountDisp AS TDRD_BankAccountDisp,TDRD.AccountNoDisp AS TDRD_AccountNoDisp,CASE WHEN tdrd.PayToDisp = 'P' THEN 'Provider' WHEN tdrd.PayToDisp = 'M' THEN 'Member' WHEN tdrd.PayToDisp = 'C' THEN 'Client'END AS TDRD_PayTo,CASE WHEN tdr.ClaimType = 'C' THEN 'Cashless' WHEN tdr.ClaimType = 'R' THEN 'Reimburse'END AS TDR_ClaimType,CASE WHEN TDR.DocumentSource = 'P' THEN 'Provider' WHEN TDR.DocumentSource = 'M' THEN 'Member' WHEN TDR.DocumentSource = 'C' THEN 'Client'END AS TDR_DocumentSource,CAST(TDR.DocumentReceiveDate AS DATE) AS TDR_DocumentReceiveDate,TDR.CUser AS TDR_CUser,CAST(TDR.CDate AS DATE) AS TDR_CDate,TDR.RefNo AS TDR_RefNo,TDR.TotalBill AS TDR_TotalBill,COALESCE(FORMAT(TDR.DeliveryReceiveDate, 'yyyy-MM-dd'), '') AS TDR_DeliveryReceiveDate FROM dbo.tbl_disp_registration_document AS TDRD LEFT JOIN dbo.tbl_disp_registration AS TDR ON TDR.RegistrationNo = TDRD.RegistrationNo AND TDR.Client = TDRD.Client LEFT JOIN dbo.Policy_Member AS PM ON TDRD.MNO = PM.MNo LEFT JOIN dbo.Policy AS P3 ON PM.PNO = P3.PNO LEFT JOIN dbo.Profile AS P4 ON P3.ClientID = P4.ID LEFT JOIN dbo.ProductType AS pt ON pt.ProductType = TDRD.ProductType WHERE TDRD.DocumentNo="+DocNo //1127116
		}
		
		GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_TDRD, Inputan)
	

} ///unregMember=No


if (UnregMember=='Yes')
{
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Follow Up/Txt_Remarks'), RemarksFollowUp)
	WebUI.delay(3)
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Pop Up Follow Up/Btn_Save'))	
	GEN5.ProcessingCommand()
	
	List<String> Hasil = GEN5.getPopUpText(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Saved Document No/Label_DocNo'))
	println (Hasil)
	DocNo = Hasil[0]
	SendTo = Hasil[1]
	println (DocNo)
	println (SendTo)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Saved Document No/Button X'))
	GEN5.ProcessingCommand()
	
}

String TypeDocNo

if (UnregMember=='No')
{
	if (ProductType=='Inpatient' && Prepost==false)
	{
		TypeDocNo = 'DocNo IP - DocSource '+DocSource+' - '+RoomOption
	}
	else if (ProductType=='Outpatient')
	{
		TypeDocNo = 'DocNo OP - DocSource '+DocSource
	}
	else if (ProductType=='Maternity' && Prepost==false)
	{
		TypeDocNo = 'DocNo MA - DocSource '+DocSource+' - '+RoomOption
	}
}
else if (UnregMember=='Yes')
{
	TypeDocNo = 'Unreg Member - Docsource '+DocSource
}

GEN5.InsertIntoDataHealth(TypeDocNo,'Gen5Health',DocNo)
WebUI.delay(3)




//klik button send batch
WebUI.callTestCase(findTestCase('Pages/Web/Health/Claim Registration/Batch - Doc Source Provider/Send Batch'),
	[('isSendBatch'):isSendBatch,('DocNo'):DocNo,('Discount2') : Discount2,
			('Stamp2') : Stamp2,('Username') : Username,('today2') : today2 ,('UnregMember'):UnregMember],  FailureHandling.STOP_ON_FAILURE)

