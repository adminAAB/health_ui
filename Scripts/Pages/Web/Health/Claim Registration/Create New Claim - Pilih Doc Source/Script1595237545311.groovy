import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

//println CR_DocumentSource

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Btn_CreateNew'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Btn_DocumentSource'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/List_DocumentSource', [('CR_DocSource') : CR_DocumentSource]))


if (CR_DocumentSource == 'Provider')
{
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Btn_SearchProvider'))
GEN5.ProcessingCommand()

WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Pop Up Search Provider/Txt_ProviderName'), ProviderName)
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Pop Up Search Provider/Btn_Search'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Pop Up Search Provider/Row1_GridProvider'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Pop Up Search Provider/Btn_Select'))
GEN5.ProcessingCommand()

	if (isOverseasProvider=='Yes')
	{
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Btn_OverseasProvider'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/List_OverseasProvider',[('isOverseasProvider'):isOverseasProvider]))
	}

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Btn_Next'))
GEN5.ProcessingCommand()

}
else if (CR_DocumentSource == 'Client')
{
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Btn_SearchClient'))
GEN5.ProcessingCommand()

WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Pop Up Search Client/Txt_ClientName'), ClientName)
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Pop Up Search Client/Btn_SearchClient'))
GEN5.ProcessingCommand()


WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Pop Up Search Client/Row1_GridClient'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Pop Up Search Client/Btn_Select'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Btn_Next'))
GEN5.ProcessingCommand()
}
else if (CR_DocumentSource == 'Member')
{

	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Btn_Member'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Pop Up Search Member/Txt_MemberNo'), MemberNo)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Pop Up Search Member/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Pop Up Search Member/Row1_GridMember'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Pop Up Search Member/Btn_Select'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Pop Up Document Source/Btn_Next'))
	GEN5.ProcessingCommand()
		
}









