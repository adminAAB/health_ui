import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5


WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Btn_CreateNew'))
GEN5.ProcessingCommand()
WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Txt_HospitalInvoiceNo'), GlobalVariable.CR_HospitalInvNo)
GEN5.ProcessingCommand()
WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Txt_Discount'), GlobalVariable.CR_Discount)
GEN5.ProcessingCommand()

if(GlobalVariable.CR_Stamp=='0')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/RadioBtn_Stamp0'))
	GEN5.ProcessingCommand()
}
else if(GlobalVariable.CR_Stamp=='3000')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/RadioBtn_Stamp3000'))
	GEN5.ProcessingCommand()
}
else if(GlobalVariable.CR_Stamp=='6000')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/RadioBtn_Stamp6000'))
	GEN5.ProcessingCommand()
}

if (GlobalVariable.CR_ChangeAccount=='Yes')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Btn_AccountNo'))
	GEN5.ProcessingCommand()
	GEN5.ClickExpectedRow(findTestObject('Web/Health/Claim Registration/Doc Source Provider/Pop Up Select Account/Grid_AccountNo'), 'Account No', GlobalVariable.CR_AccountNo_Change)
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Select Account/Btn_Select'))
	GEN5.ProcessingCommand()
}

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Btn_CreateNew'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Btn_Search'))
GEN5.ProcessingCommand()


if (GlobalVariable.CR_ClearSearchClaim=='Yes')
{
	String TxtProviderName2 = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Txt_ProviderName'), 'value', FailureHandling.STOP_ON_FAILURE)
	println TxtProviderName2
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Txt_MemberName'), GlobalVariable.CR_MemberName)
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Txt_MemberNo'), GlobalVariable.CR_MemberNo)
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Txt_GLNo'), GlobalVariable.CR_GLNo)
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Btn_Search'))
	GEN5.ProcessingCommand()
	
		
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Txt_ProviderName'), TxtProviderName2)
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Btn_Search'))
	GEN5.ProcessingCommand()

	
}

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Search Claim/Btn_NewClaim'))
GEN5.ProcessingCommand()










