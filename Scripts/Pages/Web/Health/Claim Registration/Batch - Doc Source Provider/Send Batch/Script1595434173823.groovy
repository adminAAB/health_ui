import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil

import com.keyword.GEN5

String TypeDocNo

if (GlobalVariable.CR_isUnregMember=='No' && GlobalVariable.CR_UnregAccount!='Yes')
{
	if (GlobalVariable.CR_ProductType=='Inpatient' && GlobalVariable.CR_isPrepost==false)
	{
		TypeDocNo = 'DocNo IP - DocSource '+GlobalVariable.CR_DocSource+' - '+GlobalVariable.CR_RoomOption
	}
	else if (GlobalVariable.CR_ProductType=='Outpatient')
	{
		TypeDocNo = 'DocNo OP - DocSource '+GlobalVariable.CR_DocSource
	}
	else if (GlobalVariable.CR_ProductType=='Maternity' && GlobalVariable.CR_isPrepost==false)
	{
		TypeDocNo = 'DocNo MA - DocSource '+GlobalVariable.CR_DocSource+' - '+GlobalVariable.CR_RoomOption
	}
}
else if (GlobalVariable.CR_isUnregMember=='Yes')
{
	TypeDocNo = 'Unreg Member - Docsource '+GlobalVariable.CR_DocSource
}
else if (GlobalVariable.CR_UnregAccount=='Yes')
{
	TypeDocNo = 'UnregAccount - Docsource '+GlobalVariable.CR_DocSource
}

GEN5.InsertIntoDataHealth(TypeDocNo,'Gen5Health',DocNo)
WebUI.delay(3)


if (GlobalVariable.CR_isSendBatch==true)
{
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Btn_Send'))
GEN5.ProcessingCommand()
WebUI.delay(2)
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Send Batch/Btn_Yes'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Provider/Pop Up Send Batch Success/Btn_X'))
GEN5.ProcessingCommand()

	if (GlobalVariable.CR_isUnregMember=='No')
	{
			String PCB_Status
			String PCB_AUser
			String PCB_ADate
			String PCB_AutoApprovalStatus
			String PCB_AutoApprovalDate
			
			
			
			if (Discount2!='0' || Stamp2!='0')
			{
				PCB_Status = 'A'
				PCB_AUser = GlobalVariable.CR_UserName
				PCB_ADate = today2
				PCB_AutoApprovalStatus = 'A'
				PCB_AutoApprovalDate = today2
			}
			else if (Discount2=='0' && Stamp2=='0')
			{
				PCB_Status = 'O'
				PCB_AUser = ''
				PCB_ADate = ''
				PCB_AutoApprovalStatus = ''
				PCB_AutoApprovalDate = ''
			}
			
			ArrayList Inputan2 = [PCB_Status,PCB_AUser,PCB_ADate,PCB_AutoApprovalStatus,PCB_AutoApprovalDate]
			String Query_PCB = "SELECT PCB.Status AS PCB_Status,COALESCE(PCB.AUser,'') AS PCB_AUser,COALESCE(FORMAT(PCB.ADate, 'yyyy-MM-dd'), '') AS PCB_ADate,ISNULL(PCB.AutoApprovalStatus,'') AS PCB_AutoApprovalStatus,COALESCE(FORMAT(PCB.AutoApprovalDate, 'yyyy-MM-dd'), '') AS PCB_AutoApprovalDate FROM dbo.tbl_disp_registration_document AS TDRD JOIN dbo.tbl_disp_registration AS TDR ON TDR.RegistrationNo = TDRD.RegistrationNo JOIN dbo.Post_Claim_Batch AS PCB ON TDR.RefBatchNo=CAST(pcb.RegID AS VARCHAR(10)) WHERE TDRD.DocumentNo="+DocNo
			
			
			GEN5.compareRowDBtoArray("172.16.94.70", "SEA", Query_PCB, Inputan2)
			WebUI.delay(3)
	}
}