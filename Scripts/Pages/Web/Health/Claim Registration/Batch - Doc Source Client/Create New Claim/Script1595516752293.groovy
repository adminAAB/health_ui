import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Client/Txt_ReferenceNo'), GlobalVariable.CR_ReferenceNo)
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Client/Btn_CreateNew'))
GEN5.ProcessingCommand()

boolean PopUpSearchMember = WebUI.waitForElementVisible(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Client/Pop Up Search Member/Label_SearchMember'), 2, FailureHandling.OPTIONAL)
if (PopUpSearchMember)
{
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Client/Pop Up Search Member/Txt_MemberNo'), GlobalVariable.CR_MemberNo)
	
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Client/Pop Up Search Member/Btn_Search'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Client/Pop Up Search Member/Row1_GridMember'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Client/Pop Up Search Member/Btn_Select'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Client/PopUp Search Claim/Btn_NewClaim'))
	GEN5.ProcessingCommand()
	
}
else 
{
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Client/PopUp Search Claim/Txt_MemberNo'), GlobalVariable.CR_MemberNo)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Client/PopUp Search Claim/Btn_Search'))
	GEN5.ProcessingCommand()
		
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/Doc Source Client/PopUp Search Claim/Btn_NewClaim'))
	GEN5.ProcessingCommand()
	
	
}











