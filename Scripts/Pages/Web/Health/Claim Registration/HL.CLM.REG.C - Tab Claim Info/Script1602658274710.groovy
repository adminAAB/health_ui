import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil

import com.keyword.GEN5


//klik Tab Claim Info
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab_ClaimInfo'))
GEN5.ProcessingCommand()


//input Delivery Received Date jika Doc Source Member
if (GlobalVariable.CR_DocSource == 'Member')
{
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_DeliveryReceivedDate'))
GEN5.ProcessingCommand()

GEN5.DatePicker(GlobalVariable.CR_DeliveryReceivedDate, findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/DatePicker_DeliveryReceivedDate'))
GEN5.ProcessingCommand()
}

//input member
if (GlobalVariable.CR_DocSource == 'Client' || GlobalVariable.CR_DocSource == 'Provider')
{
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_SearchMember'))
WebUI.delay(3)

if (GlobalVariable.CR_CheckSearchMember=='Yes')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_X'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_SearchMember'))
	WebUI.delay(3)
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_EmpID'), GlobalVariable.CR_EmpID)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_MemberName'), GlobalVariable.CR_MemberName)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Clear'))
	GEN5.ProcessingCommand()
	
	if (GlobalVariable.CR_DocSource == 'Provider')	
	{
		WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_ClientName'), GlobalVariable.CR_ClientName)
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Search'))
		GEN5.ProcessingCommand()
		
		//sorting
	
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Sorting_PatientName'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Sorting_EmpID'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Sorting_MemberNo'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Sorting_ClientName'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Sorting_Sex'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Sorting_Birthdate'))
		GEN5.ProcessingCommand()
		
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Clear'))
		GEN5.ProcessingCommand()
	}
	if	(GlobalVariable.CR_DocSource == 'Client')
	{
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Search'))
		GEN5.ProcessingCommand()
		
		//sorting
	
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Sorting_PatientName'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Sorting_EmpID'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Sorting_MemberNo'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Sorting_ClientName'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Sorting_Sex'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Sorting_Birthdate'))
		GEN5.ProcessingCommand()
		
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Clear'))
		GEN5.ProcessingCommand()
	}
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_EmpID'), GlobalVariable.CR_EmpID)
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_MemberName'), GlobalVariable.CR_MemberName)
	GEN5.ProcessingCommand()
	
	if (GlobalVariable.CR_DocSource == 'Provider')
	{
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_ClientName'), GlobalVariable.CR_ClientName)
	GEN5.ProcessingCommand()
	}
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_MemberNo'), GlobalVariable.CR_MemberNo)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Clear'))
	GEN5.ProcessingCommand()
	
		
}

	if (GlobalVariable.CR_isUnregMember == 'No')
		{
			WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Txt_MemberNo'), GlobalVariable.CR_MemberNo)
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Search'))
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Row1_GridMember'))
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Select'))
			GEN5.ProcessingCommand()
		}
	else if (GlobalVariable.CR_isUnregMember == 'Yes')
		{
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_Search'))
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Member/Btn_UnregMember'))
			GEN5.ProcessingCommand()
			
			WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Txt_MemberName'), GlobalVariable.CR_MemberName)
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_SearchClient'))
			GEN5.ProcessingCommand()
			
			WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Client/Txt_ClientName'), GlobalVariable.CR_ClientName)
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Client/Btn_Search'))
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Client/Row1_GridClient'))
			GEN5.ProcessingCommand()
			
			WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Client/Btn_Select'))
			GEN5.ProcessingCommand()
											
		}
}

//input provider jika doc source client dan member

if (GlobalVariable.CR_DocSource == 'Client' || GlobalVariable.CR_DocSource == 'Member')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_Provider'))
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Provider/Txt_ProviderName'), GlobalVariable.CR_ProviderName)
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Provider/Btn_Search'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Provider/Row1_GridProvider'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Provider/Btn_Select'))
	GEN5.ProcessingCommand()

}


//input Treatment Start dan Treatment End
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_TreatmentStart'))
GEN5.ProcessingCommand()
GEN5.DatePicker(GlobalVariable.CR_TreatmentStart, findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/DatePicker_TreatmentStart'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_TreatmentEnd'))
GEN5.ProcessingCommand()
GEN5.DatePicker(GlobalVariable.CR_TreatmentEnd, findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/DatePicker_TreatmentEnd'))
GEN5.ProcessingCommand()


//input Product Type
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_ProductType'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_ProductType',[('ProductType') : GlobalVariable.CR_ProductType]))
GEN5.ProcessingCommand()

//input Diagnosis
	if (GlobalVariable.CR_CheckSearchDiagnosis == 'Yes')
	{
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_Diagnosis'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search and Select Diagnosis/Btn_Search'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search and Select Diagnosis/Btn_Sorting_DiagnosisID'))
		GEN5.ProcessingCommand()
		
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search and Select Diagnosis/Btn_Sorting_DiagnosisName'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search and Select Diagnosis/Btn_X'))
		GEN5.ProcessingCommand()
	}


WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_Diagnosis'))
GEN5.ProcessingCommand()
WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search and Select Diagnosis/Txt_Diagnosis'), GlobalVariable.CR_Diagnosis)
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search and Select Diagnosis/Btn_Search'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search and Select Diagnosis/Row1_GridDiagnosis'))
GEN5.ProcessingCommand()
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search and Select Diagnosis/Btn_Select'))
GEN5.ProcessingCommand()

//input Billed
WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Txt_Billed'), GlobalVariable.CR_Billed)
GEN5.ProcessingCommand()

//input Doctor
WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_SearchDoctor'))
GEN5.ProcessingCommand()

WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Doctor/Txt_DoctorName'), GlobalVariable.CR_Doctor)
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Doctor/Btn_Search'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Doctor/Chk_SelectedProviderOnly'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Doctor/Btn_Search'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Doctor/Row1_GridDoctor'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Doctor/Btn_Select'))
GEN5.ProcessingCommand()

//input TreatmentRBClass
if (GlobalVariable.CR_ProductType=='Inpatient' || GlobalVariable.CR_ProductType=='Maternity' )
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_TreatmentRBClass'))
	GEN5.ProcessingCommand()

	if (GlobalVariable.CR_ProductType=='Maternity')
	{
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Provider Room List/Btn_TreatmentType'))
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Provider Room List/List_TreatmentType',[('TreatmentType'):GlobalVariable.CR_TreatmentType]))
		GEN5.ProcessingCommand()
	}
	
	GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Provider Room List/Grid Provider Room List'), 'Room Type', GlobalVariable.CR_RoomType_TreatmentRBClass)
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Provider Room List/Btn_Select'))
	GEN5.ProcessingCommand()
}


//input Room Option
if (GlobalVariable.CR_ProductType=='Inpatient' || GlobalVariable.CR_ProductType=='Maternity' )
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_RoomOption'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_RoomOption',[('RoomOption') : GlobalVariable.CR_RoomOption]))
	GEN5.ProcessingCommand()
}


if (GlobalVariable.CR_DocSource == 'Client')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_Payto'))
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_Payto',[('PayTo') : GlobalVariable.CR_PayTo2]))
	GEN5.ProcessingCommand()

}

if (GlobalVariable.CR_UnregAccount=='Yes')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_SearchAccount'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Account/Btn_UnregAccount'))
	GEN5.ProcessingCommand()
	
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Txt_AccountNo'), GlobalVariable.CR_AccountNo_Unreg)
	
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Txt_AccountName'), GlobalVariable.CR_AccountName_Unreg)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_SearchBankName'))
	GEN5.ProcessingCommand()
	WebUI.setText(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Bank Name/Txt_BankName'), GlobalVariable.CR_BankName_Unreg)
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Bank Name/Btn_Search'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Bank Name/Row1_GridAccount'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Pop Up Search Bank Name/Btn_Select'))
	GEN5.ProcessingCommand()
	
}


//input COB BPJS
if (GlobalVariable.CR_CobBpjs=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_CobBpjs'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_CobBpjs',[('isCobBpjs'): 'Yes']))
	GEN5.ProcessingCommand()
}

//input CashPlan BPJS
if (GlobalVariable.CR_CashPlanBpjs=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_CashPlanBpjs'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_CashPlanBpjs',[('isCashPlanBpjs'): 'Yes']))
	GEN5.ProcessingCommand()
}

//input Double Insured
if (GlobalVariable.CR_DoubleInsured=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_DoubleInsured'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_CashPlanBpjs',[('isCashPlanBpjs'): 'Yes']))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_DoubleInsured',[('isDoubleInsured'):'Yes']))
	GEN5.ProcessingCommand()
}


//input Perawatan Khusus
if (GlobalVariable.CR_PerawatanKhusus=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_PerawatanKhusus'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_PerawatanKhusus',[('isPerawatanKhusus'): 'Yes']))
	GEN5.ProcessingCommand()
}


//input Pasien Meninggal
if (GlobalVariable.CR_PasienMeninggal=='true')
{	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_PasienMeninggal'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_PasienMeninggal',[('isPasienMinggal'): 'Yes']))
	GEN5.ProcessingCommand()
}

//input Traffic Accident
if (GlobalVariable.CR_TrafficAccident=='true')
{	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Btn_TrafficAccident'))
	GEN5.ProcessingCommand()
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/List_TraficAccident',[('isTraficAccident'): 'Yes']))
	GEN5.ProcessingCommand()
}

//input Additional Info - CT Scan
if (GlobalVariable.CR_CTScan=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_CTSCan'))
	GEN5.ProcessingCommand()
}


//input Additional Info - MRI
if (GlobalVariable.CR_MRI=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_MRI'))
	GEN5.ProcessingCommand()
}

//input Additional Info - PA
if (GlobalVariable.CR_PA=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_PA'))
	GEN5.ProcessingCommand()
}


//input Additional Info - Lab
if (GlobalVariable.CR_Lab=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_Lab'))
	GEN5.ProcessingCommand()
}


//input Additional Info - Rontgen
if (GlobalVariable.CR_Rontgen=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_Rontgen'))
	GEN5.ProcessingCommand()
}


//input Additional Info - USG
if (GlobalVariable.CR_USG=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_USG'))
	GEN5.ProcessingCommand()
}


//input Additional Info - SRIC
if (GlobalVariable.CR_SRIC=='true')
{
	WebUI.click(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Tab Claim Info/Chk_AdditionalInfo_SRIC'))
	GEN5.ProcessingCommand()
}



WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Claim Registration/HL.CLM.REG.C/Object_Atas'), 2)
GEN5.ProcessingCommand()