import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/TabReferenceBook'), FailureHandling.STOP_ON_FAILURE)

// Add ReferenceBook Pertama
WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/IconAddReferenceBook'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/IconSearchFile'), FailureHandling.STOP_ON_FAILURE)

GEN5.UploadFile2(Referencebook)

WebUI.setText(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/FieldDescription'), Description, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/CheckboxActive'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/IconEffectivePeriod'), FailureHandling.STOP_ON_FAILURE)

GEN5.DatePicker(EffectivePeriod, findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/Date1'))

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/IconExpireDate'), FailureHandling.STOP_ON_FAILURE)

GEN5.DatePicker(ExpireDate, findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/Date2'))

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/ButtonSave'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

// Add ReferenceBook Kedua
WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/IconAddReferenceBook'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/IconSearchFile'), FailureHandling.STOP_ON_FAILURE)

GEN5.UploadFile2(Referencebook2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/FieldDescription'), Description2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/CheckboxActive'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/IconEffectivePeriod'), FailureHandling.STOP_ON_FAILURE)

GEN5.DatePicker(EffectivePeriod2, findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/Date1'))

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/IconExpireDate'), FailureHandling.STOP_ON_FAILURE)

GEN5.DatePicker(ExpireDate2, findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/Date2'))

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/ButtonSave'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/BookDeleted', [('BookDeleted'):BookDeleted]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Health/Profile Maintenance/ReferenceBook/IconViewReferenceBook'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ReferenceBook/IconCloseViewFile'), FailureHandling.STOP_ON_FAILURE)
