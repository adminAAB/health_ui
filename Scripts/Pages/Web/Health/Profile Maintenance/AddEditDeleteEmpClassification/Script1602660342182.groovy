import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/TabEmpClassification'), FailureHandling.STOP_ON_FAILURE)

// Add Classification Pertama
WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/EmpClassification/IconAddClassification'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Profile Maintenance/EmpClassification/FieldInputClassification'), Classification1, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/EmpClassification/ButtonSaveClassification'), FailureHandling.STOP_ON_FAILURE)

// Add Classification Kedua
WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/EmpClassification/IconAddClassification'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Profile Maintenance/EmpClassification/FieldInputClassification'), Classification2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/EmpClassification/ButtonSaveClassification'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/EmpClassification/ClassificationDeleted', [('ClassificationDeleted'):ClassificationDeleted]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/EmpClassification/IconDeleteClassification'), FailureHandling.STOP_ON_FAILURE)

