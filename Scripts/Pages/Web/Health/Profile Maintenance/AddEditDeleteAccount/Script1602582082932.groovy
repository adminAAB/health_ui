import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/TabAccounts'), FailureHandling.STOP_ON_FAILURE)

// Add Account Pertama
WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/IconAddAccount'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/IconSearchBank'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/FieldInputBank'), Bank, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/ButtonSearchBank'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/BankSelected', [('Bank'):Bank]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/ButtonSelectBank'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/FieldAccountNo'), AccountNo, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/FieldAccountName'), AccountName, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/ButtonSaveAccount'), FailureHandling.STOP_ON_FAILURE)

// Add Account Kedua
WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/IconAddAccount'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/IconSearchBank'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/FieldInputBank'), Bank2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/ButtonSearchBank'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/BankSelected', [('Bank'):Bank2]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/ButtonSelectBank'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/FieldAccountNo'), AccountNo2, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/FieldAccountName'), AccountName2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/ButtonSaveAccount'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/AccountDeleted', [('AccountDeleted'):AccountDeleted]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/Accounts/IconDeleteAccount'), FailureHandling.STOP_ON_FAILURE)




