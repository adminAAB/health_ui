import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import org.junit.After
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ProfileSelected', [('ProfileName'): GlobalVariable.ProfileNameEdited]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/ButtonRoomAndBoard'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/RoomAndBoard/FieldRnBSchemeEffectiveDate'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/RoomAndBoard/SchemeNew'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/RoomAndBoard/IconEffectiveDate'), FailureHandling.STOP_ON_FAILURE)

GEN5.DatePicker(EffectiveDate, findTestObject('Object Repository/Web/Health/Profile Maintenance/RoomAndBoard/EffectiveDateSelected'))

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/RoomAndBoard/ButtonSetDate'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/RoomAndBoard/FieldScheme'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/RoomAndBoard/SchemeSelected', [('Scheme'):Scheme]), FailureHandling.STOP_ON_FAILURE)

if(Scheme=='Garda Medika RB Class'){
	WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/RoomAndBoard/FieldGolongan1'), FailureHandling.STOP_ON_FAILURE)

	WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/RoomAndBoard/GMRnBClassSelected1', [('GMRnBClass'): GMRnBClass]), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/RoomAndBoard/GMRnBClassSelected2', [('GMRnBClass2'): GMRnBClass2]), FailureHandling.STOP_ON_FAILURE)
}

WebUI.click(findTestObject('Object Repository/Web/Health/Profile Maintenance/RoomAndBoard/ButtonSaveRoomAndBoard'), FailureHandling.STOP_ON_FAILURE)

GEN5.ProcessingCommand()










