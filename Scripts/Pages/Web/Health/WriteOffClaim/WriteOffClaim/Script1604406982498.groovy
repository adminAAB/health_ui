import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.keyword.GEN5

WebUI.click(findTestObject('Object Repository/Web/Health/WriteOffClaim/IconAddNote'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/WriteOffClaim/FieldDocNo'), DocNo, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/WriteOffClaim/FieldDocNo'), NoteNo, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Web/Health/WriteOffClaim/FieldDocNo'), InvoiceNo, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/WriteOffClaim/IconPeriodStart'), FailureHandling.STOP_ON_FAILURE)

GEN5.DatePicker(PeriodStart, findTestObject('Object Repository/Web/Health/WriteOffClaim/StartSelected'))

WebUI.click(findTestObject('Object Repository/Web/Health/WriteOffClaim/IconPeriodEnd'), FailureHandling.STOP_ON_FAILURE)

GEN5.DatePicker(PeriodEnd, findTestObject('Object Repository/Web/Health/WriteOffClaim/EndSelected'))

WebUI.click(findTestObject('Object Repository/Web/Health/WriteOffClaim/FieldNoteType'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/WriteOffClaim/NoteTypeSelected', [('NoteType'):NoteType]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/WriteOffClaim/FieldSettledStatus'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/WriteOffClaim/SettledStatusSelected', [('SettledStatus'):SettledStatus]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web/Health/WriteOffClaim/ButtonSearch'), FailureHandling.STOP_ON_FAILURE)