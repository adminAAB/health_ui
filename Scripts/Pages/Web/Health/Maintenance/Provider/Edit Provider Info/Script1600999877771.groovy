import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.util.KeywordUtil

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Txt_ProviderName'), ProviderName)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Btn_Search_Provider'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Select_Provider'))

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Btn_Edit'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Btn_ContractType'))

GlobalVariable.ContractType = ContractType
WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Lst_ContractType', [('ContractType') : GlobalVariable.ContractType]))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Btn_MOUPeriodStart'))

GEN5.DatePicker(StartDate, findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Lst_MOUPeriodStart'))
GlobalVariable.StartDate = StartDate
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Btn_MOUPeriodEnd'))

GEN5.DatePicker(EndDate, findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Lst_MOUPeriodEnd'))
GlobalVariable.EndDate = EndDate
WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_TaxID'), TaxID)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_DiscLeadTime'), DiscLeadTime)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_DiscRefund'), DiscRefund)
