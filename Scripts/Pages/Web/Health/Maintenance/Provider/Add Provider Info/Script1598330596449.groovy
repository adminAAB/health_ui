import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.util.KeywordUtil

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Btn_Add'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Chk_Active'))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_ProviderIDAdmedika'), ProviderID)
WebUI.delay(2)

GlobalVariable.ProviderName = ProviderName
WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_ProviderName'), ProviderName)
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_Address'), Address)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Btn_City'))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_City'), City)
WebUI.delay(2)

WebUI.click(findTestObject('Web/Health/Maintenance/Provider/Provider Info/Lst_ComboSearchTop1', [('key'): City]))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Btn_Country'))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_Country'), Country)
WebUI.delay(2)

WebUI.click(findTestObject('Web/Health/Maintenance/Provider/Provider Info/Lst_ComboSearchTop1', [('key'): Country]))
WebUI.delay(2)


WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_Latitude'), Latitude)
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_Longitude'), Longitude)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Btn_TreatmentMedical'))
WebUI.delay(2)

GlobalVariable.TreatMedInformation = TreatmentMedical
WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Lst_TreatmentMedical', [('TreatmentMedical') : GlobalVariable.TreatMedInformation]))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Btn_ProviderType'))

GlobalVariable.ProviderType = ProviderType
WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Lst_ProviderType' , [('ProviderType') : GlobalVariable.ProviderType]))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Btn_ProviderClassification'))
WebUI.delay(2)

GlobalVariable.ProviderClassification = ProviderClass
WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Lst_ProviderClassification', [('ProviderClass') : GlobalVariable.ProviderClassification]))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Btn_Rating'))
WebUI.delay(2)

GlobalVariable.Rating = Rating
WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Lst_Rating', [('Rating') : GlobalVariable.Rating]))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/ChkBox_IP'))
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/ChkBox_OP'))
WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Maintenance/Provider/ObjectAtas'), 2)
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Btn_ContractType'))

GlobalVariable.ContractType = ContractType
WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Lst_ContractType', [('ContractType') : GlobalVariable.ContractType]))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_MOUNumber'), MOUNumber)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Btn_MOUPeriodStart'))

GEN5.DatePicker(StartDate, findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Lst_MOUPeriodStart'))
GlobalVariable.StartDate = StartDate
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Btn_MOUPeriodEnd'))

GEN5.DatePicker(EndDate, findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Lst_MOUPeriodEnd'))
GlobalVariable.EndDate = EndDate
WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_TaxID'), TaxID)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_DiscLeadTime'), DiscLeadTime)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_DiscRefund'), DiscRefund)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_RebateGM'), RebateGM)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_RebateMember'), RebateMember)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_DiscountCondition'), DiscCondition)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_AdminFee'), AdminFee)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_AdminFeeMax'), AdminFeeMax)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_PaymentDue'), PaymentDue)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_LateCharge'), LateCharge)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Btn_AddAdmHours'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/AdministrativeHours/Chk_Monday'))
WebUI.delay(1)

//WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/AdministrativeHours/Chk_Tuesday'))
//WebUI.delay(1)

//WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/AdministrativeHours/Chk_Wednesday'))
//WebUI.delay(1)

//WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/AdministrativeHours/Chk_Thursday'))
//WebUI.delay(1)

//WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/AdministrativeHours/Chk_Friday'))
//WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/AdministrativeHours/Txt_OpenHours'), OpenHours)
WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/AdministrativeHours/Txt_CloseHours'), CloseHours)
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/AdministrativeHours/Btn_Save_AdmHours'))
WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Object Repository/Web/Health/Maintenance/Provider/ObjectAtas'), 2)
WebUI.delay(2)



