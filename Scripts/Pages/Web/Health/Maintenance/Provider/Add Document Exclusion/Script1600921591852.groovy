import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.util.KeywordUtil

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Document Exclusion/Btn_Tab'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Document Exclusion/Add_DocExclusion'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Document Exclusion/Btn_DocExclusion'))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Document Exclusion/Txt_DocExclusion'), DocExclusion)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Document Exclusion/Select_DocExclusion', [('key'):DocExclusion]))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Document Exclusion/Btn_Save_DocExclusion'))
WebUI.delay(2)