import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.util.KeywordUtil

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Documents/Btn_Tab'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Documents/Add_Document'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Documents/Btn_DocType'))
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Documents/Lst_DocType', [('DocType') : DocType]))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Documents/Btn_ContractType'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Documents/Lst_ContractType', [('key'): ContractType]))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Documents/Txt_DocNumber'), DocNumber)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Documents/Btn_StartDate'))

GEN5.DatePicker(StartDate, findTestObject('Object Repository/Web/Health/Maintenance/Provider/Documents/Lst_StartDate'))
GlobalVariable.StartDate = StartDate
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Documents/Btn_EndDate'))

GEN5.DatePicker(EndDate, findTestObject('Object Repository/Web/Health/Maintenance/Provider/Documents/Lst_EndDate'))
GlobalVariable.EndDate = EndDate
WebUI.delay(1)


WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Documents/Btn_Search_File'))
WebUI.delay(2)

GEN5.UploadFile2(File_Document)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Documents/Btn_SaveDoc'))
GEN5.ProcessingCommand()