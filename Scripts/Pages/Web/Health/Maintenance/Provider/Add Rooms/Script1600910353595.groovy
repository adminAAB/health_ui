import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.util.KeywordUtil

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Btn_Rooms'))

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Add_Rooms'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Btn_Date'))
GEN5.DatePicker(StartDate, findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Lst_Date'))
GlobalVariable.StartDate = StartDate
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Btn_SetDate'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Add_Rooms'))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Txt_ProviderRoom'), ProviderRoom)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/LookUp_GardaMedika'))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Txt_RoomName'), RoomName)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Btn_Search_RoomName'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Select_RoomName'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Btn_Select_RoomName'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/LookUp_RoomCategory'))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Txt_RoomCategory'), RoomCategory)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Btn_Search_RoomCategory'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Select_RoomCategory'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Btn_Select_RoomCategory'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Btn_TreatmentType'))

GlobalVariable.TreatmentType = TreatmentType
WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Lst_TreatmentType' , [('TreatmentType') : GlobalVariable.TreatmentType]))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Txt_RoomRate'), RoomRate)
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Txt_NumberOfBed'), NumberOfBed)
WebUI.delay(2)

//WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Txt_NumberOfBed_Max'), Max)
//WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Chk_AC'))

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Chk_TV'))

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Chk_Toilet'))

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Chk_Sofa'))

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Chk_CupBoard'))

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Chk_Telephone'))

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Rooms/Btn_Save_Rooms'))
WebUI.delay(2)





