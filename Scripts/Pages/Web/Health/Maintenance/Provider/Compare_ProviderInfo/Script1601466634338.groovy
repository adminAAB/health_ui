import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.util.KeywordUtil


String ProviderIDGardamedika = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_ProviderIDGardaMedika'), 'value')
String Province = WebUI.getAttribute(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Provider Info/Txt_Provinsi'), 'value')

ArrayList dataui = [ProviderIDGardamedika,ProviderID,ProviderName,Address,City,Province,Latitude,Longitude,
	TreatmentMedical,ProviderType,ProviderClass,Rating,ContractType,MOUNumber,StartDate,EndDate,TaxID,DiscLeadTime,
	DiscRefund,RebateGM,RebateMember,DiscCondition,AdminFee,AdminFeeMax,PaymentDue,LateCharge,OpenHours,CloseHours]

println(dataui)

def query = ("""SELECT 
 PS.ProviderIDGardamedika,
 PS.ProviderIDAdmedika,
 C.Name AS ProviderName,
 C.Address_1 AS Address,
 A.Description AS City,
 A.Province,
 PM.Latitude,
 PM.Longitude,
 PM.GiveMedicalInfo,
 PM.PType,
 PM.ProviderClassification,
 PM.ProviderRating,
 P.ContractType,
 PM.DocNo AS MOU_Number,
 PM.PDate AS MOU_Period_Start,
 PM.ExpDate AS MOU_Period_End,
 C.TAXID AS TaxIDNumber,
 PM.DiscountLeadTime,PM.DiscountRefundPct,PM.DiscountRebateForGMpct,PM.DiscountRebateForMemberPct,
 PM.RemarksDiscount AS DiscountCondition,
 PM.AdminFeePct,PM.MaxAdminFee,
 PM.PaymentDue,
 PM.PenaltyAmount AS LateCharge,
 PA.Day AS AdministrativeHour_Day,
 PA.HourStart,
 PA.HourEnd
FROM 
 dbo.Provider AS PM
 LEFT JOIN dbo.ProviderCodeValidate AS PS ON PM.ID = PS.ProviderIDGardamedika
 LEFT JOIN dbo.Profile AS C ON C.ID = PM.ID
 LEFT JOIN Area AS A ON C.Area = A.Area
 LEFT JOIN dbo.Provider_ContractType AS P ON P.PCTID = PM.ContractType
 LEFT JOIN dbo.ProviderAdministrativeHour AS PA ON PA.ProviderID = PM.ID
WHERE PM.ID = '"""+ProviderIDGardamedika+"""' """)

GEN5.compareRowDBtoArray('172.16.94.70', 'SEA', query, dataui)
