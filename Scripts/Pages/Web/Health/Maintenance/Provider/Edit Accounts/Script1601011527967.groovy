import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.util.KeywordUtil

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Btn_TabAccounts'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Select_Accounts'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Edit_Accounts'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/LookUp_BankAccount'))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Txt_BankName'), BankName)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Btn_Search_BankName'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Select_BankName'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Btn_Select_BankName'))
WebUI.delay(2)

GEN5.DeleteWrite(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Txt_AccountNo'), AccountNo)
WebUI.delay(2)

GEN5.DeleteWrite(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Txt_AccountName'), AccountName)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/LookUp_BankBranch'))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Txt_BankBranch'), BankBranch)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Btn_Search_BankBranch'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Select_BankBranch'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Btn_Select_BankBranch'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Provider/Accounts/Btn_Save_Account'))
WebUI.delay(2)