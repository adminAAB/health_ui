import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.util.KeywordUtil

GlobalVariable.Doctor = Doctor
WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Btn_Add'))
GEN5.ProcessingCommand()

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Txt_Name'), Doctor)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Btn_Rating'))

GlobalVariable.Rating = Rating
WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Lst_Rating' , [('Rating') : GlobalVariable.Rating]))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Cbx_Active'))
WebUI.delay(2)

//Add Specialty
WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add_Specialty'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add Specialty/Btn_LookUp_Specialty'))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add Specialty/Txt_Specialty'), Specialty)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add Specialty/Btn_Search'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add Specialty/Lst_Specialty'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add Specialty/Btn_Select'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add Specialty/Btn_Save'))
GEN5.ProcessingCommand()

//Add Provider
WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add_Provider'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add Provider/Btn_LookUp_Provider'))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add Provider/Txt_Provider'), Provider)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add Provider/Btn_Search'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add Provider/Lst_Provider'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add Provider/Btn_Select'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Add Provider/Btn_Save'))
GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Health/Maintenance/Doctor/Doctor Details/Btn_Save'))
GEN5.ProcessingCommand()

//Compare Database
String Query_Doctor = " SELECT * FROM Doctor WHERE DoctorName = '$Doctor' "
String db_DoctorID = GEN5.getValueDatabase("172.16.94.70", "SEA", Query_Doctor, "DoctorID")
println(db_DoctorID)

String Query_All = " SELECT PM.DoctorName, CASE WHEN PM.DoctorRating = '1' THEN '1 - Blacklist' WHEN PM.DoctorRating = '2' THEN '2 - Redlist' WHEN PM.DoctorRating = '3' THEN '3 - Average' WHEN PM.DoctorRating = '4' THEN '4 - Good' WHEN PM.DoctorRating = '5' THEN '5 - Excellent' END AS DoctorRating, PS.Specialty, C.Name AS ProviderName FROM dbo.Doctor AS PM LEFT JOIN dbo.DoctorSpecialty AS PS ON PM.DoctorID = PS.DoctorID LEFT JOIN dbo.ProviderDoctor AS PA ON PM.DoctorID = PA.DoctorID LEFT JOIN dbo.Profile AS C ON C.ID = PA.ProviderID WHERE PM.DoctorID = '$db_DoctorID' "
String db_DoctorName = GEN5.getValueDatabase("172.16.94.70", "SEA", Query_All, "DoctorName")
println(db_DoctorName)
String db_Rating = GEN5.getValueDatabase("172.16.94.70", "SEA", Query_All, "DoctorRating")
println(db_Rating)
String db_Specialty = GEN5.getValueDatabase("172.16.94.70", "SEA", Query_All, "Specialty")
println(db_Specialty)
String db_Provider = GEN5.getValueDatabase("172.16.94.70", "SEA", Query_All, "ProviderName")
println(db_Provider)


if ((db_DoctorName == Doctor) && (db_Rating == GlobalVariable.Rating) && (db_Specialty == Specialty) && (db_Provider == Provider) )
{
	KeywordUtil.markPassed("PASSED")
}
else {
KeywordUtil.markFailedAndStop("FAILED")
}








