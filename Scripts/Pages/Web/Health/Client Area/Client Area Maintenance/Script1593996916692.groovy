import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

GEN5.Click(findTestObject('Object Repository/Web/Health/Client Area/Span_clientName'))
WebUI.setText(findTestObject('Object Repository/Web/Health/Client Area/Txt_clientName'), Client)
GlobalVariable.Client
GEN5.Click(findTestObject('Object Repository/Web/Health/Client Area/Btn_searchClientName'))
GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/Health/Client Area/Tbl_clientName'), "Client Name", Client)
GlobalVariable.Client
GEN5.Click(findTestObject('Object Repository/Web/Health/Client Area/Btn_selectClientName'))
GEN5.Click(findTestObject('Object Repository/Web/Health/Client Area/Btn_addClientArea'))